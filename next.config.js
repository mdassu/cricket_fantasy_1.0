// next.config.js 
const withCSS = require('@zeit/next-css')
const withPWA = require('next-pwa');

module.exports =
  withPWA(
    withCSS({

      module: {
        loaders: [
          {
            test: /\.(sass|less|css)$/,
            loaders: ['style-loader', 'css-loader', 'less-loader']
          }
        ],
        cssModules: true,
        cssLoaderOptions: {
          importLoaders: 1,
          localIdentName: "[local]___[hash:base64:5]",
        }

      },
      devIndicators: {
        autoPrerender: false,
      },
      env: {
        GET_API_URL: 'https://dev.sports.cards:8082/api/RTFGet/',
        POST_API_URL: 'https://dev.sports.cards:8082/api/RTFPost/',
        IMG_URL: 'https://dev.sports.cards:8082/',
        REGISTER_BONUS: 101,
        DAILY_WINNING: '1,000,000',
        HOME_VIDEO_URL: 'https://www.youtube.com/embed/rd3uOAjJ1Ek',
        GOOGLE_LOGIN_CLIENT_ID: '198744805274-td1o8hl04hoo62tcfmacan7385m742h8.apps.googleusercontent.com',
        FACEBOOK_LOGIN_CLIENT_ID: '377741300232516'
      },
      pwa: {
        dest: "public"
      }
    })
  );