import firebase from 'firebase';
// Sports.Cards app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyDvHe3wJ5oe5DDyg50XpJUr-DiQ23sHRIs",
  authDomain: "sports-cards-552b2.firebaseapp.com",
  databaseURL: "https://sports-cards-552b2.firebaseio.com",
  projectId: "sports-cards-552b2",
  storageBucket: "sports-cards-552b2.appspot.com",
  messagingSenderId: "582014397778",
  appId: "1:582014397778:web:6348054eab5aab76fade8b"
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export default firebase;