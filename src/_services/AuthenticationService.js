import React from 'react';
import Cookies from 'js-cookie';
import { BehaviorSubject } from 'rxjs';
import { httpPost, httpPostTemp, httpUpoadPost } from '_helper/ApiBase';
import { encrypt, decrypt } from '_helper/EncrDecrypt';
import { Redirect } from 'react-router-dom';
import moment from 'moment';

var data = null;
if (Cookies.get('_user')) {
  data = decrypt(Cookies.get('_user'));
}
const currentUserSubject = new BehaviorSubject(data);
// if (data && moment(data['last_login']).format('YYYY-MM-DD') != moment().format('YYYY-MM-DD')) {
//   logout();
// }

export const AuthenticationService = {
  signIn,
  logout,
  verifyOTP,
  signUp,
  logout,
  clearCookies,
  getProfile,
  editProfile,
  profileUpload,
  emailVerify,
  dailyReward,
  checkReferralCode,
  sendVerifyEmail,
  currentUserSubject,
  currentUser: currentUserSubject.asObservable(),
  get currentUserValue() { return currentUserSubject.value },
  get isLogged() {
    if (this.currentUserValue && this.currentUserValue != null) {
      return true;
    } else {
      return false;
    }
  }
};

function signIn(params, cb) {

  httpPostTemp('signIn', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: {}
        };
      } else {
        var res = {
          status: false,
          message: response.message
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  })
}

function logout() {
  // remove user from local storage to log user out
  Cookies.remove('_user');
  Cookies.remove('_accessToken');
  Cookies.remove('_pack');
  Cookies.remove('_match');
  Cookies.remove('_ctData');
  Cookies.remove('_pId');
  currentUserSubject.next(null);

  // this.props.history.push('/dashboard');
}

function clearCookies() {
  Cookies.remove('_pack');
  Cookies.remove('_match');
  Cookies.remove('_ctData');
  Cookies.remove('_pId');
  Cookies.remove('_amount');
  Cookies.remove('_mobData');
  Cookies.remove('_wR');
}

// verify otp api
function verifyOTP(params, cb) {

  httpPostTemp('otpVerification', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: {}
        };
        var userData = response['data'];
        var userJson = {
          country_code_id: userData['country_code_id'],
          mobile: userData['mobile'],
          first_name: userData['first_name'],
          last_name: userData['last_name'],
          last_login: userData['last_login'],
          email: userData['email'],
          profile_picture: userData['profile_picture'],
          walletBalance: userData['walletBalance'],
          addedBalance: userData['addedBalance'],
          bonusBalance: userData['bonusBalance'],
          winningBalance: userData['winningBalance'],
          creditedCash: userData['creditedCash'],
          creditedType: userData['creditType'],
          referralCode: userData['referral_code'],
          isEmailVerified: userData['email_is_verified']
        };
        var encryptData = encrypt(userJson);
        Cookies.set('_accessToken', userData['token'], { expires: 30 });
        Cookies.set('_user', encryptData, { expires: 30 });
        currentUserSubject.next(userJson);
      } else {
        var res = {
          status: false,
          message: response.message
        };
      }
      cb(res);
    } catch (err) {
      throw err;
    }
  });
}

// signup api
function signUp(params, cb) {

  httpPostTemp('signUp', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: {}
        };
      } else {
        var res = {
          status: false,
          message: response.message
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}

// check referral code api
function checkReferralCode(params, cb) {

  httpPostTemp('referralVerification', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: {}
        };
      } else {
        var res = {
          status: false,
          message: response.message
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}

// get user profile api
function getProfile(cb) {

  httpPostTemp('myProfile', {}, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          message: response.message
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}

// edit user profile api
function editProfile(params, cb) {

  httpPostTemp('editprofile', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          message: response.message
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}

// user profile pic upload api
function profileUpload(params, cb) {

  httpUpoadPost('uploadProfile', params, (response) => {
    try {
      // if (response.result == 1) {
      //   var res = {
      //     status: true,
      //     data: response['data']
      //   };
      // } else {
      //   var res = {
      //     status: false,
      //     message: response.message
      //   };
      // }
      cb(response);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}

// email verification api
function emailVerify(params, cb) {

  httpPostTemp('emailVerify', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response.message
        };
        if (Cookies.get('_user')) {
          data = decrypt(Cookies.get('_user'));
          data['isEmailVerified'] = 1;
          var encryptData = encrypt(data);
          Cookies.set('_user', encryptData, { expires: 30 });
          currentUserSubject.next(data);
        }
      } else {
        var res = {
          status: false,
          message: response.message
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}

function dailyReward(cb) {
  httpPostTemp('dailyReward', {}, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response.data
        };
      } else {
        var res = {
          status: false,
          message: response.message
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  })
}

function sendVerifyEmail(cb) {
  httpPostTemp('sendVerifyMail', {}, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response.data
        };
      } else {
        var res = {
          status: false,
          message: response.message
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  })
}
