import React from 'react';
import Cookies from 'js-cookie';
import { BehaviorSubject } from 'rxjs';
import { httpPost, httpGet, httpPostTemp } from '_helper/ApiBase';
import { AuthenticationService } from '_services/AuthenticationService'
import { encrypt } from '_helper/EncrDecrypt';

var data = null;
if (Cookies.get('_configuration')) {
  data = decrypt(Cookies.get('_configuration'));
}
const globalConfiguration = new BehaviorSubject(data);
const countrySubject = new BehaviorSubject(null);

export const CommonService = {
  getConfiguration,
  updateUserWallet,
  getCountry,
  saveDeviceToken,
  globalConfiguration,
  countrySubject,
  shareEmailReferral
};

function getConfiguration(cb) {

  httpGet('getconfiguration', (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response.data
        }
        // globalConfiguration.next();
      } else {
        var res = {
          status: false,
          message: response.message
        }
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getCountry() {
  if (countrySubject.value == null) {
    httpGet('getCountry', (response) => {
      try {
        if (response.result == 1) {
          // var res = {
          //   status: true,
          //   data: response.data
          // }
          countrySubject.next(response['data']);
        }
        // cb(res);
      } catch (err) {
        throw err;
        // cb(err)
      }
    })
  }
}

function updateUserWallet() {

  httpPostTemp('getUserWallet', {}, (response) => {
    try {
      if (response.result == 1) {
        var user = AuthenticationService.currentUserValue;
        user['walletBalance'] = response['data']['total_balance'];
        user['addedBalance'] = response['data']['amount'];
        user['winningBalance'] = response['data']['winning'];
        user['bonusBalance'] = response['data']['cash_bonus'];
        Cookies.set('_user', encrypt(user), { expires: 30 });
        // AuthenticationService.currentUserSubject.next(user);
      }
    } catch (err) {
      throw err;
      // cb(err)
    }
  })
}

function shareEmailReferral(params, cb) {
  httpPost('referralEmail', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          message: response.message
        };
      } else {
        var res = {
          status: false,
          message: response.message
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}

function saveDeviceToken(params, cb) {

  httpPostTemp('mapdevicetoken', params, (response) => {
    try {
      if (response.result == 1) {
        cb(response);
      }
    } catch (err) {
      throw err;
      // cb(err)
    }
  })
}