import Cookies from 'js-cookie';
import { httpPost, httpPostTemp } from '_helper/ApiBase';
import { encrypt, decrypt } from '_helper/EncrDecrypt';


export const BankingService = {
  addCash,
  saveTransaction
};

function addCash(params, cb) {
  httpPostTemp('addCash', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response.data
        };
      } else {
        var res = {
          status: false,
          message: response.message
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}

function saveTransaction(params, cb) {
  httpPostTemp('saveTransaction', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response.data
        };
      } else {
        var res = {
          status: false,
          message: response.message
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });
}








