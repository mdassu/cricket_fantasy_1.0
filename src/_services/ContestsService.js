import Cookies from 'js-cookie';
import { httpPost, httpPostTemp } from '_helper/ApiBase';
import { encrypt, decrypt } from '_helper/EncrDecrypt';


export const ContestsService = {
  getMatchContests,
  getMyContests,
  getMyPacks,
  getMatchDetails,
  getContestDetails,
  getWinners,
  getContestWinner
};

function getMatchContests(params, cb) {

  httpPostTemp('getMatchContest', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getMyContests(params, cb) {

  httpPostTemp('getMyMatchContest', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getMyPacks(params, cb) {

  httpPostTemp('getMyPacks', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getMatchDetails(params, cb) {

  httpPostTemp('getMatchDetails', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getContestDetails(params, cb) {

  httpPostTemp('getContestDetails', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getWinners(cb) {

  httpPostTemp('showWinner', {}, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getContestWinner(params, cb) {

  httpPostTemp('showWinnerbyContest', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}
