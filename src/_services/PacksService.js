import Cookies from 'js-cookie';
import { BehaviorSubject } from 'rxjs';
import { httpPost, httpPostTemp } from '_helper/ApiBase';
import { encrypt, decrypt } from '_helper/EncrDecrypt';

const currentUserSubject = new BehaviorSubject({});

export const PacksService = {
  getPacksMarket,
  savePack,
  getPackDetails
};

function getPacksMarket(params, cb) {

  httpPostTemp('getPacksStaticData', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function savePack(params, cb) {

  httpPostTemp('submitPack', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getPackDetails(params, cb) {

  httpPostTemp('getPackDetails', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}