import Cookies from 'js-cookie';
import { httpPost, httpPostTemp } from '_helper/ApiBase';
import { encrypt, decrypt } from '_helper/EncrDecrypt';


export const HomeService = {
  getHomeMatches,
  getMyMatches
};

function getHomeMatches(cb) {

  httpPost('getHomeMatches', {}, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getMyMatches(params, cb) {

  httpPostTemp('getHomeMyMatches', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}






