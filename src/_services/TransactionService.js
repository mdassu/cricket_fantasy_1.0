import Cookies from 'js-cookie';
import { httpPost, httpPostTemp } from '_helper/ApiBase';
import { encrypt, decrypt } from '_helper/EncrDecrypt';


export const TransactionService = {
  joinContest,
  getMyTransactions,
  withdrawMoney,
  verifyRequest
};

function joinContest(params, cb) {

  httpPostTemp('packContestMapping', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function getMyTransactions(cb) {

  httpPostTemp('getWalletTransaction', {}, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          data: response['data']
        };
      } else {
        var res = {
          status: false,
          data: {}
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function withdrawMoney(params, cb) {

  httpPostTemp('walletWithdraw', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          message: response['message']
        };
      } else {
        var res = {
          status: false,
          message: response['message']
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}

function verifyRequest(params, cb) {

  httpPostTemp('verifyWithdrawRequest', params, (response) => {
    try {
      if (response.result == 1) {
        var res = {
          status: true,
          message: response['message']
        };
      } else {
        var res = {
          status: false,
          message: response['message']
        };
      }
      cb(res);
    } catch (err) {
      throw err;
      // cb(err)
    }
  });

}