import React from 'react';
import Axios from 'axios';
import Cookies from 'js-cookie';
var CryptoJS = require("crypto-js");
var keys = "Se5ZfFTzJ36PH7T6";
// encrypt
export function encrypt(data) {
  // Encrypt
  // var encryptData = CryptoJS.AES.encrypt(JSON.stringify(data), 'Se5ZfFTzJ36PH7T6').toString();
  var key = CryptoJS.enc.Utf8.parse(keys);
  var iv = CryptoJS.enc.Utf8.parse(keys);
  var value = JSON.stringify(data);
  var encryptData = CryptoJS.AES.encrypt(
    CryptoJS.enc.Utf8.parse(value.toString()),
    key,
    {
      keySize: 128 / 8,
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    }
  );
  return encryptData.toString();
}

// decrypt
export function decrypt(data) {
  // Decrypt
  // var bytes = CryptoJS.AES.decrypt(data, 'Se5ZfFTzJ36PH7T6');
  // var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  var key = CryptoJS.enc.Utf8.parse(keys);
  var iv = CryptoJS.enc.Utf8.parse(keys);
  var decrypted = CryptoJS.AES.decrypt(data, key, {
    keySize: 128 / 8,
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7,
  });
  var decryptedData = JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));
  return decryptedData;
}


