import React from 'react';
import Axios from 'axios';
import Cookies from 'js-cookie';
import { encrypt } from '_helper/EncrDecrypt';

function setHeader() {

  const _headers = {
    headers: {
      'Content-Type': 'application/json',
    }
  };
  if (Cookies.get('_accessToken')) {
    _headers['headers']['Authorization'] = `Bearer ${Cookies.get('_accessToken')}`;
  }

  return _headers;
}

export function httpGet(url, cb) {
  Axios.get(`${process.env.GET_API_URL}${url}`, setHeader()).then((response, err) => {
    // return response['data'];
    cb(response['data'], err);
  }).catch(err => {
    cb({}, err);
  });
}

export function httpPost(url, params, cb) {
  // { hdndata: encrypt(params) }
  Axios.post(`${process.env.POST_API_URL}${url}`, { hdndata: encrypt(params) }, setHeader()).then((response, err) => {
    cb(response['data'], err);
  }).catch(err => {
    cb({}, err);
  });
}

export function httpPostTemp(url, params, cb) {
  Axios.post(`${process.env.POST_API_URL}${url}`, params, setHeader()).then((response, err) => {
    cb(response['data'], err);
  }).catch(err => {
    cb({}, err);
  });
}

export function httpUpoadPost(url, params, cb) {
  Axios.post(`${process.env.POST_API_URL}${url}`, params, setHeader()).then((response, err) => {
    cb(response['data'], err);
  }).catch(err => {
    cb({}, err);
  });
}