import ReactGA from 'react-ga';

export const GAevent = (categoryName, eventName, labelName, value) => {
  ReactGA.event({
    category: categoryName,  // Required
    action: eventName,       // Required
    label: labelName,
    value: value,
    nonInteraction: false
  });
}