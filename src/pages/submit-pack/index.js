import React, { Component } from 'react';
import Cookies from 'js-cookie';
import Box from '@material-ui/core/Box';
import { Container, Row, Col, Card, Badge, Button, Table } from 'react-bootstrap';
import TextField from '@material-ui/core/TextField';
import Layout from 'components/Layout/Layout';
import Dialog from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import Link from 'next/link';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Slide from '@material-ui/core/Slide';
import DialogTitle from '@material-ui/core/DialogTitle';
import { decrypt, encrypt } from '_helper/EncrDecrypt';
import Router, { useRouter } from 'next/router';
import { ContestsService } from '_services/ContestsService';
import { useTheme } from '@material-ui/core';
import { PacksService } from '_services/PacksService';
import { AuthenticationService } from '_services/AuthenticationService';
import { TransactionService } from '_services/TransactionService';
import { CommonService } from '_services/CommonService';
import Loader from 'components/CommonComponents/Loader';
import AlertPopup from 'components/CommonComponents/AlertPopup';
import { GAevent } from "_helper/google-analytics";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
class SubmitPack extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      card: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      loopData: [5, 6, 7, 8, 9, 10, 11],
      packData: [],
      matchData: {},
      totalSelectedCredit: 0,
      remainCredit: 0,
      totalSelectedPotential: 0,
      // isLoggedIn: false,
      userWalletBalance: 0,
      userAddedBalance: 0,
      userWinningBalance: 0,
      confirmationPopup: false,
      contestEntryFee: 0,
      bonusBalance: 0,
      contestId: 0,
      packId: 0,
      congratsPopup: false,
      isLoaded: false,
      matchInterval: 0,
      alertPopup: false,
      isDisabled: false,
      email: '',
      mobile: ''
    };
  }

  componentDidMount() {
    if (Cookies.get('_pack')) {
      var data = decrypt(Cookies.get('_pack'));
      this.setState({
        packData: data.selectedCredit,
        totalSelectedCredit: data.totalSelectedCredit,
        remainCredit: data.remainCredit,
        totalSelectedPotential: data.totalSelectedPotential,
        isLoaded: true
      });
    } else {
      Router.push('/');
    }
    this.getMatchDetails();

    // if (AuthenticationService.currentUserValue && AuthenticationService.currentUserValue != null) {
    //   this.setState({
    //     isLoggedIn: true
    //   });
    // } else {
    //   this.setState({
    //     isLoggedIn: false
    //   });
    // }
    this.getCurrentUser();
  }

  getCurrentUser() {
    AuthenticationService.currentUser.subscribe(user => {
      if (user && user != null) {
        this.setState({
          userWalletBalance: parseFloat(user['walletBalance']),
          userAddedBalance: parseFloat(user['addedBalance']),
          userWinningBalance: parseFloat(user['winningBalance']),
          email: user['email'],
          mobile: user['mobile']
        });
      }
    });
  }

  getMatchDetails() {
    clearInterval(this.state.matchInterval)
    var params = {
      match_id: decrypt(Router.query.m)
    }
    ContestsService.getMatchDetails(params, res => {
      if (res.status) {
        if (res['data']['is_Live'] == 1) {
          this.setState({
            matchData: res['data']
          });
          this.setState({
            matchInterval: setInterval(() => {
              this.getMatchDetails();
            }, 10000)
          })
        } else {
          this.setState({
            matchData: res['data']
          });
        }
      } else {
        this.setState({
          matchData: {}
        });
      }
    });
  }

  submitPack = () => {

    if (AuthenticationService.isLogged) {
      if (Cookies.get('_ctData')) {
        var contestData = decrypt(Cookies.get('_ctData'));
        if (this.state.userWalletBalance >= contestData.entryFee) {
          this.setState({
            contestEntryFee: contestData.entryFee,
            contestId: contestData.contestId,
            confirmationPopup: true
          });
        } else {
          var remainAmount = contestData.entryFee - this.state.userWalletBalance;
          Cookies.set('_amount', encrypt(remainAmount));
          Router.push({
            pathname: '/low-balance',
          });
        }
      }
    } else {
      Router.push({
        pathname: '/login'
      });
    }

  }

  joinContest = () => {
    this.setState({
      isDisabled: true
    }, () => {
      var value = '';
      if (this.state.mobile && this.state.email) {
        value = this.state.mobile + '-' + this.state.email;
      } else if (this.state.mobile) {
        value = this.state.mobile;
      } else if (this.state.email) {
        value = this.state.email;
      } else {
        value = '1';
      }
      GAevent('Started_Payment_Process', 'On Join Contest', 'How many people started payment process (from which page)', value);
      var params = {
        match_id: decrypt(Router.query.m),
        list: this.state.packData,
        match_contest_id: this.state.contestId
      };
      PacksService.savePack(params, (res) => {
        if (res.status) {
          AuthenticationService.clearCookies();
          this.setState({
            packId: res.data,
            confirmationPopup: false,
            congratsPopup: true
          });
          CommonService.updateUserWallet();
        } else {
          this.setState({
            confirmationPopup: false,
            alertPopup: true
          });
        }
      });
    });
  }

  confirmPopupClose = () => {
    this.setState({
      confirmationPopup: false,
      contestEntryFee: 0,
      bonusBalance: 0,
    });
  }

  success = () => {
    AuthenticationService.clearCookies();
    Router.push(
      {
        pathname: '/contests',
        query: { m: Router.query.m }
      }
    );
  }

  componentWillUnmount() {
    clearInterval(this.state.matchInterval);
  }

  render() {
    const { matchData, alertPopup, isDisabled } = this.state;
    const { router } = this.props;
    return (
      <Layout customClass="homepage-class" header="packHeader" headerText="Card Preview" params={router.query}>
        { this.state.isLoaded ? (
          <div className="create-packs-tabs">

            <Row className="fantasy-pack match-pack m-0">
              {/* <DialogActions className="previewPack">
              <Button className="close-no" onClick={this.previewPackClose}>
                <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fillRule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                </svg>
              </Button>
            </DialogActions>

            <Col lg={12}>
              <div className="modaltile">
                Card Preview
                     </div>
            </Col> */}

              {/* <Col lg={12} className="starttime text-center">
                      <Countdown date={Date.now() + 60000 * 5} renderer={renderer} /> to new multiplier
                    </Col> */}

              <Col lg={3} xs={3} md={3} sm={3} className="mr-25">
              </Col>
              <Col lg={2} xs={2} md={2} sm={2} className="text-center p-0 mrm--25">
                <div className="my-team-name mi-team" style={{ background: matchData.hometeamcolorcode }}>
                  {matchData.hometeamname}
                </div>
                <br />
                <div className="totalscore">
                  {matchData.home_team_runs != null ? matchData.home_team_runs : '--'}/{matchData.home_team_wickets != null ? matchData.home_team_wickets : '--'} ({matchData.home_team_overs != null ? matchData.home_team_overs : '--'})
                      </div>
              </Col>
              <Col lg={2} xs={2} md={2} sm={2} className="text-center p-0">
                <img src="static/images/whitevs.png" className="team-vs" />
              </Col>
              <Col lg={2} xs={2} md={2} sm={2} className="text-center p-0 mlm-25">
                <div className="my-team-name iplteam" style={{ background: matchData.awayteamcolorcode }}>
                  {matchData.awayteamname}
                </div>
                <br />
                <div className="totalscore">
                  {matchData.away_team_runs != null ? matchData.away_team_runs : '--'}/{matchData.away_team_wickets != null ? matchData.away_team_wickets : '--'} ({matchData.away_team_overs != null ? matchData.away_team_overs : '--'})
                      </div>
              </Col>
              <Col lg={3} xs={3} md={3} sm={3} className="text-right ml-25">
              </Col>
            </Row>
            {/* Fill stepper */}
            <React.Fragment>
              <div className="create-my-pack header-row-my-pack  m-0">
                <Row className="m-0 header-selection justify-content-center align-items-center">
                  <Col lg={4} xs={4} md={4} sm={4} className="text-left p-0">
                    <span> Selections <b>{this.state.packData ? this.state.packData.length : 0}</b> </span>
                  </Col>
                  <Col lg={4} xs={4} md={4} sm={4} className="text-center p-0">
                    <span> Potential Point <b>{this.state.totalSelectedPotential ? parseFloat(this.state.totalSelectedPotential).toFixed(2) : 0}</b></span>
                  </Col>
                  <Col lg={4} xs={4} md={4} sm={4} className="text-right p-0">
                    <span> Credit <b>{this.state.remainCredit ? parseFloat(this.state.remainCredit).toFixed(2) : 0}</b></span>
                  </Col>
                </Row>
                <Row className="m-0 header-row justify-content-center align-items-center">
                  <Col lg={6} xs={5} md={3} sm={3} className="pl-2">
                    <span className="match-win">
                      <small className="mr-2" onClick={() => Router.back()}>
                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-pencil-fill" fill="#646b78" xmlns="http://www.w3.org/2000/svg">
                          <path fillRule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z" />
                        </svg>
                      </small>
                              My fantasy card
                            </span>
                  </Col>
                  <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                    <span> Multiplier</span>
                  </Col>
                  <Col lg={2} xs={3} md={3} sm={3} className="text-center">
                    <span>Credit</span>
                  </Col>
                  <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                    <span className="potential-point"> Potential Point</span>
                  </Col>
                </Row>
                {/* End Match Win row */}


                {this.state.packData && this.state.packData.length > 0 ? this.state.packData.map((item, key) => (
                  <Row key={key} className="m-0 fansty-row match-win-row justify-content-center align-items-center">
                    <Col lg={6} xs={5} md={3} sm={3} className="pl2">
                      <span className="serial-circle">{key + 1}</span>
                      <label className="team-label">
                        {item.marketName} @ {item.selectionName}
                      </label>
                    </Col>
                    <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                      <div className="multiplier-value">{item.multiplier}</div>
                    </Col>
                    <Col lg={2} xs={3} md={3} sm={3} className="text-center pr-1 pl-1">
                      <div className="credit-value ">
                        <div className="blank-input">
                          {item.credit}
                        </div>
                      </div>
                    </Col>
                    <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                      <div className="potential-value">{item.potential}</div>
                    </Col>
                  </Row>
                )) : ''}

                {[...Array(11 - this.state.packData.length)].map((item, key) => {
                  return (
                    <Row key={key} className="m-0 fansty-row match-win-row justify-content-center align-items-center">
                      <Col lg={6} xs={5} md={3} sm={3} className="pl2">
                        <span className="serial-circle">{this.state.packData.length + key + 1}</span>
                        <label className="team-label">
                        </label>
                      </Col>
                      <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                        <div className="multiplier-value"></div>
                      </Col>
                      <Col lg={2} xs={3} md={3} sm={3} className="text-center pr-1 pl-1">
                        <div className="credit-value ">
                          <div className="blank-input">
                          </div>
                        </div>
                      </Col>
                      <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                        <div className="potential-value"></div>
                      </Col>
                    </Row>
                  )
                })}

                <div className="pb-3"></div>
                {/* End Man of the match */}
              </div>
              <Col xs={12} className="mobile-bottom-btn p-0">
                <div className="common-btn">
                  <Button variant="success" onClick={this.submitPack} title="Submit Card">Submit Card</Button>
                </div>
              </Col>
            </React.Fragment>
          </div>
        ) : (
            <Loader />
          )}
        {/* Confirmation modal */}
        <Dialog
          className="common-modals"
          TransitionComponent={Transition}
          open={this.state.confirmationPopup}
          keepMounted
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <CloseIcon className="close-popup" title="Close" onClick={this.confirmPopupClose} />
          <DialogTitle id="alert-dialog-slide-title">
            Confirmation
            <p>
              Wallet amount =  <b>&#x20B9;{this.state.userWalletBalance}</b>
            </p>
          </DialogTitle>
          <DialogContent>
            {/* <DialogContentText id="alert-dialog-slide-description"> */}
            <Table border-0="true" className="justify-content-center align-items-center">
              <tbody>
                <tr>
                  <td className="text-left">
                    Entry
                    </td>
                  <td className="text-right">&#x20B9;{this.state.contestEntryFee}</td>
                </tr>
                {/* <tr>
                  <td className="text-left">
                    <img src="static/images/bonus.png" />
                      Bonus
                    </td>
                  <td className="text-right">-?{this.state.bonusBalance}</td>
                </tr> */}
                <tr className="tablefooter">
                  <td className="text-left">
                    To Pay
                    </td>
                  <td className="text-right">
                    &#x20B9;{this.state.contestEntryFee - this.state.bonusBalance}
                  </td>
                </tr>
              </tbody>
            </Table>
            {/* </DialogContentText> */}
          </DialogContent>
          <DialogActions>
            <div className="common-btn">
              <Button variant="success" title="Join Contest" onClick={this.joinContest} disabled={isDisabled}>Join Contest</Button>
            </div>
          </DialogActions>
        </Dialog>
        {/* End Confirmation modal */}

        {/* Confirmation modal */}
        <Dialog
          className="common-modals"
          TransitionComponent={Transition}
          open={this.state.congratsPopup}
          keepMounted
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogContent>
            {/* <DialogContentText id="alert-dialog-slide-description"> */}
            <Row className="m-0  justify-content-center align-items-center">
              <Col xs={12} className="text-center slide-modal-body ">
                <h5>Congratulations!</h5>
                {/* <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle" fill="#844ff7" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                    <path d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588z" />
                    <circle cx="8" cy="4.5" r="1" />
                  </svg> */}
                <p>Your Card purchased successfully!</p>
              </Col>
            </Row>
            {/* </DialogContentText> */}
          </DialogContent>
          <DialogActions>
            <div className="common-btn">
              {/* <Link href={{ pathname: '/contests', query: { m: Router.query.m } }}> */}
              <Button variant="success" title="Done" onClick={this.success}>Done</Button>
              {/* </Link> */}
            </div>
          </DialogActions>
        </Dialog>
        {/* End Confirmation modal */}
        {/* Confirmation modal */}
        {
          alertPopup ? (
            <AlertPopup type='' alertPopup={alertPopup} alertMsg='Something went wrong!' alertButton='Ok' buttonAction={() => this.setState({ alertPopup: false })} />
          ) : ''
        }
        {/* End Confirmation modal */}
      </Layout >
    );
  }
}

const defaultPage = () => {
  const router = useRouter();
  const theme = useTheme();
  return (
    <SubmitPack router={router} theme={theme} />
  )
}

export default defaultPage;
