import React from 'react';
import Layout from 'components/Layout/Layout';
import { render } from 'react-dom';
import Link from 'next/link';
import { Container, Row, Col, Card, ListGroup, Badge, Button, Table } from 'react-bootstrap';
const ContactUs = () => {
  return (
    <Layout title="Contact Us" header="innerHeader" headerText="Contact Us">
      <div className="slikslider-bar">
        <div className="slikslider-banner"></div>
      </div>
      <Col lg={12} className="content-pages conatctpage p-0">
        <div className="outer-card mt-3">
          <Card className="">
            <Card.Body className="text-center">

              <h6>
                Contact Us
              </h6>
              <div className="justify-content-center align-items-center">
                <span className="contact-circle">
                  <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-envelope" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z" />
                  </svg>
                </span>
              </div>

              <p >

                Have a question? Need a tip? You can feel free for any queries send us an email on
                <a href="mailto:helpdesk@sports.cards" title="Mail Us" className="mailto"> helpdesk@sports.cards</a>
              </p>
            </Card.Body>
          </Card>
        </div>
      </Col>
    </Layout >
  )
}
export default ContactUs;