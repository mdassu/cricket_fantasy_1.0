import React, { Component } from "react";
import Cookies from "js-cookie";
import Link from 'next/link';
import { Container, Row, Col, Card, Badge, Button, Table } from 'react-bootstrap';
import Layout from 'components/Layout/Layout';
import Form from 'react-bootstrap/Form';
import Overlay from 'react-bootstrap/Overlay';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import { TransactionService } from '_services/TransactionService';
import { AuthenticationService } from "_services/AuthenticationService";
import { CommonService } from "_services/CommonService";
import { decrypt } from "_helper/EncrDecrypt";
import Loader from "components/CommonComponents/Loader";

class Wallet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      isLoaded: false
    }
  }

  componentDidMount() {
    CommonService.updateUserWallet();
    setTimeout(() => {
      this.getCurrentUser();
    }, 700);
  }

  getCurrentUser() {
    // AuthenticationService.currentUser.subscribe(user => {
    if (Cookies.get('_user')) {
      var user = decrypt(Cookies.get('_user'));
      if (user && user != null) {
        this.setState({
          user: {
            firstName: user['first_name'],
            lastName: user['last_name'],
            profilePic: user['profile_picture'],
            walletBalance: user['walletBalance'],
            addedBalance: user['addedBalance'],
            bonusBalance: user['bonusBalance'],
            winningBalance: user['winningBalance']
          },
          isLoaded: true
        });
      }
    }
    // });
  }

  render() {
    const { user, isLoaded } = this.state;
    return (
      <React.Fragment>
        <Layout header="tabHeader" headerText="Wallet" customClass="walletpage-class">
          {
            isLoaded ? (
              <div className="" >
                <Row>
                  <Col lg={12}>
                    <div className="outer-card mt-0">
                      <div className="view-all-winner-div mt-3">
                        <Col lg={12} className="p-1 ">
                          <Card>
                            <Card.Body>
                              <Row className="justify-content-center align-items-center add-cash-page">
                                <Col xs={12} className="mobile-bottom-btn add-cash text-center">
                                  <span>
                                    Total Balance
                              </span>
                                  <br />
                                  <b>
                                    &#x20B9;{parseFloat(user['walletBalance'])}
                                  </b>
                                  <div className="common-btn w-100">
                                    <Link href="/add-cash">
                                      <Button className="" variant="success" title="Add Cash" >Add Cash</Button>
                                    </Link>
                                  </div>
                                </Col>
                                <Col xs={12} className="wallet-listing mt-4">
                                  <ul>
                                    <li className="">
                                      <h5> Amount Added</h5>
                                  &#x20B9;{parseFloat(user['addedBalance']) ? parseFloat(user['addedBalance']) : 0}
                                      <span className="left-info-icon">
                                        <OverlayTrigger
                                          placement="left"
                                          delay={{ show: 100, hide: 400 }}
                                          overlay={<Tooltip id="button-tooltip">Added Amount</Tooltip>}
                                        >
                                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                          </svg>
                                        </OverlayTrigger>
                                      </span>
                                    </li>
                                    <li>
                                      <h5> Winnings</h5>
                                        &#x20B9;{parseFloat(user['winningBalance'])}
                                      <Link href="/withdrawal">
                                        <Button className="withdrawal_btn" variant="success" title="Withdrawal Request" >Withdrawal Request</Button>
                                      </Link>

                                      <span className="left-info-icon">
                                        <OverlayTrigger
                                          placement="left"
                                          delay={{ show: 100, hide: 400 }}
                                          overlay={<Tooltip id="button-tooltip">Winning Amount</Tooltip>}
                                        >
                                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                          </svg>
                                        </OverlayTrigger>
                                      </span>
                                    </li>
                                    <li>
                                      <h5>  Cash Bonus</h5>
                                &#x20B9; {parseFloat(user['bonusBalance']) ? parseFloat(user['bonusBalance']) : 0}
                                      <span className="left-info-icon">
                                        <OverlayTrigger
                                          placement="left"
                                          delay={{ show: 100, hide: 400 }}
                                          overlay={<Tooltip id="button-tooltip">Bonus Amount</Tooltip>}
                                        >
                                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                          </svg>
                                        </OverlayTrigger>
                                      </span>
                                    </li>
                                  </ul>
                                </Col>
                              </Row>
                            </Card.Body>
                          </Card>
                        </Col>
                        {/*  */}
                        <Col lg={12} className="p-1">
                          <div className="outer-card  wallet-listing">
                            <Card>
                              <Card.Body className="addcards">
                                <Row className="justify-content-center align-items-center">
                                  <Col lg={12} xs={12} sm={12} md={12}  >
                                    <Link href="/transactions">
                                      <a title="My Recent Transactions">
                                        My Recent Transactions
                                    <span> For seamless, convenient payments</span>
                                        <span className="left-info-icon">
                                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-chevron-right" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                                          </svg>
                                        </span>
                                      </a>
                                    </Link>
                                  </Col>
                                </Row>
                              </Card.Body>
                            </Card>
                          </div>
                        </Col>
                        {/*  <Col lg={12} className="p-1">
                      <div className="outer-card  wallet-listing">
                        <Card>
                          <Card.Body className="addcards">
                            <Row className="justify-content-center align-items-center">
                              <Col lg={12} xs={12} sm={12} md={12}  >
                                <Link href="/manage-payments">
                                  <a title="Manage Payments">
                                    Manage Payments
                                    <span>Add/ Remove Cads, Wallets, etc.</span>
                                    <span className="left-info-icon">
                                      <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-chevron-right" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                                      </svg>
                                    </span>
                                  </a>
                                </Link>
                              </Col>
                            </Row>
                          </Card.Body>
                        </Card>
                      </div>
                    </Col>*/}
                        <Col lg={12} className="p-1">
                          <div className="outer-card  wallet-listing">
                            <Card>
                              <Card.Body className="addcards">
                                <Row className="justify-content-center align-items-center">
                                  <Col lg={12} xs={12} sm={12} md={12}  >
                                    <Link href="/invite-friends">
                                      <a title=" Refer and Earn">
                                        Refer and Earn
                                    <span>Invite a friend and earn up to &#x20B9;100 Bonus</span>
                                        <span className="left-info-icon">
                                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-chevron-right" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                                          </svg>
                                        </span>
                                      </a>
                                    </Link>
                                  </Col>
                                </Row>
                              </Card.Body>
                            </Card>
                          </div>
                        </Col>
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
            ) : <Loader />
          }
        </Layout>
      </React.Fragment >
    )
  }
}
export default Wallet;
