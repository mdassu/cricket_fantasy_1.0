import React, { Component } from "react";
import Cookies from 'js-cookie';
import Layout from "components/Layout/Layout";
import { Button, Modal, TextField, Select, MenuItem, Snackbar } from '@material-ui/core';
import Link from "next/link";
import { Container, Row, Col, Card, Badge } from 'react-bootstrap';
const { flag, code, name, countries } = require('country-emoji');
import ReactCountryFlag from "react-country-flag";
import { CommonService } from '_services/CommonService';
import MuiAlert from '@material-ui/lab/Alert';

import { ErrorMessages, isNameValid, isMobileNumberValid, isEmailValid } from 'components/MyValidations/MyValidations';
import { AuthenticationService } from "_services/AuthenticationService";
import Router from 'next/router';
import { encrypt } from "_helper/EncrDecrypt";
import SocialButton from 'components/socialButton';
import { GAevent } from "_helper/google-analytics";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class Register extends Component {

  // const classes = useStyles();
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      popupView: 'login',
      otpMobile: '',
      otpReferral: '',
      otpCountryCode: '',
      isLoaded: true,
      terms: false,
      mobile: '',
      formData: {
        mobile: '',
        countryCode: '91',
        otp: '',
        firstName: '',
        lastName: '',
        emailId: '',
        referralCode: ''
      },
      error: false,
      errorMessage: {
        mobile: '',
        firstName: '',
        lastName: '',
        emailId: '',
        otp: '',
        terms: '',
        referralCode: ''
      },
      alert: {
        toast: false,
        toastMessage: '',
        severity: ''
      },
      countDown: 0,
      countries: [],
      logged: false,
      user: {},
      currentProvider: ''
    }
    this.nodes = {}

    this.logout = this.logout.bind(this)
  }

  componentDidMount() {
    AuthenticationService.clearCookies();
    CommonService.countrySubject.subscribe(countries => {
      this.setState({
        countries: countries
      });
    });
    if (Router.query.refId) {
      this.setState({
        formData: {
          mobile: '',
          countryCode: '91',
          otp: '',
          firstName: '',
          lastName: '',
          emailId: '',
          referralCode: Router.query.refId
        }
      });
    }
  }

  handleChange = name => ({ target: { value, checked } }) => {
    if (name == 'terms') {
      this.setState({
        formData: {
          ...this.state.formData,
          [name]: checked
        }
      });
    } else {
      this.setState({
        formData: {
          ...this.state.formData,
          [name]: value
        }
      });
    }

  }

  registerSubmit = (event) => {
    event.preventDefault();
    const { formData } = this.state;
    var procced = false;

    var msgMobile = '';
    var msgFirstName = '';
    var msgLastName = '';
    var msgEmailId = '';
    var msgCheckTerms = '';

    var isValid = false;

    // first name validation
    if (formData.firstName) {
      // if (!isNameValid(formData.firstName)) {
      //   msgFirstName = (ErrorMessages.VALIDATION_ERROR_NAME).replace('full', 'first');
      // }
    } else {
      msgFirstName = 'First name is required';
    }

    // last name validation
    if (formData.lastName) {
      if (!isNameValid(formData.lastName)) {
        msgLastName = (ErrorMessages.VALIDATION_ERROR_NAME).replace('full', 'last');
      }
    } else {
      msgLastName = 'Last name is required';
    }

    // email id validation
    if (formData.emailId) {
      if (!isEmailValid(formData.emailId)) {
        msgEmailId = (ErrorMessages.VALIDATION_ERROR_EMAIL);
      }
    } else {
      msgEmailId = 'Email id is required';
    }

    // mobile number. validation
    if (formData.mobile) {
      if (!isMobileNumberValid(formData.mobile)) {
        msgMobile = ErrorMessages.VALIDATION_ERROR_MOBILE;
      }
    } else {
      msgMobile = 'Mobile number is required';
    }

    // if (!this.state.terms) {
    //   msgCheckTerms = 'Please accept Terms & Conditions';
    // }

    if (!msgFirstName && !msgLastName && !msgMobile && !msgEmailId && !msgCheckTerms) {
      isValid = true;
    }

    if (isValid) {
      this.setState({
        error: true,
        errorMessage: {
          firstName: '',
          lastName: '',
          emailId: '',
          mobile: '',
          terms: ''
        },
        isLoaded: false
      });
      if (formData.referralCode != '' && formData.referralCode != undefined) {
        var request = {
          referral_code: formData.referralCode,
        }
        AuthenticationService.checkReferralCode(request, (res) => {
          if (res.status) {
            this.setState({
              otpReferral: formData.referralCode
            });
            var params = {
              country_code: formData['countryCode'],
              mobile: formData['mobile'],
              first_name: formData['firstName'],
              last_name: formData['lastName'],
              email: formData['emailId']
            }
            AuthenticationService.signUp(params, (res) => {
              if (res.status) {
                var data = {
                  otpMobile: formData.mobile,
                  otpCountryCode: formData.countryCode,
                  otpReferral: formData.referralCode
                };
                Cookies.set('_mobData', encrypt(JSON.stringify(data)));
                Router.push('/verify-otp');
              } else {
                this.setState({
                  error: true,
                  errorMessage: { mobile: res.message },
                  isLoaded: true
                });
              }
            });
          } else {
            this.setState({
              error: true,
              errorMessage: {
                referralCode: res.message
              },
              isLoaded: true
            });
          }
        });
      } else {
        var params = {
          country_code: formData['countryCode'],
          mobile: formData['mobile'],
          first_name: formData['firstName'],
          last_name: formData['lastName'],
          email: formData['emailId']
        }
        AuthenticationService.signUp(params, (res) => {
          if (res.status) {
            var data = {
              otpMobile: formData.mobile,
              otpCountryCode: formData.countryCode,
              otpReferral: formData.referralCode
            };
            Cookies.set('_mobData', encrypt(JSON.stringify(data)));
            Router.push('/verify-otp');
          } else {
            if (res.message.search('Email') != -1) {
              this.setState({
                error: true,
                errorMessage: { emailId: res.message },
                isLoaded: true
              });
            } else {
              this.setState({
                error: true,
                errorMessage: { mobile: res.message },
                isLoaded: true
              });
            }
          }
        });
      }
    } else {
      this.setState({
        error: true,
        errorMessage: {
          firstName: msgFirstName,
          lastName: msgLastName,
          emailId: msgEmailId,
          mobile: msgMobile,
          terms: msgCheckTerms
        }
      });
    }
  };

  handleSocialLogin = (user) => {
    console.log(user);
    var params = {
      country_code: '',
      mobile: '',
      first_name: '',
      last_name: '',
      email: ''
    };
    if (user._profile) {
      if (user._profile['firstName']) {
        params['first_name'] = user._profile['firstName'];
      }
      if (user._profile['lastName']) {
        params['last_name'] = user._profile['lastName'];
      }
      if (user._profile['email']) {
        params['email'] = user._profile['email'];
      }
      if (user._profile['mobile']) {
        params['mobile'] = user._profile['mobile'];
      }
      console.log(params);
      AuthenticationService.signUp(params, (res) => {
        if (res.status) {
          var userData = response['data'];
          var userJson = {
            country_code_id: userData['country_code_id'],
            mobile: userData['mobile'],
            first_name: userData['first_name'],
            last_name: userData['last_name'],
            last_login: userData['last_login'],
            email: userData['email'],
            profile_picture: userData['profile_picture'],
            walletBalance: userData['walletBalance'],
            addedBalance: userData['addedBalance'],
            bonusBalance: userData['bonusBalance'],
            winningBalance: userData['winningBalance'],
            creditedCash: userData['creditedCash'],
            creditedType: userData['creditType'],
            referralCode: userData['referral_code'],
            isEmailVerified: userData['email_is_verified']
          };
          var encryptData = encrypt(userJson);
          Cookies.set('_accessToken', userData['token'], { expires: 30 });
          Cookies.set('_user', encryptData, { expires: 30 });
          CommonService.currentUserSubject.next(userJson);
          Router.push('/');
        } else {
          this.setState({
            alert: {
              toast: true,
              toastMessage: res.message,
              severity: 'error'
            },
          });
        }
      });
      var value = '';
      if (params['mobile'] && params['email']) {
        value = params['mobile'] + '-' + params['email'];
      } else if (params['mobile']) {
        value = params['mobile'];
      } else if (params['email']) {
        value = params['email'];
      } else {
        value = '1';
      }
      GAevent('Successfull_Login', 'Successfull Login', 'How many users logged in', value);
    }
  }

  handleSocialLoginFailure = (err) => {
    this.setState({
      alert: {
        toast: true,
        toastMessage: 'Something went wrong.',
        severity: 'error'
      },
    });
  }

  setCountryCode = (event) => {
    const { value } = event.currentTarget.dataset;
    this.setState({
      formData: {
        countryCode: value
      }
    })
  }

  checkTerms = (event) => {
    this.setState({
      terms: event.target.checked
    });
  }

  setNodeRef(provider, node) {
    if (node) {
      this.nodes[provider] = node
    }
  }

  logout() {
    const { logged, currentProvider } = this.state

    if (logged && currentProvider) {
      this.nodes[currentProvider].props.triggerLogout()
    }
  }
  render() {
    // const classes = useStyles;
    const { countries, isLoaded, formData: { mobile, countryCode, otp, firstName, lastName, emailId, terms, referralCode }, otpMobile, otpCountryCode, alert } = this.state;
    var country = '';
    if (countries && countries.length > 0) {
      country = countries.map((country, key) =>
        <MenuItem key={key} value={country.country_code}>
          <ReactCountryFlag countryCode={code(country.country_name)} svg /> &nbsp;+{country.country_code}
        </MenuItem>
      );
    }
    return (
      <Layout header="innerHeader" headerText="Register" customClass="myregister-class">
        <Snackbar open={alert.toast} autoHideDuration={2000} onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })}>
          <Alert onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })} severity={alert.severity}>
            {alert.toastMessage}
          </Alert>
        </Snackbar>
        <Row className="user-pages">
          <Col lg={12}>
            <div className="outer-card mt-3">
              <Card>
                <Card.Body className="">
                  <Row className="justify-content-center align-items-center pt-lg-5 pl-lg-5 pr-lg-5 pb-3">
                    <Col lg={12}>
                      <h3 className="modal-title">Register</h3>
                    </Col>
                    <Col lg={12}>
                      <p className="modal-description">
                        Create your account, it’s free!
                    </p>
                    </Col>
                    <Col lg={12}>
                      <form className="register-form" onSubmit={this.registerSubmit}>
                        <div className="phone-input clearfix">
                          <Row>

                            <Col lg={6} xs={12} md={6} md={6} className="pr-lg-2" >
                              <TextField
                                label="First Name"
                                type="text"
                                variant="outlined"
                                fullWidth
                                className="mb-3"
                                name="firstName"
                                inputProps={{
                                  name: 'phone',
                                  autoFocus: true,
                                  maxLength: 20
                                }}
                                value={firstName}
                                onChange={this.handleChange('firstName')}
                                error={!!this.state.errorMessage.firstName}
                                helperText={!!this.state.errorMessage.firstName ? this.state.errorMessage.firstName : ''}
                              />
                            </Col>
                            <Col lg={6} xs={12} md={6} md={6} className="pl-lg-2" >
                              <TextField
                                label="Last Name"
                                type="text"
                                variant="outlined"
                                fullWidth
                                className="mb-3"
                                name="lastName"
                                value={lastName}
                                onChange={this.handleChange('lastName')}
                                error={!!this.state.errorMessage.lastName}
                                helperText={!!this.state.errorMessage.lastName ? this.state.errorMessage.lastName : ''}
                                inputProps={{ maxLength: 20 }}
                              />
                            </Col>
                            <Col lg={6} xs={12} md={6} md={6} className="inputNumber">
                              {country != '' ?
                                (< Select value={this.state.formData['countryCode']} onChange={this.handleChange('countryCode')} variant="outlined" className="country-select">
                                  {country}
                                </Select>)
                                : ''}
                              <TextField
                                className="number-input mobile-input mb-3"
                                variant="outlined"
                                type="number"
                                inputProps={{
                                  name: 'phone',

                                }}
                                value={mobile}
                                onChange={this.handleChange('mobile')}
                                error={!!this.state.errorMessage.mobile}
                                helperText={!!this.state.errorMessage.mobile ? this.state.errorMessage.mobile : ''}
                              />
                            </Col>

                            <Col lg={6} xs={12} md={6} md={6} className="pl-lg-2" >
                              <TextField
                                label="Email Id"
                                type="text"
                                variant="outlined"
                                fullWidth
                                className="mb-3"
                                name="emailId"
                                value={emailId}
                                onChange={this.handleChange('emailId')}
                                error={!!this.state.errorMessage.emailId}
                                helperText={!!this.state.errorMessage.emailId ? this.state.errorMessage.emailId : ''}
                              />
                            </Col>

                            <Col lg={12} xs={12} md={12} md={12} >
                              <TextField
                                label="Referral Code"
                                type="text"
                                variant="outlined"
                                fullWidth
                                value={referralCode}
                                name="referralCode"
                                onChange={this.handleChange('referralCode')}
                                error={!!this.state.errorMessage.referralCode}
                                helperText={!!this.state.errorMessage.referralCode ? this.state.errorMessage.referralCode : ''}
                              />
                            </Col>

                          </Row>

                        </div>
                        <Button type="submit" title="Register Now" fullWidth variant="contained" color="primary" className="register-now text-capitalize p-9 mt-26 mt-lg-5">
                          Register Now
                        </Button>
                      </form>
                    </Col>
                    {/* Social Login Buttons */}
                    <Col lg={12} className="text-center mt-4">
                      <div className="or-icon">
                        <span>OR</span>
                      </div>
                    </Col>
                    <Col lg={12} xs={12} sm={12} md={12} className="text-center p-0 socillogin mt-4  justify-content-center align-items-center">
                      <SocialButton
                        provider='facebook'
                        appId='401646980960098'
                        onLoginSuccess={this.handleSocialLogin}
                        onLoginFailure={this.handleSocialLoginFailure}
                        className="facebooklogin"
                      >
                        <img src="static/images/facebooklogin.svg" />  Facebook
                       </SocialButton>
                      <SocialButton
                        provider='google'
                        appId='646960656174-jrund09ejt85d0knjrtsm6s8b535gt49.apps.googleusercontent.com'
                        onLoginSuccess={this.handleSocialLogin}
                        onLoginFailure={this.handleSocialLoginFailure}
                        getInstance={this.setNodeRef.bind(this, 'google')}
                        key={'google'}
                        scope={'https://www.googleapis.com/auth/user.gender.read'}
                        className="googlelogin"
                      >
                        <img src="static/images/google.svg" />
                        Google
                        </SocialButton>
                    </Col>
                    {/* End Social Login Buttons */}
                    <Col lg={12}>
                      <div className="register-here">
                        <small>Already have an account?
                          <Link href="/login" >
                            <a title="Log in here"> Log in here</a>
                          </Link>
                        </small>
                      </div>
                    </Col>
                  </Row>
                </Card.Body>
              </Card>

            </div>
          </Col>
        </Row>

      </Layout >
    );
  }
}
export default Register;
