import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
// import TabPanel from '@material-ui/lab/TabPanel';
import Box from '@material-ui/core/Box';
import Link from 'next/link';
import Layout from 'components/Layout/Layout';
import { Container, ProgressBar, Row, Col, Card, Badge, Button, Table } from 'react-bootstrap';
import MyPacks from "components/Contests/MyPacks";
import MyContest from 'components/Contests/MyContests';
import { ContestsService } from '_services/ContestsService';
import Router from 'next/router';
import { decrypt } from '_helper/EncrDecrypt';
import MyCompletedContests from 'components/Matches/Details/MyCompletedContests';
import MyCompletedPacks from 'components/Matches/Details/MyCompletedPacks';
import LeaderBoard from 'components/Contests/LeaderBoard';

const styles = {
  tabPanelRoot: 'tabPanelRoot',
  tabPanelText: 'tabPanelText',
  tabContainer: 'tabContainer'
}


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

class MatchDetails extends Component {

  constructor(props) {
    super(props);
    this.handleChangeIndex = this.handleChangeIndex.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      value: 0,
      matchData: {},
      myContestCount: 0,
      myPackCount: 0,
      contestDetails: {},
      prizeDetails: []
    };
  }

  componentDidMount() {
    this.getMatchDetails();
    this.getContestDetails();
  }

  getMatchDetails = () => {
    var params = {
      match_id: decrypt(Router.query.m)
    }
    ContestsService.getMatchDetails(params, res => {
      if (res.status) {
        this.setState({
          matchData: res['data']
        });
      } else {
        this.setState({
          matchData: []
        });
      }
    });
  }

  getContestDetails = () => {
    var params = {
      contest_id: decrypt(Router.query.c)
    }
    ContestsService.getContestDetails(params, (res) => {
      if (res.status) {
        this.setState({
          contestDetails: res['data']['contestDetails'],
          prizeDetails: res['data']['leaderboardData']
        })
      } else {

      }
    })
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  a11yProps(index) {
    return {
      id: `full-width-tab-${index}`,
      'aria-controls': `full-width-tabpanel-${index}`,
    };
  }


  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  setContestCount = (data) => {
    this.setState({
      myContestCount: data.contestCount,
      myPackCount: data.packCount
    });
  }

  render() {
    const { value, prizeDetails, contestDetails } = this.state;
    const { theme } = this.props;
    return (
      <Layout header="innerHeader" headerText="">
        <div className="contests-tabs mycontent-tab">
          <Row className="m-0 match-detail-header  justify-content-center align-items-center">
            <Col lg={4} xs={3} >
              <div className="header-team-score">
                {this.state.matchData.hometeamname}
                <b>{this.state.matchData.home_team_runs ? this.state.matchData.home_team_runs : '--'}/{this.state.matchData.home_team_wickets ? this.state.matchData.home_team_wickets : '--'} ({this.state.matchData.home_team_overs ? this.state.matchData.home_team_overs : '--'})</b>
              </div>
            </Col>
            <Col lg={4} xs={6} className="text-center p-0 match-result-text" >
              <div className="completed-match-label">Completed</div>
              {this.state.matchData.match_result ? (<p>{this.state.matchData.match_result}</p>) : ''}
            </Col>
            <Col lg={4} xs={3} className="text-right">
              <div className="header-team-score">
                {/* <div className="pts-circle">
                  pst
                </div> */}
                {this.state.matchData.awayteamname}
                <b>{this.state.matchData.away_team_runs ? this.state.matchData.away_team_runs : '--'}/{this.state.matchData.away_team_wickets ? this.state.matchData.away_team_wickets : '--'} ({this.state.matchData.away_team_overs ? this.state.matchData.away_team_overs : '--'})</b>
              </div>
            </Col>
          </Row>

          <AppBar position="static" color="default">
            <Tabs
              value={value}
              onChange={this.handleChange}
              indicatorColor="primary"
              textColor="primary"
              variant="fullWidth"
              aria-label="full width tabs example"
            >
              <Tab className={styles.tabPanelText} label="Contests Details" />
              <Tab className={styles.tabPanelText} label="Leaderboard" />
            </Tabs>
          </AppBar>
          <SwipeableViews animateHeight={false} className={styles.tabContainer} axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'} index={value} onChangeIndex={this.handleChangeIndex}>
            <TabPanel value={value} index={0} dir={theme.direction}>
              <div className="contests-tabs">
                <Table bordered hover>
                  <thead>
                    <tr>
                      <th>Rank</th>
                      <th className="text-right">Prize</th>
                    </tr>
                  </thead>
                  <tbody>
                    {prizeDetails.map(item => (
                      <tr >
                        <td className="text-left">#{item.ranks_From == item.ranks_To ? item.ranks_To : item.ranks_From + '-' + item.ranks_To}</td>
                        <td className="text-right">&#x20B9; {item.prize_Money_per_person}</td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
            </TabPanel>
            <TabPanel value={value} index={1} dir={theme.direction}>
              {/* <NoData /> */}
              <LeaderBoard matchStatus={contestDetails.match_status} />
            </TabPanel>
          </SwipeableViews>
        </div>

      </Layout>
    );
  }
}

export default withStyles(styles, { withTheme: true })(MatchDetails);