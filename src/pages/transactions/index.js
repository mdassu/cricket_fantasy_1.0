import React, { Component } from "react";
import Cookies from "js-cookie";
import Link from 'next/link';
import { Container, Row, Col, Card, Badge, Button, Table } from 'react-bootstrap';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Layout from 'components/Layout/Layout';
import Form from 'react-bootstrap/Form'
import { TransactionService } from '_services/TransactionService';;

import * as moment from 'moment';
import NoData from "components/Contests/NoData";
import Loader from "components/CommonComponents/Loader";

class Transactions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transactionsDate: [],
      transactions: [],
      expanded: '00',
      isLoaded: false
    }
  }

  componentDidMount() {
    this.getTransactions();
  }

  getTransactions = () => {
    TransactionService.getMyTransactions((res) => {
      if (res.status) {
        this.setState({
          transactions: res['data'],
          transactionsDate: Object.keys(res['data']),
          isLoaded: true
        });
      } else {
        this.setState({
          transactions: [],
          transactionsDate: [],
          isLoaded: true
        });
      }
    })
  }

  changeExpend = (key) => {
    this.setState({ expanded: key })
  }

  render() {
    const { expanded } = this.state
    return (
      <React.Fragment>
        <Layout header="innerHeader" headerText="Transactions" customClass="createpage-class">
          {
            this.state.isLoaded ? (
              <div className="my-tractions" >
                <Row >
                  <Col lg={12}>
                    {
                      this.state.transactionsDate && this.state.transactionsDate.length > 0 ?
                        this.state.transactionsDate.map((item, key) => (
                          <div className="outer-card mt-3" key={key}>
                            <h5 className="tractions-title">{moment(item).format('DD MMMM, YYYY')}</h5>
                            {
                              this.state.transactions[item] ? this.state.transactions[item].map((trans, tKey) => (
                                <Accordion expanded={expanded == key.toString() + tKey.toString()} onChange={() => this.changeExpend(key.toString() + tKey.toString())} className="mb-3" key={tKey}>
                                  <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                  >
                                    <Typography>
                                      <span>{trans.type_name == 'Joined a Contest' || trans.status == 'Success' || trans.status == 'In-Process' || trans.status == 'Failed' ? '-' : '+'} &#x20B9;{trans.amount}</span>
                                      {trans.type_name}</Typography>
                                  </AccordionSummary>
                                  <AccordionDetails>
                                    <Table border-0 >
                                      <tbody>
                                        <tr>
                                          <td className="text-left">
                                            <b>
                                              Transaction Id:
                                </b>
                                          </td>
                                          <td className="text-right">{trans.transaction_id}</td>
                                        </tr>
                                        <tr>
                                          <td className="text-left">
                                            <b>
                                              Transaction Date:
                                </b>
                                          </td>
                                          <td className="text-right">{moment(trans.created_at).format('DD MMMM, h:mm a')}</td>
                                        </tr>
                                        {
                                          trans.pack_name != '' && trans.pack_name != null ? (
                                            <tr>
                                              <td className="text-left">
                                                <b>Card Name:</b>
                                              </td>
                                              <td className="text-right">{trans.pack_name}</td>
                                            </tr>
                                          ) : ''
                                        }

                                        {
                                          trans.status != '' ? (
                                            <tr>
                                              <td className="text-left">
                                                <b>Status:</b>
                                              </td>
                                              <td className="text-right">{trans.status}</td>
                                            </tr>
                                          ) : ''
                                        }

                                      </tbody>
                                    </Table>
                                  </AccordionDetails>
                                </Accordion>
                              )) : ''
                            }
                          </div>
                        )) : <NoData headText="You haven't any transactions!" />
                    }
                  </Col>
                </Row>
              </div>
            ) : (
                <Loader />
              )
          }
        </Layout>
      </React.Fragment >
    )
  }
}
export default Transactions;