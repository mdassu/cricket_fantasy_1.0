import React, { Component } from "react";
import Layout from "components/Layout/Layout";
import { MenuItem, Snackbar, Dialog, DialogActions, DialogContent, Slide } from '@material-ui/core';
import { Container, Row, Col, Card, Badge, Button } from 'react-bootstrap';
const { code } = require('country-emoji');
import ReactCountryFlag from "react-country-flag";
import OtpInput from 'react-otp-input';
import Cookies from 'js-cookie';
import { decrypt, encrypt } from "_helper/EncrDecrypt";
import { AuthenticationService } from "_services/AuthenticationService";
import Router from "next/router";
import MuiAlert from '@material-ui/lab/Alert';
import { TransactionService } from "_services/TransactionService";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class VerifyRequest extends Component {

  // const classes = useStyles();
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      popupView: 'login',
      otpMobile: '',
      otpReferral: '',
      otpCountryCode: '',
      isLoaded: true,
      terms: false,
      successMsg: '',
      successPopup: false,
      mobile: '',
      formData: {
        mobile: '',
        countryCode: '91',
        otp: '',
        firstName: '',
        lastName: '',
        referralCode: ''
      },
      error: false,
      errorMessage: {
        mobile: '',
        firstName: '',
        lastName: '',
        otp: '',
        terms: '',
        referralCode: ''
      },
      alert: {
        toast: false,
        toastMessage: '',
        severity: ''
      },
      countDown: 0,
      countries: [],
      congratsPopup: false,
      userWalletBalance: 0,
      entryFee: 0,
      creditedCash: 0
    }
  }

  componentDidMount() {
  }

  handleOTPChange = otp => this.setState({
    formData: {
      otp: otp
    }
  });

  verifyOTP = (event) => {
    event.preventDefault();
    const { formData, otpMobile, otpReferral } = this.state;
    if (formData.otp && formData.otp.length == 6) {
      var params = {
        otp: parseInt(formData.otp)
      }
      this.setState({
        isLoaded: false
      });
      TransactionService.verifyRequest(params, (res) => {
        if (res.status) {
          Cookies.remove('_wR');
          this.setState({
            successMsg: res.message,
          }, () => {
            this.setState({
              successPopup: true
            });
          });

        } else {
          // Cookies.remove('_mobData');
          this.setState({
            error: true,
            errorMessage: { otp: res.message },
            isLoaded: true
          });
        }
      });
    } else {
      this.setState({
        error: true,
        errorMessage: { otp: 'Please enter otp.' },
        isLoaded: true
      });
    }
  };

  setCountryCode = (event) => {
    const { value } = event.currentTarget.dataset;
    this.setState({
      formData: {
        countryCode: value
      }
    })
  }

  render() {
    // const classes = useStyles;
    const { countries, isLoaded, formData: { mobile, countryCode, otp, firstName, lastName, terms, referralCode }, otpMobile, otpCountryCode, alert, congratsPopup, successMsg, successPopup } = this.state;
    var country = '';
    if (countries && countries.length > 0) {
      country = countries.map((country, key) =>
        <MenuItem key={key} value={country.country_code}>
          <ReactCountryFlag countryCode={code(country.country_name)} svg /> &nbsp;+{country.country_code}
        </MenuItem>
      );
    }
    return (
      <Layout header="innerHeader" headerText="Verify OTP">
        <Snackbar open={alert.toast} autoHideDuration={2000} onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })}>
          <Alert onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })} severity={alert.severity}>
            {alert.toastMessage}
          </Alert>
        </Snackbar>
        <Row className="user-pages">
          <Col lg={12}>
            <div className="outer-card mt-3">
              <Card>
                <Card.Body className="pt-4">
                  <Row className="justify-content-center align-items-center p-lg-5 pb-3">
                    <Col lg={12}>
                      <h3 className="modal-title">Enter verification code</h3>
                    </Col>
                    <Col lg={12}>
                      <p className="modal-description">
                        OTP sent to your registered mobile number.
                      </p>
                    </Col>
                    <Col lg={12}>
                      <form className="otp-form" onSubmit={this.verifyOTP}>
                        <div className="phone-input clearfix">
                          <Row>
                            <Col lg={12} xs={12} md={12} md={12} className="user-otp text-center p-0">
                              <input type="hidden" name="mobile" value={otpMobile} />
                              <OtpInput
                                value={otp}
                                onChange={this.handleOTPChange}
                                numInputs={6}
                                isInputNum
                                shouldAutoFocus
                              // separator={<span>-</span>}
                              />
                              {this.state.errorMessage.otp ? (<small className="text-danger">{this.state.errorMessage.otp}</small>) : ''}
                            </Col>
                          </Row>
                        </div>
                        <button type="submit" title="Verify & Proceed" fullWidth color="" className="register-now verifybtn text-capitalize p-9 mt-26">
                          Verify & Proceed
</button>
                      </form>
                    </Col>
                    {/* <Col lg={12}>
                      <div className="register-here resend-otp">
                        {this.state.countDown ? (<small>00:
                          {this.state.countDown} sec</small>) : (<small>Resend <strong title="OTP" onClick={() => this.logInSubmit()}>OTP?</strong></small>)}<br />
                      </div>
                    </Col> */}
                  </Row>
                </Card.Body>
              </Card>

            </div>
          </Col>
        </Row>
        {
          congratsPopup ? (
            <iframe src="https://catalyst.vnative.net/pixel?adid=5fa13173f1db73326c3b1a07" scrolling="no" frameborder="0" width="1" height="1"></iframe>
          ) : ''
        }

        <Dialog
          className="common-modals"
          TransitionComponent={Transition}
          open={successPopup}
          keepMounted
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogContent>
            <Row className="m-0 justify-content-center align-items-center">
              <Col xs={12} className="text-center slide-modal-body ">
                {/* <h5>Congratulations!</h5> */}
                <p>{successMsg != '' ? successMsg : ''}</p>
              </Col>
            </Row>
          </DialogContent>
          <DialogActions>
            <div className="common-btn">

              <Button variant="success" title="Ok" onClick={() => Router.push('/wallet')}>Ok</Button>

            </div>
          </DialogActions>
        </Dialog>
      </Layout >
    );
  }
}
export default VerifyRequest;