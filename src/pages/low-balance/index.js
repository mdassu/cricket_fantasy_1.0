import React, { Component } from "react";
import Cookies from "js-cookie";
import Link from "next/link";
import { Container, Row, Col, Card, Badge, Button } from "react-bootstrap";
import {
    Snackbar,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    Slide,
} from "@material-ui/core";
import Layout from "components/Layout/Layout";
import Form from "react-bootstrap/Form";
import { AuthenticationService } from "_services/AuthenticationService";
import { BankingService } from "_services/BankingService";
import Router from "next/router";
import { decrypt } from "_helper/EncrDecrypt";
import AlertPopup from 'components/CommonComponents/AlertPopup';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

class LowBalance extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userWalletBalance: 0,
            amount: 0,
            congratsPopup: false,
            entryFee: 0,
            emailIsVerified: false,
            alertPopup: false,
            alertType: 'sendMail',
            alertSecButton: '',
            alertMsg: 'Please verify your email id before adding cash.',
            alertButton: 'Verify Email',
            mailSentStatus: false
        };
    }

    componentDidMount() {
        this.getCurrentUser();
        if (Cookies.get("_amount") && Cookies.get("_pack")) {
            this.setState({
                amount: parseFloat(decrypt(Cookies.get("_amount"))),
            });
        } else {
            Router.push('/');
        }
    }

    getCurrentUser() {
        // AuthenticationService.currentUser.subscribe((user) => {
        AuthenticationService.getProfile((res) => {
            if (res.status) {
                var user = res['data']
                var emailVerify = false;
                if (user && user != null) {
                    if (user['email_is_verified'] == 1) {
                        emailVerify = true;
                    }
                    this.setState(
                        {
                            userWalletBalance:
                                parseFloat(user["walletBalance"]),
                            emailIsVerified: emailVerify,
                        },
                        () => {
                            this.setState({
                                entryFee:
                                    parseFloat(this.state.userWalletBalance) +
                                    parseFloat(decrypt(Cookies.get("_amount"))),
                            });
                        }
                    );
                }
            }
        });
        // });
    }

    handleChange = (event) => {
        this.setState({
            amount: parseFloat(event.target.value),
        });
    };

    changeAmount = (amount) => {
        this.setState({
            amount: parseFloat(amount),
        });
    };

    addCash = () => {
        this.getCurrentUser();
        setTimeout(() => {
            if (this.state.emailIsVerified) {
                var params = {
                    amount: parseFloat(this.state.amount)
                }
                BankingService.addCash(params, (res) => {
                    if (res.status) {
                        window.location.href = res['data'];
                    } else {

                    }
                });
            } else {
                this.setState({
                    mailSentStatus: false,
                    alertType: 'sendMail',
                    alertMsg: 'Please verify your email id before adding cash.',
                    alertButton: 'Verify Email'
                }, () => {
                    this.setState({
                        alertPopup: true
                    });
                })
            }
        }, 200)
    };

    alertAction = (type = '') => {
        if (type == 'sendMail') {
            this.setState({
                mailSentStatus: true,
                alertButton: 'Sending Mail...'
            });
            AuthenticationService.sendVerifyEmail((res) => {
                if (res.status) {
                    this.setState({
                        alertType: '',
                        alertButton: 'Ok',
                        alertSecButton: '',
                        mailSentStatus: false,
                        alertMsg: 'Mail have been sent successfully. Please check your mail inbox!'
                    });
                } else {
                    this.setState({
                        alertType: 'sendMail',
                        alertButton: 'Resend Verify Email',
                        alertSecButton: 'Cancel',
                        mailSentStatus: false,
                        alertMsg: 'Mail have been not sent successfully. Please try again!'
                    });
                }
            });
        } else {
            this.setState({
                alertPopup: false
            });
        }
    }

    cancelTrans = () => {
        AuthenticationService.clearCookies();
        Router.push('/');
    }

    render() {
        const { alertPopup, alertType, alertMsg, alertButton, alertSecButton, mailSentStatus } = this.state;
        return (
            <React.Fragment>
                <Layout header="innerHeader" headerText="Low Balance">
                    <div className="">
                        <Row>
                            <Col lg={12}>
                                <div className="outer-card mt-0">
                                    <Card className="rounded-0 all-winner-content">
                                        <Card.Body>
                                            <div className="float-left">
                                                <h5>Current Balance</h5>
                                            </div>
                                            <div
                                                className="ipl-matches float-right"
                                                style={{ marginBottom: "10px" }}
                                            >
                                                <h5>&#x20B9;{this.state.userWalletBalance}</h5>
                                            </div>
                                            <div className="clearfix"></div>
                                            <div className="float-left">
                                                <h5>Entry</h5>
                                            </div>
                                            <div
                                                className="ipl-matches float-right"
                                                style={{ marginBottom: "10px" }}
                                            >
                                                <h5>&#x20B9;{this.state.entryFee}</h5>
                                            </div>
                                            <div className="clearfix"></div>
                                        </Card.Body>
                                    </Card>
                                    {/* total Rank loop name wise */}
                                    <div className="view-all-winner-div mt-3">
                                        <Col lg={12} className="p-1 ">
                                            <Card>
                                                <Card.Body>
                                                    <Row className="justify-content-center align-items-center">
                                                        <Col md={12} className="add-cash-col">
                                                            <Form.Label>Amount to add</Form.Label>
                                                            <Form.Control
                                                                type="number"
                                                                placeholder="Enter amount"
                                                                value={this.state.amount}
                                                                onChange={this.handleChange}
                                                            />
                                                            <Row className="add-money-label pb-2">
                                                                <Col lg={4} xs={4} md={4} sm={4}>
                                                                    <Button
                                                                        className=""
                                                                        variant="outline-secondary"
                                                                        title="200"
                                                                        onClick={() => this.changeAmount(200)}
                                                                    >
                                                                        &#x20B9;200
                                  </Button>
                                                                </Col>
                                                                <Col lg={4} xs={4} md={4} sm={4}>
                                                                    <Button
                                                                        className=""
                                                                        variant="outline-secondary"
                                                                        title="300"
                                                                        onClick={() => this.changeAmount(300)}
                                                                    >
                                                                        &#x20B9;300
                                  </Button>
                                                                </Col>
                                                                <Col lg={4} xs={4} md={4} sm={4}>
                                                                    <Button
                                                                        className=""
                                                                        variant="outline-secondary"
                                                                        title="500"
                                                                        onClick={() => this.changeAmount(500)}
                                                                    >
                                                                        &#x20B9;500
                                  </Button>
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                        <Col xs={12} className="mobile-bottom-btn add-cash">
                                                            <div className="common-btn w-100">
                                                                <Button
                                                                    className=""
                                                                    variant="success"
                                                                    title="Add Cash"
                                                                    onClick={this.addCash}
                                                                >
                                                                    Add Cash
                                </Button>
                                                                <Button
                                                                    className=""
                                                                    variant="secondary"
                                                                    title="Cancel"
                                                                    onClick={this.cancelTrans}
                                                                >
                                                                    Cancel
                                </Button>
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                </Card.Body>
                                            </Card>
                                        </Col>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>
                    {/* Confirmation modal */}
                    <Dialog
                        className="common-modals"
                        TransitionComponent={Transition}
                        open={this.state.congratsPopup}
                        keepMounted
                        aria-labelledby="alert-dialog-slide-title"
                        aria-describedby="alert-dialog-slide-description"
                    >
                        <DialogContent>
                            {/* <DialogContentText id="alert-dialog-slide-description"> */}
                            <Row className="m-0  justify-content-center align-items-center">
                                <Col xs={12} className="text-center slide-modal-body ">
                                    <h5>Add Cash!</h5>
                                    {/* <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle" fill="#844ff7" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                    <path d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588z" />
                    <circle cx="8" cy="4.5" r="1" />
                  </svg> */}
                                    <p>Your cash added successfully!</p>
                                </Col>
                            </Row>
                            {/* </DialogContentText> */}
                        </DialogContent>
                        <DialogActions>
                            <div className="common-btn">
                                {/* <Link href="/"> */}
                                <Button
                                    variant="success"
                                    title="Ok"
                                    onClick={() => this.setState({ congratsPopup: false })}
                                >
                                    Ok
                </Button>
                                {/* </Link> */}
                            </div>
                        </DialogActions>
                    </Dialog>
                    {/* End Confirmation modal */}
                    {/* Confirmation modal */}
                    {
                        alertPopup ? (
                            <AlertPopup type={alertType} alertPopup={alertPopup} alertMsg={alertMsg} alertButton={alertButton} buttonAction={this.alertAction} alertSecButton={alertSecButton} buttonStatus={mailSentStatus} secButtonAction={() => this.setState({
                                alertPopup: false, alertType: 'sendMail',
                                alertSecButton: '',
                                alertMsg: 'Please verify your email id before adding cash.',
                                mailSentStatus: true,
                                alertButton: 'Verify Email'
                            })} />
                        ) : ''
                    }
                    {/* End Confirmation modal */}
                </Layout>
            </React.Fragment>
        );
    }
}
export default LowBalance;
