import React from 'react';
import Layout from 'components/Layout/Layout';
import { render } from 'react-dom';
import { Container, Row, Col, Card, ListGroup, Badge, Button, Table } from 'react-bootstrap';
const FaqPage = () => {
  return (
    <Layout title="FAQs" header="innerHeader" headerText="FAQs">
      <div className="slikslider-bar">
        <div className="slikslider-banner"></div>
      </div>
      <Col lg={12} className="content-pages p-0">
        <div className="outer-card mt-3">
          <Card>
            <Card.Body>
              <h4>
                1. What is Sports.cards?
              </h4>
              <p>
                Sports.cards is India’s first real time online fantasy sports gaming platform in India. Sports.cards enables the users to earn winnings by using their skills and knowledge of online fantasy sports gaming.
                </p>
              <h4>
                2. How do you sign up?
                </h4>
              <p>
                The users can sign up on Sports.cards by filling a short registration form available on the homepage of Sports.cards website.

              </p>
              <h4>3. Why can’t I log in to my account?</h4>
              <p>
                The users may encounter signup problems due to various reasons. The primary reasons due to which the user might not be able to signup can include the user might be entering incorrect phone number. If the user hasn't received the login/register OTP on his mobile number then he can click “Resend OTP”

              </p>
              <h4>4. Can I have multiple accounts on Sports.cards?</h4>
              <p>
                No, Every customer can create only one account
             </p>
              <h4>
                5. Can I update or edit my information?
             </h4>
              <p>
                The users can easily update/edit their information by logging into their Sports.cards profile page. After logging into their MyTeam11 account the users can edit/update their information by clicking on ‘Others’ on the bottom banner. The users will be able to edit their basic information including date of birth, profile name and address but the users won’t be allowed to change their email address, mobile number or team name once registered with Sports.cards.
             </p>
              <h4>
                6. I’m not receiving the login OTP
             </h4>
              <h4>7. I did not get any confirmation email after I signed up</h4>
              <h4>8. I don’t know much about cricket - can I still play?</h4>
              <p>
                The online fantasy gaming services offered by Sports.cards are for everyone including those who don’t know much about cricket. So, yes, you can play even if you don’t know much about cricket. In addition, the “How to Play” section of Sports.cards website can assist you in playing the game by briefing you about the playing tactics.

              </p>
              <h4>9 . How do I transfer the prize money into my bank account?</h4>
              <p>
                You will be able to withdraw your winning after the tournament ends</p>
              <h4>
                10. Why doesn’t my prize money reflect in my account?
             </h4>
              <p>
                Winners will be announced within 12 hours after the match has concluded. The prize money reflects immediately after the match concludes
           </p>
              <h4>
                11. What if there’s a tie between contestants?
           </h4>
              <p>
                The prize money will be equally distributed between tied contestants. For example, if there’s a tie between two players in a contest of prize money ₹10,000, the tied players will get ₹5,000 each.

           </p>
              <h4>12. Can I play if I am under the age of 18?
</h4>
              <p>
                No, you have to be 18 or above to play Sports.card

</p>
              <h4>

                13.  Why was my money refunded?

</h4>
              <p>
                We refund your money within 24 hours when a contest is cancelled. This happens only if the match is called off or the contest does not gather the required number of contestants. A match can be called off without a ball bowled or sometimes even after the start of play due to rain, dangerous pitch, among other reasons.

</p>
              <h4>
                14.  How will I know if I have won a prize money?

</h4>
              <p>
                Depending on your points and ranking of a contest, your winnings will be credited automatically to your Sports.cards account.

</p>
              <h4>
                15. When do I get the result of a contest?

</h4>

              <p>
                Based on the scorecard, our team will verify and update the points and rankings within a 12 hours after the match. Please note that this process may take long to ensure accuracy.

        </p>
              <h4>
                16. What if I do not withdraw my prize money?

        </h4>
              <p>
                Your prize money remains safe with us.

        </p>
              <h4>
                17.  Can I join a contest once the match starts?

        </h4>
              <p>
                Yes, you can join the contest as long as the match exist.

        </p>
              <h4>
                18. Can I leave a joined contest?

        </h4>
              <p>
                No, you cannot leave a contest after joining.

        </p>
            </Card.Body>
          </Card>

        </div>
      </Col>
    </Layout >
  )
}
export default FaqPage;