import React, { Component } from "react";
import Cookies from "js-cookie";
import Link from 'next/link';
import { Container, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import { Snackbar, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import Layout from 'components/Layout/Layout';
import Form from 'react-bootstrap/Form'
class ManagePayments extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  render() {
    return (
      <React.Fragment>
        <Layout header="innerHeader" headerText="Manage Payments">
          <div className="" >
            <Row >
              <Col lg={12}>
                <div className="outer-card mt-3">
                  <Card>
                    <Card.Body className="addcards">
                      <Row className="justify-content-center align-items-center">
                        <Col lg={1} xs={1} xs={2} sm={1} md={1}>
                          <img src="static/images/payment.png" />
                        </Col>
                        <Col lg={11} xs={10} sm={11} md={11} >
                          <Link href="/">
                            <a title="Add / Remove Cards">
                              Add / Remove Cards
                              <span> For seamless, convenient payments</span>
                            </a>
                          </Link>
                        </Col>
                      </Row>
                    </Card.Body>
                  </Card>
                </div>
                <div className="outer-card mt-3">
                  <Card>
                    <Card.Body className="addcards">
                      <Row className="justify-content-center align-items-center">
                        <Col lg={1} xs={2} sm={1} md={1}>
                          <img src="static/images/paytm.png" />
                        </Col>
                        <Col lg={4} xs={5} sm={4} md={4} >
                          <Link href="/">
                            <a title="Paytm">
                              Paytm
                            </a>
                          </Link>
                        </Col>
                        <Col lg={7} xs={5} sm={7} md={7} className="text-right link-account">
                          <Link href="/">
                            <a title="Link Account">
                              Link Account
                            </a>
                          </Link>
                        </Col>
                      </Row>
                    </Card.Body>
                  </Card>
                </div>
                <div className="outer-card mt-3">
                  <Card>
                    <Card.Body className="addcards">
                      <Row className=" justify-content-center align-items-center">
                        <Col lg={1} xs={2} sm={1} md={1} >
                          <img src="static/images/pay.png" />
                        </Col>
                        <Col lg={4} xs={5} sm={4} md={4}>
                          <Link href="/">
                            <a title="AmazonPay">
                              AmazonPay
                            </a>
                          </Link>
                        </Col>
                        <Col lg={7} xs={5} sm={7} md={7} className="text-right link-account">
                          <Link href="/">
                            <a title="Link Account">
                              Link Account
                            </a>
                          </Link>
                        </Col>
                      </Row>
                    </Card.Body>
                  </Card>
                </div>
              </Col>
            </Row>
          </div>
        </Layout>
      </React.Fragment >
    )
  }
}
export default ManagePayments;