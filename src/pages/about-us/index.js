import React from 'react';
import Layout from 'components/Layout/Layout';
import { render } from 'react-dom';
import { Container, Row, Col, Card, ListGroup, Badge, Button, Table } from 'react-bootstrap';
const AboutUs = () => {
  return (
    <Layout title="About Us" header="innerHeader" headerText="About Us">
      <div className="slikslider-bar">
        <div className="slikslider-banner"></div>
      </div>
      <Col lg={12} className="content-pages p-0">
        <div className="outer-card mt-3">
          <Card>
            <Card.Body>
              <h4>
                Creating Your Team
              </h4>
              <p>
                Every cricket team you build on  has to have 11 players, of which a maximum of 7 players can be from any one team playing the real-life match.
                </p>
              <p>
                <b> Player Combinations</b> <br />
                Your  can have different combinations of players, but has to be within the 100 credit cap and must qualify the following team selection criteria:
              </p>
              <h4>What is Lorem Ipsum?</h4>
              <ul>
                <li>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </li>
                <li>
                  of Letraset sheets containing Lorem
                </li>
              </ul>
              <h5>sometimes by accident, s</h5>
              <ol>
                <li> sometimes by accident, sometimes on purpose (injected humour and the like).</li>
                <li>sometimes on purpose (injected humour and the like).</li>
                <li> sometimes by accident, sometimes on purpose (injected humour and the like).  sometimes by accident, sometimes on purpose (injected humour and the like).</li>
              </ol>
              <div className="table-responsive">
                <table className="table table-striped">
                  <tbody><tr>
                    <th align="left">Player Type</th>
                    <th>Min</th>
                    <th>Max</th>
                  </tr>
                    <tr>
                      <td>Wicket Keeper - WK</td>
                      <td>1</td>
                      <td>4</td>
                    </tr>
                    <tr>
                      <td>Batsman - BAT</td>
                      <td>3</td>
                      <td>6</td>
                    </tr>
                    <tr>
                      <td>All Rounder - AR</td>
                      <td>1</td>
                      <td>4</td>
                    </tr>
                    <tr>
                      <td>Bowler - BWL</td>
                      <td>3</td>
                      <td>6</td>
                    </tr>
                  </tbody></table>
              </div>
            </Card.Body>
          </Card>

        </div>
      </Col>
    </Layout >
  )
}
export default AboutUs;