import React, { Component } from 'react';
import Cookies from 'js-cookie';
import Box from '@material-ui/core/Box';
import { Container, Row, Col, Card, Badge, Button, Table } from 'react-bootstrap';
import TextField from '@material-ui/core/TextField';
import Layout from 'components/Layout/Layout';
import Dialog from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import Link from 'next/link';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Slide from '@material-ui/core/Slide';
import DialogTitle from '@material-ui/core/DialogTitle';
import { decrypt, encrypt } from '_helper/EncrDecrypt';
import Router, { useRouter } from 'next/router';
import { ContestsService } from '_services/ContestsService';
import { useTheme } from '@material-ui/core';
import { PacksService } from '_services/PacksService';
import { AuthenticationService } from '_services/AuthenticationService';
import { TransactionService } from '_services/TransactionService';
import { CommonService } from '_services/CommonService';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
class PreviewPack extends Component {

  constructor(props) {
    super(props);
    this.state = {
      packData: [],
      matchData: {},
      totalSelection: 0,
      totalSelectedPotential: 0,
      totalSelectedCredit: 0,
    };
  }

  componentDidMount() {

    this.getMatchDetails();
    this.getPackDetails();
  }

  getMatchDetails() {
    var params = {
      match_id: decrypt(Router.query.m)
    }
    ContestsService.getMatchDetails(params, res => {
      if (res.status) {
        this.setState({
          matchData: res['data']
        });
      } else {
        this.setState({
          matchData: {}
        });
      }
    });
  }

  getPackDetails() {
    var params = {
      pack_id: decrypt(Router.query.p)
    }
    PacksService.getPackDetails(params, res => {
      if (res.status) {
        this.setState({
          packData: res['data']
        });
      } else {
        this.setState({
          packData: {}
        });
      }
    });
  }

  // back = () => {
  //   Router.back();
  // }

  render() {
    const { matchData } = this.state;
    const { router } = this.props;
    return (
      <Layout customClass="homepage-class" header="packHeader" headerText="Card Preview" params={router.query}>
        <div className="create-packs-tabs">

          <Row className="fantasy-pack match-pack m-0">

            <Col lg={3} xs={3} md={3} sm={3} className="mr-25">
            </Col>
            <Col lg={2} xs={2} md={2} sm={2} className="text-center p-0 mrm--25">
              <div className="my-team-name mi-team" style={{ background: matchData.hometeamcolorcode }}>
                {matchData.hometeamname}
              </div>
              <br />
              <div className="totalscore">
                {matchData.home_team_runs != null ? matchData.home_team_runs : '--'}/{matchData.home_team_wickets != null ? matchData.home_team_wickets : '--'} ({matchData.home_team_overs != null ? matchData.home_team_overs : '--'})
                      </div>
            </Col>
            <Col lg={2} xs={2} md={2} sm={2} className="text-center p-0">
              <img src="static/images/whitevs.png" className="team-vs" />
            </Col>
            <Col lg={2} xs={2} md={2} sm={2} className="text-center p-0 mlm-25">
              <div className="my-team-name iplteam" style={{ background: matchData.awayteamcolorcode }}>
                {matchData.awayteamname}
              </div>
              <br />
              <div className="totalscore">
                {matchData.away_team_runs != null ? matchData.away_team_runs : '--'}/{matchData.away_team_wickets != null ? matchData.away_team_wickets : '--'} ({matchData.away_team_overs != null ? matchData.away_team_overs : '--'})
                      </div>
            </Col>
            <Col lg={3} xs={3} md={3} sm={3} className="text-right ml-25">
            </Col>
          </Row>
          {/* Fill stepper */}
          <React.Fragment>
            <div className="create-my-pack header-row-my-pack  m-0">
              <Row className="m-0 header-selection justify-content-center align-items-center">
                <Col lg={3} xs={3} md={3} sm={3} className="text-left p-0">
                  <span> Selections <b>{this.state.packData['selection_total']}</b> </span>
                </Col>
                <Col lg={3} xs={3} md={3} sm={3} className="text-center p-0">
                  <span> Potential Point <b className="pointpotential">{this.state.packData['total_potential_payout'] ? parseFloat(this.state.packData['total_potential_payout']) : 0}</b></span>
                </Col>
                <Col lg={3} xs={3} md={3} sm={3} className="text-center p-0">
                  <span> Points Earned <b className="pointpotential">{this.state.packData['final_potential_payout'] ? parseFloat(this.state.packData['final_potential_payout']) : 0}</b></span>
                </Col>
                <Col lg={3} xs={3} md={3} sm={3} className="text-right p-0">
                  <span> Credit <b>{this.state.packData['totalCredits']}</b></span>
                  {/* <span> Potential Point <b>{this.state.packData['total_potential_payout'] ? parseFloat(this.state.packData['total_potential_payout']) : 0}</b></span> */}
                </Col>
              </Row>
              <Row className="m-0 header-row justify-content-center align-items-center">
                <Col lg={6} xs={5} md={3} sm={3} className="pl-2">
                  <span className="match-win">
                    {/* <small className="mr-2">
                      <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-pencil-fill" fill="#646b78" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z" />
                      </svg>
                    </small> */}
                    {this.state.packData['pack_name']}
                  </span>
                </Col>
                <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                  <span> Multiplier</span>
                </Col>
                <Col lg={2} xs={3} md={3} sm={3} className="text-center">
                  <span>Credit</span>
                </Col>
                <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                  <span className="potential-point"> Potential Point</span>
                </Col>
              </Row>
              {/* End Match Win row */}


              {this.state.packData['list'] && this.state.packData['list'].length > 0 ? this.state.packData['list'].map((item, key) => (
                <Row key={key} className="m-0 fansty-row match-win-row justify-content-center align-items-center" style={{ background: item.won_status == 1 ? '#b0ffd6' : '' }}>
                  <Col lg={6} xs={5} md={3} sm={3} className="pl2">
                    <span className="serial-circle">{key + 1}</span>
                    <label className="team-label">
                      {item.market_name ? item.market_name : ''} @ {item.selection_name ? item.selection_name : ''}
                    </label>
                  </Col>
                  <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                    <div className="multiplier-value">{item.multipliers}</div>
                  </Col>
                  <Col lg={2} xs={3} md={3} sm={3} className="text-center pr-1 pl-1">
                    <div className="credit-value ">
                      <div className="blank-input">
                        {item.credits}
                      </div>
                    </div>
                  </Col>
                  <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                    <div className="potential-value">{parseFloat(item.multipliers * item.credits).toFixed(2)}</div>
                  </Col>
                </Row>
              )) : ''}

              {this.state.packData['selection_total'] ? [...Array(11 - this.state.packData['selection_total'])].map((item, key) => {
                return (
                  <Row key={key} className="m-0 fansty-row match-win-row justify-content-center align-items-center">
                    <Col lg={6} xs={5} md={3} sm={3} className="pl2">
                      <span className="serial-circle">{this.state.packData['selection_total'] + key + 1}</span>
                      <label className="team-label">
                      </label>
                    </Col>
                    <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                      <div className="multiplier-value"></div>
                    </Col>
                    <Col lg={2} xs={3} md={3} sm={3} className="text-center pr-1 pl-1">
                      <div className="credit-value ">
                        <div className="blank-input">
                        </div>
                      </div>
                    </Col>
                    <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                      <div className="potential-value"></div>
                    </Col>
                  </Row>
                )
              }) : ''}

              <div className="pb-3"></div>
              {/* End Man of the match */}
            </div>
            {/* <Col xs={12} className="mobile-bottom-btn p-0">
              <div className="common-btn">
                <Button variant="success" onClick={this.back} title="Back">Back</Button>
              </div>
            </Col> */}
          </React.Fragment>
        </div>
      </Layout >
    );
  }
}

const defaultPage = () => {
  const router = useRouter();
  const theme = useTheme();
  return (
    <PreviewPack router={router} theme={theme} />
  )
}

export default defaultPage;
