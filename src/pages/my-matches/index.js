import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
// import TabPanel from '@material-ui/lab/TabPanel';
import Box from '@material-ui/core/Box';
import NoData from "components/Contests/NoData";
import Live from "components/Matches/Live";
import Upcoming from 'components/Matches/Upcoming'
import Completed from 'components/Matches/Completed'
import Layout from 'components/Layout/Layout';
import { HomeService } from '_services/HomeService';
const styles = {
  tabPanelRoot: 'tabPanelRoot',
  tabPanelText: 'tabPanelText',
  tabContainer: 'tabContainer'
}


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

class MyMatches extends Component {

  constructor(props) {
    super(props);
    this.handleChangeIndex = this.handleChangeIndex.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      value: 0,
      liveMatches: [],
      upcomingMatches: [],
      completedMatches: []
    };
  }

  componentDidMount() {

  }


  handleChange = (event, value) => {
    this.setState({ value });
  };

  a11yProps(index) {
    return {
      id: `full-width-tab-${index}`,
      'aria-controls': `full-width-tabpanel-${index}`,
    };
  }


  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  render() {
    const { value } = this.state;
    const { theme } = this.props;
    return (
      <Layout header="tabHeader" headerText="My Matches" customClass="mymatches-class">
        <div className="contests-tabs">
          <AppBar position="static">
            <Tabs value={value} variant="fullWidth" onChange={this.handleChange}>
              <Tab className={styles.tabPanelText} label="Upcoming" />
              <Tab className={styles.tabPanelText} label="Live" />
              <Tab className={styles.tabPanelText} label="Completed" />
            </Tabs>
          </AppBar>
          <SwipeableViews animateHeight={false} className={styles.tabContainer} axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'} index={this.state.value} onChangeIndex={this.handleChangeIndex}>
            <TabPanel value={this.state.value} index={0} dir={theme.direction}>
              <Upcoming />
            </TabPanel>
            <TabPanel value={this.state.value} index={1} dir={theme.direction}>
              <Live />
            </TabPanel>
            <TabPanel value={this.state.value} index={2} dir={theme.direction}>
              <Completed />
            </TabPanel>
          </SwipeableViews>
        </div>

      </Layout>
    );
  }
}

export default withStyles(styles, { withTheme: true })(MyMatches);