import React, { Component } from "react";
import Cookies from "js-cookie";
import Link from 'next/link';
import { Container, Row, Col, Card, Badge, Button, Table } from 'react-bootstrap';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import Layout from 'components/Layout/Layout';
import Overlay from 'react-bootstrap/Overlay';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip'
import ShareIcon from '@material-ui/icons/Share';
import { AuthenticationService } from '_services/AuthenticationService';
import { CommonService } from "_services/CommonService";
import { copyToClipboard } from 'js-copy-to-clipboard';
import MuiAlert from '@material-ui/lab/Alert';
import { Snackbar } from '@material-ui/core';
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


class InviteFriends extends Component {
  constructor(props) {
    super(props);
    this.state = {
      referralCode: '',
      myRefAmount: 0,
      email: '',
      friendRefAmount: 0,
      alert: {
        toast: false,
        toastMessage: '',
        severity: ''
      },
      errorMessage: {
        email: ''
      }
    }
  }

  componentDidMount() {
    this.getCurrentUser();
    this.getConfigration();
  }

  getConfigration() {
    CommonService.getConfiguration(res => {
      if (res.status) {
        this.setState({
          myRefAmount: res['data']['referral_code'],
          friendRefAmount: res['data']['referral_code']
        });
      }
    });
  }


  getCurrentUser() {
    AuthenticationService.currentUser.subscribe(user => {
      // console.log(user);
      if (user && user != null) {
        this.setState({
          referralCode: user['referralCode']
        });

      }
    });
  }



  shareReferral = (type) => {
    if (type == 'email') {
      this.setState({
        open: true,
        error: true,
        errorMessage: {
          email: ''
        }
      });
    } else {
      if (type == "fb") {
        var ref = "Facebook";
        var url =
          "https://www.facebook.com/sharer/sharer.php?u=" +
          location.protocol +
          "//dev.sports.cards" +
          "?refId=" +
          this.state.referralCode +
          '&display=popup&ref=plugin&src=like&kid_directed_site=0&description=Come join me in the fantasy world of our favourite game - CRICKET, where you can bet and vote for you favourite team, play online quizzes, and earn lots of coins. Let’s multiply the fun by playing together!';
      }
      if (type == "tw") {
        var ref = "Twitter";
        var url =
          "https://twitter.com/intent/tweet?original_referer=" +
          location.protocol +
          "//" +
          location.hostname +
          "/register?refId=" +
          this.state.referralCode +
          '&text=Come join me in the fantasy world of our favourite game - CRICKET, where you can bet and vote for you favourite team, play online quizzes, and earn lots of coins. Let’s multiply the fun by playing together! &url=' +
          location.protocol +
          "//" +
          location.hostname +
          "/register?refId=" +
          this.state.referralCode;
      }
      if (type == 'wa') {
        var ref = "WhatsApp";
        var url =
          "https://api.whatsapp.com/send?text= Come join me in the fantasy world of our favourite game - CRICKET, where you can bet and vote for you favourite team, play online quizzes, and earn lots of coins. Let’s multiply the fun by playing together! " +
          location.protocol +
          "//" +
          location.hostname +
          "/register?refId=" + this.state.referralCode;
      }
      window.open(url, ref, "width=700,height=500");
    }

  }

  shareEmail = (e) => {
    e.preventDefault();
    var msgEmail = '';

    var isValid = true;
    if (this.state.email) {
      if (!isEmailValid(this.state.email)) {
        msgEmail = (ErrorMessages.VALIDATION_ERROR_EMAIL);
        isValid = false;
      }
    } else {
      msgEmail = 'Email id is required'
      isValid = false;
    }

    if (isValid) {
      var params = {
        email: this.state.email,
        url: location.protocol + "//" + location.hostname + "?refId=" + this.state.referralCode
      }
      CommonService.shareEmailReferral(params, res => {
        if (res.status) {
          this.setState({
            open: false,
            email: '',
            alert: {
              toast: true,
              toastMessage: res.message,
              severity: 'success'
            },
          });
        } else {
          this.setState({
            alert: {
              toast: true,
              toastMessage: res.message,
              severity: 'error'
            },
          });
        }
      });
    } else {
      this.setState({
        error: true,
        errorMessage: {
          email: msgEmail
        }
      });
    }
  }

  handleChange = (event) => {
    this.setState({
      email: event.target.value
    });
  }

  handleClose = () => {
    this.setState({
      open: false
    });
  };

  copyReferralCode = (code) => {
    copyToClipboard(code);
    this.setState({
      alert: {
        toast: true,
        toastMessage: 'Copied to clipboard!',
        severity: 'success'
      },
    });
  }


  render() {
    const { alert } = this.state;
    return (
      <React.Fragment  >
        <Snackbar open={alert.toast} autoHideDuration={2000} onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })}>
          <Alert onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })} severity={alert.severity}>
            {alert.toastMessage}
          </Alert>
        </Snackbar>
        <Layout customClass="dynamic-class" header="innerHeader" headerText="Invite Friends" >
          <div className="invite-card" >
            <Row >
              <div className="invite-friend-top"></div>
              <Col lg={12} className="invite-friends">
                <div className="outer-card mt-0">
                  <div className=" mt-3">
                    <Col lg={12} className="p-1 ">
                      <Card>
                        <Card.Body>
                          <Row className="justify-content-center align-items-center add-cash-page">
                            <Col xs={12} className="mobile-bottom-btn add-cash text-center">
                              <span>
                                Earn up to &#x20B9;{this.state.myRefAmount} Cash Bonus pre Friend
                              </span>
                              <p>
                                Give your friends &#x20B9;{this.state.friendRefAmount} Cash Bonus when they join <br />
                                Sports.Cards.
                              </p>
                              <OverlayTrigger
                                placement="left"
                                delay={{ show: 100, hide: 400 }}
                                overlay={<Tooltip id="button-tooltip">How it works</Tooltip>}
                              >
                                <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle-fill" fill="#646b78" xmlns="http://www.w3.org/2000/svg">
                                  <path fillRule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                </svg>
                              </OverlayTrigger>
                              <b>How it works</b>
                            </Col>
                          </Row>
                        </Card.Body>
                      </Card>


                      {this.state.referralCode == "" ? (
                        ''
                      ) : (
                          <div>
                            <Card className="invite-header-card text-center">
                              <Card.Header>
                                <div className="copycode-clipboard">
                                  <span>{this.state.referralCode}</span>
                                  <Button title="Copy Code" className="copycode filter-btn" onClick={() => this.copyReferralCode(this.state.referralCode)}>Copy</Button>
                                </div>
                              </Card.Header>


                              <Card.Body className="invite-body ">
                                <Card.Title>Or share it via</Card.Title>
                                {/* <Button variant="success" className="phonecontact">Invite Phone Contacts</Button> */}
                                <Button variant="outline-secondary" onClick={() => this.shareReferral('fb')}>
                                  <FacebookIcon></FacebookIcon>Share on Facebook
                                </Button>
                                <Button variant="outline-secondary" onClick={() => this.shareReferral('tw')}>
                                  <TwitterIcon></TwitterIcon>Share on Twitter
                                </Button>
                                <Button variant="outline-secondary" onClick={() => this.shareReferral('wa')}>
                                  <WhatsAppIcon></WhatsAppIcon>Share on Whatsapp
                                </Button>
                                {/* More Options*/}

                                {/* <Button variant="outline-secondary">
                                  <ShareIcon />More Options</Button> */}
                              </Card.Body>
                            </Card>

                          </div>
                        )}

                    </Col>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </Layout>

      </React.Fragment >
    )
  }
}
export default InviteFriends;