import React, { useState } from "react";
import Cookies from "js-cookie";
import Layout from "components/Layout/Layout";
import Home from "components/Home/Home";
import { isMobile } from 'react-device-detect';
import { useRouter } from 'next/router';
import { Container, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import { render } from "react-dom";
import { AuthenticationService } from "_services/AuthenticationService";

import UpdateHomePage from "components/Home/UpdateHomePage";


class Main extends React.Component {

  constructor(props) {
    super();
    this.state = {
      // isLoggedIn: false
    }
  }

  componentDidMount() {
    AuthenticationService.clearCookies();
    Cookies.remove('_mobData');
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker
        .register('/serviceWorker.js')
        .then(registration => {
          console.log('service worker registration successful')
        })
        .catch(err => {
          console.warn('service worker registration failed', err.message)
        })
    }
  }

  // changeLogged = (val) => {
  //   this.setState({
  //     isLoggedIn: val
  //   });
  // }

  render() {
    const { router } = this.props;
    return (
      <React.Fragment>

        {
          AuthenticationService.isLogged ? (
            <Layout customClass="homepage-class" header="homeHeader" params={router.query}>
              <Home />
            </Layout>
          ) : (
              <Layout customClass="updated-home-page" params={router.query}>
                < UpdateHomePage />
              </Layout>
            )
        }

      </React.Fragment>
    );
  }
}
const defaultPage = () => {

  const router = useRouter();

  return (
    <Main router={router} />
  );
};

export default defaultPage;