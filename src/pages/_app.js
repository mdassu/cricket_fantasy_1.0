import App from 'next/app';
import { Container } from 'react-bootstrap';
import Head from 'next/head';
import React from 'react';
import moment from 'moment';
import Cookies from 'js-cookie';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Dialog, DialogContent, DialogActions, Button } from '@material-ui/core';
import { encrypt, decrypt } from '_helper/EncrDecrypt';
import { CommonService } from '_services/CommonService';
import responsiveStyles from '../../styles/responsive';
import baseStyles from '../../styles/base';
import fontTheme from '../../styles/font';
import Router from 'next/router';
import { AuthenticationService } from '_services/AuthenticationService';
import firebase from '../sc-firebase';
import ReactGA from 'react-ga';

export default class MyApp extends App {
  constructor(props) {
    super(props);
    this.state = {
      // isLoggedIn: false,
      continue: false,
      prevRoute: '',
      open: false,
      coins: ''
    }
  }
  componentDidMount() {
    this.checkRoute();
    // const msg = firebase.messaging();
    // msg.requestPermission().then(() => {
    //   return msg.getToken();
    // }).then((data) => {
    //   // console.log(data);
    // });
    this.initGA();
  }

  initGA = () => {
    ReactGA.initialize('UA-183895735-1'); // put your tracking id here
  }

  componentDidUpdate(prevProps) {
    if (this.state.prevRoute != Router.pathname) {
      this.checkRoute();
    }
  }

  checkRoute = () => {
    var prevRoute = Router.pathname;
    if (Router.pathname != '/' && Router.pathname != '/login' && Router.pathname != '/register' && Router.pathname != '/verify-otp' && Router.pathname != '/how-to-play' && Router.pathname.search('/email-verification') == -1) {
      AuthenticationService.currentUser.subscribe(user => {
        if (user && user != null) {
          this.setState({
            // isLoggedIn: true,
            continue: true,
            prevRoute: prevRoute
          });
          if (Router.pathname == '/login' || Router.pathname == '/register' || Router.pathname == '/verify-otp' || Router.pathname == '/how-to-play') {
            Router.push('/').then(() => {
              this.setState({
                continue: true,
                prevRoute: prevRoute
              });
            });
          }
        } else {
          Router.push('/').then(() => {
            this.setState({
              continue: true,
              prevRoute: prevRoute
            });
          });
        }
      });
    } else {
      if (AuthenticationService.isLogged && (Router.pathname == '/login' || Router.pathname == '/register' || Router.pathname == '/verify-otp' || Router.pathname == '/how-to-play')) {
        Router.push('/').then(() => {
          this.setState({
            continue: true,
            prevRoute: prevRoute
          });
        });
      }
      this.setState({
        continue: true,
        prevRoute: prevRoute
      });
    }
  }

  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return { pageProps }
  }

  render() {
    const { Component, pageProps } = this.props

    return (
      <React.Fragment>
        <style jsx global>
          {fontTheme}
        </style>
        <style jsx global>
          {baseStyles}
        </style>
        <style jsx global>
          {responsiveStyles}
        </style>
        <Head>
          <title>Sports.Cards</title>
          {/* Global site tag (gtag.js) - Google Analytics */}
          <link rel="manifest" href="manifest.json" />
          {/* <script async src="https://www.googletagmanager.com/gtag/js?id=G-CJ5C4QT41P"></script>
          <script
            dangerouslySetInnerHTML={{
              __html: `
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
              
                gtag('config', 'G-CJ5C4QT41P');
              `
            }}
          /> */}
        </Head>

        {this.state.continue ? (<Component {...pageProps} />) : ''}
      </React.Fragment>
    )
  }
}
