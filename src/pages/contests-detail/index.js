import React, { Component } from 'react';
import Cookies from 'js-cookie';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { useTheme, withStyles } from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
// import TabPanel from '@material-ui/lab/TabPanel';
import Box from '@material-ui/core/Box';
import LeaderBoard from "components/Contests/LeaderBoard";
import { Container, ProgressBar, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import TextField from '@material-ui/core/TextField';
import Layout from 'components/Layout/Layout';
import Link from 'next/link';
import Table from 'react-bootstrap/Table'
import Router, { useRouter } from 'next/router';
import { AuthenticationService } from '_services/AuthenticationService';
import { ContestsService } from '_services/ContestsService'
import { decrypt, encrypt } from '_helper/EncrDecrypt';
import millify from 'millify';
import AlertPopup from 'components/CommonComponents/AlertPopup';


const styles = {
  tabPanelRoot: 'tabPanelRoot',
  tabPanelText: 'tabPanelText',
  tabContainer: 'tabContainer'
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

class ContestsDetails extends Component {

  constructor(props) {
    super(props);
    this.handleChangeIndex = this.handleChangeIndex.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      value: 0,
      card: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      contestDetails: [],
      prizeDetails: [],
      alertPopup: false
    };
  }

  componentDidMount() {
    this.getContestDetails()
  }

  getContestDetails = () => {
    var params = {
      contest_id: decrypt(Router.query.c)
    }
    ContestsService.getContestDetails(params, (res) => {
      if (res.status) {
        this.setState({
          contestDetails: res['data']['contestDetails'],
          prizeDetails: res['data']['leaderboardData']
        })
      } else {

      }
    })
  }

  enterContest = (entryFee, contestId, purchasedPackCount, remainingPacks) => {
    AuthenticationService.clearCookies();
    if (remainingPacks == 0) {
      this.setState({
        alertPopup: true
      });
    } else {
      var contestData = {
        entryFee: entryFee,
        contestId: contestId
      }
      Cookies.set('_ctData', encrypt(contestData));
      Cookies.set('_match', Router.query.m);
      Router.push({
        pathname: '/create-pack',
        query: {
          m: Router.query.m
        }
      });
    }
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };
  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  render() {
    const { value, contestDetails, prizeDetails, alertPopup } = this.state;
    const { theme, router } = this.props;
    return (
      <Layout header="contestHeader" params={router.query.m}>
        <div className="contests-tabs">
          <Card className="rounded-0 mb-0">
            <Card.Body>
              <div className="contents-listing">
                <Row className="m-0">
                  <Col lg={6} xs={6} md={6} sm={6} className="p-0">
                    <span className="prizepool">Prize Pool</span>
                    {/* <h4 className="current-prize">Curr &#x20B9; {contestDetails['current_contest_value']}</h4> */}
                    <h4 className="current-prize">&#x20B9; {contestDetails['max_prize']}</h4>
                    {/* <h6 className="max-prize">Max &#x20B9; {contestDetails['max_prize']}</h6> */}
                  </Col>
                  <Col lg={6} xs={6} md={6} sm={6} className="p-0 text-right">
                    <span className="prizepool">Entry Fee - &#x20B9; {contestDetails['entry_amount']}</span>
                    {/* <Link href={{pathname:'/create-pack', query: {m:}}}> */}
                    <a className="prize-pool-btn" onClick={() => this.enterContest(contestDetails.entry_amount, contestDetails.match_contest_id, contestDetails.purchased_pack_count, contestDetails.remaining_packs)} style={{ cursor: 'pointer' }}>Enter Contest</a>
                    {/* </Link> */}
                  </Col>
                  <Col lg={12} className="progressbar p-0">
                    <ProgressBar now={(contestDetails.packs - contestDetails.remaining_packs) * 100 / contestDetails.packs} />
                    <ul className="float-left ">
                      <li className="left-point">{contestDetails['remaining_packs']} cards left </li>
                    </ul>
                    <ul className=" float-right  ">
                      <li className="totalpacks">{contestDetails['packs']} Cards</li>
                    </ul>
                    <div className="clearfix"></div>
                  </Col>
                </Row>
              </div>
            </Card.Body>
            <Card.Header className="cardfooter">
              <ul className="float-left cardfooter-inner">
                <li>
                  <img src="static/images/trophy.svg" className="winner-trophy" /> 1st Prize &#x20B9;{contestDetails.top_price > 0 ? millify(contestDetails.top_price) : 0}
                </li>
              </ul>
              <ul className=" float-right cardfooter-inner ">
                <li className=" mr-0"><img src="static/images/star.svg" className="winner-trophy" /> {contestDetails['pack_win_percentage']}% cards win</li>
              </ul>
              <div className="clearfix"></div>
            </Card.Header>
          </Card>
          <AppBar position="static" color="default">
            <Tabs
              value={value}
              onChange={this.handleChange}
              indicatorColor="primary"
              textColor="primary"
              variant="fullWidth"
              aria-label="full width tabs example"
            >
              <Tab className={styles.tabPanelText} label="Contests Details" />
              <Tab className={styles.tabPanelText} label="Leaderboard" />
            </Tabs>
          </AppBar>
          <SwipeableViews animateHeight={false} className={styles.tabContainer} axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'} index={value} onChangeIndex={this.handleChangeIndex}>
            <TabPanel value={value} index={0} dir={theme.direction}>
              <div className="contests-tabs">
                <Table bordered hover>
                  <thead>
                    <tr>
                      <th>Rank</th>
                      <th className="text-right">Prize</th>
                    </tr>
                  </thead>
                  <tbody>
                    {prizeDetails.map(item => (
                      <tr >
                        <td className="text-left">#{item.ranks_From == item.ranks_To ? item.ranks_To : item.ranks_From + '-' + item.ranks_To}</td>
                        <td className="text-right">&#x20B9; {item.prize_Money_per_person}</td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
            </TabPanel>
            <TabPanel value={value} index={1} dir={theme.direction}>
              {/* <NoData /> */}
              <LeaderBoard matchStatus={contestDetails.match_status} />
            </TabPanel>
          </SwipeableViews>
        </div>
        {
          alertPopup ? (
            <AlertPopup type='' alertPopup={alertPopup} alertMsg='Contest full house!' alertButton='Ok' buttonAction={() => this.setState({ alertPopup: false })} />
          ) : ''
        }
      </Layout>
    );
  }
}

const defaultPage = () => {
  const router = useRouter();
  const theme = useTheme();
  return (
    <ContestsDetails theme={theme} router={router} />
  )
}
export default defaultPage;

// export default withStyles(styles, { withTheme: true })(ContestsDetails);
