import React, { Component } from "react";
import Cookies from 'js-cookie';
import { Container, Row, Col, Card } from 'react-bootstrap';
import TextField from '@material-ui/core/TextField';
import { Button, Snackbar } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import { AuthenticationService } from "_services/AuthenticationService";
import { CommonService } from "_services/CommonService";
import Layout from "components/Layout/Layout";
import { TransactionService } from "_services/TransactionService";
import { ErrorMessages, isNameValid, isMobileNumberValid, isEmailValid } from 'components/MyValidations/MyValidations';
import { encrypt } from "_helper/EncrDecrypt";
import Router from "next/router";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class Withdrawal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      formData: {
        accountNumber: '',
        ifscCode: '',
        name: '',
        email: '',
        phone: '',
        amount: ''
      },
      errorMessage: {
        accountNumber: '',
        ifscCode: '',
        name: '',
        email: '',
        phone: '',
        amount: ''
      },
      alert: {
        toast: false,
        toastMessage: '',
        severity: 'info'
      },
      isLoggedIn: false,
      walletBalance: '',
      isLoaded: true,
      minLimit: 1
    }
  }

  componentDidMount() {
    AuthenticationService.currentUser.subscribe(user => {
      if (user && user != null) {
        this.setState({
          isLoggedIn: true,
          walletBalance: user['winningBalance']
        })
      }
    });

    CommonService.getConfiguration(res => {
      if (res.status) {
        this.setState({
          minLimit: res['data']['withdrawal_limit']
        })
      }
    });
  }

  handleChange = (event) => {
    if (event.target.name == 'amount' && event.target.value) {
      if (isNaN(event.target.value)) {
        event.target.value = '';
      }
      this.setState({
        formData: {
          ...this.state.formData,
          [event.target.name]: event.target.value
        }
      });
    } else {
      this.setState({
        formData: {
          ...this.state.formData,
          [event.target.name]: event.target.value
        }
      });
    }

  }

  sumbmitDetails = (e) => {
    e.preventDefault();
    const { formData, minLimit } = this.state;
    var isValid = true;
    var msgAccountNumber = '';
    var msgIfscCode = '';
    var msgName = '';
    var msgEmail = '';
    var msgPhone = '';
    var msgAmount = '';

    if (formData.accountNumber.trim() == '') {
      msgAccountNumber = 'Please enter account number';
      isValid = false;
    }

    if (formData.ifscCode.trim() == '') {
      msgIfscCode = 'Please enter ifsc code';
      isValid = false;
    }

    if (formData.name.trim() == '') {
      msgName = 'Please enter name';
      isValid = false;
    } else {
      if (!isNameValid(formData.name)) {
        msgName = (ErrorMessages.VALIDATION_ERROR_NAME);
      }
    }

    if (formData.email.trim() == '') {
      msgEmail = 'Please enter email id';
      isValid = false;
    } else {
      if (!isEmailValid(formData.email)) {
        msgEmail = (ErrorMessages.VALIDATION_ERROR_EMAIL);
      }
    }

    if (formData.phone.trim() == '') {
      msgPhone = 'Please enter mobile no.';
      isValid = false;
    } else {
      if (!isMobileNumberValid(formData.phone)) {
        msgPhone = ErrorMessages.VALIDATION_ERROR_MOBILE;
      }
    }

    if (formData.amount == null) {
      msgAmount = 'Please enter amount';
      isValid = false;
    } else if (formData.amount < minLimit) {
      msgAmount = 'Amount should be greater than ' + minLimit;
      isValid = false;
    } else if (formData.amount > 999999) {
      msgAmount = 'Entered amount digit should be less than or equal to 6';
      isValid = false;
    } else if (formData.amount > this.state.walletBalance) {
      msgAmount = 'Entered amount should be less than or equal to winning balance';
      isValid = false;
    }

    if (isValid) {
      this.setState({
        errorMessage: {
          accountNumber: '',
          ifscCode: '',
          name: '',
          email: '',
          phone: '',
          amount: ''
        },
        isLoaded: false
      })
      var params = {
        bank_account: parseInt(formData.accountNumber),
        ifsc: formData.ifscCode,
        amount: parseFloat(formData.amount),
        name: formData.name,
        email: formData.email,
        phone: formData.phone
      }

      TransactionService.withdrawMoney(params, (res) => {
        if (res.status) {
          // this.setState({
          //   formData: {
          //     accountNumber: '',
          //     ifscCode: '',
          //     name: '',
          //     email: '',
          //     phone: '',
          //     amount: '',
          //   },
          //   isLoaded: true
          // }, () => {
          //   setTimeout(() => {

          //   }, 300);
          // });
          var data = {
            withdrawRequest: true
          };
          Cookies.set('_wR', encrypt(data));
          Router.push('/verify-request');

        } else {
          this.setState({
            alert: {
              toast: true,
              toastMessage: res.message,
              severity: 'error'
            },
            isLoaded: true
          });
        }
      })
    } else {
      this.setState({
        errorMessage: {
          accountNumber: msgAccountNumber,
          ifscCode: msgIfscCode,
          name: msgName,
          email: msgEmail,
          phone: msgPhone,
          amount: msgAmount,
        },
        isLoaded: true
      })
    }
  }

  render() {
    const { formData, alert, walletBalance, isLoaded } = this.state;
    return (
      <Layout header="innerHeader" headerText="Withdrawal Request">
        <Snackbar open={alert.toast} autoHideDuration={2000} onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })}>
          <Alert onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })} severity={alert.severity}>
            {alert.toastMessage}
          </Alert>
        </Snackbar>
        <Row className="user-pages">
          <Col lg={12}>
            <div className="outer-card mt-4">
              <Card>
                <Card.Body className="">
                  <Row className="justify-content-center align-items-center p-3">
                    <Col lg={12}>
                      <div className="deposit-method">
                        {/* <h6 className="mb-3">Withdrawal request</h6> */}
                        <div className="card-detail-box">
                          <form className="register-form" onSubmit={this.sumbmitDetails}>
                            <div className="phone-input clearfix">
                              <Row>
                                <Col lg={6} xs={12} md={6} md={6} className="pl-lg-2" >
                                  <TextField
                                    label="Account Number"
                                    variant="outlined"
                                    className="mb-3"
                                    placeholder="Enter Account Number"
                                    name="accountNumber"
                                    value={formData.accountNumber}
                                    onChange={this.handleChange}
                                    error={!!this.state.errorMessage.accountNumber}
                                    helperText={!!this.state.errorMessage.accountNumber ? this.state.errorMessage.accountNumber : ''}
                                  />
                                </Col>
                                <Col lg={6} xs={12} md={6} md={6} className="pl-lg-2" >
                                  <TextField
                                    label="IFSC code"
                                    variant="outlined"
                                    className="mb-3"
                                    placeholder="Enter IFSC code"
                                    name="ifscCode"
                                    value={formData.ifscCode}
                                    onChange={this.handleChange}
                                    error={!!this.state.errorMessage.ifscCode}
                                    helperText={!!this.state.errorMessage.ifscCode ? this.state.errorMessage.ifscCode : ''}
                                  />
                                </Col>
                                <Col lg={6} xs={12} md={6} md={6} className="pl-lg-2" >
                                  <TextField
                                    label="Name"
                                    variant="outlined"
                                    className="mb-3"
                                    placeholder="Enter Name"
                                    name="name"
                                    value={formData.name}
                                    onChange={this.handleChange}
                                    error={!!this.state.errorMessage.name}
                                    helperText={!!this.state.errorMessage.name ? this.state.errorMessage.name : ''}
                                  />
                                </Col>
                                <Col lg={6} xs={12} md={6} md={6} className="pl-lg-2" >
                                  <TextField
                                    label="Email id"
                                    variant="outlined"
                                    className="mb-3"
                                    placeholder="Enter Email id"
                                    name="email"
                                    value={formData.email}
                                    onChange={this.handleChange}
                                    error={!!this.state.errorMessage.email}
                                    helperText={!!this.state.errorMessage.email ? this.state.errorMessage.email : ''}
                                  />
                                </Col>
                                <Col lg={6} xs={12} md={6} md={6} className="pl-lg-2" >
                                  <TextField
                                    label="Mobile no"
                                    variant="outlined"
                                    className="mb-3"
                                    placeholder="Enter Mobile no"
                                    name="phone"
                                    value={formData.phone}
                                    onChange={this.handleChange}
                                    error={!!this.state.errorMessage.phone}
                                    helperText={!!this.state.errorMessage.phone ? this.state.errorMessage.phone : ''}
                                  />
                                </Col>
                                <Col lg={6} xs={12} md={6} md={6} className="pl-lg-2" >
                                  <TextField
                                    label="Amount"
                                    variant="outlined"
                                    className="mb-3"
                                    placeholder="Enter amount"
                                    name="amount"
                                    value={formData.amount}
                                    onChange={this.handleChange}
                                    error={!!this.state.errorMessage.amount}
                                    helperText={!!this.state.errorMessage.amount ? this.state.errorMessage.amount : ''}
                                  />
                                </Col>
                                <Col md={12} className="text-center">
                                  {isLoaded ? (
                                    <Button type="submit" title="Send Request" fullWidth variant="contained" color="primary" className="register-now text-capitalize p-9 mt-26 mt-lg-5">
                                      Send Request
                                    </Button>
                                  ) : (
                                      <Button type="submit" fullWidth variant="contained" color="primary" className="register-now text-capitalize p-9 mt-26">
                                        <img src="static/images/loader.gif" alt="" style={{ width: '7.388%' }} className="empty-mybet" />
                                      </Button>
                                    )}
                                </Col>
                              </Row>
                            </div>
                          </form>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </Card.Body>
              </Card>
            </div>
          </Col>
        </Row>
      </Layout>
    )
  }
}
export default Withdrawal;
