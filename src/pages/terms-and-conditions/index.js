import React from 'react';
import Layout from 'components/Layout/Layout';
import { render } from 'react-dom';
import { Container, Row, Col, Card, ListGroup, Badge, Button, Table } from 'react-bootstrap';
const TermsAndConditions = () => {
return (
<Layout title=" Terms & Conditions" header="innerHeader" headerText=" Terms & Conditions">
  <div className="slikslider-bar">
    <div className="slikslider-banner"></div>
  </div>
  <Col lg={12} className="content-pages p-0">
  <div className="outer-card mt-3">
    <Card>
      <Card.Body>



        <h4>1. Sports.cards:-</h4>
        <p>
          Sports.cards is the flagship brand of Telconi Trading Private Limited. Through Sports.cards, along with its
          sub-pages, and the Sports.cards App, Sports.cards operates a portal through which it offers cricket based
          online fantasy games. Sports.cards as used herein shall be construed as a collective reference to Sports.cards
          and the Sports.cards App.
        </p>

        <h4>
          2. Usage of Sports.cards
        </h4>

        <ol>
          <li>Any person ("User") accessing Sports.cards or the Sports.cards App (Sports.cards platform') for
            participating in the various contests and games (including fantasy games), available on Sports.cards
            platform ("Contest(s)") (Sports.cards Services') shall be bound by these Terms and Conditions, and all other
            rules, regulations and terms of use referred to herein or provided by Sports.cards in relation to any
            Sports.cards Services.</li>
          <li>
            Sports.cards shall be entitled to modify these Terms and Conditions, rules, regulations and terms of use
            referred to herein or provided by Sports.cards in relation to any Sports.cards Services, at any time, by
            posting the same on Sports.cards. Use of Sports.cards constitutes the User's acceptance of such Terms and
            Conditions, rules, regulations and terms of use referred to herein or provided by Sports.cards in relation
            to any Sports.cards Services, as may be amended from time to time. Sports.cards may, at its sole discretion,
            also notify the User of any change or modification in these Terms and Conditions, rules, regulations and
            terms of use referred to herein or provided by Sports.cards, by way of sending an email to the User's
            registered email address or posting notifications in the User accounts. The User may then exercise the
            options provided in such an email or notification to indicate non-acceptance of the modified Terms and
            Conditions, rules, regulations and terms of use referred to herein or provided by Sports.cards. If such
            options are not exercised by the User within the time frame prescribed in the email or notification, the
            User will be deemed to have accepted the modified Terms and Conditions, rules, regulations and terms of use
            referred to herein or provided by Sports.cards
          </li>
          <li>

            Certain Sports.cards Services being provided on Sports.cards may be subject to additional rules and
            regulations set down in that respect. To the extent that these Terms and Conditions are inconsistent with
            the additional conditions set down, the additional conditions shall prevail
          </li>

          <li>
            Sports.cards may, at its sole and absolute discretion:
            <ol>
              <li>Restrict, suspend, or terminate any User's access to all or any part of Sports.cards or Sports.cards
                Platform Services;
              </li>
              <li>
                Change, suspend, or discontinue all or any part of the Sports.cards Platform Services;

              </li>
              <li>
                Reject, move, or remove any material that may be submitted by a User;

              </li>
              <li>
                Move or remove any content that is available on Sports.cards Platform;

              </li>
              <li>Deactivate or delete a User's account and all related information and files on the account;
              </li>
              <li>Establish general practices and limits concerning use of Sports.cards Platform;
              </li>
              <li>Offer discounts to its users in form it deems fit ("Cash Bonus"). All such discounts shall be credited
                in a separate account called as Cash Bonus Account
              </li>
              <li>Revise or make additions and/or deletions to the list of markets or Selections available in a Contest;
              </li>
              <li>Assign its rights and liabilities to all User accounts hereunder to any entity (post such assignment
                intimation of such assignment shall be sent to all Users to their registered email ids)</li>

            </ol>
          </li>
          <li>In the event any User breaches, or Sports.cards reasonably believes that such User has breached these
            Terms and Conditions, or has illegally or improperly used Sports.cards or the Sports.cards Services,
            Sports.cards may, at its sole and absolute discretion, and without any notice to the User, restrict, suspend
            or terminate such User's access to all or any part of Sports.cards Contests or the Sports.cards Platform,
            deactivate or delete the User's account and all related information on the account, delete any content
            posted by the User on Sports.cards and further, take technical and legal steps as it deems necessary
          </li>

          <p>
            If Sports.cards charges its Users a platform fee in respect of any Sports.cards Services, Sports.cards
            shall, without delay, repay such platform fee in the event of suspension or removal of the User's account or
            Sports.cards Services on account of any negligence or deficiency on the part of Sports.cards, but not if
            such suspension or removal is effected due to:
          </p>

          <li>
            any breach or inadequate performance by the User of any of these Terms and Conditions; or

          </li>
          <li>any circumstances beyond the reasonable control of Sports.cards.
          </li>

          <li>Users consent to receiving communications such as announcements, administrative messages and
            advertisements from Sports.cards or any of its partners, licensors or associates.
          </li>

        </ol>
        <h4>
          3. Intellectual Property

        </h4>
        <ol>
          <li>
            Sports.cards includes a combination of content created by Sports.cards, its partners, affiliates, licensors,
            associates and/or Users. The intellectual property rights ("Intellectual Property Rights") in all software
            underlying Sports.cards and the Sports.cards Platform and material published on Sports.cards, including (but
            not limited to) games, Contests, software, advertisements, written content, photographs, graphics, images,
            illustrations, marks, logos, audio or video clippings and Flash animation, is owned by Sports.cards, its
            partners, licensors and/or associates. Users may not modify, publish, transmit, participate in the transfer
            or sale of, reproduce, create derivative works of, distribute, publicly perform, publicly display, or in any
            way exploit any of the materials or content on Sports.cards either in whole or in part without express
            written license from Sports.cards
          </li>
          <li>
            Users may request permission to use any Sports.cards content by writing in to Sports.cards Helpdesk.

          </li>
          <li>
            Users are solely responsible for all materials (whether publicly posted or privately transmitted) that they
            upload, post, e-mail, transmit, or otherwise make available on Sports.cards ("Users' Content"). Each User
            represents and warrants that he/she owns all Intellectual Property Rights in the User's Content and that no
            part of the User's Content infringes any third party rights. Users further confirm and undertake to not
            display or use of the names, logos, marks, labels, trademarks, copyrights or intellectual and proprietary
            rights of any third party on Sports.cards. Users agree to indemnify and hold harmless Sports.cards, its
            directors, employees, affiliates and assigns against all costs, damages, loss and harm including towards
            litigation costs and counsel fees, in respect of any third party claims that may be initiated including for
            infringement of Intellectual Property Rights arising out of such display or use of the names, logos, marks,
            labels, trademarks, copyrights or intellectual and proprietary rights on Sports.cards, by such User or
            through the User's commissions or omissions
          </li>

          <p>
            Users hereby grant to Sports.cards and its affiliates, partners, licensors and associates a worldwide,
            irrevocable, royalty-free, non-exclusive, sub-licensable license to use, reproduce, create derivative works
            of, distribute, publicly perform, publicly display, transfer, transmit, and/or publish Users' Content for
            any of the following purposes:
          </p>
          <ol>
            <li>displaying Users' Content on Sports.cards
            </li>
            <li>distributing Users' Content, either electronically or via other media, to other Users seeking to
              download or otherwise acquire it, and/or
            </li>
            <li>storing Users' Content in a remote database accessible by end users, for a charge.
            </li>
            <li>
              This license shall apply to the distribution and the storage of Users' Content in any form, medium, or
              technology.

            </li>


          </ol>
          <li>
            All names, logos, marks, labels, trademarks, copyrights or intellectual and proprietary rights on
            Sports.cards(s) belonging to any person (including User), entity or third party are recognized as
            proprietary to the respective owners and any claims, controversy or issues against these names, logos,
            marks, labels, trademarks, copyrights or intellectual and proprietary rights must be directly addressed to
            the respective parties under notice to Sports.cards.
          </li>

        </ol>

        <h4>

          4. Third Party Sites, Services and Products

        </h4>

        <ol>
          <li>
            Sports.cards may contain links to other Internet sites owned and operated by third parties. Users' use of
            each of those sites is subject to the conditions, if any, posted by the sites. Sports.cards does not
            exercise control over any Internet sites apart from Sports.cards and cannot be held responsible for any
            content residing in any third-party Internet site. Sports.cards's inclusion of third-party content or links
            to third-party Internet sites is not an endorsement by Sports.cards of such third-party Internet site.

          </li>
          <li>
            Users' correspondence, transactions/offers or related activities with third parties, including payment
            providers and verification service providers, are solely between the User and that third party. Users'
            correspondence, transactions and usage of the services/offers of such third party shall be subject to the
            terms and conditions, policies and other service terms adopted/implemented by such third party, and the User
            shall be solely responsible for reviewing the same prior to transacting or availing of the services/offers
            of such third party. User agrees that Sports.cards will not be responsible or liable for any loss or damage
            of any sort incurred as a result of any such transactions/offers with third parties. Any questions,
            complaints, or claims related to any third party product or service should be directed to the appropriate
            vendor.

          </li>
          <li>
            Sports.cards contains content that is created by Sports.cards as well as content provided by third parties.
            Sports.cards does not guarantee the accuracy, integrity, quality of the content provided by third parties
            and such content may not relied upon by the Users in utilizing the Sports.cards Services provided on
            Sports.cards including while participating in any of the contests hosted on Sports.cards.
          </li>



          <h4>
            5. Privacy Policy

          </h4>
          <p>
            All information collected from Users, such as registration and credit card information, is subject to
            Sports.cards's Privacy Policy which is available at Privacy Policy
          </p>
          <h4>
            6. User Conduct

          </h4>
          <p>
            Users agree to abide by these Terms and Conditions and all other rules, regulations and terms of use of the
            Website. In the event User does not abide by these Terms and Conditions and all other rules, regulations and
            terms of use, Sports.cards may, at its sole and absolute discretion, take necessary remedial action,
            including but not limited to:
          </p>


          <ol>
            <li>
              restricting, suspending, or terminating any User's access to all or any part of Sports.cards Services;

            </li>
            <li>
              deactivating or deleting a User's account and all related information and files on the account. Any
              amount remaining unused in the User's Game account or Winnings Account on the date of deactivation or
              deletion shall be transferred to the User's bank account on record with Sports.cards subject to a
              processing fee (if any) applicable on such transfers as set out herein; or

            </li>
            <li>
              refraining from awarding any prize(s) to such User.

            </li>


            <li>
              Users agree to provide true, accurate, current and complete information at the time of registration and
              at all other times (as required by Sports.cards). Users further agree to update and keep updated their
              registration information
            </li>
            <li>
              A User shall not register or operate more than one User account with Sports.cards.
              Users agree to ensure that they can receive all communication from Sports.cards by marking e-mails or
              sending SMSs from Sports.cards as part of their "safe senders" list. Sports.cards shall not be held
              liable if any e-mail/SMS remains unread by a User as a result of such e-mail getting delivered to the
              User's junk or spam folder.
            </li>
            <li>
              Any password issued by Sports.cards to a User may not be revealed to anyone else. Users may not use
              anyone else's password. Users are responsible for maintaining the confidentiality of their accounts and
              passwords. Users agree to immediately notify Sports.cards of any unauthorized use of their passwords or
              accounts or any other breach of security.
            </li>
            <li>
              Users agree to exit/log-out of their accounts at the end of each session. Sports.cards shall not be
              responsible for any loss or damage that may result if the User fails to comply with these requirements.
            </li>
            <li>
              Users agree not to use cheats, exploits, automation, software, bots, hacks or any unauthorised third
              party software designed to modify or interfere with Sports.cards Services and/or Sports.cards experience
              or assist in such activity.
            </li>
            <li>
              Users agree not to copy, modify, rent, lease, loan, sell, assign, distribute, reverse engineer, grant a
              security interest in, or otherwise transfer any right to the technology or software underlying
              Sports.cards or Sports.cards’s Services.
            </li>
            <li>
              Users agree that without Sports.cards's express written consent, they shall not modify or cause to be
              modified any files or software that are part of Sports.cards's Services.
              Users agree not to disrupt, overburden, or aid or assist in the disruption or overburdening of (a) any
              computer or server used to offer or support Sports.cards or the Sports.cards’s Services (each a
              "Server"); or (2) the enjoyment of Sports.cards Services by any other User or person.
            </li>
            <li>
              Users agree not to institute, assist or become involved in any type of attack, including without
              limitation to distribution of a virus, denial of service, or other attempts to disrupt Sports.cards
              Services or any other person's use or enjoyment of Sports.cards Services.
            </li>
            <li>
              Users shall not attempt to gain unauthorised access to the User accounts, Servers or networks connected
              to Sports.cards Services by any means other than the User interface provided by Sports.cards, including
              but not limited to, by circumventing or modifying, attempting to circumvent or modify, or encouraging or
              assisting any other person to circumvent or modify, any security, technology, device, or software that
              underlies or is part of Sports.cards Services.
            </li>



          </ol>

          <ol>
            <li>
              Without limiting the foregoing, Users agree not to use Sports.cards for any of the following:
              <ol>
                <li>
                  To engage in any obscene, offensive, indecent, racial, communal, anti-national, objectionable,
                  defamatory or abusive action or communication;
                </li>
                <li>

                  To harass, stalk, threaten, or otherwise violate any legal rights of other individuals;
                  To publish, post, upload, e-mail, distribute, or disseminate (collectively, "Transmit") any
                  inappropriate,
                </li>
                <li>
                  profane, defamatory, infringing, obscene, indecent, or unlawful content;
                  To Transmit files that contain viruses, corrupted files, or any other similar software or programs
                  that
                  may damage or adversely affect the operation of another person's computer, Sports.cards, any
                  software,
                </li>
                <li>
                  hardware, or telecommunications equipment;
                  To advertise, offer or sell any goods or services for any commercial purpose on Sports.cards without
                  the
                  express written consent of Sports.cards;
                </li>
                <li>

                  To download any file, recompile or disassemble or otherwise affect our products that you know or
                  reasonably should know cannot be legally obtained in such manner;
                  To falsify or delete any author attributions, legal or other proper notices or proprietary
                  designations or
                  labels of the origin or the source of software or other material;
                  To restrict or inhibit any other user from using and enjoying any public area within our sites;
                </li>
                <li>
                  To collect or store personal information about other Users;
                </li>
                <li>
                  To interfere with or disrupt Sports.cards, servers, or networks;
                </li>
                <li>
                  To impersonate any person or entity, including, but not limited to, a representative of
                  Sports.cards, or
                  falsely state or otherwise misrepresent User's affiliation with a person or entity;
                </li>
                <li>
                  To forge headers or manipulate identifiers or other data in order to disguise the origin of any
                  content transmitted through Sports.cards or to manipulate User's presence on Sports.cards(s);

                </li>
                <li>
                  To take any action that imposes an unreasonably or disproportionately large load on our
                  infrastructure;

                </li>

                <li>
                  To engage in any illegal activities. You agree to use our bulletin board services, chat areas, news
                  groups, forums, communities and/or message or communication facilities (collectively, the "Forums")
                  only to send and receive messages and material that are proper and related to that particular Forum.

                </li>
              </ol>
            </li>








          </ol>
          <p> If a User chooses a username that, in Sports.cards's considered opinion is obscene, indecent, abusive or
            that might subject Sports.cards to public disparagement or scorn, or a name which is an official
            team/league/franchise names and/or name of any sporting personality, as the case may be, Sports.cards
            reserves the right, without prior notice to the User, to restrict usage of such names, which in
            Sports.cards’s opinion fall within any of the said categories and/or change such username and intimate the
            User or delete such username and posts from Sports.cards, deny such User access to Sports.cards, or any
            combination of these options.</p>




          <p>
            Unauthorized access to Sports.cards is a breach of these Terms and Conditions, and a violation of the law.
            Users agree not to access Sports.cards by any means other than through the interface that is provided by
            Sports.cards for use in accessing Sports.cards. Users agree not to use any automated means, including,
            without limitation, agents, robots, scripts, or spiders, to access, monitor, or copy any part of our sites,
            except those automated means that we have approved in advance and in writing.
          </p>

          <p>
            Use of Sports.cards is subject to existing laws and legal processes. Nothing contained in these Terms and
            Conditions shall limit Sports.cards's right to comply with governmental, court, and law-enforcement requests
            or requirements relating to Users' use of Sports.cards.
          </p>
          <p>
            Users may reach out to Sports.cards through -
          </p>
          <p>
            Helpdesk if the user has any concerns with regard to a match and/or contest within Forty Eight (48)
            hours of winner declaration for the concerned contest.
          </p>
          <p>
            Persons below the age of eighteen (18) years are not allowed to participate on any of the contests, games
            (by whatever name called) on the Sports.cards Platform. The Users will have to disclose their real age at
            the time of getting access into the Sports.cards Platform.
          </p>
          <p>
            Sports.cards may not be held responsible for any content contributed by Users on the Sports.cards.
          </p>
        </ol>

        <h4>
          7. Conditions of Participation

        </h4>
        <p>
          By entering a Contest, user agrees to be bound by these Terms and the decisions of Sports.cards. Subject to
          the terms and conditions stipulated herein below, the Company, at its sole discretion, may disqualify any user
          from a Contest, refuse to award benefits or prizes and require the return of any prizes, if the user engages
          in unfair conduct, which the Company deems to be improper, unfair or otherwise adverse to the operation of the
          Contest or is in any way detrimental to other Users which includes, but is not limited to:

        </p>
        <ol>
          <li>
            Falsifying ones’ own personal information (including, but not limited to, name, email address, bank account
            details and/or any other information or documentation as may be requested by Sports.cards to enter a contest
            and/or claim a prize/winning.;
          </li>
          <li>
            Engaging in any type of financial fraud or misrepresentation including unauthorized use of credit/debit
            instruments, payment wallet accounts etc. to enter a Contest or claim a prize. It is expressly clarified
            that
            the onus to prove otherwise shall solely lie on the user.;
          </li>
          <li>
            Colluding with any other user(s) or engaging in any type of syndicate play;
            Any violation of Contest rules or the Terms of Use;
            Accumulating points or prizes through unauthorized methods such as automated bots, or other automated means;
          </li>
          <li>
            Using automated means (including but not limited to harvesting bots, robots, parser, spiders or screen
            scrapers) to obtain, collect or access any information on the Website or of any User for any purpose
            Any type of Cash Bonus misuse, misuse of the Invite Friends program, or misuse of any other offers or
            promotions;
          </li>
          <li>
            Tampering with the administration of a Contest or trying to in any way tamper with the computer programs or
            any security measure associated with a Contest;
          </li>
          <li>
            Obtaining other users’ information without their express consent and/or knowledge and/or spamming other
            users
            (Spamming may include but shall not be limited to send unsolicited emails to users, sending bulk emails to
            Sports.cards Users, sending unwarranted email content either to selected Users or in bulk); or
            Abusing the Website in any way (‘unparliamentary language, slangs or disrespectful words’ are some of the
            examples of Abuse)

          </li>

        </ol>

        <p>
          It is clarified that in case a User is found to be in violation of this policy, Sports.cards reserves its
          right to initiate appropriate Civil/Criminal remedies as it may be advised other than forfeiture and/or
          recovery of prize money if any.
        </p>
        <h4>8. Registration for a contest:</h4>
        <ul className="c7 lst-kix_cmkyeyv27mo1-0">
          <li className="c1 c3"><span className="c0">In order to register for the Contest(s), Participants are required
              to
              accurately provide the following information:</span></li>
        </ul>
        <ul className="c7 lst-kix_cmkyeyv27mo1-1 start">
          <li className="c2"><span className="c0">Full Name</span></li>
          <li className="c2"><span className="c0">Phone Number</span></li>
          <li className="c2"><span className="c0">E-mail address</span></li>
          <li className="c2"><span className="c0">Password</span></li>
          <li className="c2"><span className="c0">State of Residence</span></li>
          <li className="c2"><span className="c0">Gender</span></li>
          <li className="c2"><span className="c9">Date of birth</span></li>
        </ul>
        <ul className="c7 lst-kix_cmkyeyv27mo1-0">
          <li className="c1 c3"><span className="c0">Participants are also required to confirm that they have read, and
              shall
              abide
              by, these Terms and Conditions.</span></li>
          <li className="c1 c3"><span className="c0">In the event a Participant indicates, while entering an address,
              that
              he/she is
              a resident of either Assam, Odisha, Sikkim, Nagaland, Telangana or Andhra Pradesh, such Participant will
              not
              be permitted to proceed to sign up for any match in the paid version of the Contest as described
              below.</span>
          </li>
          <li className="c1 c3"><span className="c23">Once the Participants have entered the above information, and
              clicked on
              the
              &quot;register&quot; tab, and such Participants are above the age of 18 years, they are sent an email
              confirming their registration and containing their login information.</span></li>
        </ul>
        <p className="c5 c17 c3"><span className="c29 c28"></span></p>
        <h4>
          9. Contest(s), Participation and Prizes
        </h4>
        <ul className="c7 lst-kix_cmkyeyv27mo1-0">
          <li className="c1 c3"><span className="c0">As part of its services, Sports.cards may make available the
              contest(s) on
              the
              Sports.cards platform.</span></li>
          <li className="c1 c3"><span className="c0">Currently, following contests are made available on Sports.cards
              platform:
              1) A
              fantasy cricket game. Individual users wishing to participate in the contest
              (&quot;&ldquo;Participants&quot;&rdquo;) are invited to create their own fantasy cards
              (&quot;&ldquo;Card/s&quot;&rdquo;) consisting of real life cricket events (as applicable) involved in the
              real-life cricket match (as applicable), series or tournament (each a &quot;Sport Event&quot;) to which
              the
              fantasy game relates. Sports.cards offers its platform to Participants for fantasy game Contest(s) being
              created relating to each Sport Event, and Participants can participate in such Contest(s) with their
              Cards.</span></li>
          <li className="c1 c3"><span className="c0">Depending upon the circumstances of each match, the participants
              can create
              their cards till the official match end time as declared by the officials of the Sport Event or adjusted
              deadline, as specified below.</span></li>
        </ul>
        <ul className="c7 lst-kix_cmkyeyv27mo1-1 start">
          <li className="c2"><span className="c0">Sports.cards reserves the right to abandon a specific round or adjust
              the
              deadline
              of a round in certain specific, uncertain scenarios, which are beyond Sports.cards&rsquo;s reasonable
              control,
              including but not limited to the ones&rsquo; mentioned herein below:</span></li>
        </ul>
        <ol className="c7 lst-kix_cmkyeyv27mo1-2 start" start="1">
          <li className="c5 c26 c3"><span className="c20">Actual match end time is before the official deadline:</span>
          </li>
        </ol>
        <ul className="c7 lst-kix_cmkyeyv27mo1-3 start">
          <li className="c5 c3 c30"><span className="c0">Cricket -</span></li>
        </ul>
        <ul className="c7 lst-kix_cmkyeyv27mo1-4 start">
          <li className="c5 c10 c3"><span className="c0">Sports.cards reserves the right to adjust the deadline by a
              Maximum of
              10
              minutes or 3 overs to be bowled, whichever is less, before the official match end time.</span></li>
          <li className="c5 c3 c10"><span className="c0">In cases where official match time cannot be verified by
              Sports.cards
              through reliable and/or publicly available sources, Sports.cards reserves the right to adjust the deadline
              to
              such a time by which a maximum of 3 overs in the given match to be bowled.</span></li>
        </ul>
        <ol className="c7 lst-kix_cmkyeyv27mo1-2" start="2">
          <li className="c5 c3 c26"><span className="c20">Actual match end time is after the official
              deadline:</span><span className="c0"><br />Sports.cards reserves the right to adjust the deadline or
              abandon the contest/game based
              on
              the
              circumstances such as delayed toss, interruption on account of weather, non-appearance of teams,
              technical/equipment glitches causing delays etc.</span></li>
        </ol>
        <ul className="c7 lst-kix_cmkyeyv27mo1-0">
          <li className="c1 c3"><span className="c0">Sports.cards shall endeavor to send communications through emails
              and/or
              SMS
              communication, about any such change as is contemplated in the aforementioned paragraphs to keep the User
              updated.</span></li>
          <li className="c1 c3"><span className="c0">Teams are awarded points on the basis of the real life cricket&#39;
              events
              that
              occurred during a designated match, match or tournament of the Contest(s). The Participant(s) whose
              Card(s)
              have achieved the highest aggregate point(s) in the Contest(s) shall be declared winners
              (&quot;Winners&quot;). In certain pre-specified Contests, there may be more than one Winner and
              distribution
              of prizes to such Winners will be in increasing order of their Team&#39;s aggregate score at the end of
              the
              designated match(s) of the Contests.</span></li>
          <li className="c1 c3"><span className="c0">The Contest(s) across the Sports.cards Services shall, in addition
              to the
              Terms
              and Conditions, rules and regulations mentioned herein, be governed by:</span></li>
        </ul>
        <ul className="c7 lst-kix_cmkyeyv27mo1-1 start">
          <li className="c2"><span className="c9">&quot;Fantasy Rules&quot; available at </span><span className="c9"><a
                className="c19"
                href="https://www.google.com/url?q=https://www.dream11.com/games/fantasy-cricket/how-to-play&amp;sa=D&amp;ust=1604563116007000&amp;usg=AOvVaw24U60KR8yNwWvBaACM0TJD">How
                To Play - Fantasy Cricket</a></span><span className="c0">&nbsp;</span></li>
          <li className="c2"><span className="c0">Other rules and regulations (including rules and regulation in
              relation to any
              payments made to participate in the Contest(s); and all Participants agree to abide by the same.</span>
          </li>
        </ul>
        <ul className="c7 lst-kix_cmkyeyv27mo1-0">
          <li className="c1 c3"><span className="c0">Currently, there are paid versions of the Contest(s) made available
              on
              Sports.cards platform. Users may participate in the Contest(s) by paying the pre-designated amount as
              provided
              on the relevant Contest page. The &lsquo;pre-designated amount&rsquo; means and includes pre-determined
              platform fee for accessing Sports.cards services and pre-determined participant&#39;&rsquo;s contribution
              towards prize money pool. The Participant with the highest aggregate points at the end of the
              pre-determined
              match shall be eligible to win a pre-designated prize which is disbursed out of prize money pool, as
              stated on
              the relevant Contest(s) page.</span></li>
          <li className="c1 c3"><span className="c0">A Participant may create different Teams for participation in
              Contest(s) in
              relation to a Sport Event across the Sports.cards Services. However, unless Sports.cards specifies
              otherwise
              in relation to any Contest (&quot;&ldquo;Multiple Entry Contest&quot;&rdquo;), Participants acknowledge
              and
              agree that they may enter only one Card in any Contest offered in relation to a Sport Event. In case of
              Multiple Entry Contest(s), a Participant may enter more than one Card in a single Multiple Entry Contest.
              In
              addition, it is expressly clarified that Sports.cards may, from time to time, restrict the maximum number
              of
              Cards that may be created by a single User account (for each format of the contest) or which a single User
              account may enter in a particular Multiple Entry Contest, in each case to such number as determined by
              Sports.cards in its sole discretion.</span></li>
          <li className="c1 c3"><span className="c0">Participant shall pay a pre-designated amount for participating in
              the
              contest(s) being created on the Sports.cards platform.<br />In the event a Participant indicates, while
              entering
              an address, that he/she is a resident of either Assam, Odisha, Sikkim, Nagaland, Telangana or Andhra
              Pradesh,
              such Participant will not be permitted to proceed to sign up for the match or contest and may not
              participate
              in any paid version of the Contest(s).</span></li>
          <li className="c1 c3"><span className="c0">In two members and above public contests, where all participants
              have
              entered
              the contest and earned the exact same points, in such an event, contest prize money shall be equally
              divided
              amongst all participants and the amount shall be deposited in the Sports.cards winning account of all
              participants. In the event a user is found to be violating this policy, the Company reserves the right to
              initiate appropriate action against such users as it deems fit, which shall also include forfeiture of the
              Winning Amount.</span></li>
        </ul>
        <p className="c12 c15"><span className="c11"></span></p>
        <h4>
          10. Contest Formats
        </h4>
        <ol className="c7 lst-kix_mttsevi03hc3-1 start" start="1">
          <li className="c1"><span className="c0">Currently only one format of contest is made available on Sports.cards
              platform -
              Public Contest where Users can participate in a Contest with other Users without any restriction on
              participation. A user can enter into a maximum of 500 contests per match. Any participation in a contest
              more
              than 500 shall be automatically prohibited. All rules applicable to Contest(s) as set out herein shall be
              applicable to both formats of the Contest(s). Users by participating in a Contest(s) hereby authorize
              Sports.cards to appoint a third party/ Trustee/Escrow Agent to manage users funds on users behalf.</span>
          </li>
          <li className="c1"><span className="c13">Public contest</span></li>
        </ol>
        <ol className="c7 lst-kix_mttsevi03hc3-2 start" start="1">
          <li className="c4"><span className="c0">In the Public contest format of the Contest(s), Sports.cards may make
              available
              the Contest(s) comprising of 2 -&ndash; 10000 Participants or any other pre-designated number of
              Participants.</span></li>
          <li className="c4"><span className="c0">Sports.cards may create this format of the Contest(s) as a paid format
              and the
              Winner will be determinable at the end of the match as per rule of the contests.</span></li>
          <li className="c4"><span className="c0">The number of Participants required to make the Contest(s) operational
              will be
              pre-specified and once the number of Participants in such Contest(s) equals the pre-specified number
              required
              for that Contest(s), such Contest(s) shall be operational. In case the number of Participants is less than
              the
              pre-specified number at the time of commencement of the match, such Contest(s) will not be operational and
              the
              pre-designated amount paid by each Participant shall be returned to the account of such User without any
              charge or deduction.</span></li>
          <li className="c4"><span className="c0">In certain Contests across the Sports.cards Services, designated as
              &ldquo;Confirmed contests&rdquo;, the Contest(s) shall become operational only when a minimum of two users
              join a Confirmed Contest. The pre-specified number of winners to be declared in such Contest(s), even if
              all
              available Participant slots (as pre-specified in relation to the Contest(s)) remain unfilled. It is
              clarified
              that notwithstanding the activation of such Contest(s), Participants can continue to join such Contest(s)
              till
              either (i) all available Participant slots of such Contest(s) are filled or (ii) the match to which the
              Contest (s) relates commences, whichever is earlier. In the event of shortfall in the number of
              participants
              joining the Confirmed Contest, Sports.cards shall continue with such contests and the short fall in the
              prize
              pool shall be borne by Sports.cards.</span></li>
        </ol>
        <p className="c5 c17 c32"><span className="c0"></span></p>
        <h4>
          11. Legality of Game of Skill

        </h4>
        <ol className="c7 lst-kix_mttsevi03hc3-1 start" start="1">
          <li className="c1"><span className="c0">Games of skill are legal, as they are excluded from the ambit of
              Indian
              gambling
              legislations including, the Public Gambling Act of 1867.The Indian Supreme Court in the cases of State of
              Andhra Pradesh v. K Satyanarayana (AIR 1968 SC 825) and KR Lakshmanan v. State of Tamil Nadu (AIR 1996 SC
              1153) has held that a game in which success depends predominantly upon the superior knowledge, training,
              attention, experience and adroitness of the player shall be classified as a game of skill.</span></li>
          <li className="c1"><span className="c0">The Contest (s) described above (across the Sports.cards Services) are
              games
              of
              skill as success of Participants depends primarily on their superior knowledge of the games of cricket
              statistics, knowledge of players&#39; relative form, players&#39; performance in a particular territory,
              conditions and/or format (such as ODIs, test cricket and Twenty20 in the cricket fantasy game), attention
              and
              dedication towards the Contest(s) and adroitness in playing the Contest(s). The Contest(s) also requires
              Participants to field well-balanced sides with limited resources and make substitutions at appropriate
              times
              to gain the maximum points.</span></li>
          <li className="c1"><span className="c0">By participating in this Contest(s), each Participant acknowledges and
              agrees
              that
              he/she is participating in a game of skill.</span></li>
        </ol>
        <h4>
          12. Eligibility
        </h4>
        <ol className="c7 lst-kix_mttsevi03hc3-1 start" start="1">
          <li className="c1"><span className="c0">The Contest(s) are open only to persons above the age of 18
              years.</span></li>
          <li className="c1"><span className="c0">The Contest(s) are open only to persons, currently residing in
              India.</span>
          </li>
          <li className="c1"><span className="c0">Sports.cards may, in accordance with the laws prevailing in certain
              Indian
              states,
              bar individuals residing in those states from participating in the Contest(s). Currently, individuals
              residing
              in the Indian states of Assam, Odisha, Sikkim, Nagaland, Andhra Pradesh or Telangana may not participate
              in
              the paid version of the Contest as the laws of these states are unclear/ bar persons from participating in
              games of skill where participants are required to pay to enter.</span></li>
          <li className="c1"><span className="c0">Persons who wish to participate must have a valid email
              address.</span></li>
          <li className="c1"><span className="c0">Sports.cards may on receipt of information bar a person from
              participation
              and/or
              withdrawing winning amounts if such person is found to be one with insider knowledge of participating
              teams in
              any given contests/match, organizing boards, leagues etc.</span></li>
          <li className="c1"><span className="c0">Only those Participants who have successfully registered on the
              Sports.cards
              as
              well as registered prior to each match in accordance with the procedure outlined above shall be eligible
              to
              participate in the Contest and win prizes.</span></li>
        </ol>
        <h4>13. Payment Terms
        </h4>
        <ol className="c7 lst-kix_mttsevi03hc3-1 start" start="1">
          <li className="c1"><span className="c0">In respect of any transactions entered into on the Sports.cards
              platform,
              including making a payment to participate in the paid versions of Contest(s), Users agree to be bound by
              the
              following payment terms:</span></li>
        </ol>
        <ol className="c7 lst-kix_mttsevi03hc3-2 start" start="1">
          <li className="c4"><span className="c0">The payment of pre-designated amount Users make to participate in the
              Contest(s)
              is inclusive of the pre-designated platform fee for access to the Sports.cards Services charged by
              Sports.cards and pre-determined participant&rsquo;s contribution towards prize money pool.</span></li>
          <li className="c4"><span className="c0">Subject to these Terms and Conditions, all amounts collected from the
              User are
              held in a separate non-interest earning bank Accounts. The said accounts are operated by a third party
              appointed by Sports.cards in accordance with Clause 10 of these Terms and Conditions. From these bank
              accounts, the payouts can be made to (a) Users (towards their withdrawals), (b) Sports.cards (towards its
              Platform Fees) and to (c) Government (towards TDS on Winnings Amount). Sports.cards receives only its
              share of
              the platform Fees through the said Escrow Agent.</span></li>
          <li className="c4"><span className="c0">The Sports.cards reserves the right to charge a Platform Fee, which
              would be
              specified and notified by Sports.cards on the Contest page, being created on Sports.cards platform, prior
              to a
              User&#39;s joining of such Contest. The Platform Fee (inclusive of applicable tax thereon) will be debited
              from the User&rsquo;s account balance and Sports.cards shall issue an invoice for such debit to the
              User.</span></li>
          <li className="c4"><span className="c0">The User may participate in a Contest wherein the User has to
              contribute a
              pre-specified contribution towards the Prize Money Pool of such Contest, which will be passed on to the
              Winner(s) of the Contest after the completion of the Contest as per the terms and conditions of such
              Contest.
              It is clarified that Sports.cards has no right or interest in this Prize Money Pool, and only acts as an
              intermediary engaged in collecting and distributing the Prize Money Pool in accordance with the Contest
              terms
              and conditions. The amount to be paid-in by the User towards the Prize Money Pool would also be debited
              from
              the User&rsquo;s account balance maintained with the Trustee.</span></li>
          <li className="c4"><span className="c0">Any user availing Sports.cards services are provided with two
              categories of
              accounts for the processing and reconciliation of payments: &#39;Cash&#39; Account, &nbsp; Winnings
              Account
              and Bonus Account. It is clarified that in no instance the transfer of any amounts in the User&#39;s
              accounts
              to any other category of account held by the user or any third party account, including a bank account
              held by
              a third party:</span></li>
        </ol>
        <ol className="c7 lst-kix_mttsevi03hc3-3 start" start="1">
          <li className="c5 c26"><span className="c0">User&#39;s winnings in any Contest will reflect as credits to the
              User&#39;s
              Winnings Account.</span></li>
          <li className="c5 c26"><span className="c0">User&rsquo;s remitting the amount the designated payment gateway
              shall be
              credited to User&rsquo;s account</span></li>
        </ol>
        <ol className="c7 lst-kix_mttsevi03hc3-2" start="6">
          <li className="c4"><span className="c0">Each time a User participates in any contest on Sports.cards platform,
              the
              pre-designated amount shall be debited in the User&rsquo;s &#39;Cash&#39; Account. In debiting amounts
              from
              the User&rsquo;s accounts towards the pre-designated amount of such user shall be debited from the
              User&rsquo;s &#39;Cash&#39; Account and thereafter, any remaining amount of participation fee shall be
              debited
              from the User&rsquo;s Winning Account and finally from the Bonus Account.</span></li>
          <li className="c4"><span className="c0">In case there is any amount remaining to be paid by the User in
              relation to
              such
              User&rsquo;s participation in any match(s) or Contest(s), the User will be taken to the designated payment
              gateway to give effect to such payment. In case any amount added by the User through such payment gateway
              exceeds the remaining amount of the pre-designated amount, the amount in excess shall be transferred to
              the
              User&rsquo;s &#39;Cash&#39; Account and will be available for use in participation in any match(s) or
              Contest(s) or for withdrawal in accordance with these Terms and Conditions.</span></li>
          <li className="c4"><span className="c0">Debits from the &#39;Cash&#39; Account for the purpose of enabling a
              user&rsquo;s
              participation in a Contest shall be made in order of the date of credit of amounts in the &#39;Cash&#39;
              Account, and accordingly amounts credited into &#39;Cash&#39; Account earlier in time shall be debited
              first.</span></li>
          <li className="c4"><span className="c0">A User shall be permitted to withdraw any amounts credited into such
              User&#39;s
              &#39;Cash&#39; Account for any reason whatsoever by contacting Sports.cards Customer Support. The minimum
              amount that can be withdrawn by a User from his/her &#39;Cash&#39; Account is Rs. 200 (Two Hundred). All
              amounts credited into a User&#39;s &#39;Cash&#39; Account must be utilised within 335 days of credit. In
              case
              any unutilised amount lies in the &#39;Cash&#39; Account after the completion of 335 days from the date of
              credit of such amount, Sports.cards reserves the right to forfeit such unutilised amount, without
              liability or
              obligation to pay any compensation to the User.</span></li>
          <li className="c4"><span className="c0">Withdrawal of any amount standing to the User&#39;s credit in the
              Winnings
              Account
              may be made by way of a request to Sports.cards but shall occur automatically upon completion of 335 days
              from
              the date of credit of such amount in the User&#39;s Winnings Account. In either case, Sports.cards shall
              effect an online transfer to the User&#39;s bank account on record with Sports.cards within a commercially
              reasonable period of time. Such transfer will reflect as a debit to the User&#39;s Winnings Account.
              Sports.cards shall not charge any processing fee for the online transfer of such amount from the Winnings
              Account to the User&#39;s bank account on record with Sports.cards. Users are requested to note that they
              will
              be required to provide valid photo identification and address proof documents for proof of identity and
              address in order for Sports.cards to process the withdrawal request. The name mentioned on the User&#39;s
              photo identification document should correspond with the name provided by the User at the time of
              registration
              on Sports.cards, as well as the name and address existing in the records of the User&#39;s bank account as
              provided to Sports.cards. In the event that no bank account has been registered by the User against such
              User&#39;s account with Sports.cards, or the User has not verified his/her User account with Sports.cards,
              to
              Sports.cards&#39;s satisfaction and in accordance with these Terms and Conditions, Sports.cards shall
              provide
              such User with a notification to the User&#39;s email address as on record with Sports.cards at least 30
              days
              prior to the Auto Transfer Date, and in case the User fails to register a bank account with his/her User
              Account and/or to verify his/her User Account by the Auto Transfer Date, Sports.cards shall be entitled to
              forfeit any amounts subject to transfer on the Auto Transfer Date. Failure to provide Sports.cards with a
              valid bank account or valid identification documents (to Sports.cards&#39;s satisfaction) may result in
              the
              forfeiture of any amounts subject to transfer in accordance with this clause.</span></li>
          <li className="c4"><span className="c0">Further, in order to conduct promotional activities, Sports.cards may
              gratuitously
              issue Cash Bonus to the User for the purpose of participation in any Contest(s) and no User shall be
              permitted
              to transfer or request the transfer of any amount into the Cash Bonus. The usage of any Cash Bonus issued
              shall be subject to the limitations and restrictions, including without limitation, restrictions as to
              time
              within which such Cash Bonus must be used, as applied by Sports.cards and notified to the User at the time
              of
              issue of such amount. The issue of any Cash Bonus to the user is subject to the sole discretion of
              Sports.cards and cannot be demanded by any User as a matter of right. The issue of any Cash Bonus by
              Sports.cards on any day shall not entitle the user to demand the issuance of such Cash Bonus at any
              subsequent
              period in time nor create an expectation of recurring issue of such Cash Bonus by Sports.cards to such
              User.
              The Cash Bonus granted to the user may be used by such User for the purpose of setting off against the
              contribution to prize pool in any Contest, in accordance with these Terms and Conditions. The Cash Bonus
              shall
              not be withdraw-able or transferrable to any other account of the User, including the bank account of such
              User, or of any other User or person, other that as part of the winnings of a User in any Contest(s). In
              case
              the User terminates his/her account with Sports.cards or such account if terminated by Sports.cards, all
              Cash
              Bonus granted to the user shall return to Sports.cards and the User shall not have any right or interest
              on
              such points.</span></li>
          <li className="c4"><span className="c0">All Cash Bonus credited in the User account shall be valid for a
              period of 28
              days
              from the date of credit. The unutilized Cash Bonus shall expire at the end of 28 days from the date of
              credit.</span></li>
          <li className="c4"><span className="c0">Users agree that once they confirm a transaction on Sports.cards, they
              shall
              be
              bound by and make payment for that transaction.</span></li>
          <li className="c4"><span className="c0">The User acknowledges that subject to time taken for bank
              reconciliations and
              such
              other external dependencies that Sports.cards has on third parties, any transactions on Sports.cards
              Platform
              may take up to 24 hours to be processed. Any amount paid or transferred into the User&#39;s &#39;Cash&#39;
              Account or Winnings Account may take up to 24 hours to reflect in the User&#39;s &#39;Cash&#39; Account or
              Winnings Account balance. Similarly, the utilization of the Cash Bonus or money debited from
              &#39;Cash&#39;
              Account or Winnings Account may take up to 24 hours to reflect in the User&#39;s &#39;Cash&#39; Account or
              Winnings Account balance. Users agree not to raise any complaint or claim against Sports.cards in respect
              of
              any delay, including any lost opportunity to join any Contest or match due to delay in crediting of
              transaction amount into any of the User&#39;s accounts<br />A transaction, once confirmed, is final and no
              cancellation is permissible.<br />Sports.cards may, in certain exceptional circumstances and at its sole
              and
              absolute discretion, refund the amount to the User after deducting applicable cancellation charges and
              taxes.
              At the time of the transaction, Users may also be required to take note of certain additional terms and
              conditions and such additional terms and conditions shall also govern the transaction. To the extent that
              the
              additional terms and conditions contain any clause that is conflicting with the present terms and
              conditions,
              the additional terms and conditions shall prevail.</span></li>
        </ol>
        <ol className="c7 lst-kix_mttsevi03hc3-0" start="14">
          <li className="c18 c3 c21"><span className="c20">
              <h4>
                Tabulation of fantasy points

              </h4><br />
            </span><span className="c0">Sports.cards
              may
              obtain the score feed and other information required for the computation and tabulation of fantasy points
              from
              third party service provider(s) and/or official website of the match organiser. In the rare event that any
              error in the computation or tabulation of fantasy points, selection of winners, abandonment of a match
              etc.,
              as a result of inaccuracies in or incompleteness of the feed provided by the third party service provider
              and/or official website of the match organiser comes to its attention, Sports.cards shall use best efforts
              to
              rectify such error prior to the distribution of prizes. However, Sports.cards hereby clarifies that it
              relies
              on the accuracy and completeness of such third party score/statistic feeds and does not itself warrant or
              make
              any representations concerning the accuracy thereof and, in any event, shall take no responsibility for
              inaccuracies in computation and tabulation of fantasy points or the selection of winners as a result of
              any
              inaccurate or incomplete scores/statistics received from such third party service provider. Users and
              Participants agree not to make any claim or raise any complaint against Sports.cards in this
              respect.</span>
          </li>

          <h4>
            15. Selection and Verification of Winners and Conditions relating to the Prizes

          </h4>
        </ol>
        <ol className="c7 lst-kix_mttsevi03hc3-1 start" start="1">
          <li className="c1"><span className="c20">Selection of Winners<br /></span><span className="c0">Winners will be
              decided on
              the
              basis of the scores of the Cards in a designated match (which may last anywhere between one day and an
              entire
              tournament) of the Contest(s). The Participant(s) owning the Card(s) with the highest aggregate point in a
              particular match shall be declared the Winner(s). In certain pre-specified Contests, Sports.cards may
              declare
              more than one Winner and distribute prizes to such Winners in increasing order of their Card&#39;s
              aggregate
              score at the end of the designated match of the Contest. The contemplated number of Winners and the prize
              due
              to each Winner in such Contest shall be as specified on the Contest page prior to the commencement of the
              Contest.<br />Participants creating Cards on behalf of any other Participant or person shall be
              disqualified.<br />In the event of a tie, the winning Participants shall be declared Winners and the prize
              shall
              be equally divided among such Participants.<br />Sports.cards shall not be liable to pay any prize if it
              is
              discovered that the Winner(s) have not abided by these Terms and Conditions, and other rules and
              regulations
              in relation to the use of the Sports.cards, Contest, &quot;Fantasy Rules&quot;, etc.</span></li>
          <li className="c1"><span className="c20">Contacting Winners<br /></span><span className="c0">Winners shall be
              contacted by
              Sports.cards or the third party conducting the Contest on the e-mail address provided at the time of
              registration. The verification process and the documents required for the collection of prize shall be
              detailed to the Winners at this stage. As a general practice, winners will be required to provide
              following
              documents:</span></li>
        </ol>
        <ol className="c7 lst-kix_mttsevi03hc3-2 start" start="1">
          <li className="c4"><span className="c0">Photocopy of the User&#39;s PAN card;</span></li>
          <li className="c4"><span className="c0">Photocopy of a government-issued residence proof;</span></li>
          <li className="c4"><span className="c0">User&#39;s bank account details and proof of the same.</span></li>
        </ol>
        <ol className="c7 lst-kix_mttsevi03hc3-1" start="3">
          <li className="c1"><span className="c0">Sports.cards shall not permit a Winner to withdraw his/her
              prize(s)/accumulated
              winnings unless the above-mentioned documents have been received and verified within the time-period
              stipulated by Sports.cards. The User represents and warrants that the documents provided in the course of
              the
              verification process are true copies of the original documents to which they
              relate.<br /><br />Participants
              are
              required to provide proper and complete details at the time of registration. Sports.cards shall not be
              responsible for communications errors, commissions or omissions including those of the Participants due to
              which the results may not be communicated to the Winner.<br /><br />The list of Winners shall be posted on
              a
              separate web-page on the Sports.cards. The winners will also be intimated by e-mail.<br /><br />In the
              event
              that
              a Participant has been declared a Winner on the abovementioned web-page but has not received any
              communication
              from Sports.cards, such Participant may contact Sports.cards within the time specified on the
              webpage.</span>
          </li>
          <li className="c1"><span className="c20">Verification process<br /></span><span className="c0">Only those
              Winners who
              successfully complete the verification process and provide the required documents within the time limit
              specified by Sports.cards shall be permitted to withdraw/receive their accumulated winnings (or any part
              thereof). Sports.cards shall not entertain any claims or requests for extension of time for submission of
              documents.<br />Sports.cards shall scrutinize all documents submitted and may, at its sole and absolute
              discretion, disqualify any Winner from withdrawing his accumulated winnings (or any part thereof) on the
              following matchs:</span></li>
        </ol>
        <ol className="c7 lst-kix_mttsevi03hc3-2 start" start="1">
          <li className="c4"><span className="c0">Determination by Sports.cards that any document or information
              submitted by
              the
              Participant is incorrect, misleading, false, fabricated, incomplete or illegible; or</span></li>
          <li className="c4"><span className="c0">Participant does not fulfill the Eligibility Criteria specified in
              Clause 10
              above; or</span></li>
          <li className="c4"><span className="c0">Any other match.</span></li>
        </ol>
        <ol className="c7 lst-kix_mttsevi03hc3-1" start="5">
          <li className="c1"><span className="c20">Taxes Payable<br /></span><span className="c0">All prizes shall be
              subject to
              deduction
              of tax (&quot;TDS&quot;) as per the Income Tax Act 1961. As of April 1, 2018, the TDS rate prescribed by
              the
              Government of India with respect to any prize money amount that is in excess of Rs. 10,000/- is 30% of the
              total prize money amount. In case of any revisions by the Government of India to the aforementioned rate
              in
              the future, TDS will be deducted by Sports.cards in accordance with the then current prescribed TDS rate.
              Winners will be provided TDS certificates in respect of such tax deductions. The Winners shall be
              responsible
              for payment of any other applicable tax, including but not limited to, income tax, gift tax, etc. in
              respect
              of the prize money.</span></li>
          <li className="c1"><span className="c20">Miscellaneous<br /></span><span className="c0">The decision of
              Sports.cards with
              respect to the awarding of prizes shall be final, binding and non-contestable.<br /><br />Participants
              playing
              the
              paid formats of the Contest(s) confirm that they are not residents of any of the following Indian states -
              Assam, Odisha, Sikkim, Nagaland, Telangana or Andhra Pradesh. If it is found that a Participant playing
              the
              paid formats of the Contest(s) is a resident of any of the abovementioned states, Sports.cards shall
              disqualify such Participant and forfeit any prize won by such Participant. Further Sports.cards may, at
              its
              sole and absolute discretion, suspend or terminate such Participant&#39;s account with Sports.cards. Any
              amount remaining unused in the User&#39;s Game Account or Winnings Account on the date of deactivation or
              deletion shall be reimbursed to the User by an online transfer to the User&#39;s bank account on record
              with
              Sports.cards, subject to the processing fee (if any) applicable on such transfers as set out
              herein.<br /><br />If
              it is found that a Participant playing the paid formats of the Contest(s) is under the age of eighteen
              (18),
              Sports.cards shall be entitled, at its sole and absolute discretion, to disqualify such Participant and
              forfeit his/her prize. Further, Sports.cards may, at its sole and absolute discretion, suspend or
              terminate
              such Participant&#39;s account.<br /><br />To the extent permitted by law, Sports.cards makes no
              representations
              or warranties as to the quality, suitability or merchantability of any prizes and shall not be liable in
              respect of the same.<br /><br />Sports.cards may, at its sole and absolute discretion, vary or modify the
              prizes
              being offered to winners. Participants shall not raise any claim against Sports.cards or question its
              right to
              modify such prizes being offered, prior to closure of the Contest.<br /><br />Sports.cards will not bear
              any
              responsibility for the transportation or packaging of prizes to the respective winners. Sports.cards shall
              not
              be held liable for any loss or damage caused to any prizes at the time of such
              transportation.<br /><br />The
              Winners shall bear the shipping, courier or any other delivery cost in respect of the
              prizes.<br /><br />The
              Winners shall bear all transaction charges levied for delivery of cash prizes.<br /><br />All prizes are
              non-transferable and non-refundable. Prizes cannot be exchanged / redeemed for cash or kind. No cash
              claims
              can be made in lieu of prizes in kind.</span></li>
        </ol>
        <ol className="c7 lst-kix_mttsevi03hc3-0" start="16">
          <li className="c21 c18 c3"><span className="c20">
              <h4>Publicity</h4><br />
            </span><span className="c0">Acceptance of a
              prize by the
              Winner
              constitutes permission for Sports.cards, and its affiliates to use the Winner&#39;s name, likeness, voice
              and
              comments for advertising and promotional purposes in any media worldwide for purposes of advertising and
              trade
              without any further permissions or consents and / or additional compensation whatsoever.<br />The Winners
              further undertake that they will be available for promotional purposes as planned and desired by
              Sports.cards
              without any charge. The exact dates remain the sole discretion of Sports.cards. Promotional activities may
              include but not be limited to press events, internal meetings and ceremonies/functions.</span></li>
          <li className="c21 c18 c3"><span className="c20">
              <h4>General Conditions</h4><br />
            </span><span className="c0">If it
              comes to the
              notice
              of Sports.cards that any governmental, statutory or regulatory compliances or approvals are required for
              conducting any Contest(s) or if it comes to the notice of Sports.cards that conduct of any such Contest(s)
              is
              prohibited, then Sports.cards shall withdraw and / or cancel such Contest(s) without prior notice to any
              Participants or winners of any Contest(s). Users agree not to make any claim in respect of such
              cancellation
              or withdrawal of the Contest or Contest it in any manner.<br /><br />Employees, directors, affiliates,
              relatives
              and family members of Sports.cards, will not be eligible to participate in any Contest(s).</span></li>
          <h4>18. Dispute and Dispute Resolution
          </h4>
        </ol>
        <ol className="c7 lst-kix_mttsevi03hc3-1 start" start="1">
          <li className="c1"><span className="c0">The courts of competent jurisdiction at Mumbai shall have exclusive
              jurisdiction
              to determine any and all disputes arising out of, or in connection with, the Sports.cards Services
              provided by
              Sports.cards (including the Contest(s)), the construction, validity, interpretation and enforceability of
              these Terms and Conditions, or the rights and obligations of the User(s) (including Participants) or
              Sports.cards, as well as the exclusive jurisdiction to grant interim or preliminary relief in case of any
              dispute referred to arbitration as given below. All such issues and questions shall be governed and
              construed
              in accordance with the laws of the Republic of India.</span></li>
          <li className="c1"><span className="c0">In the event of any legal dispute (which may be a legal issue or
              question)
              which
              may arise, the party raising the dispute shall provide a written notification (&quot;Notification&quot;)
              to
              the other party. On receipt of Notification, the parties shall first try to resolve the dispute through
              discussions. In the event that the parties are unable to resolve the dispute within fifteen (15) days of
              receipt of Notification, the dispute shall be settled by arbitration.</span></li>
          <li className="c1"><span className="c0">The place of arbitration shall be Mumbai, India. All arbitration
              proceedings
              shall
              be conducted in English and in accordance with the provisions of the Arbitration and Conciliation Act,
              1996,
              as amended from time to time.</span></li>
          <li className="c1"><span className="c0">The arbitration award will be final and binding on the Parties, and
              each Party
              will bear its own costs of arbitration and equally share the fees of the arbitrator unless the arbitral
              tribunal decides otherwise. The arbitrator shall be entitled to pass interim orders and awards, including
              the
              orders for specific performance and such orders would be enforceable in competent courts. The arbitrator
              shall
              give a reasoned award.</span></li>
          <li className="c1"><span className="c0">Nothing contained in these Terms and Conditions shall prevent
              Sports.cards
              from
              seeking and obtaining interim or permanent equitable or injunctive relief, or any other relief available
              to
              safeguard Sports.cards&#39;s interest prior to, during or following the filing of arbitration proceedings
              or
              pending the execution of a decision or award in connection with any arbitration proceedings from any court
              having jurisdiction to grant the same. The pursuit of equitable or injunctive relief shall not constitute
              a
              waiver on the part of Sports.cards to pursue any remedy for monetary damages through the arbitration
              described
              herein.</span></li>
        </ol>
        <h4>19. Release and Limitations of Liability
        </h4>
        <ol className="c7 lst-kix_mttsevi03hc3-1 start" start="1">
          <li className="c1"><span className="c0">Users shall access the Sports.cards Services provided on Sports.cards
              voluntarily
              and at their own risk. Sports.cards shall, under no circumstances be held responsible or liable on account
              of
              any loss or damage sustained (including but not limited to any accident, injury, death, loss of property)
              by
              Users or any other person or entity during the course of access to the Sports.cards Services (including
              participation in the Contest(s)) or as a result of acceptance of any prize.</span></li>
          <li className="c1"><span className="c0">By entering the contests and accessing the Sports.
              <p>cards Services
                provided
                therein,
                Users hereby release from and agree to indemnify Sports.cards, and/ or any of its directors, employees,
                partners, associates and licensors, from and against all liability, cost, loss or expense arising out
                their
                access to the Sports.cards Services including (but not limited to) personal injury and damage to
                property
                and
                whether direct, indirect, consequential, foreseeable, due to some negligent act or omission on their
                part,
                or
                otherwise.</p>
            </span></li>
          <li className="c1"><span className="c0">Sports.cards accepts no liability, whether jointly or severally, for
              any
              errors or
              omissions, whether on behalf of itself or third parties in relation to the prizes.</span></li>
          <li className="c1"><span className="c0">Users shall be solely responsible for any consequences which may arise
              due to
              their access of Sports.cards Services by conducting an illegal act or due to non-conformity with these
              Terms
              and Conditions and other rules and regulations in relation to Sports.cards Services, including provision
              of
              incorrect address or other personal details. Users also undertake to indemnify Sports.cards and their
              respective officers, directors, employees and agents on the happening of such an event (including without
              limitation cost of attorney, legal charges etc.) on full indemnity basis for any loss/damage suffered by
              Sports.cards on account of such act on the part of the Users.</span></li>
          <li className="c1"><span className="c0">Users shall indemnify, defend, and hold Sports.cards harmless from any
              third
              party/entity/organization claims arising from or related to such User&#39;s engagement with the
              Sports.cards
              or participation in any Contest. In no event shall Sports.cards be liable to any User for acts or
              omissions
              arising out of or related to User&#39;s engagement with the Sports.cards or his/her participation in any
              Contest(s).</span></li>
          <li className="c1"><span className="c0">In consideration of Sports.cards allowing Users to access the
              Sports.cards
              Services, to the maximum extent permitted by law, the Users waive and release each and every right or
              claim,
              all actions, causes of actions (present or future) each of them has or may have against Sports.cards, its
              respective agents, directors, officers, business associates, group companies, sponsors, employees, or
              representatives for all and any injuries, accidents, or mishaps (whether known or unknown) or (whether
              anticipated or unanticipated) arising out of the provision of Sports.cards Services or related to the
              Contests
              or the prizes of the Contests.</span></li>
        </ol>
        <h4>20. Disclaimers</h4>
        <ol className="c7 lst-kix_mttsevi03hc3-1 start" start="1">
          <li className="c1"><span className="c0">To the extent permitted under law, neither Sports.cards nor its
              parent/holding
              company, subsidiaries, affiliates, directors, officers, professional advisors, employees shall be
              responsible
              for the deletion, the failure to store, the mis-delivery, or the untimely delivery of any information or
              material.</span></li>
          <li className="c1"><span className="c0">To the extent permitted under law, Sports.cards shall not be
              responsible for
              any
              harm resulting from downloading or accessing any information or material, the quality of servers, games,
              products, Sports.cards services or sites, cancellation of competition and prizes. Sports.cards disclaims
              any
              responsibility for, and if a User pays for access to one of Sports.cards&#39;s Services the User will not
              be
              entitled to a refund as a result of, any inaccessibility that is caused by Sports.cards&#39;s maintenance
              on
              the servers or the technology that underlies our sites, failures of Sports.cards&#39;s service providers
              (including telecommunications, hosting, and power providers), computer viruses, natural disasters or other
              destruction or damage of our facilities, acts of nature, war, civil disturbance, or any other cause beyond
              our
              reasonable control. In addition, Sports.cards does not provide any warranty as to the content on the
              Sports.cards(s). Sports.cards(s) content is distributed on an &quot;as is, as available&quot;
              basis.</span>
          </li>
          <li className="c1"><span className="c0">Any material accessed, downloaded or otherwise obtained through
              Sports.cards
              is
              done at the User&#39;s discretion, competence, acceptance and risk, and the User will be solely
              responsible
              for any potential damage to User&#39;s computer system or loss of data that results from a User&#39;s
              download
              of any such material.</span></li>
          <li className="c1"><span className="c0">Sports.cards shall make best endeavours to ensure that the
              Sports.cards(s) is
              error-free and secure, however, neither Sports.cards nor any of its partners, licensors or associates
              makes
              any warranty that:</span></li>
        </ol>
        <ol className="c7 lst-kix_mttsevi03hc3-2 start" start="1">
          <li className="c4"><span className="c0">the Sports.cards(s) will meet Users&#39; requirements,</span></li>
          <li className="c4"><span className="c0">Sports.cards(s) will be uninterrupted, timely, secure, or error
              free</span>
          </li>
          <li className="c4"><span className="c0">the results that may be obtained from the use of Sports.cards(s) will
              be
              accurate
              or reliable; and</span></li>
          <li className="c4"><span className="c0">the quality of any products, Sports.cards Services, information, or
              other
              material
              that Users purchase or obtain through Sports.cards(s) will meet Users&#39; expectations.</span></li>
        </ol>
        <ol className="c7 lst-kix_mttsevi03hc3-1" start="5">
          <li className="c1"><span className="c0">In case Sports.cards discovers any error, including any error in the
              determination
              of Winners or in the transfer of amounts to a User&#39;s account, Sports.cards reserves the right
              (exercisable
              at its discretion) to rectify the error in such manner as it deems fit, including through a set-off of the
              erroneous payment from amounts due to the User or deduction from the User&#39;s account of the amount of
              erroneous payment. In case of exercise of remedies in accordance with this clause, Sports.cards agrees to
              notify the User of the error and of the exercise of the remedy(ies) to rectify the same.</span></li>
          <li className="c1"><span className="c0">To the extent permitted under law, neither Sports.cards nor its
              partners,
              licensors or associates shall be liable for any direct, indirect, incidental, special, or consequential
              damages arising out of the use of or inability to use our sites, even if we have been advised of the
              possibility of such damages.</span></li>
          <li className="c1"><span className="c0">Any Sports.cards Services, events or Contest(s) being hosted or
              provided, or
              intended to be hosted on Sports.cards platform and requiring specific permission or authority from any
              statutory authority or any state or the central government, or the board of directors shall be deemed
              cancelled or terminated, if such permission or authority is either not obtained or denied either before or
              after the availability of the relevant Sports.cards Services, events or Contest(s) are hosted or
              provided.</span></li>
          <li className="c1"><span className="c9">To the extent permitted under law, in the event of suspension or
              closure of
              any
              Services, events or Contests, Users (including Participants) shall not be entitled to make any demands,
              claims, on any nature whatsoever.</span></li>
        </ol>
        <h4>21. Miscellaneous</h4>
        <ol className="c7 lst-kix_mttsevi03hc3-1 start" start="1">
          <li className="c1"><span className="c0">Sports.cards may be required under certain legislations, to notify
              User(s) of
              certain events. User(s) hereby acknowledge and consent that such notices will be effective upon
              Sports.cards
              posting them on the Sports.cards or delivering them to the User through the email address provided by the
              User
              at the time of registration. User(s) may update their email address by logging into their account on the
              Sports.cards. If they do not provide Sports.cards with accurate information, Sports.cards cannot be held
              liable for failure to notify the User.</span></li>
          <li className="c1"><span className="c9">Sports.cards shall not be liable for any delay or failure to perform
              resulting
              from causes outside its reasonable control, including but not limited to any failure to perform due to
              unforeseen circumstances or cause beyond Sports.cards&#39;s control such as acts of God, war, terrorism,
              riots, embargoes, acts of civil or military authorities, fire, floods, accidents, network infrastructure
              failures, strikes, or shortages of transportation facilities, fuel, energy, labor or materials or any
              cancellation of any cricket</span><span className="c9 c22">&nbsp;</span><span className="c0">match to
              which a
              Contest
              relates. In such circumstances, Sports.cards shall also be entitled to cancel any related Contest(s) and
              to
              process an appropriate refund for all Participants.</span></li>
          <li className="c1"><span className="c0">Sports.cards&#39;s failure to exercise or enforce any right or
              provision of
              these
              Terms and Conditions shall not constitute a waiver of such right or provision.</span></li>
          <li className="c1"><span className="c0">Users agree that regardless of any statute or law to the contrary, any
              claim
              or
              cause of action arising out of or related to use of the Sports.cards or these Terms must be filed within
              thirty (30) days of such claim or cause of action arising or be forever barred.</span></li>
          <li className="c1"><span className="c9">These Terms and Conditions, including all terms, conditions, and
              policies that
              are
              incorporated herein by reference, constitute the entire agreement between the User(s) and Telconi
              Trading</span><span className="c9">&nbsp;Private Limited </span><span className="c0">and govern your use
              of the
              Sports.cards, superseding any prior agreements that any User may have with Telconi Trading Private
              Limited.</span></li>
          <li className="c1"><span className="c0">If any part of these Terms and Conditions is determined to be
              indefinite,
              invalid,
              or otherwise unenforceable, the rest of these Terms and Conditions shall continue in full force.</span>
          </li>
          <li className="c1"><span className="c0">Sports.cards reserves the right to moderate, restrict or ban the use
              of the
              Sports.cards, specifically to any User, or generally, in accordance with Sports.cards&#39;s
              policy/policies
              from time to time, at its sole and absolute discretion and without any notice.</span></li>
          <li className="c1"><span className="c0">Sports.cards may, at its sole and absolute discretion, permanently
              close or
              temporarily suspend any Sports.cards Services (including any Contest(s)).</span></li>
          <li className="c1"><span className="c0">Sports.cards may from time to time conduct/organize, promotions/offers
              on the
              platform. Any two or more promotions cannot be clubbed together with any other promotions that are running
              simultaneously on the Sports.cards platform. Also, promotions/offers cannot be clubbed with Cash Bonus
              available with any user.</span></li>
        </ol>
      </Card.Body>
    </Card>

  </div>
  </Col>
</Layout>
)
}
export default TermsAndConditions;