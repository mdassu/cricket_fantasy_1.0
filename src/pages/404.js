import React, { Component } from 'react';
import Layout from 'components/Layout/Layout';
import Link from "next/link";
class ErrorPage extends Component {

  constructor(props) {
    super(props);;
    this.state = {
    };
  }

  render() {
    return (
      <Layout header="homeHeader">
        <div className="no-data text-center">
          <h6>Page not Found</h6>
          <img src="static/images/empty.png" className="" />
          <div className="create-pack">
            <Link href="/">
              <a title="Back to Home">Home Page</a>
            </Link>
          </div>

        </div>
      </Layout>
    );
  }
}

export default ErrorPage;