import React, { Component } from "react";
import Cookies from "js-cookie";
import Link from 'next/link';
import { Container, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import { Snackbar, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import Layout from 'components/Layout/Layout';
import { ContestsService } from "_services/ContestsService";
import { decrypt } from "_helper/EncrDecrypt";
import Router from "next/router";
import * as moment from 'moment';
import NoData from "components/Contests/NoData";
import millify from "millify";
import Loader from "components/CommonComponents/Loader";

class ViewAllWinners extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoaded: false
    }
  }

  componentDidMount() {
    this.getContestWinners();
  }

  getContestWinners = () => {
    var params = {
      contest_id: decrypt(Router.query.c)
    }
    ContestsService.getContestWinner(params, (res) => {
      if (res.status) {
        this.setState({
          data: res['data'],
          isLoaded: true
        });
      } else {
        this.setState({
          data: [],
          isLoaded: true
        });
      }
    })
  }

  render() {
    const { data, isLoaded } = this.state;
    return (
      <React.Fragment>
        <Layout header="innerHeader" headerText="Contest Winners">
          {
            isLoaded ? (
              <div className="" >
                <Row>
                  <Col lg={12}>
                    <div className="outer-card ">
                      <Card className="rounded-0 all-winner-content mb-3">
                        <Card.Body>
                          <div className="float-left content-number">{data.contestData.contest_name}</div>
                          <div className="ipl-matches float-right">{moment(data.contestData.match_date).format('DD MMM, YYYY')}</div>
                          <div className="clearfix"></div>
                          <div className="team-listing ">
                            {/* <Link href="/contests"> */}
                            <Row className="justify-content-center align-items-center">
                              <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                                <div className="team-logo team-left">
                                  <div className="logo-line line-left">
                                  </div>
                                  <div className="team-name team-left-name" style={{ background: data.contestData.home_team_color }}>
                                    {data.contestData.home_team_name}
                                  </div>
                                </div>
                              </Col>
                              <Col lg={4} xs={4} md={4} sm={4} className="text-center p-0">
                                <div className="ipl-match-number">
                                  <span>{data.contestData.matchNumber}</span>
                                </div>
                                <img src="static/images/vs.svg" className="team-vs" />
                              </Col>
                              <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                                <div className="team-logo team-right">
                                  <div className="logo-line line-right">
                                  </div>
                                  <div className="team-name team-right-name" style={{ background: data.contestData.away_team_color }}>
                                    {data.contestData.away_team_name}
                                  </div>
                                </div>
                              </Col>
                            </Row>
                            {/* </Link> */}
                          </div>
                        </Card.Body>
                        <div className="winners-card">
                          <Link href="/">
                            <a title=" View Winning Card"> View Winning Card</a>
                          </Link>
                        </div>
                      </Card>
                      {/* total Rank loop name wise */}
                      <div className="view-all-winner-div">
                        <Col lg={12} className="p-1 ">
                          {data.winnerModel.map((user, key) => (
                            <Card>
                              <Card.Body>
                                <Row className="justify-content-center align-items-center">
                                  <Col lg={2} xs={3} sm={4} md={4} className="text-left">
                                    <Card.Img variant="top" src="static/images/winner.jpg" />
                                  </Col>
                                  <Col lg={10} xs={9} sm={8} md={8} className="text-left p-0">
                                    <Card.Title>{user.first_name} {user.last_name}</Card.Title>
                                    <Card.Text>
                                      {/* <small> West Bengal</small> */}
                                      <span>{user.pack_name}</span>
                                    </Card.Text>
                                  </Col>
                                </Row>
                              </Card.Body>
                              <Card.Footer className="view-all-footer">
                                <Row>
                                  <Col lg={6} xs={6} sm={6} md={6} className="text-left">
                                    Rank
                                    <br />
                                    <span>#{user.rank && user.rank != null ? user.rank : 0}</span>
                                  </Col>
                                  <Col lg={6} xs={6} sm={6} md={6} className="text-right">
                                    Amount Won
                                    <br />
                                    <span>&#x20B9; {user.won_amount && user.won_amount != null ? millify(user.won_amount) : 0}</span>
                                  </Col>
                                </Row>
                              </Card.Footer>
                            </Card>
                          ))}
                        </Col>
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
            ) : <Loader />
          }
        </Layout>
      </React.Fragment>
    )
  }
}
export default ViewAllWinners;