import React, { Component } from 'react';
import Cookies from 'js-cookie';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Snackbar from '@material-ui/core/Snackbar';
import { useTheme, withStyles } from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
// import TabPanel from '@material-ui/lab/TabPanel';
import Box from '@material-ui/core/Box';
import NoData from "components/Contests/NoData";
import MyPacks from "components/Contests/MyPacks";
import { Container, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import TextField from '@material-ui/core/TextField';
import Layout from 'components/Layout/Layout';
import Router, { useRouter } from 'next/router';
import { ContestsService } from '_services/ContestsService';
import { PacksService } from '_services/PacksService';
import { decrypt, encrypt } from '_helper/EncrDecrypt';
import Countdown from 'react-countdown';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import { TransactionService } from '_services/TransactionService';
import MuiAlert from '@material-ui/lab/Alert';

import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import { AuthenticationService } from '_services/AuthenticationService';
import Loader from 'components/CommonComponents/Loader';


import { Steps, Hints } from "intro.js-react";
import "intro.js/introjs.css";

import $ from 'jquery';



const styles = {
  tabPanelRoot: 'tabPanelRoot',
  tabPanelText: 'tabPanelText',
  tabContainer: 'tabContainer'
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

class CreatePacks extends Component {

  constructor(props) {
    super(props);
    this.handleChangeIndex = this.handleChangeIndex.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      matchData: {},
      categories: [],
      marketSelections: [],
      selectedCredit: [],
      selectedCategoryCount: {},
      // selectedCreditPreview: [],
      selectedCreditPotential: {},
      totalCredit: 0,
      remainCredit: 0,
      totalSelectedCredit: 0,
      totalSelectedPotential: 0,
      value: 0,
      creditOpen: false,
      errorMsg: '',
      previewsModal: false,
      continueEnable: false,
      // isLoggedIn: false,
      goBackModalOpen: false,
      minSelection: 1,
      isLoaded: false,
      confimChange: false,
      totalCounter: 5,
      minutes: 5,
      seconds: 0,
      matchInterval: 0,
      alert: {
        toast: false,
        toastMessage: '',
        severity: ''
      },
      stepsEnabled: false,
      initialStep: 0,
      steps: [
        {
          element: ".eventStep",
          intro: "<div class='steptext'><b>Event</b> <p> A particular event of the sport. Eg. Match win </p></div>"
        },
        {
          element: ".pickStep",
          intro: "<div class='steptext'><b>Pick</b><p> A probable outcome of the event. Eg. India wins </p></div>"
        },
        {
          element: ".multiplierStep",
          intro: "<div class='steptext'><b>Multiplier</b><p> The weightage given to the outcome of the event</p></div>"
        },
        {
          element: ".potentialStep",
          intro: "<div class='steptext'><b>Potential point</b><p> Multiplier x Credits entered ( If the outcome occurs)</p></div>"
        },
        {
          element: ".selectionsStep",
          intro: "<div class='steptext'><b>Selections </b><p>The no. of picks that you have made (Max. 4)</p></div>"
        },
        {
          element: ".creditStep",
          intro: "<div class='steptext'><b>Credit</b><p> The credits available to distribute among your picks</p></div>"
        }
      ],
      hintsEnabled: true,
      hints: [
        {
          element: ".hello",
          hint: "Hello hint",
          hintPosition: "middle-right"
        }
      ]
    };
    this.timer = 0;
  }

  componentDidMount() {
    // window.addEventListener('popstate', (event) => {
    //   AuthenticationService.clearCookies();
    // });
    if (!Cookies.get('_guide')) {
      this.setState({
        stepsEnabled: true
      });
    }
    this.getMatchDetails();
    this.getPackMarkets();
    this.startCounter();


    // $(window).scroll(function () {
    //   if ($(window).scrollTop() > 100) {

    //     $(".create-packs-tabs").addClass("create_packs_tabs_scroll");
    //   }
    //   else {

    //     $(".create-packs-tabs").removeClass("create_packs_tabs_scroll");
    //   }
    // });


  }

  startCounter = () => {
    this.timer = setInterval(() => {
      if (this.state.minutes == 0 && this.state.seconds == 0) {
        this.setState({
          confimChange: true
        });
      } else {
        if (this.state.seconds == 0) {
          this.setState({
            minutes: this.state.minutes - 1,
            seconds: 59
          });
        } else {
          this.setState({
            seconds: this.state.seconds - 1
          });
        }
      }
    }, 1000);
  }

  getMatchDetails() {
    clearInterval(this.state.matchInterval)
    var params = {
      match_id: decrypt(Router.query.m)
    }
    ContestsService.getMatchDetails(params, res => {
      if (res.status) {
        if (res['data']['match_status'] != 2) {
          if (res['data']['is_Live'] == 1) {
            this.setState({
              matchData: res['data'],
              minSelection: 4,
            });
            this.setState({
              matchInterval: setInterval(() => {
                this.getMatchDetails();
              }, 10000)
            })
          } else {
            this.setState({
              matchData: res['data'],
              minSelection: 1,
            });
          }
        } else {
          Router.push('/');
        }
      } else {
        this.setState({
          matchData: {}
        });
      }
    });
  }

  getPackMarkets = () => {
    var params = {
      match_id: decrypt(Router.query.m)
    }
    PacksService.getPacksMarket(params, res => {
      if (res.status) {
        var data = res['data'];
        this.setState({
          categories: data.cat_data,
          selectedCategoryCount: data.cat_data,
          marketSelections: data.cat_market_data,
          totalCredit: data.total_credits,
          remainCredit: data.total_credits,
          confimChange: false,
          isLoaded: true
        }, () => {
          if (Cookies.get('_pack')) {
            var data = decrypt(Cookies.get('_pack'));
            this.setState({
              selectedCredit: data.selectedCredit,
              selectedCreditPotential: data.selectedCreditPotential,
              selectedCategoryCount: data.selectedCategoryCount,
              totalSelectedCredit: data.totalSelectedCredit,
              remainCredit: data.remainCredit,
              totalSelectedPotential: data.totalSelectedPotential,
              continueEnable: true,
              errorMsg: ''
            });
          }
          if (this.state.selectedCredit.length > 0) {
            var selectedCredit = this.state.selectedCredit;
            this.setState({
              selectedCredit: [],
              selectedCreditPotential: {},
              totalSelectedCredit: 0,
              remainCredit: 0,
              totalSelectedPotential: 0,
              continueEnable: false,
              isLoaded: false
            }, () => {
              selectedCredit.map((item, key) => {
                setTimeout(() => {
                  var sData = [];
                  this.state.marketSelections.filter(categoryData => {
                    if (categoryData.category_id == item.category_id) {
                      categoryData.market_data.filter(marketData => {
                        if (marketData.market_id == item.market_id) {
                          sData = marketData.selection_data.filter(selectionData => {
                            return selectionData.selection_id == item.selection_id
                          });
                        }
                      });
                    }
                  });
                  if (sData.length > 0) {
                    this.handleCreditChange(0, item.category_id, item.market_id, item.selection_id, sData[0].multiplier, item.marketName, item.selectionName, item.credit)
                  }
                  if (key == selectedCredit.length - 1) {
                    this.setState({
                      isLoaded: true
                    })
                  }
                }, 50);
              });
            });
          } else {
            this.setState({
              isLoaded: true
            })
          }
        });
      } else {
        this.setState({
          matchData: {},
          isLoaded: true
        });
      }
    });
  }

  handleCreditChange = (matchId, categoryId, marketId, selectionId, multiplier, marketName, selectionName, credit) => {
    if (credit && credit != 0) {
      if (credit <= this.state.totalCredit && credit <= 25) {
        var credit = parseFloat(credit).toFixed(2);
        if (this.state.selectedCredit.length > 0) {
          var i = 0;
          this.state.selectedCredit.map((data, key) => {
            if (data.selection_id == selectionId) {
              if (credit <= this.state.remainCredit + this.state.selectedCreditPotential['credit' + selectionId]) {
                var selectionPotaintials = this.state.selectedCreditPotential;
                selectionPotaintials['selection' + selectionId] = parseFloat((credit * multiplier).toFixed(2));
                selectionPotaintials['credit' + selectionId] = parseFloat(credit);
                this.setState({
                  selectedCreditPotential: selectionPotaintials
                });
              }
              data.credit = parseFloat(credit);
              data.potential = parseFloat((credit * multiplier).toFixed(2));
              // var previewPack = this.state.selectedCreditPreview;
              // previewPack.find(data => data.selectionId == selectionId).credit = parseFloat(credit);
              // previewPack.find(data => data.selectionId == selectionId).potential = parseFloat((credit * multiplier).toFixed(2));
              i++;
            }
          });
          if (i == 0) {
            if (credit <= this.state.remainCredit) {
              this.state.selectedCredit.push({
                category_id: categoryId,
                market_id: marketId,
                selection_id: selectionId,
                marketName: marketName,
                selectionName: selectionName,
                multiplier: multiplier,
                credit: parseFloat(credit),
                potential: parseFloat((credit * multiplier).toFixed(2))
              });
              var selectionPotaintials = this.state.selectedCreditPotential;
              selectionPotaintials['selection' + selectionId] = parseFloat((credit * multiplier).toFixed(2));
              selectionPotaintials['credit' + selectionId] = parseFloat(credit);
              this.setState({
                selectedCreditPotential: selectionPotaintials
              });
              var categoryCount = this.state.selectedCategoryCount;
              if (categoryCount.find(o => o.category_id == categoryId).count) {
                var count = categoryCount.find(o => o.category_id == categoryId).count + 1;
                categoryCount.find(o => o.category_id == categoryId).count = count;
              } else {
                categoryCount.find(o => o.category_id == categoryId).count = 1;
              }
              this.setState({
                selectedCategoryCount: categoryCount
              });
            }

          }

        } else {
          this.state.selectedCredit.push({
            category_id: categoryId,
            market_id: marketId,
            selection_id: selectionId,
            marketName: marketName,
            selectionName: selectionName,
            multiplier: multiplier,
            credit: parseFloat(credit),
            potential: parseFloat((credit * multiplier).toFixed(2))
          });
          var selectionPotaintials = this.state.selectedCreditPotential;
          selectionPotaintials['selection' + selectionId] = parseFloat((credit * multiplier).toFixed(2));
          selectionPotaintials['credit' + selectionId] = parseFloat(credit);
          this.setState({
            selectedCreditPotential: selectionPotaintials
          });
          var categoryCount = this.state.selectedCategoryCount;
          categoryCount.find(o => o.category_id == categoryId).count = 1;
          this.setState({
            selectedCategoryCount: categoryCount
          });
        }
      }
      else {
        this.setState({
          alert: {
            toast: true,
            toastMessage: 'Max credit allowed for a selection is 25.',
            severity: 'error'
          }
        });
      }
    } else {

      this.state.selectedCredit.map((item, key) => {
        if (item.selection_id == selectionId) {
          this.state.selectedCredit.splice(key, 1);
          delete this.state.selectedCreditPotential['selection' + selectionId];
          delete this.state.selectedCreditPotential['credit' + selectionId];
          this.setState({
            selectedCreditPotential: this.state.selectedCreditPotential
          })
          var categoryCount = this.state.selectedCategoryCount;
          if (categoryCount.find(o => o.category_id == categoryId).count) {
            var count = categoryCount.find(o => o.category_id == categoryId).count - 1;
            categoryCount.find(o => o.category_id == categoryId).count = count;
          }
          this.setState({
            selectedCategoryCount: categoryCount
          });
        }
      });
    }

    var objs = Object.keys(this.state.selectedCreditPotential);
    var selectedTotalCredit = 0;
    var selectedTotalPotential = 0;
    if (objs.length > 0) {
      objs.map((item, key) => {
        if (item.search('credit') != -1) {
          selectedTotalCredit = selectedTotalCredit + parseFloat(this.state.selectedCreditPotential[item]);
        } else {
          selectedTotalPotential = selectedTotalPotential + parseFloat(this.state.selectedCreditPotential[item]);
        }
        if (key == objs.length - 1) {
          this.setState({
            totalSelectedCredit: selectedTotalCredit,
            totalSelectedPotential: selectedTotalPotential,
            remainCredit: this.state.totalCredit - selectedTotalCredit
          });
        }
      });
    } else {
      this.setState({
        totalSelectedCredit: 0,
        selectedTotalPotential: 0,
        remainCredit: this.state.totalCredit
      });
    }

    setTimeout(() => {
      var i = 0;
      this.state.selectedCategoryCount.map((item, key) => {
        if (item.count >= this.state.minSelection) {
          i++;
        }
      });
      if (this.state.selectedCategoryCount.length == i) {
        this.setState({
          continueEnable: true,
          errorMsg: ''
        });
      } else {
        this.setState({
          continueEnable: false
        });
      }
    }, 300);

  }

  submitPack = () => {
    var i = 0;
    this.state.selectedCategoryCount.map((item, key) => {
      if (item.count >= this.state.minSelection) {
        i++;
      }
    });
    if (this.state.selectedCategoryCount.length == i) {
      var jsonData = {
        selectedCredit: this.state.selectedCredit,
        selectedCreditPotential: this.state.selectedCreditPotential,
        selectedCategoryCount: this.state.selectedCategoryCount,
        totalSelectedCredit: this.state.totalSelectedCredit,
        remainCredit: this.state.remainCredit,
        totalSelectedPotential: this.state.totalSelectedPotential
      };
      Cookies.set('_pack', encrypt(jsonData));
      Router.push({
        pathname: '/submit-pack',
        query: { m: Router.query.m }
      });
    } else {
      this.setState({
        errorMsg: `Select at least ${this.state.matchData['is_Live'] == 1 ? `4 events` : this.state.selectedCategoryCount.length == 1 ? `${this.state.selectedCategoryCount.length} event` : `${this.state.selectedCategoryCount.length} events (1 in each tab)`}`
      });
    }
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  a11yProps(index) {
    return {
      id: `full-width-tab-${index}`,
      'aria-controls': `full-width-tabpanel-${index}`,
    };
  }

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  confimationDilogOpen = () => {
    this.setState({
      previewsModal: true
    });
  };

  previewPackClose = () => {
    this.setState({
      previewsModal: false
    });
  };

  renderer = ({ hours, minutes, seconds, completed }) => {
    if (completed) {
      this.setState({
        confimChange: true
      });
      return 0;
    } else {
      // Render a countdown
      return <span>{minutes < 10 ? '0' + minutes : minutes}:{seconds < 10 ? '0' + seconds : seconds}</span>;
    }
  };

  approveMultiplier = () => {
    this.setState({
      isLoaded: false,
      confimChange: false,
      minutes: this.state.totalCounter,
      seconds: 0
    }, () => {
      this.getPackMarkets();
      clearInterval(this.timer);
      this.startCounter();
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.matchInterval);
  }


  onExit = () => {
    Cookies.set('_guide', encrypt({ status: true }));
    this.setState(() => ({ stepsEnabled: false }));
  };





  render() {
    const { stepsEnabled, steps, initialStep, hintsEnabled, hints } = this.state;
    const { value, matchData, categories, marketSelections, alert } = this.state;
    const { theme, router } = this.props;

    const { user, popupOpen } = this.state;
    const position = this.state.isMobile ? false : true;
    return (
      <Layout customClass="createpage-class" header="packHeader" headerText="Create Card" params={router.query}>
        <Snackbar open={alert.toast} autoHideDuration={2500} onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })}>
          <Alert onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })} severity={alert.severity}>
            {alert.toastMessage}
          </Alert>
        </Snackbar>
        { this.state.isLoaded ? (
          <div className="create-packs-tabs sticky-packs-tabs">
            <div className="stickybar">
              <Row className="fantasy-pack match-pack m-0 " sticky="top">
                <Col lg={12} className="starttime text-center">


                  <Steps
                    enabled={stepsEnabled}
                    steps={steps}
                    initialStep={initialStep}
                    onExit={this.onExit}
                  />

                  {/* <Hints enabled={hintsEnabled} hints={hints} />
                  <li className="hello"></li>
                  <li className="world"></li>
                  <li className="test"></li>
                  <li className="test2"></li> */}

                  {/* <Countdown date={Date.now() + 30000} renderer={this.renderer} />  */}
                  {this.state.minutes < 10 ? '0' + this.state.minutes : this.state.minutes}:{this.state.seconds < 10 ? '0' + this.state.seconds : this.state.seconds} to new multiplier
              </Col>
                <Col lg={3} xs={3} md={3} sm={3} className="mr-25">
                  <h6 className="scoreboard-top selectionsStep">
                    Selections <br /><b>{this.state.selectedCredit ? this.state.selectedCredit.length : 0}</b>
                  </h6>
                </Col>
                <Col lg={2} xs={2} md={2} sm={2} className="text-center p-0 mrm--25">
                  <div className="my-team-name mi-team world" style={{ background: matchData.hometeamcolorcode }}>
                    {matchData.hometeamname}
                  </div>
                  <br />
                  <div className="totalscore test">
                    {matchData.home_team_runs != null ? matchData.home_team_runs : '--'}/{matchData.home_team_wickets != null ? matchData.home_team_wickets : '--'} ({matchData.home_team_overs != null ? matchData.home_team_overs : '--'})
               </div>
                </Col>
                <Col lg={2} xs={2} md={2} sm={2} className="text-center p-0">
                  <img src="static/images/whitevs.png" className="team-vs" />
                </Col>
                <Col lg={2} xs={2} md={2} sm={2} className="text-center p-0 mlm-25 ">
                  <div className="my-team-name iplteam" style={{ background: matchData.awayteamcolorcode }}>
                    {matchData.awayteamname}
                  </div>
                  <br />
                  <div className="totalscore">
                    {matchData.away_team_runs != null ? matchData.away_team_runs : '--'}/{matchData.away_team_wickets != null ? matchData.away_team_wickets : '--'} ({matchData.away_team_overs != null ? matchData.away_team_overs : '--'})
               </div>
                </Col>
                <Col lg={3} xs={3} md={3} sm={3} className="text-right ml-25">
                  <h6 className="scoreboard-top creditStep">
                    Credits <br /> <b>{this.state.remainCredit ? this.state.remainCredit : 0}</b>
                  </h6>
                </Col>
              </Row>
              {/* Fill stepper */}

              <Row className="total-top-fill-board">
                <Col lg={12}>
                  <ul className=" justify-content-center align-items-center d-flex ">
                    <li className={this.state.selectedCredit && this.state.selectedCredit.length > 0 ? 'active-board' : 'requird-borad'}>{this.state.selectedCredit.length == 1 ? '1' : ' '}</li>
                    <li className={this.state.selectedCredit && this.state.selectedCredit.length > 1 ? 'active-board' : 'requird-borad'}>{this.state.selectedCredit.length == 2 ? '2' : ' '}</li>
                    <li className={this.state.selectedCredit && this.state.selectedCredit.length > 2 ? 'active-board' : 'requird-borad'}>{this.state.selectedCredit.length == 3 ? '3' : ' '}</li>
                    <li className={this.state.selectedCredit && this.state.selectedCredit.length > 3 ? 'active-board' : 'requird-borad'}>{this.state.selectedCredit.length == 4 ? '4' : ' '}</li>
                    <li className={this.state.selectedCredit && this.state.selectedCredit.length > 4 ? 'active-board' : ''}>{this.state.selectedCredit.length == 5 ? '5' : ' '}</li>
                    <li className={this.state.selectedCredit && this.state.selectedCredit.length > 5 ? 'active-board' : ''}>{this.state.selectedCredit.length == 6 ? '6' : ' '}</li>
                    <li className={this.state.selectedCredit && this.state.selectedCredit.length > 6 ? 'active-board' : ''}>{this.state.selectedCredit.length == 7 ? '7' : ' '}</li>
                    <li className={this.state.selectedCredit && this.state.selectedCredit.length > 7 ? 'active-board' : ''}>{this.state.selectedCredit.length == 8 ? '8' : ' '}</li>
                    <li className={this.state.selectedCredit && this.state.selectedCredit.length > 8 ? 'active-board' : ''}>{this.state.selectedCredit.length == 9 ? '9' : ' '}</li>
                    <li className={this.state.selectedCredit && this.state.selectedCredit.length > 9 ? 'active-board' : ''}>{this.state.selectedCredit.length == 10 ? '10' : ' '}</li>
                    <li className={this.state.selectedCredit && this.state.selectedCredit.length > 10 ? 'active-board' : ''}>11</li>

                    {/* <li className="requird-borad">3</li>
                <li className="requird-borad">4</li>
                {this.state.loopData.map(item => (
                  <li>{item}</li>
                ))} */}

                  </ul>
                </Col>
              </Row>
              {/* End Fill stepper */}

              <AppBar position="static">
                <Tabs
                  value={this.state.value}
                  onChange={this.handleChange}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="fullWidth"
                  aria-label="full width tabs example"
                >
                  {
                    this.state.selectedCategoryCount && this.state.selectedCategoryCount.length > 0 ?
                      this.state.selectedCategoryCount.map((item, key) => {
                        var count = '';
                        if (item.count) {
                          count = '(' + item.count + ')';
                        }
                        return (
                          <Tab className={styles.tabPanelText} key={key} label={item.category_name + count} />
                        )
                      })
                      : ''
                  }
                  {/* <Tab className={styles.tabPanelText} label="Runs" />
              <Tab className={styles.tabPanelText} label="Boundaries" />
              <Tab className={styles.tabPanelText} label="Wickets" /> */}
                </Tabs>
              </AppBar>
              {
                this.state.errorMsg != '' ? (
                  <Row className="m-0 select-row">
                    <Col lg={12} className="">
                      <p style={{ color: 'red' }}>{this.state.errorMsg}</p>
                    </Col>
                  </Row>
                ) : ''
              }
            </div>
            {
              marketSelections && marketSelections.length > 0 ?
                (
                  <SwipeableViews animateHeight={false} className={styles.tabContainer} axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'} index={this.state.value} onChangeIndex={this.handleChangeIndex}>
                    {
                      marketSelections.map((item, key) => {
                        return (
                          <TabPanel value={this.state.value} key={key} index={key} dir={theme.direction}>
                            <React.Fragment>
                              <div className="create-my-pack">

                                {/* Match Win row */}

                                {
                                  item.market_data.length > 0 ? item.market_data.map((mData, mKey) => {
                                    return (
                                      <React.Fragment key={mKey}>
                                        <Row className="m-0 header-row justify-content-center align-items-center">
                                          <Col lg={6} xs={5} md={3} sm={3} className="">
                                            <span className="match-win test eventStep">{mData.market_name}
                                              <OverlayTrigger
                                                placement="right"
                                                delay={{ show: 100, hide: 400 }}
                                                overlay={<Tooltip id="button-tooltip">{mData.market_name}</Tooltip>}
                                              >
                                                <small>
                                                  <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle-fill" fill="#646b78" xmlns="http://www.w3.org/2000/svg">
                                                    <path fillRule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                  </svg>
                                                </small>
                                              </OverlayTrigger>

                                            </span>
                                          </Col>
                                          {
                                            mKey == 0 ? (
                                              <React.Fragment>
                                                <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                                                  <span> Multiplier</span>
                                                </Col>
                                                <Col lg={2} xs={3} md={3} sm={3} className="text-center">
                                                  <span>Credit</span>
                                                </Col>
                                                <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                                                  <span className="potential-point"> Potential Point</span>
                                                </Col>
                                              </React.Fragment>
                                            ) : (
                                                <React.Fragment>
                                                  <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                                                  </Col>
                                                  <Col lg={2} xs={3} md={3} sm={3} className="text-center">
                                                  </Col>
                                                  <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                                                  </Col>
                                                </React.Fragment>
                                              )
                                          }
                                        </Row>
                                        {/* End Match Win row */}

                                        {/* Win toss row */}
                                        {
                                          mData.selection_data.length > 0 ? mData.selection_data.map((sData, sKey) => {
                                            return (
                                              <Row key={sKey} className={`m-0 match-win-row justify-content-center align-items-center ${(this.state.remainCredit == 0 && !this.state.selectedCreditPotential['credit' + sData.selection_id]) || (this.state.selectedCredit.length == 11 && !this.state.selectedCreditPotential['credit' + sData.selection_id]) ? 'disabled-row' : ''}`}>
                                                <Col lg={6} xs={5} md={3} sm={3} className="pl2">
                                                  <label className="team-label pickStep ">{sData.selection_name}</label>
                                                  {/* <p>Lorem ipsum dolor sit amet</p> */}
                                                </Col>
                                                <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                                                  <div className="multiplier-value multiplierStep">{sData.multiplier}</div>
                                                </Col>
                                                <Col lg={2} xs={3} md={3} sm={3} className="text-center pr-1 pl-1">
                                                  <div className="credit-value ">
                                                    <TextField
                                                      id="creditValue"
                                                      label=""
                                                      type="number"
                                                      variant="filled"
                                                      color="primary"
                                                      inputProps={{
                                                        min: 0,
                                                        inputMode: 'numeric'
                                                      }}
                                                      disabled={(this.state.remainCredit == 0 && !this.state.selectedCreditPotential['credit' + sData.selection_id]) || (this.state.selectedCredit.length == 11 && !this.state.selectedCreditPotential['credit' + sData.selection_id]) ? true : false}
                                                      onKeyDown={(evt) => (evt.key === '-' || evt.key === 'e' || evt.key === '+') && evt.preventDefault()}
                                                      value={this.state.selectedCreditPotential['credit' + sData.selection_id] ? this.state.selectedCreditPotential['credit' + sData.selection_id] : ''}
                                                      onChange={($event) => this.handleCreditChange(sData.match_id, mData.category_id, sData.market_id, sData.selection_id, sData.multiplier, mData.market_name, sData.selection_name, $event.target.value)}
                                                    />
                                                  </div>
                                                </Col>
                                                <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                                                  <div className="potential-value potentialStep">{this.state.selectedCreditPotential && Object.keys(this.state.selectedCreditPotential).length > 0 ? this.state.selectedCreditPotential['selection' + sData.selection_id] : ''}</div>
                                                </Col>
                                              </Row>
                                            )
                                          }) : ''
                                        }

                                      </React.Fragment>
                                    )
                                  }) : ''
                                }
                                <div className="pb-3"></div>
                              </div>
                              {/* <div className="common-btn">
                              <Button variant="success" title="Preview Card" onClick={this.confimationDilogOpen}>Preview Card</Button>
                              <Button variant={this.state.continueEnable ? 'success' : 'secondary'} onClick={this.submitPack} title="Continue">Continue</Button>
                            </div> */}
                            </React.Fragment>
                          </TabPanel>
                        )
                      })
                    }
                  </SwipeableViews>
                ) : ''
            }
          </div>
        ) : (
            <Loader />
          )}

        {/* Open confirmation modal */}
        {/* <Button variant="success" onClick={this.goBackModal} title="Preview Card">Open Modal</Button> */}

        {/* Modal slidwe */}
        <Col xs={12} className="mobile-bottom-btn p-0">
          <div className="common-btn">
            <Button variant="success" title="Preview Card" onClick={this.confimationDilogOpen}>Preview Card</Button>
            {/* <Link href="/my-fantasy"> */}
            <Button variant={this.state.continueEnable ? 'success' : 'secondary'} onClick={this.submitPack} title="Continue">Continue</Button>
            {/* </Link> */}
          </div>
        </Col>
        <Dialog
          className="dilogs-modal create-packmodal"
          open={this.state.previewsModal}
          TransitionComponent={Transition}
          keepMounted
          // onClick={this.previewPackClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <div >

            <DialogContent >
              <div>
                <div className="create-packs-tabs justify-content-center align-items-center">
                  <Row className="fantasy-pack match-pack m-0">
                    <DialogActions className="previewPack">
                      <Button className="close-no" onClick={this.previewPackClose}>
                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fillRule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                      </Button>
                    </DialogActions>

                    <Col lg={12}>
                      <div className="modaltile">
                        Card Preview
                     </div>
                    </Col>

                    <Col lg={12} className="starttime text-center">
                      {this.state.minutes < 10 ? '0' + this.state.minutes : this.state.minutes}:{this.state.seconds < 10 ? '0' + this.state.seconds : this.state.seconds} to new multiplier
                    </Col>

                    <Col lg={3} xs={3} md={3} sm={3} className="mr-25">
                    </Col>
                    <Col lg={2} xs={2} md={2} sm={2} className="text-center p-0 mrm--25">
                      <div className="my-team-name mi-team" style={{ background: matchData.hometeamcolorcode }}>
                        {matchData.hometeamname}
                      </div>
                      <br />
                      <div className="totalscore">
                        {matchData.home_team_runs != null ? matchData.home_team_runs : '--'}/{matchData.home_team_wickets != null ? matchData.home_team_wickets : '--'} ({matchData.home_team_overs != null ? matchData.home_team_overs : '--'})
                      </div>
                    </Col>
                    <Col lg={2} xs={2} md={2} sm={2} className="text-center p-0">
                      <img src="static/images/whitevs.png" className="team-vs" />
                    </Col>
                    <Col lg={2} xs={2} md={2} sm={2} className="text-center p-0 mlm-25">
                      <div className="my-team-name iplteam" style={{ background: matchData.awayteamcolorcode }}>
                        {matchData.awayteamname}
                      </div>
                      <br />
                      <div className="totalscore">
                        {matchData.away_team_runs != null ? matchData.away_team_runs : '--'}/{matchData.away_team_wickets != null ? matchData.away_team_wickets : '--'} ({matchData.away_team_overs != null ? matchData.away_team_overs : '--'})
                      </div>
                    </Col>
                    <Col lg={3} xs={3} md={3} sm={3} className="text-right ml-25">
                    </Col>
                  </Row>
                  {/* Fill stepper */}
                  <React.Fragment>
                    <div className="create-my-pack header-row-my-pack  m-0">
                      <Row className="m-0 header-selection justify-content-center align-items-center">
                        <Col lg={4} xs={4} md={4} sm={4} className="text-left p-0">
                          <span> Selections <b>{this.state.selectedCredit ? this.state.selectedCredit.length : 0}</b> </span>
                        </Col>
                        <Col lg={4} xs={4} md={4} sm={4} className="text-center p-0">
                          <span> Potential Point <b className="pointpotential">{this.state.totalSelectedPotential ? this.state.totalSelectedPotential.toFixed(2) : 0}</b></span>
                        </Col>
                        <Col lg={4} xs={4} md={4} sm={4} className="text-right p-0">
                          <span> Credit <b>{this.state.remainCredit ? this.state.remainCredit : 0}</b></span>
                        </Col>
                      </Row>
                      <Row className="m-0 header-row justify-content-center align-items-center">
                        <Col lg={6} xs={5} md={3} sm={3} className="pl-2">
                          <span className="match-win">
                            {/* <small className="mr-2">
                              <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-pencil-fill" fill="#646b78" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z" />
                              </svg>
                            </small> */}
                              My Card
                            </span>
                        </Col>
                        <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                          <span> Multiplier</span>
                        </Col>
                        <Col lg={2} xs={3} md={3} sm={3} className="text-center">
                          <span>Credit</span>
                        </Col>
                        <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                          <span className="potential-point"> Potential Point</span>
                        </Col>
                      </Row>
                      {/* End Match Win row */}


                      {this.state.selectedCredit && this.state.selectedCredit.length > 0 ? this.state.selectedCredit.map((item, key) => (
                        <Row key={key} className="m-0 fansty-row match-win-row justify-content-center align-items-center">
                          <Col lg={6} xs={5} md={3} sm={3} className="pl2">
                            <span className="serial-circle">{key + 1}</span>
                            <label className="team-label">
                              {item.marketName} @ {item.selectionName}
                            </label>
                          </Col>
                          <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                            <div className="multiplier-value">{item.multiplier}</div>
                          </Col>
                          <Col lg={2} xs={3} md={3} sm={3} className="text-center pr-1 pl-1">
                            <div className="credit-value ">
                              <div className="blank-input">
                                {item.credit}
                              </div>
                            </div>
                          </Col>
                          <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                            <div className="potential-value">{item.potential}</div>
                          </Col>
                        </Row>
                      )) : ''}

                      {[...Array(11 - this.state.selectedCredit.length)].map((item, key) => {
                        return (
                          <Row key={key} className="m-0 fansty-row match-win-row justify-content-center align-items-center">
                            <Col lg={6} xs={5} md={3} sm={3} className="pl2">
                              <span className="serial-circle">{this.state.selectedCredit.length + key + 1}</span>
                              <label className="team-label">
                              </label>
                            </Col>
                            <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                              <div className="multiplier-value"></div>
                            </Col>
                            <Col lg={2} xs={3} md={3} sm={3} className="text-center pr-1 pl-1">
                              <div className="credit-value ">
                                <div className="blank-input">
                                </div>
                              </div>
                            </Col>
                            <Col lg={2} xs={2} md={3} sm={3} className="text-center p-0">
                              <div className="potential-value"></div>
                            </Col>
                          </Row>
                        )
                      })}

                      <div className="pb-3"></div>
                      {/* End Man of the match */}
                    </div>

                  </React.Fragment>
                </div>
              </div>
            </DialogContent>
          </div>
        </Dialog>
        {/* Confirmation modal */}
        <Dialog
          className="modal-slide-modal"
          id="myCofirmation"
          open={this.state.confimChange}
          keepMounted
          onClose={this.confimationDilogClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <div >
            <DialogContent >
              <div>
                <div className="create-packs-tabs ">
                  <React.Fragment>
                    <div className="create-my-pack header-row-my-pack  m-0">
                      <Row className="m-0  justify-content-center align-items-center">
                        <Col xs={12} className="text-center slide-modal-body ">
                          <h5> Alert!</h5>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-question-circle" fill="#844ff7" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                            <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z" />
                          </svg>
                          <p>The multipliers have changed, click approve to continue!</p>
                        </Col>
                        <DialogActions >
                          <Col xs={12} className="slide-modal mobile-bottom-btn p-0">
                            <div className="common-btn">
                              <Button variant="success" onClick={this.approveMultiplier} color="primary">
                                Approve
                            </Button>
                            </div>
                          </Col>
                        </DialogActions>
                      </Row>
                    </div>
                  </React.Fragment>
                </div>
              </div>
            </DialogContent>
          </div>
        </Dialog>
        {/* End Confirmation modal */}

      </Layout>
    );
  }
}

const defaultPage = () => {
  const router = useRouter();
  const theme = useTheme();
  return (
    <CreatePacks router={router} theme={theme} />
  )
}

export default defaultPage;
// export default withStyles(styles, { withTheme: true })(CreatePacks);
