import React, { Component } from "react";
import Layout from 'components/Layout/Layout';
import { render } from 'react-dom';
import Link from 'next/link';
import { Container, ProgressBar, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import SplashHome from "components/Home/SplashHome";
import Play from "components/CommonComponents/Play";
import { AuthenticationService } from "_services/AuthenticationService";
class HowToPlay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      card: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    }
  }
  render() {
    return (
      <React.Fragment>
        {
          AuthenticationService.isLogged ? (
            <Layout title="How to play" header="innerHeader" headerText="How to play">
              <Play />
            </Layout >
          ) : (
              <Layout customClass="without-login" title="How to play">
                <Play />
              </Layout >
            )
        }
      </React.Fragment>
    )
  }
}
export default HowToPlay;
