import Link from 'next/link';
import React, { Component } from "react";
import EmailVerification from "components/EmailVerification/EmailVerification";
import Layout from 'components/Layout/Layout';

class Verification extends Component {

  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     verifyMessage: '',
  //     isLoaded: false,
  //     isVerified: false
  //   }
  // }

  // componentDidMount() {
  //   this.verifyEmail();
  // }

  // verifyEmail = () => {
  //   AuthenticationService.emailVerify(Router.query, res => {
  //     if (res.status) {
  //       this.setState({
  //         verifyMessage: 'Your email has been verified successfully',
  //         isVerified: true,
  //         isLoaded: true
  //       });
  //     } else {
  //       this.setState({
  //         verifyMessage: 'Your email is not verified! Please try again',
  //         isLoaded: true
  //       });
  //     }
  //   });
  // }

  render() {
    return (
      <div>
        <Layout header="innerHeader" headerText="Email Verification">
          <EmailVerification />
        </Layout>

      </div>
    )
  }
}
export default Verification;

