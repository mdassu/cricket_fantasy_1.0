import React, { Component } from "react";
import Cookies from "js-cookie";
import Link from 'next/link';
import { Container, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import CloseIcon from '@material-ui/icons/Close';
import { Snackbar, Dialog, DialogActions, MenuItem, DialogContent, TextField, Select, DialogContentText, Slide } from '@material-ui/core';
import Layout from 'components/Layout/Layout';
import Form from 'react-bootstrap/Form'
import { AuthenticationService } from "_services/AuthenticationService";
import { BankingService } from "_services/BankingService";
import Router, { useRouter } from "next/router";
import { decrypt } from "_helper/EncrDecrypt";
import Loader from 'components/CommonComponents/Loader';
import AlertPopup from "components/CommonComponents/AlertPopup";
const { flag, code, name, countries } = require('country-emoji');
import ReactCountryFlag from "react-country-flag";
import { CommonService } from '_services/CommonService';
import OtpInput from 'react-otp-input';
import { GAevent } from "_helper/google-analytics";


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

class AddCash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      walletBalance: 0,
      amount: 100,
      congratsPopup: false,
      emailIsVerified: false,
      alertPopup: false,
      isLoaded: false,
      alertType: 'sendMail',
      alertSecButton: '',
      alertMsg: 'Please verify your email id before adding cash.',
      alertButton: 'Verify Email',
      mailSentStatus: false,
      mobile: '',
      email: '',
      otpCountryCode: '',
      formData: {
        countryCode: '91',
        otp: '',
        emailId: '',
      },
      countries: [],
    }
  }

  componentDidMount() {
    AuthenticationService.clearCookies();
    CommonService.countrySubject.subscribe(countries => {
      this.setState({
        countries: countries
      });
    });
    this.getCurrentUser();
    this.getProfile();
    if (Router.query.o) {
      this.setState({
        isLoaded: false
      })
      this.saveTransaction();
    } else {
      this.setState({
        isLoaded: true
      })
    }
  }

  getCurrentUser() {
    AuthenticationService.currentUser.subscribe(user => {
      if (user && user != null) {
        this.setState({
          walletBalance: parseFloat(user['walletBalance']),
          mobile: user['mobile'],
          email: user['email']
        });
      }
    });
  }

  getProfile() {
    AuthenticationService.getProfile((res) => {
      if (res.status) {
        var user = res['data']
        var emailVerify = false;
        if (user && user != null) {
          if (user['email_is_verified'] == 1) {
            emailVerify = true;
          }
          if (user && user != null) {
            this.setState({
              emailIsVerified: emailVerify
            });
          }
        }
      }
    });
  }
  handleOTPChange = otp => this.setState({
    formData: {
      otp: otp
    }
  });
  handleChange = (event) => {
    this.setState({
      amount: parseFloat(event.target.value)
    });
  }

  changeAmount = (amount) => {
    this.setState({
      amount: parseFloat(amount)
    });
  }

  addCash = () => {
    this.getProfile();
    setTimeout(() => {
      if (this.state.emailIsVerified) {
        var params = {
          amount: parseFloat(this.state.amount)
        }
        BankingService.addCash(params, (res) => {
          if (res.status) {
            window.location.href = res['data'];
          } else {

          }
        });
      } else {
        this.setState({
          alertType: 'sendMail',
          alertMsg: 'Please verify your email id before adding cash.',
          alertButton: 'Verify Email'
        }, () => {

          this.setState({
            alertPopup: true
          });
        })
      }
    }, 200)
  }

  saveTransaction = () => {
    var orderId = Router.query.o.replace(/ /g, '+');
    var params = {
      orderId: decrypt(orderId).toString()
    };
    BankingService.saveTransaction(params, (res) => {
      if (res.status) {
        CommonService.updateUserWallet();
        if (Cookies.get('_amount')) {
          Cookies.remove('_amount');
          Router.push({
            pathname: '/contests',
            query: { m: Cookies.get('_match') }
          });
        } else {
          this.setState({
            isLoaded: true,
            congratsPopup: true
          });
          CommonService.updateUserWallet();
        }
      } else {
        var value = '';
        if (this.state.mobile && this.state.email) {
          value = this.state.mobile + '-' + this.state.email;
        } else if (this.state.mobile) {
          value = this.state.mobile;
        } else if (this.state.email) {
          value = this.state.email;
        } else {
          value = '1';
        }
        if (res.message == 'FAILED') {
          GAevent('Failed_DropOffs_On_PG', 'On Drop Off From Payment Gateway - Failed', 'How many drop-offs on the payment gateway - Failed', value);
        } else if (res.message == 'CANCELLED') {
          GAevent('Cancelled_DropOffs_On_PG', 'On Drop Off From Payment Gateway - Cancelled', 'How many drop-offs on the payment gateway - Cancelled', value);
        }
        this.setState({
          isLoaded: true,
          alertType: 'transFail',
          alertMsg: 'The transaction was unsuccessfull!',
          alertButton: 'Please try again!'
        }, () => {
          this.setState({
            alertPopup: true,
          })
        });
      }
    });
  }
  handleClose = () => {
    this.setState({
      alertPopup: false
    });

  };
  alertAction = (type = '') => {
    if (type == 'sendMail') {
      this.setState({
        mailSentStatus: true,
        alertButton: 'Sending Mail...'
      });
      AuthenticationService.sendVerifyEmail((res) => {
        if (res.status) {
          this.setState({
            alertType: '',
            alertButton: 'Ok',
            alertSecButton: '',
            mailSentStatus: false,
            alertMsg: 'Mail have been sent successfully. Please check your mail inbox!'
          });
        } else {
          this.setState({
            alertType: 'sendMail',
            alertButton: 'Resend Verify Email',
            alertSecButton: 'Cancel',
            mailSentStatus: false,
            alertMsg: 'Mail have been not sent successfully. Please try again!'
          });
        }
      });
    } else {
      this.setState({
        alertPopup: false
      });
    }
  }

  render() {
    const { isLoaded, walletBalance, countryCode, countries, amount, congratsPopup, alertPopup, alertButton, alertMsg, alertType, alertSecButton, mailSentStatus } = this.state;
    var country = '';
    if (countries && countries.length > 0) {
      country = countries.map((country, key) =>
        <MenuItem key={key} value={country.country_code}>
          <ReactCountryFlag countryCode={code(country.country_name)} svg /> &nbsp;+{country.country_code}
        </MenuItem>
      );
    }
    return (
      <React.Fragment>
        <Layout header="innerHeader" headerText="Add Cash">
          {
            isLoaded ? (
              <React.Fragment>
                <div className="">
                  <Row>
                    <Col lg={12}>
                      <div className="outer-card mt-0">
                        <Card className="rounded-0 all-winner-content">
                          <Card.Body>
                            <div className="float-left">
                              <h5>
                                Current Balance
                        </h5>
                            </div>
                            <div className="ipl-matches float-right">
                              <h5>&#x20B9;{walletBalance}</h5>
                            </div>
                            <div className="clearfix"></div>
                          </Card.Body>
                        </Card>
                        {/* total Rank loop name wise */}
                        <div className="view-all-winner-div mt-3">
                          <Col lg={12} className="p-1 ">
                            <Card>
                              <Card.Body>
                                <Row className="justify-content-center align-items-center">
                                  <Col md={12} className="add-cash-col">
                                    <Form.Label>Amount to add</Form.Label>
                                    <Form.Control type="number" placeholder="Enter amount" value={amount} onChange={this.handleChange} />
                                    <Row className="add-money-label pb-2">
                                      <Col lg={4} xs={4} md={4} sm={4} >
                                        <Button className="" variant="outline-secondary" title="200" onClick={() => this.changeAmount(200)}>&#x20B9;200</Button>
                                      </Col>
                                      <Col lg={4} xs={4} md={4} sm={4}>
                                        <Button className="" variant="outline-secondary" title="300" onClick={() => this.changeAmount(300)}>&#x20B9;300</Button>
                                      </Col>
                                      <Col lg={4} xs={4} md={4} sm={4}>
                                        <Button className="" variant="outline-secondary" title="500" onClick={() => this.changeAmount(500)}>&#x20B9;500</Button>
                                      </Col>
                                    </Row>
                                  </Col>
                                  <Col xs={12} className="mobile-bottom-btn add-cash">
                                    <div className="common-btn w-100">
                                      <Button className="" variant="success" title="Add Cash" onClick={this.addCash}>Add Cash</Button>
                                    </div>
                                  </Col>
                                </Row>
                              </Card.Body>
                            </Card>
                          </Col>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </div>
                {/* Confirmation modal */}
                <Dialog
                  className="common-modals"
                  TransitionComponent={Transition}
                  open={congratsPopup}
                  keepMounted
                  aria-labelledby="alert-dialog-slide-title"
                  aria-describedby="alert-dialog-slide-description"
                >
                  <DialogContent>

                    {/* <DialogContentText id="alert-dialog-slide-description"> */}
                    <Row className="m-0  justify-content-center align-items-center">
                      <Col xs={12} className="text-center slide-modal-body ">
                        <h5>Add Cash!</h5>

                        <p>Your cash added successfully!</p>
                      </Col>
                    </Row>
                    {/* </DialogContentText> */}
                  </DialogContent>
                  <DialogActions>
                    <div className="common-btn">

                      {/* <Link href="/"> */}
                      <Button variant="success" title="Ok" onClick={() => this.setState({ congratsPopup: false })}>Ok</Button>
                      {/* </Link> */}
                    </div>
                  </DialogActions>
                </Dialog>
                {/* End Confirmation modal */}
                {/* Confirmation modal */}
                {
                  alertPopup ? (
                    <AlertPopup type={alertType} alertPopup={alertPopup} alertMsg={alertMsg} alertButton={alertButton} buttonAction={this.alertAction} alertSecButton={alertSecButton} buttonStatus={mailSentStatus} secButtonAction={() => this.setState({
                      alertPopup: false, alertType: 'sendMail',
                      alertSecButton: '',
                      alertMsg: 'Please verify your email id before adding cash.',
                      mailSentStatus: true,
                      alertButton: 'Verify Email'
                    })} />

                    // ==========================
                    // <Dialog
                    //   className="common-modals new-common-modal"
                    //   TransitionComponent={Transition}
                    //   open={alertPopup}
                    //   onClose={this.handleClose}
                    //   keepMounted
                    //   aria-labelledby="alert-dialog-slide-title"
                    //   aria-describedby="alert-dialog-slide-description"
                    // >
                    //   <DialogContent>

                    //     {/* <DialogContentText id="alert-dialog-slide-description"> */}
                    //     <Row className="m-0  justify-content-center align-items-center">
                    //       <Col xs={12} className="text-center slide-modal-body ">
                    //         <Button type="button" className="close-btn" title="Close" onClick={this.handleClose}>
                    //           <CloseIcon />
                    //         </Button>
                    //         <form className="login-form new-form-addcash" >
                    //           <div className="phone-input clearfix">

                    //             <Row>
                    //               <Col lg={12} className="text-left"> <h3 class="modal-title">Verify</h3>
                    //                 <p class="modal-description">Enter your details to proceed</p></Col>
                    //               <Col lg={12} className="inputNumber">

                    //                 {country != '' ?
                    //                   (< Select value={this.state.formData['countryCode']} variant="outlined" className="country-select">
                    //                     {country}
                    //                   </Select>)
                    //                   : ''}
                    //                 <TextField
                    //                   className="number-input mobile-input"
                    //                   variant="outlined"
                    //                   type="number"
                    //                   inputProps={{
                    //                     name: 'phone',
                    //                   }}
                    //                   autoFocus={true}
                    //                   value=''
                    //                 />

                    //               </Col>
                    //               <Col lg={12} xs={12} md={12} md={12} className="mt-3" >
                    //                 <TextField
                    //                   label="Email Id"
                    //                   type="text"
                    //                   variant="outlined"
                    //                   fullWidth
                    //                   className="mb-3 emailfield"
                    //                   name="emailId"
                    //                   value=''
                    //                 />
                    //               </Col>
                    //               <Col lg={12} xs={12} md={12} md={12} className="user-otp  otp-form text-center p-0">
                    //                 <input type="hidden" name="mobile" value='' />
                    //                 <OtpInput
                    //                   value=''
                    //                   onChange={this.handleOTPChange}
                    //                   numInputs={6}
                    //                   isInputNum
                    //                   shouldAutoFocus
                    //                 // separator={<span>-</span>}
                    //                 />
                    //               </Col>
                    //             </Row>
                    //           </div>
                    //           <Button type="submit" title="Save" fullWidth variant="contained" color="primary" className="save-data-btn register-now text-capitalize  mt-26 mt-lg-5">
                    //             Save
                    //             </Button>
                    //         </form>
                    //       </Col>
                    //     </Row>
                    //     {/* </DialogContentText> */}
                    //   </DialogContent>
                    // </Dialog>
                    // ==========================

                  ) : ''
                }
              </React.Fragment>
            ) : (
                <Loader />
              )
          }
        </Layout>
      </React.Fragment>
    )
  }
}

const defaultPage = (req, res) => {
  const router = useRouter();
  return (
    <AddCash router={router} />
  )
}

export default defaultPage;

// export default AddCash;
