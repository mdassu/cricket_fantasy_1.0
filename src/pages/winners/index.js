import React, { Component } from "react";
import Cookies from "js-cookie";
import Link from 'next/link';
import { Container, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import { Snackbar, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import Layout from 'components/Layout/Layout';
import Slider from "react-slick";
import CardDeck from 'react-bootstrap/CardDeck';
import { ContestsService } from "_services/ContestsService";
import NoData from "components/Contests/NoData";
import Loader from "components/CommonComponents/Loader";
import * as moment from 'moment';
import millify from "millify";
import { encrypt } from "_helper/EncrDecrypt";

class Winners extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contestList: [],
      winner: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      open: false,
      isLoaded: false
    }
  }

  componentDidMount() {
    this.getWinners();
  }

  getWinners = () => {
    ContestsService.getWinners((res) => {
      if (res.status) {
        this.setState({
          contestList: res['data'],
          isLoaded: true
        });
      } else {
        this.setState({
          contestList: [],
          isLoaded: true
        });
      }
    });
  }

  render() {
    const { contestList, isLoaded } = this.state;
    const settings = {
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 4,
      lazyLoad: true,
      centerPadding: "5px",

      // speed: 2000,
      dots: false,
      arrows: false,
      responsive: [
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          }
        }
      ]
    };

    const contestCard = contestList.map((item, key) => {
      return (
        <div className="outer-card mt-3">
          <Card key={key}>
            <Card.Body>
              <div className="float-left content-number text-capitalize">{item.contestData.contest_name}</div>
              <div className="ipl-matches float-right">{moment(item.contestData.match_date).format('DD MMM, YYYY')}</div>
              <div className="clearfix"></div>
              <div className="team-listing ">
                <Link href={{ pathname: '/winners-contest', query: { c: encrypt(item.contestData.match_contest_id) } }}>
                  <Row className="justify-content-center align-items-center">
                    <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                      <div className="team-logo team-left">
                        <div className="logo-line line-left">
                        </div>
                        <div className="team-name team-left-name" style={{ background: item.contestData.home_team_color }}>
                          {item.contestData.home_team_name}
                        </div>
                      </div>
                    </Col>
                    <Col lg={4} xs={4} md={4} sm={4} className="text-center p-0">
                      <div className="ipl-match-number">
                        <span>{item.contestData.matchNumber}</span>
                      </div>
                      <img src="static/images/vs.svg" className="team-vs" />
                    </Col>
                    <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                      <div className="team-logo team-right">
                        <div className="logo-line line-right">
                        </div>
                        <div className="team-name team-right-name" style={{ background: item.contestData.away_team_color }}>
                          {item.contestData.away_team_name}
                        </div>
                      </div>
                    </Col>
                  </Row>
                </Link>
              </div>
            </Card.Body>
            <div className="winners-card">
              <Slider {...settings} >
                {
                  item.winnerModel && item.winnerModel.length > 0 ?
                    item.winnerModel.map((user, uKey) => (
                      <Col lg={12} className="p-1" key={uKey}>
                        <Card>
                          <Card.Body>
                            <Card.Title>Rank #{user.rank}</Card.Title>
                            <Card.Text>
                              {user.first_name} {user.last_name}
                            </Card.Text>
                          </Card.Body>
                          <Card.Img variant="top" src={user.profile_img && user.profile_img != '' && user.profile_img != null ? process.env.IMG_URL + user.profile_img : 'static/images/winner.jpg'} />
                          {
                            user.won_amount && user.won_amount != null ? (
                              <Card.Footer className="text-center">
                                Won &#x20B9;{millify(user.won_amount)}
                              </Card.Footer>
                            ) : ''
                          }
                        </Card>
                      </Col>
                    )) : ''
                }
                {/*  View all Winners Button */}
                {/* {
                  item.winnerModel.length > 4 ? (
                    <Col lg={12} className="p-1 ">
                      <Card className="text-center view-all-winners ">
                        <Card.Body >
                          <Link href="/view-all-winners">
                            <Card.Text>
                              <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trophy-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" d="M2.5.5A.5.5 0 0 1 3 0h10a.5.5 0 0 1 .5.5c0 .538-.012 1.05-.034 1.536a3 3 0 1 1-1.133 5.89c-.79 1.865-1.878 2.777-2.833 3.011v2.173l1.425.356c.194.048.377.135.537.255L13.3 15.1a.5.5 0 0 1-.3.9H3a.5.5 0 0 1-.3-.9l1.838-1.379c.16-.12.343-.207.537-.255L6.5 13.11v-2.173c-.955-.234-2.043-1.146-2.833-3.012a3 3 0 1 1-1.132-5.89A33.076 33.076 0 0 1 2.5.5zm.099 2.54a2 2 0 0 0 .72 3.935c-.333-1.05-.588-2.346-.72-3.935zm10.083 3.935a2 2 0 0 0 .72-3.935c-.133 1.59-.388 2.885-.72 3.935z" />
                              </svg>
                              View all Winners
                            </Card.Text>
                          </Link>
                        </Card.Body>
                      </Card>
                    </Col>
                  ) : ''
                } */}
              </Slider>
            </div>
          </Card>
        </div>
      );
    });

    return (
      <React.Fragment>
        <Layout customClass="winner-page-class" header="tabHeader" headerText="Winners">
          {
            isLoaded ? (
              <div className="winner-content" >
                <Row>
                  <Col lg={12}>
                    {
                      contestCard && contestCard != '' ? contestCard : <NoData headText="You don't have any purchased contest" />
                    }
                  </Col>
                </Row>
              </div>
            ) : <Loader />
          }
        </Layout>
      </React.Fragment>
    )
  }
}
export default Winners;