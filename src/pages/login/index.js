import React, { Component } from "react";
import Layout from "components/Layout/Layout";
import { Button, Modal, TextField, Select, MenuItem, Snackbar } from '@material-ui/core';
import Link from "next/link";
import { Container, Row, Col, Card, Badge } from 'react-bootstrap';
const { flag, code, name, countries } = require('country-emoji');
import ReactCountryFlag from "react-country-flag";
import { CommonService } from '_services/CommonService';
import Cookies from 'js-cookie';
import MuiAlert from '@material-ui/lab/Alert';
import { GAevent } from "_helper/google-analytics";


import SocialButton from 'components/socialButton'


import { ErrorMessages, isNameValid, isMobileNumberValid } from 'components/MyValidations/MyValidations';
import Router from "next/router";
import { AuthenticationService } from "_services/AuthenticationService";
import { encrypt } from "_helper/EncrDecrypt";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class Login extends Component {

  // const classes = useStyles();
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      popupView: 'login',
      otpMobile: '',
      otpReferral: '',
      otpCountryCode: '',
      isLoaded: true,
      terms: false,
      mobile: '',
      formData: {
        mobile: '',
        countryCode: '91',
        otp: '',
        firstName: '',
        lastName: '',
        referralCode: ''
      },
      error: false,
      errorMessage: {
        mobile: '',
        firstName: '',
        lastName: '',
        otp: '',
        terms: '',
        referralCode: ''
      },
      alert: {
        toast: false,
        toastMessage: '',
        severity: ''
      },
      countDown: 0,
      countries: [],
      logged: false,
      user: {},
      currentProvider: ''
    }
    this.nodes = {}
    this.logout = this.logout.bind(this)
  }

  componentDidMount() {
    AuthenticationService.clearCookies();
    CommonService.countrySubject.subscribe(countries => {
      this.setState({
        countries: countries
      });
    });
  }

  handleChange = name => ({ target: { value, checked } }) => {
    if (name == 'terms') {
      this.setState({
        formData: {
          ...this.state.formData,
          [name]: checked
        }
      });
    } else {
      this.setState({
        formData: {
          ...this.state.formData,
          [name]: value
        }
      });
    }
  }

  logInSubmit = (event, type = null) => {
    if (event) {
      event.preventDefault();
    }
    const { formData } = this.state;
    var mobileNumber = formData.mobile;
    if (type == 'resendOtp') {
      this.setState({
        countDown: 59
      });
      var countdown = setInterval(() => {
        this.setState({
          countDown: this.state.countDown - 1
        });
        if (this.state.countDown == 0) {
          clearInterval(countdown);
          this.setState({
            countDown: 0
          });
        }
      }, 1000);
      mobileNumber = this.state.mobile;
      this.setState({
        formData: {
          mobile: this.state.mobile,
          countryCode: '91',
          otp: '',
          firstName: '',
          lastName: '',
        }
      });
    } else {
      this.setState({
        mobile: mobileNumber
      });
    }

    // const { formData } = this.state;
    var isValid = false;
    if (mobileNumber) {
      if (!isMobileNumberValid(mobileNumber)) {
        this.setState({
          error: true,
          errorMessage: { mobile: ErrorMessages.VALIDATION_ERROR_MOBILE }
        });
      } else {
        isValid = true;
      }
    } else {
      this.setState({
        error: true,
        errorMessage: { mobile: 'Mobile number is required' }
      });
    }

    if (isValid) {
      this.setState({
        error: false,
        errorMessage: {},
        isLoaded: false
      });
      var params = {
        country_code: formData.countryCode,
        mobile: mobileNumber
      };
      AuthenticationService.signIn(params, (res) => {
        if (res.status) {
          var data = {
            otpMobile: formData.mobile,
            otpCountryCode: formData.countryCode,
          }
          Cookies.set('_mobData', encrypt(JSON.stringify(data)));
          Router.push('/verify-otp');
        } else {
          this.setState({
            error: true,
            errorMessage: { mobile: res.message },
            isLoaded: true
          });
        }
      });
    }
  };

  handleSocialLogin = (user) => {
    console.log(user);
    var params = {
      country_code: '',
      mobile: '',
      first_name: '',
      last_name: '',
      email: ''
    };
    if (user._profile) {
      if (user._profile['firstName']) {
        params['first_name'] = user._profile['firstName'];
      }
      if (user._profile['lastName']) {
        params['last_name'] = user._profile['lastName'];
      }
      if (user._profile['email']) {
        params['email'] = user._profile['email'];
      }
      if (user._profile['mobile']) {
        params['mobile'] = user._profile['mobile'];
      }
      console.log(params);
      AuthenticationService.signUp(params, (res) => {
        if (res.status) {
          var userData = response['data'];
          var userJson = {
            country_code_id: userData['country_code_id'],
            mobile: userData['mobile'],
            first_name: userData['first_name'],
            last_name: userData['last_name'],
            last_login: userData['last_login'],
            email: userData['email'],
            profile_picture: userData['profile_picture'],
            walletBalance: userData['walletBalance'],
            addedBalance: userData['addedBalance'],
            bonusBalance: userData['bonusBalance'],
            winningBalance: userData['winningBalance'],
            creditedCash: userData['creditedCash'],
            creditedType: userData['creditType'],
            referralCode: userData['referral_code'],
            isEmailVerified: userData['email_is_verified']
          };
          var encryptData = encrypt(userJson);
          Cookies.set('_accessToken', userData['token'], { expires: 30 });
          Cookies.set('_user', encryptData, { expires: 30 });
          CommonService.currentUserSubject.next(userJson);
          Router.push('/');
        } else {
          this.setState({
            alert: {
              toast: true,
              toastMessage: res.message,
              severity: 'error'
            },
          });
        }
      });
      var value = '';
      if (params['mobile'] && params['email']) {
        value = params['mobile'] + '-' + params['email'];
      } else if (params['mobile']) {
        value = params['mobile'];
      } else if (params['email']) {
        value = params['email'];
      } else {
        value = '1';
      }
      GAevent('Successfull_Login', 'Successfull Login', 'How many users logged in', value);
    }
  }

  handleSocialLoginFailure = (err) => {
    this.setState({
      alert: {
        toast: true,
        toastMessage: 'Something went wrong.',
        severity: 'error'
      },
    });
  }

  setNodeRef(provider, node) {
    if (node) {
      this.nodes[provider] = node
    }
  }

  logout() {
    const { logged, currentProvider } = this.state

    if (logged && currentProvider) {
      this.nodes[currentProvider].props.triggerLogout()
    }
  }

  render() {
    // const classes = useStyles;
    const { countries, isLoaded, formData: { mobile, countryCode, otp, firstName, lastName, terms, referralCode }, otpMobile, otpCountryCode, alert } = this.state;
    var country = '';
    if (countries && countries.length > 0) {
      country = countries.map((country, key) =>
        <MenuItem key={key} value={country.country_code}>
          <ReactCountryFlag countryCode={country.iso_code} svg /> &nbsp;+{country.country_code}
        </MenuItem>
      );
    }
    return (
      <Layout header="innerHeader" headerText="Login" customClass="mylogin-class">
        <Snackbar open={alert.toast} autoHideDuration={2000} onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })}>
          <Alert onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })} severity={alert.severity}>
            {alert.toastMessage}
          </Alert>
        </Snackbar>
        <Row className="user-pages">
          <Col lg={12}>
            <div className="outer-card mt-3">
              <Card>
                <Card.Body className="pt-4">
                  <Row className="justify-content-center align-items-center p-lg-5 pb-3">
                    <Col lg={12}>
                      <h3 className="modal-title">Login</h3>
                    </Col>
                    <Col lg={12}>
                      <p className="modal-description">
                        Enter your mobile number to proceed
                    </p>
                    </Col>
                    <Col lg={12}>
                      <form onSubmit={this.logInSubmit} className="login-form">
                        <div className="phone-input clearfix">
                          <Row>
                            <Col lg={12} className="inputNumber">
                              {country != '' ?
                                (< Select value={this.state.formData['countryCode']} onChange={this.handleChange('countryCode')} variant="outlined" className="country-select">
                                  {country}
                                </Select>)
                                : ''}
                              <TextField
                                className="number-input mobile-input"
                                variant="outlined"
                                type="number"
                                inputProps={{
                                  name: 'phone',
                                }}
                                autoFocus={true}
                                value={mobile}
                                onChange={this.handleChange('mobile')}
                                error={!!this.state.errorMessage.mobile}
                                helperText={!!this.state.errorMessage.mobile ? this.state.errorMessage.mobile : ''}
                              />
                            </Col>
                          </Row>

                        </div>
                        <Button type="submit" title="Continue" fullWidth variant="contained" color="primary" className="register-now continue-btn text-capitalize p-9 ">
                          Continue
                      </Button>
                      </form>
                    </Col>
                    {/* Social Login Buttons */}
                    <Col lg={12} className="text-center mt-4">
                      <div className="or-icon">
                        <span>OR</span>
                      </div>
                    </Col>
                    <Col lg={12} xs={12} sm={12} md={12} className="text-center p-0 socillogin mt-4  justify-content-center align-items-center">
                      <SocialButton
                        provider='facebook'
                        appId={process.env.FACEBOOK_LOGIN_CLIENT_ID}
                        onLoginSuccess={this.handleSocialLogin}
                        onLoginFailure={this.handleSocialLoginFailure}
                        className="facebooklogin"
                      >
                        <img src="static/images/facebooklogin.svg" />  Facebook
                       </SocialButton>
                      <SocialButton
                        provider='google'
                        appId={process.env.GOOGLE_LOGIN_CLIENT_ID}
                        onLoginSuccess={this.handleSocialLogin}
                        onLoginFailure={this.handleSocialLoginFailure}
                        getInstance={this.setNodeRef.bind(this, 'google')}
                        key={'google'}
                        scope={'https://www.googleapis.com/auth/user.gender.read'}
                        className="googlelogin"
                      >
                        <img src="static/images/google.svg" />
                        Google
                        </SocialButton>
                    </Col>
                    {/* End Social Login Buttons */}

                    <Col lg={12}>
                      <div className="register-here">
                        <small>Don't have an account? <Link href="/register">
                          <a title="Register Here">Register Here</a>
                        </Link></small>
                      </div>
                    </Col>
                  </Row>
                </Card.Body>
              </Card>

            </div>
          </Col>
        </Row>

      </Layout >
    );
  }
}
export default Login;
