import React, { Component } from "react";
import Layout from 'components/Layout/Layout';
import Profile from "components/MyProfile/Profile";

class MyProfile extends Component {

  render() {
    return (
      <Layout header="innerHeader" headerText="My Profile" title="My Profile">
        {/*Start Container */}
        <Profile />
      </Layout>
    );
  }
};
export default MyProfile;