import React from 'react';
import Layout from 'components/Layout/Layout';
const Splash = () => {
  return (
    <Layout>
      <div className="container">
        {/* Start Row */}
        <div className="row">
          <div className="col-12">
            <h1 className="text-center my-5">Cricket.Bet</h1>
          </div>
        </div>
        {/* End Row */}
      </div>
    </Layout>
  )
}
export default Splash; 