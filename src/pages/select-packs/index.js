import React, { Component } from "react";

import { Container, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import { Snackbar, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import Layout from 'components/Layout/Layout';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

class SelectPacks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      card: [2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
    }
  }



  handleChange = (event, value) => {
    this.setState({ value });
  };


  render() {
    const { value } = this.state;
    return (
      <React.Fragment>
        <Layout>
          <div className="select-packs" >
            {/* Row */}
            <Row>
              <Col lg={12}>
                <div className="outer-card mt-0">
                  <RadioGroup aria-label="gender" name="gender1" value={value} onChange={this.handleChange}>
                    {/* Select All div */}
                    <Card className="rounded-0 all-winner-content">
                      <Card.Body>
                        <div className="ipl-matches float-right radionbtn selectall ">
                          <h5><FormControlLabel value="selectall" control={<Radio />} label="Select All" /> </h5>
                        </div>
                        <div className="clearfix"></div>
                      </Card.Body>
                    </Card>
                    {/* End Select All div */}

                    {/* Select All radio */}
                    <Row className="mt-4">
                      <Col lg={11} xs={11} sm={11} md={11} className="pr-0">
                        <Card className="">
                          <Card.Body className="mypacks">
                            <div className="overlay-div">
                              <Row className="m-0">
                                <Col lg={6} xs={6} md={6} sm={6} className="p-0">
                                  <h5>My Fantasy Card</h5>
                                </Col>
                              </Row>
                            </div>
                            <Row className="fantasy-pack">
                              <Col lg={5} xs={5} md={5} sm={5} className="text-right">
                                <div className="my-team-name mi-team">
                                  mi
                                </div>
                              </Col>
                              <Col lg={2} xs={2} md={2} sm={2} className="text-center">
                                <img src="static/images/vs.svg" className="team-vs img-fluid" />
                              </Col>
                              <Col lg={5} xs={5} md={5} sm={5} className="text-left">
                                <div className="my-team-name iplteam">
                                  Csk
                                </div>
                              </Col>
                            </Row>
                          </Card.Body>
                          <Card.Header>
                            <Row className="m-0 mypacks-footer">
                              <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                                <h6> Selections <b>11</b> </h6>
                              </Col>
                              <Col lg={4} xs={4} md={4} sm={4} className="p-0 text-center">
                                <h6> Potential Points <b>270</b> </h6>
                              </Col>
                              <Col lg={4} xs={4} md={4} sm={4} className="p-0 text-right">
                                <h6> Credits <b>2</b> </h6>
                              </Col>
                            </Row>
                          </Card.Header>
                        </Card>
                      </Col>
                      <Col lg={1} xs={1} sm={1} md={1} className="text-center radionbtn p-0 right-radio">
                        <FormControlLabel value="other" control={<Radio />} />
                      </Col>
                    </Row>

                    {/* Already joined  */}
                    <h5 className="join-heading">Already joined</h5>
                    <Row className="mt-4">
                      <Col lg={11} xs={11} sm={11} md={11} className="pr-0 pb-0">
                        <Card className="">
                          <Card.Body className="mypacks">
                            <div className="overlay-div">
                              <Row className="m-0">
                                <Col lg={6} xs={6} md={6} sm={6} className="p-0">
                                  <h5>My Fantasy Card</h5>
                                </Col>
                              </Row>
                            </div>
                            <Row className="fantasy-pack">
                              <Col lg={5} xs={5} md={5} sm={5} className="text-right">
                                <div className="my-team-name mi-team">
                                  mi
                                </div>
                              </Col>
                              <Col lg={2} xs={2} md={2} sm={2} className="text-center">
                                <img src="static/images/vs.svg" className="team-vs img-fluid" />
                              </Col>
                              <Col lg={5} xs={5} md={5} sm={5} className="text-left">
                                <div className="my-team-name iplteam">
                                  Csk
                                </div>
                              </Col>
                            </Row>
                          </Card.Body>
                          <Card.Header>
                            <Row className="m-0 mypacks-footer">
                              <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                                <h6> Selections <b>11</b> </h6>
                              </Col>
                              <Col lg={4} xs={4} md={4} sm={4} className="p-0 text-center">
                                <h6> Potential Points <b>270</b> </h6>
                              </Col>
                              <Col lg={4} xs={4} md={4} sm={4} className="p-0 text-right">
                                <h6> Credits <b>2</b> </h6>
                              </Col>
                            </Row>
                          </Card.Header>
                        </Card>
                      </Col>
                      <Col lg={1} xs={1} sm={1} md={1} className="text-center radionbtn p-0 right-radio">
                        <FormControlLabel value="other" control={<Radio />} />
                      </Col>
                    </Row>

                    {/* Already joined  */}





                    {this.state.card.map(item => (
                      <Row className="">
                        <Col lg={11} xs={11} sm={11} md={11} className="pr-0 pb-0">
                          <Card className="">
                            <Card.Body className="mypacks">
                              <div className="overlay-div">
                                <Row className="m-0">
                                  <Col lg={6} xs={6} md={6} sm={6} className="p-0">
                                    <h5>My Fantasy Card</h5>
                                  </Col>
                                </Row>
                              </div>
                              <Row className="fantasy-pack">
                                <Col lg={5} xs={5} md={5} sm={5} className="text-right">
                                  <div className="my-team-name mi-team">
                                    mi
                                </div>
                                </Col>
                                <Col lg={2} xs={2} md={2} sm={2} className="text-center">
                                  <img src="static/images/vs.svg" className="team-vs img-fluid" />
                                </Col>
                                <Col lg={5} xs={5} md={5} sm={5} className="text-left">
                                  <div className="my-team-name iplteam">
                                    Csk
                                </div>
                                </Col>
                              </Row>
                            </Card.Body>
                            <Card.Header>
                              <Row className="m-0 mypacks-footer">
                                <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                                  <h6> Selections <b>11</b> </h6>
                                </Col>
                                <Col lg={4} xs={4} md={4} sm={4} className="p-0 text-center">
                                  <h6> Potential Points <b>270</b> </h6>
                                </Col>
                                <Col lg={4} xs={4} md={4} sm={4} className="p-0 text-right">
                                  <h6> Credits <b>2</b> </h6>
                                </Col>
                              </Row>
                            </Card.Header>
                          </Card>
                        </Col>
                        <Col lg={1} xs={1} sm={1} md={1} className="text-center radionbtn p-0 right-radio">
                          <FormControlLabel value="other" control={<Radio />} />
                        </Col>
                      </Row>
                    ))}
                  </RadioGroup>
                </div>
              </Col>
              {/* End Col */}
              <div className="mb-3"></div>
              <Col xs={12} className="mobile-bottom-btn add-cash">
                <div className="common-btn w-100">
                  <Button className="" variant="success" title="Create Card" >Create Card</Button>
                  <Button className="join-pack" variant="secondary" title="Join" >Join</Button>
                </div>
              </Col>
            </Row>
            {/* Row */}
          </div>
        </Layout>
      </React.Fragment>
    )
  }
}
export default SelectPacks;