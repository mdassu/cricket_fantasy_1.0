import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { useTheme, withStyles } from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
// import TabPanel from '@material-ui/lab/TabPanel';
import Box from '@material-ui/core/Box';
import AllContest from "components/Contests/AllContest";
import MyContests from "components/Contests/MyContests";
import MyPacks from "components/Contests/MyPacks";
import { Container, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import TextField from '@material-ui/core/TextField';
import Layout from 'components/Layout/Layout';
import Link from 'next/link';
import Router, { useRouter } from 'next/router';
import { decrypt } from '_helper/EncrDecrypt';
import { AuthenticationService } from '_services/AuthenticationService';

import Overlay from 'react-bootstrap/Overlay';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip'


const styles = {
  tabPanelRoot: 'tabPanelRoot',
  tabPanelText: 'tabPanelText',
  tabContainer: 'tabContainer'
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

class Contests extends Component {

  constructor(props) {
    super(props);
    this.handleChangeIndex = this.handleChangeIndex.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      value: 0,
      // isLoggedIn: false,
      myContestCount: 0,
      myPackCount: 0,
      userWalletBalance: 100
    };
  }

  componentDidMount() {
    // this.getCurrentUser();
  }

  // getCurrentUser() {
  //   AuthenticationService.currentUser.subscribe(user => {
  //     if (user && user != null) {

  //       this.setState({
  //         isLoggedIn: true
  //       });
  //     }
  //   });
  // }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  setContestCount = (data) => {
    this.setState({
      myContestCount: data.contestCount,
      myPackCount: data.packCount
    })
  }

  changeTab = () => {
    this.setState({
      value: 0
    });
  }

  render() {
    const { value } = this.state;
    const { theme, router } = this.props;
    return (
      <Layout header="contestHeader" headerText="Contests">
        {AuthenticationService.isLogged ?
          (
            <div className="contests-tabs">
              <AppBar position="static" color="default">
                <Tabs
                  value={this.state.value}
                  onChange={this.handleChange}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="fullWidth"
                  aria-label="full width tabs example"
                >
                  <Tab className={styles.tabPanelText} label="Contests" />
                  <Tab className={styles.tabPanelText} label={`My Contests (${this.state.myContestCount})`} />
                  <Tab className={styles.tabPanelText} label={`My Cards (${this.state.myPackCount})`} />
                </Tabs>
              </AppBar>
              <SwipeableViews animateHeight={false} className={styles.tabContainer} axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'} index={this.state.value} onChangeIndex={this.handleChangeIndex}>
                <TabPanel value={this.state.value} index={0} dir={theme.direction}>
                  <AllContest matchId={decrypt(router.m)} setContestCount={this.setContestCount} />
                  {/* <Row className="justify-content-center align-items-center top-app-header-row">
                    <Col xs={12} className="mobile-bottom-btn p-0">
                      <div className="common-btn">
                        <Link href={{ pathname: '/create-pack', query: { m: router.m } }}>
                          <Button variant="success" title="Create Card">Create Card</Button>
                        </Link>
                      </div>
                    </Col>
                  </Row> */}
                </TabPanel>
                <TabPanel value={this.state.value} index={1} dir={theme.direction}>
                  <MyContests matchId={decrypt(router.m)} changeTab={this.changeTab} />
                </TabPanel>
                <TabPanel value={this.state.value} index={2} dir={theme.direction}>
                  <MyPacks matchId={decrypt(router.m)} />
                </TabPanel>
              </SwipeableViews>
            </div>
          ) : (
            <div className="">
              <Row className="justify-content-center align-items-center top-app-header-row">
                <Col xs={6} >
                  <h4>Contests</h4>
                </Col>
                <Col xs={6} className="text-right">
                  <h4>
                    <OverlayTrigger
                      placement="bottom"
                      delay={{ show: 100, hide: 400 }}
                      overlay={<Tooltip id="button-tooltip">Contests</Tooltip>}
                    >
                      <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle" fill="#797e87" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                        <path d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588z" />
                        <circle cx="8" cy="4.5" r="1" />
                      </svg>
                    </OverlayTrigger>
                  </h4>
                </Col>
              </Row>
              <div >

              </div>
              <AllContest matchId={decrypt(router.m)} setContestCount={this.setContestCount} />
              {/* <Col xs={12} className="mobile-bottom-btn p-0">
                <div className="common-btn">
                  <Link href={{ pathname: '/create-pack', query: { m: router.m } }}>
                    <Button variant="success" title="Create Card">Create Card</Button>
                  </Link>
                </div>
              </Col> */}
            </div>
          )}

      </Layout>
    );
  }
}

const defaultPage = () => {
  const router = useRouter();
  const theme = useTheme();
  return (
    <Contests theme={theme} router={router.query} />
  )
}
export default defaultPage;
// export default withStyles(styles, { withTheme: true })(Contests);