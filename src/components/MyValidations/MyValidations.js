
const NAME_REGEX = /^[a-zA-Z ]+$/;
const MOBILE_NUMBER_REGEX = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;;

export function isNameValid(name) {
  return NAME_REGEX.test(String(name).trim());
}

export function isMobileNumberValid(mobileNumber) {
  return MOBILE_NUMBER_REGEX.test(String(mobileNumber).trim());
}

export function isEmailValid(emailid) {
  return EMAIL_REGEX.test(String(emailid).trim());
}

export const ErrorMessages = {
  VALIDATION_ERROR_TITLE: 'Validation Error',
  VALIDATION_ERROR_NAME: 'Please enter a valid full name',
  VALIDATION_ERROR_MOBILE: 'Please enter a valid mobile number',
  VALIDATION_ERROR_EMAIL: 'Please enter a valid email id',

};

export const MOBILE_NUMBER_MAX_LENGTH = 10;
