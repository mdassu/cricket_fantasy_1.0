import React, { Component } from 'react';
import Cookies from 'js-cookie';
import Head from 'next/head';
import { Container, Row, Col, Card, Badge } from 'react-bootstrap';
import TextField from '@material-ui/core/TextField';
import { Button, Snackbar } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import Overlay from 'react-bootstrap/Overlay';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import { ErrorMessages, isNameValid, isEmailValid } from 'components/MyValidations/MyValidations';
import { AuthenticationService } from '_services/AuthenticationService';
import { decrypt, encrypt } from '_helper/EncrDecrypt';
import Loader from 'components/CommonComponents/Loader';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFile: null,
      imagePreviewUrl: 'static/images/avatar.png',
      emailIsVerified: null,
      profilePicture: '',
      formData: {
        firstName: '',
        lastName: '',
        mobile: '',
        email: '',
      },
      error: true,
      errorMessage: {
        firstName: '',
        lastName: '',
        email: ''
      },
      user: {
        country_code_id: '',
        mobile: '',
        first_name: '',
        last_name: '',
        email: '',
        profile_picture: '',
        walletBalance: '',
      },
      alert: {
        toast: false,
        toastMessage: '',
        severity: ''
      },
      isButtonLoaded: true,
      isLoaded: false
    };
  }

  componentDidMount() {
    this.getMyProfile();
    this.getCurrentUser();
  }

  getCurrentUser() {
    AuthenticationService.currentUser.subscribe(user => {
      if (user && user != null) {
        this.setState({
          user: {
            country_code_id: user['country_code_id'],
            mobile: user['mobile'],
            first_name: user['first_name'],
            last_name: user['last_name'],
            email: user['email'],
            profile_picture: user['profile_picture'],
            walletBalance: user['walletBalance'],
            referralCode: user['referralCode']
          }
        })
      }
    });
  }

  getMyProfile = () => {
    AuthenticationService.getProfile(res => {
      if (res.status) {
        var data = res.data;
        if (data.profile_picture) {
          this.setState({
            imagePreviewUrl: process.env.IMG_URL + data.profile_picture
          })
        }
        this.setState({
          formData: {
            firstName: data.first_name,
            lastName: data.last_name,
            mobile: '+' + data.country_code + ' ' + data.mobile,
            email: data.email,
          },
          emailIsVerified: data.email_is_verified,
          isLoaded: true
        });
      }
    });
  }

  handleChange = (event) => {
    this.setState({
      formData: {
        ...this.state.formData,
        [event.target.name]: event.target.value
      }
    });
  }

  updateProfile = (e, type = '') => {
    e.preventDefault();
    const { formData } = this.state;
    var msgEmail = '';
    var msgFirstName = '';
    var msgLastName = '';

    var isValid = true;

    // first name validation
    if (formData.firstName) {
      if (!isNameValid(formData.firstName)) {
        msgFirstName = (ErrorMessages.VALIDATION_ERROR_NAME).replace('full', 'first');
        isValid = false;
      }
    } else {
      msgFirstName = 'First name is required';
      isValid = false;
    }

    // last name validation
    if (formData.lastName) {
      if (!isNameValid(formData.lastName)) {
        msgLastName = (ErrorMessages.VALIDATION_ERROR_NAME).replace('full', 'last');
        isValid = false;
      }
    } else {
      msgLastName = 'Last name is required';
      isValid = false;
    }

    // email validation
    if (formData.email) {
      if (!isEmailValid(formData.email)) {
        msgEmail = (ErrorMessages.VALIDATION_ERROR_EMAIL);
        isValid = false;
      }
    }
    //  else {
    //   msgLastName = 'Last name is required';
    //   isValid = false;
    // }

    // if (!msgFirstName && !msgLastName && !msgMobile) {
    //   isValid = true;
    // }

    if (isValid) {
      var params = {
        first_name: formData.firstName,
        last_name: formData.lastName,
        email: formData.email
      }
      this.setState({
        isButtonLoaded: false
      })
      AuthenticationService.editProfile(params, res => {
        if (res.status) {
          var user = res.data;
          var message = 'Profile updated successfully';
          if (type == 'resendEmail') {
            message = 'Your verification mail sent successfully!'
          }
          this.setState({
            emailIsVerified: user['email_is_verified'],
            user: {
              country_code_id: user['country_code_id'],
              mobile: user['mobile'],
              first_name: user['first_name'],
              last_name: user['last_name'],
              email: user['email'],
              profile_picture: user['profile_picture'],
              referralCode: user['referralCode'],
              isEmailVerified: user['email_is_verified']
            },
            alert: {
              toast: true,
              toastMessage: message,
              severity: 'success'
            },
            error: true,
            errorMessage: {
              firstName: '',
              lastName: '',
              email: ''
            },
            isButtonLoaded: true
          }, () => {
            if (Cookies.get('_user')) {
              var data = decrypt(Cookies.get('_user'));
              data['first_name'] = user['first_name'];
              data['last_name'] = user['last_name'];
              data['email'] = user['email'];
              data['profile_picture'] = user['profile_picture'];
              data['isEmailVerified'] = user['email_is_verified'];
              Cookies.set('_user', encrypt(data), { expires: 30 });

              // AuthenticationService.currentUserSubject.next(data);
            }
          });
        } else {
          var message = 'Profile not updated';
          if (type == 'resendEmail') {
            message = 'Mail not sent successfully!'
          }
          this.setState({
            alert: {
              toast: true,
              toastMessage: message,
              severity: 'error'
            },
            isButtonLoaded: true,
          }, () => {
            AuthenticationService.currentUserSubject.next(this.state.user);
          });
        }
      });
    } else {
      this.setState({
        error: true,
        errorMessage: {
          firstName: msgFirstName,
          lastName: msgLastName,
          email: msgEmail
        },
        isButtonLoaded: true
      });
    }

  }

  fileChangedHandler = event => {
    if ((event.target.files[0].name).split('.').pop() == 'jpg' || (event.target.files[0].name).split('.').pop() == 'jpeg' || (event.target.files[0].name).split('.').pop() == 'png') {
      if ((event.target.files[0].size / 1048576) <= 2) {
        let reader = new FileReader();

        reader.onloadend = () => {
          this.setState({
            imagePreviewUrl: reader.result
          });
        }
        reader.readAsDataURL(event.target.files[0]);

        this.setState({
          selectedFile: event.target.files[0]
        }, () => {
          var params = new FormData();
          params.append(
            'files',
            this.state.selectedFile,
            this.state.selectedFile.name
          );

          AuthenticationService.profileUpload(params, res => {
            this.setState({
              alert: {
                toast: true,
                toastMessage: 'Profile picture uploaded successfully',
                severity: 'success'
              }
            });
            var user = this.state.user;
            this.setState({
              user: {
                country_code_id: user['country_code_id'],
                mobile: user['mobile'],
                first_name: user['first_name'],
                last_name: user['last_name'],
                email: user['email'],
                profile_picture: res,
                walletBalance: this.state.user.walletBalance,
                referralCode: user['referralCode']
              }
            }, () => {
              Cookies.remove('_user');
              Cookies.set('_user', encrypt(this.state.user), { expires: 30 });
              AuthenticationService.currentUserSubject.next(this.state.user);
            });
          });
        });
      } else {
        this.setState({
          alert: {
            toast: true,
            toastMessage: 'File size should be less then 2MB',
            severity: 'error'
          }
        });
      }

    } else {
      this.setState({
        alert: {
          toast: true,
          toastMessage: 'Please choose only jpg, jpeg or png file',
          severity: 'error'
        }
      });
    }
  }

  resendEmail = () => {
    var params = {
      email: this.state.formData.email
    }
    AuthenticationService.sendEmail(params, res => {
      if (res.status) {
        this.setState({
          alert: {
            toast: true,
            toastMessage: 'Your verification mail sent on your email id.',
            severity: 'success'
          }
        });
      } else {
        this.setState({
          alert: {
            toast: true,
            toastMessage: res['message'],
            severity: 'error'
          }
        });
      }
    });
  }


  render() {
    const { isLoaded, errorMessage } = this.state;
    let $imagePreview;
    if (this.state.imagePreviewUrl) {
      $imagePreview = (<div className="image-container" ><img src={this.state.imagePreviewUrl} alt="icon" /> </div>);
    }
    const { formData, emailIsVerified, alert, isButtonLoaded } = this.state;

    const verifiedIcon = (<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-patch-check-fll" fill="green" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" d="M10.067.87a2.89 2.89 0 0 0-4.134 0l-.622.638-.89-.011a2.89 2.89 0 0 0-2.924 2.924l.01.89-.636.622a2.89 2.89 0 0 0 0 4.134l.637.622-.011.89a2.89 2.89 0 0 0 2.924 2.924l.89-.01.622.636a2.89 2.89 0 0 0 4.134 0l.622-.637.89.011a2.89 2.89 0 0 0 2.924-2.924l-.01-.89.636-.622a2.89 2.89 0 0 0 0-4.134l-.637-.622.011-.89a2.89 2.89 0 0 0-2.924-2.924l-.89.01-.622-.636zm.287 5.984a.5.5 0 0 0-.708-.708L7 8.793 5.854 7.646a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z" />
    </svg>);

    const notVerifiedIcon = (<svg onClick={($event) => this.updateProfile($event, 'resendEmail')} width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-x-octagon-fill" fill="red" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" d="M11.46.146A.5.5 0 0 0 11.107 0H4.893a.5.5 0 0 0-.353.146L.146 4.54A.5.5 0 0 0 0 4.893v6.214a.5.5 0 0 0 .146.353l4.394 4.394a.5.5 0 0 0 .353.146h6.214a.5.5 0 0 0 .353-.146l4.394-4.394a.5.5 0 0 0 .146-.353V4.893a.5.5 0 0 0-.146-.353L11.46.146zm-6.106 4.5a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z" />
    </svg>

    );

    return (
      <React.Fragment>
        { isLoaded ? (
          <div>
            <Snackbar open={alert.toast} autoHideDuration={2000} onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })}>
              <Alert onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })} severity={alert.severity}>
                {alert.toastMessage}
              </Alert>
            </Snackbar>
            <div className="profile-breadcrumb">
              <Container className="myprofile">
                {/*Start Row */}
                <Row>
                  <Col lg={12} className="text-center">
                    <div className="upload-btn-wrapper">
                      <button className="btn-browse">
                        <span>
                          {$imagePreview}
                        </span>
                        <div className="svg-icon">
                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-camera" fill="#262f3e" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M15 12V6a1 1 0 0 0-1-1h-1.172a3 3 0 0 1-2.12-.879l-.83-.828A1 1 0 0 0 9.173 3H6.828a1 1 0 0 0-.707.293l-.828.828A3 3 0 0 1 3.172 5H2a1 1 0 0 0-1 1v6a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1zM2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2z" />
                            <path fillRule="evenodd" d="M8 11a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5zm0 1a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z" />
                            <path d="M3 6.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z" />
                          </svg>
                        </div>
                      </button>
                      <input type="file" name="avatar" onChange={this.fileChangedHandler} />
                    </div>
                    <label className="user-profilename">{formData.firstName} {formData.lastName}</label>
                  </Col>
                </Row>
              </Container>
            </div>
            <Col lg={12}>
              <div className="outer-card mt-lg-3">
                <Card>
                  <Card.Body className="">
                    <Row className="justify-content-center align-items-center p-lg-3">
                      <Col lg={12}>
                        <div className="deposit-method">
                          <div className="card-detail-box">
                            <form className="register-form userprofle-form" onSubmit={this.updateProfile}>
                              <Row>
                                <Col md={6}>
                                  <TextField
                                    variant="outlined"
                                    label="First Name"
                                    placeholder="Enter First Name"
                                    name="firstName"
                                    value={formData.firstName}
                                    onChange={this.handleChange}
                                    error={!!errorMessage.firstName}
                                    helperText={!!errorMessage.firstName ? errorMessage.firstName : ''}
                                    inputProps={{ maxLength: 20 }}
                                  />
                                </Col>
                                <Col md={6}>
                                  <TextField
                                    variant="outlined"
                                    label="Last Name"
                                    placeholder="Enter Last Name"
                                    name="lastName"
                                    value={formData.lastName}
                                    onChange={this.handleChange}
                                    error={!!errorMessage.lastName}
                                    helperText={!!errorMessage.lastName ? errorMessage.lastName : ''}
                                    inputProps={{ maxLength: 20 }}
                                  />
                                </Col>
                                <Col md={6}>
                                  <TextField
                                    className="disable-input"
                                    label="Mobile number"
                                    placeholder="Enter mobile number"
                                    name="mobile"
                                    variant="outlined"
                                    onChange={this.handleChange}
                                    value={formData.mobile}
                                    InputProps={{
                                      readOnly: true,
                                    }}
                                  />
                                </Col>
                                <Col md={6} className="email-status-main">
                                  <TextField
                                    label="Email Address"
                                    placeholder="Enter Email Address"
                                    name="email"
                                    onChange={this.handleChange}
                                    type="email"
                                    variant="outlined"
                                    value={formData.email}
                                    error={!!errorMessage.email}
                                    helperText={!!errorMessage.email ? errorMessage.email : ''}
                                  />
                                  {/* Note This below div for email status verfied and not verified */}
                                  {
                                    emailIsVerified == null ? '' :
                                      (
                                        <div className="email-status">

                                          {['top'].map((placement) => (
                                            <OverlayTrigger
                                              key={placement}
                                              placement={placement}
                                              overlay={
                                                <Tooltip id={`tooltip-${placement}`}>
                                                  {emailIsVerified == 1 ? ('Email is verified') : ('Click to resend verification email')}
                                                </Tooltip>
                                              }
                                            >
                                              {emailIsVerified == 1 ? verifiedIcon : notVerifiedIcon}
                                              {/* Not Verified */}

                                              {/* <i className="fas fa-times-circle"></i> */}
                                            </OverlayTrigger>
                                          ))}
                                        </div>
                                      )
                                  }

                                </Col>
                                <Col md={12} className="text-center mt-lg-3">
                                  {isButtonLoaded ? (
                                    <Button type="submit" variant="contained" title="Update Profile" className=" updateprofile-btn">
                                      Update Profile
                                    </Button>
                                  ) : (
                                      <Button type="button" variant="contained" title="Update Profile" className=" updateprofile-btn">
                                        <img src="static/images/loader.gif" alt="" style={{ width: '7.388%' }} className="empty-mybet" />
                                      </Button>
                                    )}
                                </Col>
                              </Row>
                            </form>
                          </div>
                        </div>
                      </Col>
                    </Row>
                  </Card.Body>
                </Card>
              </div>
            </Col>
          </div>
        ) : <Loader />}
      </React.Fragment>
    );
  }
}

export default Profile;
