import Link from 'next/link';
import Cookies from 'js-cookie';
import React, { Component } from "react";
import { Container, ProgressBar, Row, Col, Card, Badge, Button, Table } from 'react-bootstrap';
import { encrypt, decrypt } from '_helper/EncrDecrypt';
import { ContestsService } from '_services/ContestsService';
import NoData from 'components/Contests/NoData'
import Router from 'next/router';
import { AuthenticationService } from '_services/AuthenticationService';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import { TransactionService } from '_services/TransactionService';
import { PacksService } from '_services/PacksService';
import { CommonService } from '_services/CommonService';
import Loader from 'components/CommonComponents/Loader';
import AlertPopup from 'components/CommonComponents/AlertPopup';
import { GAevent } from "_helper/google-analytics";


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

class AllContest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      contestId: 0,
      packId: 0,
      matchContests: [],
      userWalletBalance: 0,
      email: '',
      mobile: '',
      bonusBalance: 0,
      payableAmount: 0,
      contestEntryFee: 0,
      confirmationPopup: false,
      congratsPopup: false,
      packCount: 0,
      discardPopup: false,
      alertPopup: false,
      isDisabled: false
    }
  }

  componentDidMount() {
    CommonService.updateUserWallet();
    this.getContests();
    this.getCurrentUser();
    if (Cookies.get('_pack') && Cookies.get('_ctData') && Cookies.get('_match')) {
      var contestData = decrypt(Cookies.get('_ctData'));
      setTimeout(() => {
        if (this.state.userWalletBalance >= contestData.entryFee) {
          this.setState({
            contestEntryFee: contestData.entryFee,
            contestId: contestData.contestId,
            confirmationPopup: true
          });
        } else {
          var remainAmount = contestData.entryFee - this.state.userWalletBalance;
          Cookies.set('_amount', encrypt(remainAmount));
          Router.push({
            pathname: '/low-balance',
          });
        }
      }, 200);
    }
  }

  getCurrentUser() {
    AuthenticationService.currentUser.subscribe(user => {
      if (user && user != null) {
        this.setState({
          userWalletBalance: parseFloat(user['walletBalance']),
          email: user['email'],
          mobile: user['mobile'],
        });
      }
    });
  }

  getContests() {
    ContestsService.getMatchContests({ match_id: this.props.matchId }, res => {
      if (res.status) {
        var data = res['data'];
        this.setState({
          matchContests: data['contestList'],
          packCount: data['packCount'],
          contestCount: data['contestCount'],
          isLoaded: true
        });
        this.props.setContestCount({
          contestCount: data['contestCount'],
          packCount: data['packCount']
        });
      } else {
        this.setState({
          matchContests: [],
          isLoaded: true
        });
      }
    })
  }

  enterContest = (entryFee, contestId, purchasedPackCount, remainingPacks) => {
    AuthenticationService.clearCookies();
    if (remainingPacks == 0) {
      this.setState({
        alertPopup: true
      });
    } else {
      var contestData = {
        entryFee: entryFee,
        contestId: contestId
      }
      Cookies.set('_ctData', encrypt(contestData));
      Cookies.set('_match', encrypt(this.props.matchId));
      Router.push({
        pathname: '/create-pack',
        query: {
          m: encrypt(this.props.matchId)
        }
      });
    }
  }

  joinContest = () => {
    this.setState({
      isDisabled: true
    }, () => {
      var value = '';
      if (this.state.mobile && this.state.mobile != '') {
        value = this.state.mobile
      } else {
        value = this.state.email
      }
      GAevent('Started_Payment_Process', 'On Join Contest', 'How many people started payment process (from which page)', value);
      var data = decrypt(Cookies.get('_pack'));
      var matchId = Cookies.get('_match');
      var params = {
        match_id: decrypt(matchId),
        list: data.selectedCredit,
        match_contest_id: this.state.contestId
      };
      PacksService.savePack(params, (res) => {
        if (res.status) {
          AuthenticationService.clearCookies();
          this.confirmPopupClose();
          this.setState({
            congratsPopup: true
          });
          GAevent('Packs_Created', 'On Successfull Purchase', 'How many packs created', value);
          GAevent('Joined_Contest', 'On Click', 'How many packs created', value);
          CommonService.updateUserWallet();
        } else {
          AuthenticationService.clearCookies();
          this.confirmPopupClose();
          Router.push({
            pathname: '/contests',
            query: {
              m: matchId
            }
          });
        }
      });
    });
  }

  confirmPopupClose = () => {
    if (Cookies.get('_pack') && Cookies.get('_ctData') && Cookies.get('_match')) {
      this.setState({
        discardPopup: true
      });
    } else {
      this.setState({
        confirmationPopup: false,
        discardPopup: false,
        contestEntryFee: 0,
        bonusBalance: 0,
      });
    }
  }

  success = () => {
    this.setState({
      congratsPopup: false
    });
    this.getContests();
  }

  discardContest = () => {
    this.setState({
      discardPopup: false,
      confirmationPopup: false,
      contestEntryFee: 0,
      bonusBalance: 0,
    });
    AuthenticationService.clearCookies();
  }

  continuePurchase = () => {
    this.setState({
      discardPopup: false,
      confirmationPopup: true
    });
  }

  render() {
    const { matchContests, isLoaded, alertPopup, confirmationPopup, userWalletBalance, contestEntryFee, bonusBalance, congratsPopup, discardPopup, isDisabled } = this.state;

    var contestCards = '';
    if (matchContests.length > 0) {
      contestCards = matchContests.map((item, key) => {
        return (
          <React.Fragment key={key}>
            <div className="outer-card">
              <h2 className="heading other-heading">{item.contest_name}</h2>
            </div>
            {/* <Link href={{ pathname: '/contests-detail', query: { c: encrypt(item.match_id) } }}> */}
            <Card>
              <Card.Body>
                <div className="contents-listing" style={{ cursor: 'pointer' }}>
                  <Row className="m-0">
                    <Link href={{ pathname: '/contests-detail', query: { m: encrypt(item.match_id), c: encrypt(item.match_contest_id) } }}>

                      <Col lg={6} xs={6} md={6} sm={6} className="p-0">
                        <span className="prizepool">Prize Pool</span>
                        {/* <h4 className="current-prize">Curr &#x20B9; {item.current_contest_value}</h4> */}
                        <h4 className="current-prize">&#x20B9; {item.max_prize}</h4>
                        {/* <h6 className="max-prize">Max &#x20B9; {item.max_prize}</h6> */}
                      </Col>
                    </Link>
                    <Col lg={6} xs={6} md={6} sm={6} className="p-0 text-right">
                      <span className="prizepool">Entry Fee - &#x20B9;{item.entry_amount}</span>
                      {/* <Link href="/"> */}
                      <a className="prize-pool-btn" onClick={() => this.enterContest(item.entry_amount, item.match_contest_id, item.purchased_pack_count, item.remaining_packs)} style={{ cursor: 'pointer' }}>Enter Contest</a>
                      {/* </Link> */}
                    </Col>
                    <Link href={{ pathname: '/contests-detail', query: { m: encrypt(item.match_id), c: encrypt(item.match_contest_id) } }}>

                      <Col lg={12} className="progressbar p-0">
                        <ProgressBar now={(item.packs - item.remaining_packs) * 100 / item.packs} />
                        <ul className="float-left ">
                          <li className="left-point">{item.remaining_packs} cards left </li>
                        </ul>
                        <ul className=" float-right  ">
                          <li className="totalpacks">{item.packs} Cards</li>
                        </ul>
                        <div className="clearfix"></div>
                      </Col>
                    </Link>
                  </Row>

                </div>
              </Card.Body>
              <Card.Header className="cardfooter">
                <ul className="float-left cardfooter-inner">
                  <li>
                    <img src="static/images/trophy.svg" className="winner-trophy" /> 1st Prize &#x20B9;{item.top_price}
                  </li>
                </ul>
                <ul className=" float-right cardfooter-inner ">
                  <li className=" mr-0"><img src="static/images/star.svg" className="winner-trophy" /> {item.pack_win_percentage}% cards win</li>
                </ul>
                <div className="clearfix"></div>
              </Card.Header>
            </Card>
            {/* </Link> */}
          </React.Fragment>
        )
      })
    }

    return (
      <React.Fragment>
        {isLoaded ?
          (
            <React.Fragment>
              { contestCards ? contestCards : <NoData headText="There is no contest for this match!" buttonText="Back to home" buttonUrl="/" />}
            </React.Fragment>
          ) : (
            <Loader />
          )
        }
        {/* Confirmation modal */}
        <Dialog
          className="common-modals"
          TransitionComponent={Transition}
          open={confirmationPopup}
          keepMounted
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <CloseIcon className="close-popup" title="Close" onClick={this.confirmPopupClose} />
          <DialogTitle id="alert-dialog-slide-title">
            Confirmation
            <p>
              Wallet amount =  <b>&#x20B9; {userWalletBalance}</b>
            </p>
          </DialogTitle>
          <DialogContent>
            {/* <DialogContentText id="alert-dialog-slide-description"> */}
            <Table border-0="true" className="justify-content-center align-items-center">
              <tbody>
                <tr>
                  <td className="text-left">
                    Entry
                    </td>
                  <td className="text-right">&#x20B9;{contestEntryFee}</td>
                </tr>
                {/* <tr>
                  <td className="text-left">
                    <img src="static/images/bonus.png" />
                      Bonus
                    </td>
                  <td className="text-right">-&#x20B9;{bonusBalance}</td>
                </tr> */}
                <tr className="tablefooter">
                  <td className="text-left">
                    To Pay
                    </td>
                  <td className="text-right">
                    &#x20B9;{contestEntryFee - bonusBalance}
                  </td>
                </tr>
              </tbody>
            </Table>
            {/* </DialogContentText> */}
          </DialogContent>
          <DialogActions>
            <div className="common-btn">
              <Button variant="success" title="Join Contest" onClick={this.joinContest} disabled={isDisabled}>Join Contest</Button>
            </div>
          </DialogActions>
        </Dialog>
        {/* End Confirmation modal */}
        {/* Congrats modal */}
        <Dialog
          className="common-modals"
          TransitionComponent={Transition}
          open={congratsPopup}
          keepMounted
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogContent>
            {/* <DialogContentText id="alert-dialog-slide-description"> */}
            <Row className="m-0  justify-content-center align-items-center">
              <Col xs={12} className="text-center slide-modal-body ">
                <h5>Congratulations!</h5>
                {/* <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle" fill="#844ff7" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                    <path d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588z" />
                    <circle cx="8" cy="4.5" r="1" />
                  </svg> */}
                <p>Your card has been submitted successfully!</p>
              </Col>
            </Row>
            {/* </DialogContentText> */}
          </DialogContent>
          <DialogActions>
            <div className="common-btn">
              {/* <Link href="/"> */}
              <Button variant="success" title="Done" onClick={this.success}>Done</Button>
              {/* </Link> */}
            </div>
          </DialogActions>
        </Dialog>
        {/* End Congrats modal */}

        {/* Go back confimation modal */}
        <Dialog
          className="modal-slide-modal"
          open={discardPopup}
          TransitionComponent={Transition}
          keepMounted
          onClick={this.goBackClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <div >
            <DialogContent >
              <div>
                <div className="create-packs-tabs">
                  <React.Fragment>
                    <div className="create-my-pack header-row-my-pack  m-0">
                      <Row className="m-0  justify-content-center align-items-center">
                        <Col xs={12} className="text-center slide-modal-body ">
                          <h5>Alert!</h5>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-exclamation-circle" fill="#844ff7" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                            <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z" />
                          </svg>
                          <p>Your Card will not be saved!</p>
                        </Col>
                        <DialogActions >
                          <Col xs={12} className="slide-modal mobile-bottom-btn p-0">
                            <div className="common-btn">
                              <Button variant="secondary" title="Continue Purchase" onClick={this.continuePurchase}>Continue Purchase</Button>
                              <Button variant="success" title="Discard Card" onClick={this.discardContest}>Discard Card</Button>
                            </div>
                          </Col>
                        </DialogActions>
                      </Row>
                    </div>
                  </React.Fragment>
                </div>
              </div>
            </DialogContent>
          </div>
        </Dialog>
        {
          alertPopup ? (
            <AlertPopup type='' alertPopup={alertPopup} alertMsg='Contest full house!' alertButton='Ok' buttonAction={() => this.setState({ alertPopup: false })} />
          ) : ''
        }
      </React.Fragment >
    )
  }
}
export default AllContest;
