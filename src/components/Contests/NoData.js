import Link from 'next/link';
import React, { Component } from "react";


class NoData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      card: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    }
  }

  render() {
    const { headText, buttonText, buttonUrl } = this.props
    return (
      <React.Fragment>
        <div className="no-data text-center">
          <h6>{headText}</h6>
          <img src="static/images/empty.png" className="" />
          {buttonUrl ? (
            <div className="create-pack">
              {
                typeof (buttonUrl) == 'function' ? (
                  <a title={buttonText} onClick={buttonUrl} style={{ cursor: 'pointer' }}>{buttonText}</a>
                ) : (
                    <Link href={buttonUrl}>
                      <a title={buttonText}>{buttonText}</a>
                    </Link>
                  )
              }
            </div>
          ) : ''}
        </div>

      </React.Fragment>
    )
  }
}
export default NoData;

