import Link from 'next/link';
import React, { Component } from "react";
import { Container, ProgressBar, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import Router from 'next/router';
import { ContestsService } from '_services/ContestsService';
import NoData from './NoData';
import { decrypt } from '_helper/EncrDecrypt';
import Loader from 'components/CommonComponents/Loader';

class LeaderBoard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      usersList: [],
      isLoaded: false
    }
  }

  componentDidMount() {
    this.getLeaderboard();
  }

  getLeaderboard = () => {
    var params = {
      contest_id: decrypt(Router.query.c)
    }
    ContestsService.getContestWinner(params, (res) => {
      if (res.status) {
        this.setState({
          usersList: res['data']['winnerModel'],
          isLoaded: true
        })
      } else {
        this.setState({
          usersList: [],
          isLoaded: true
        })
      }
    })
  }
  render() {
    const { usersList, isLoaded } = this.state;
    const { matchStatus } = this.props;

    var userData = '';
    if (usersList && usersList != null) {
      userData = usersList.map((user, key) => {
        return (
          <tr key={key} className="winner-completed-tr" style={{ background: user.my_record == 1 ? '#b0ffd6' : '#fff' }}>
            <td className="text-left text-capitalize">
              <img src={user.profile_img != '' && user.profile_img != null ? process.env.IMG_URL + user.profile_img : 'static/images/winner.jpg'} className="userimg d-inline-block" />
              {user.first_name} {user.last_name}
              <div className="d-inline-block"></div><br />(Card: {user.pack_name})
            </td>
            {
              matchStatus == 2 ? (
                <React.Fragment>
                  <td className="text-center text-capitalize">
                    {user.final_potential_payout}
                  </td>
                  <td className="text-center text-capitalize">
                    #{user.rank}
                  </td>
                  <td className="text-center text-capitalize">
                    &#x20B9;{user.won_amount}
                  </td>
                </React.Fragment>
              ) : ''
            }
          </tr>
        )
      });
    }
    return (
      <React.Fragment>
        <div className="contests-tabs leaderboard-table">
          {
            isLoaded ? (
              userData != '' ? (
                <Table bordered hover>
                  <thead>
                    <tr>
                      <th>All Cards</th>
                      {
                        matchStatus == 2 ? (
                          <React.Fragment>
                            <th className="text-center">Points Earned</th>
                            <th className="text-center">Rank</th>
                            <th className="text-center">Won Prize</th>
                          </React.Fragment>
                        ) : ''
                      }
                    </tr>
                  </thead>
                  <tbody>
                    {userData}
                  </tbody>
                </Table>
              ) : <NoData headText="Yet to be declaired!" />
            ) : <Loader />
          }
        </div>

      </React.Fragment>
    )
  }
}
export default LeaderBoard;

