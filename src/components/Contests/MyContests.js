import Link from 'next/link';
import Cookies from 'js-cookie';
import React, { Component } from "react";
import { Container, ProgressBar, Row, Col, Card, Badge, Button, Table } from 'react-bootstrap';
import { ContestsService } from '_services/ContestsService';
import NoData from 'components/Contests/NoData';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import { AuthenticationService } from '_services/AuthenticationService';
import { TransactionService } from '_services/TransactionService';
import { encrypt } from '_helper/EncrDecrypt';
import Router from 'next/router';
import Loader from 'components/CommonComponents/Loader';
import AlertPopup from 'components/CommonComponents/AlertPopup';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

class MyContests extends Component {
  constructor(props) {
    super(props);
    this.state = {
      myContests: [],
      contestId: 0,
      packId: 0,
      isLoaded: false,
      userWalletBalance: 100,
      bonusBalance: 0,
      payableAmount: 0,
      contestEntryFee: 0,
      confirmationPopup: false,
      packCount: 0,
      alertPopup: false
    }
  }


  componentDidMount() {
    this.getContests();
  }

  getContests() {
    ContestsService.getMyContests({ match_id: this.props.matchId }, res => {
      if (res.status) {
        var data = res['data'];
        this.setState({
          myContests: data['contestList'],
          packCount: data['packCount'],
          isLoaded: true
        });
      } else {
        this.setState({
          myContests: [],
          isLoaded: true
        });
      }
    })
  }

  enterContest = (entryFee, contestId, purchasedPackCount, remainingPacks) => {
    AuthenticationService.clearCookies();
    if (remainingPacks == 0) {
      this.setState({
        alertPopup: true
      });
    } else {
      var contestData = {
        entryFee: entryFee,
        contestId: contestId
      }
      Cookies.set('_ctData', encrypt(contestData));
      Cookies.set('_match', encrypt(this.props.matchId));
      Router.push({
        pathname: '/create-pack',
        query: {
          m: encrypt(this.props.matchId)
        }
      });
    }
  }


  render() {
    const { myContests, isLoaded, confirmationPopup, userWalletBalance, contestEntryFee, bonusBalance, alertPopup } = this.state;
    if (myContests.length > 0) {


      var contestCards = myContests.map((item, key) => {
        return (
          <React.Fragment key={key}>
            <div className="outer-card">
              <h2 className="heading other-heading">{item.contest_name}</h2>
            </div>
            <Card>
              <Card.Body>
                <div className="contents-listing" style={{ cursor: 'pointer' }}>
                  <Row className="m-0">
                    <Link href={{ pathname: '/contests-detail', query: { m: encrypt(item.match_id), c: encrypt(item.match_contest_id) } }}>

                      <Col lg={6} xs={6} md={6} sm={6} className="p-0">
                        <span className="prizepool">Prize Pool</span>
                        <h4 className="current-prize">Curr &#x20B9; {item.current_contest_value}</h4>
                        <h6 className="max-prize">Max &#x20B9; {item.max_prize}</h6>
                      </Col>
                    </Link>
                    <Col lg={6} xs={6} md={6} sm={6} className="p-0 text-right">
                      <span className="prizepool">Entry Fee - &#x20B9;{item.entry_amount}</span>
                      {/* <Link href="/"> */}
                      <a className="prize-pool-btn" onClick={() => this.enterContest(item.entry_amount, item.match_contest_id, item.purchased_pack_count, item.remaining_packs)} style={{ cursor: 'pointer' }}>Enter Contest</a>
                      {/* </Link> */}
                    </Col>
                    <Link href={{ pathname: '/contests-detail', query: { m: encrypt(item.match_id), c: encrypt(item.contest_id) } }}>

                      <Col lg={12} className="progressbar p-0">
                        <ProgressBar now={(item.packs - item.remaining_packs) * 100 / item.packs} />
                        <ul className="float-left ">
                          <li className="left-point">{item.remaining_packs} cards left </li>
                        </ul>
                        <ul className=" float-right  ">
                          <li className="totalpacks">{item.packs} Cards</li>
                        </ul>
                        <div className="clearfix"></div>
                      </Col>
                    </Link>
                  </Row>

                </div>
              </Card.Body>
              <Card.Header className="cardfooter">
                <ul className="float-left cardfooter-inner">
                  <li>
                    <img src="static/images/trophy.svg" className="winner-trophy" /> 1st Prize &#x20B9;{item.top_price}
                  </li>
                </ul>
                <ul className=" float-right cardfooter-inner ">
                  <li className=" mr-0"><img src="static/images/star.svg" className="winner-trophy" /> {item.pack_win_percentage}% cards win</li>
                </ul>
                <div className="clearfix"></div>
              </Card.Header>
            </Card>
          </React.Fragment>
        )
      })
    }
    return (
      <React.Fragment>
        {isLoaded ?
          (
            <React.Fragment>
              {contestCards ? contestCards : <NoData headText="You haven't joined contest!" buttonText="Join a contest" buttonUrl={this.props.changeTab} />}
              {
                alertPopup ? (
                  <AlertPopup type='' alertPopup={alertPopup} alertMsg='Contest full house!' alertButton='Ok' buttonAction={() => this.setState({ alertPopup: false })} />
                ) : ''
              }
            </React.Fragment>
          ) : (
            <Loader />
          )
        }
      </React.Fragment>
    )
  }
}
export default MyContests;
