import React, { Component } from "react";
import Layout from 'components/Layout/Layout';
import Link from 'next/link';
import { Container, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import Countdown from 'react-countdown';
import millify from 'millify';
import * as moment from 'moment';
import { HomeService } from "_services/HomeService";
import NoData from "components/Contests/NoData";
import { encrypt } from "_helper/EncrDecrypt";
import Router from "next/router";
import { AuthenticationService } from "_services/AuthenticationService";
import Loader from "components/CommonComponents/Loader";

class Upcoming extends Component {
  constructor(props) {
    super(props);
    this.state = {
      matches: [],
      isLoaded: false
    }
  }

  componentDidMount() {
    this.getMatches();
  }

  getMatches = () => {
    HomeService.getMyMatches({ match_status: 0 }, (res) => {
      if (res.status) {
        this.setState({
          matches: res['data'],
          isLoaded: true
        });
      } else {
        this.setState({
          matches: [],
          isLoaded: true
        });
      }
    });
  }

  enterContest = (matchId) => {
    if (AuthenticationService.isLogged) {
      Router.push({
        pathname: '/contests',
        query: { m: encrypt(matchId) }
      });
    } else {
      Router.push({
        pathname: '/create-pack',
        query: { m: encrypt(matchId) }
      });
    }
  }

  hexToRGBA = (hex) => {
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    if (hex) {
      hex = hex.replace(shorthandRegex, function (m, r, g, b) {
        return r + r + g + g + b + b;
      });

      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

      if (result) {
        var r = parseInt(result[1], 16);
        var g = parseInt(result[2], 16);
        var b = parseInt(result[3], 16);
        // return r + "," + g + "," + b;//return 23,14,45 -> reformat if needed
        return `rgba(${r}, ${g}, ${b}, 0.44)`
      }
    }
    // hex = '0x' + hex
    // let r = hex >> 16 & 0xFF
    // let g = hex >> 8 & 0xFF
    // let b = hex & 0xFF
  }

  render() {
    const { isLoaded, matches } = this.state;
    const renderer = ({ days, hours, minutes, seconds, completed }) => {
      if (completed) {
        return <span>{days == 0 ? '' : days < 10 ? '0' + days + 'd' : days + 'd'} {hours < 10 ? '0' + hours : hours}h {minutes < 10 ? '0' + minutes : minutes}m {seconds < 10 ? '0' + seconds : seconds}s</span>;
      } else {
        // Render a countdown
        return <span>{days == 0 ? '' : days < 10 ? '0' + days + 'd' : days + 'd'} {hours < 10 ? '0' + hours : hours}h {minutes < 10 ? '0' + minutes : minutes}m {seconds < 10 ? '0' + seconds : seconds}s</span>;
      }
    };
    return (
      <React.Fragment>
        {isLoaded ? (
          <div className="body-content" >
            <Row className="h-100">
              <Col lg={12} className="h-100">
                <div className="outer-card">
                  {
                    matches && matches.length > 0 ? (
                      matches.map((item, key) => (
                        <Card key={key}>
                          <Card.Body>
                            <div className="ipl-matches">{item.matchNumber}</div>
                            <time className="match-start-time"><Countdown date={moment(item.match_date_time).format()} renderer={renderer} /></time>
                            <div className="team-listing" onClick={() => this.enterContest(item.match_id)}>
                              {/* <Link href={}> */}
                              <Row>
                                <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                                  <div className="team-logo team-left">
                                    <div className="logo-line line-left" style={{ background: this.hexToRGBA(item.hometeamcolorcode) }}>
                                    </div>
                                    <div className="team-name team-left-name" style={{ background: item.hometeamcolorcode, boxShadow: `0px 8px 15px 0px ${this.hexToRGBA(item.hometeamcolorcode)}` }}>
                                      {item.hometeamname}
                                    </div>
                                  </div>
                                </Col>
                                <Col lg={4} xs={4} md={4} sm={4} className="text-center">
                                  <img src="static/images/vs.svg" className="team-vs" />
                                </Col>
                                <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                                  <div className="team-logo team-right">
                                    <div className="logo-line line-right" style={{ background: this.hexToRGBA(item.awayteamcolorcode) }}>
                                    </div>
                                    <div className="team-name team-right-name" style={{ background: item.awayteamcolorcode, boxShadow: `0px 8px 15px 0px ${this.hexToRGBA(item.awayteamcolorcode)}` }}>
                                      {item.awayteamname}
                                    </div>
                                  </div>
                                </Col>
                              </Row>
                              {/* </Link> */}
                            </div>
                          </Card.Body>
                          <Card.Header className="cardfooter">
                            <ul className=" float-left cardfooter-inner">
                              <li>
                                <b>{item.no_of_pack}</b> Cards
                              </li>
                              <li>
                                <b>{item.no_of_contest}</b> Contests
                               </li>
                            </ul>
                            {
                              item.won_amount && item.won_amount != null ? (
                                <ul className="float-right winner-right">
                                  <li>
                                    <img src="static/images/trophy.svg" className="winner-trophy" /> You won &#x20B9; {item.won_amount}
                                  </li>
                                </ul>
                              ) : ''
                            }
                            <div className="clearfix"></div>
                          </Card.Header>
                        </Card>
                      ))
                    ) : (<NoData headText="You don't have any upcoming matches!" />)
                  }
                </div>
              </Col>
            </Row>
          </div>
        ) : (
            <Loader />
          )
        }
      </React.Fragment >
    )
  }
}
export default Upcoming;
