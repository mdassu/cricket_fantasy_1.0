import Link from 'next/link';
import React, { Component } from "react";
import { Container, ProgressBar, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import { ContestsService } from '_services/ContestsService';
import NoData from 'components/Contests/NoData'
import { encrypt } from '_helper/EncrDecrypt';
import Loader from 'components/CommonComponents/Loader';

class MyCompletedPacks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      myPacks: [],
      isLoaded: false
    }
  }


  componentDidMount() {
    this.getPacks();
  }

  getPacks() {
    ContestsService.getMyPacks({ match_id: this.props.matchId }, res => {
      if (res.status) {
        this.setState({
          myPacks: res['data'],
          isLoaded: true
        });
      } else {
        this.setState({
          myPacks: [],
          isLoaded: true
        });
      }
    })
  }
  render() {
    const { myPacks, isLoaded } = this.state;
    var packCards = '';
    if (myPacks.length > 0) {
      packCards = myPacks.map((item, key) => {
        return (
          <Card>
            <Card.Body className="mypacks myearned-pack">
              <div className="overlay-div">
                <Row className="m-0">
                  <Col lg={6} xs={6} md={6} sm={6} className="p-0">
                    <h5>{item.pack_name}</h5>
                  </Col>
                  <Col lg={6} xs={6} md={6} sm={6} className="p-0 text-right">
                    <Link href={{ pathname: '/preview-pack', query: { m: encrypt(this.props.matchId), p: encrypt(item.pack_order_id) } }}>
                      <a title="Preview Card" className="edit-team">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-eye" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fillRule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 0 0 1.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0 0 14.828 8a13.133 13.133 0 0 0-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 0 0 1.172 8z" />
                          <path fillRule="evenodd" d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
                        </svg>

                      </a>
                    </Link>
                  </Col>
                </Row>
              </div>
              <Row className="m-0  justify-content-center align-items-center">
                <Col lg={4} xs={4} sm={4} md={4} className="earnerd-point pr-0">
                  <h4> Points Earned</h4>
                  <span>{item.final_potential_payout != null ? item.final_potential_payout : 0}</span>
                </Col>
                <Col lg={8} xs={8} sm={8} md={8}>
                  <Row className="fantasy-pack">
                    <Col lg={5} xs={5} md={5} sm={5} className="text-right">
                      <div className="my-team-name mi-team" style={{ background: item.home_team_color }}>
                        {item.home_team}
                      </div>
                    </Col>
                    <Col lg={2} xs={2} md={2} sm={2} className="text-center my-earn-vs">
                      <img src="static/images/vs.svg" className="team-vs img-fluid" />
                    </Col>
                    <Col lg={5} xs={5} md={5} sm={5} className="text-left">
                      <div className="my-team-name iplteam" style={{ background: item.away_team_color }}>
                        {item.away_team}
                      </div>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Card.Body>
            <Card.Header>
              <Row className="m-0 mypacks-footer">
                <Col lg={6} xs={6} md={6} sm={6} className="p-0">
                  <h6> Selections <b>{item.selections}</b> </h6>
                </Col>

                <Col lg={6} xs={6} md={6} sm={6} className="p-0 text-right">
                  {item.credits != null ? (<h6> Credits <b> {item.credits}</b> </h6>) : ''}
                </Col>
              </Row>
            </Card.Header>
          </Card>
        )
      });
    }
    return (
      <React.Fragment>
        {isLoaded ?
          (
            <React.Fragment>
              {packCards ? packCards : <NoData headText="You haven't created card!" />}
            </React.Fragment>
          ) : (
            <Loader />
          )
        }
      </React.Fragment>
    )
  }
}
export default MyCompletedPacks;

