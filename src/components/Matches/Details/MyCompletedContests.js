import Link from 'next/link';
import React, { Component } from "react";
import { Container, ProgressBar, Row, Col, Card, Badge, Button, Table } from 'react-bootstrap';
import { ContestsService } from '_services/ContestsService';
import NoData from 'components/Contests/NoData';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import { TransactionService } from '_services/TransactionService';
import { encrypt } from '_helper/EncrDecrypt';
import millify from 'millify';
import Loader from 'components/CommonComponents/Loader';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

class MyCompletedContests extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: [1, 2, 3, 4, 5],
      myContests: [],
      contestId: 0,
      packId: 0,
      isLoaded: false,
      userWalletBalance: 100,
      bonusBalance: 0,
      payableAmount: 0,
      contestEntryFee: 0,
      confirmationPopup: false,
      packCount: 0
    }
  }


  componentDidMount() {
    this.getContests();
  }

  getContests() {
    ContestsService.getMyContests({ match_id: this.props.matchId }, res => {
      if (res.status) {
        var data = res['data'];
        this.setState({
          myContests: data['contestList'],
          packCount: data['packCount'],
          contestCount: data['contestCount'],
          isLoaded: true
        });
        this.props.setContestCount({
          contestCount: data['contestCount'],
          packCount: data['packCount']
        });
      } else {
        this.setState({
          myContests: [],
          isLoaded: true
        });
      }
    })
  }

  enterContest = (entryFee, contestId, purchasedPackCount) => {
    if (AuthenticationService.isLogged) {
      if (this.state.userWalletBalance > entryFee) {
        var contestSame = true;
        if (this.state.packCount == 0 || (this.state.packCount == 1 && purchasedPackCount == 1 && contestSame) || (this.state.packCount == purchasedPackCount && contestSame)) {
          //create new card
          Router.push({
            pathname: '/create-pack',
            query: {
              m: encrypt(this.props.matchId)
            }
          });
        } else if ((this.state.packCount == 1 && purchasedPackCount == 0) || (this.state.packCount == 1 && !contestSame)) {
          //confirmation popup
          this.setState({
            confirmationPopup: true,
            contestEntryFee: entryFee,
            contestId: contestId
          });
        } else {
          Router.push({
            pathname: '/select-packs',
            query: {
              m: encrypt(this.props.matchId)
            }
          });
        }
      }
    } else {
      Cookies.set('_match', encrypt(this.props.matchId))
      Router.push('/login');
    }
  }

  render() {
    const { myContests, isLoaded, confirmationPopup, userWalletBalance, contestEntryFee, bonusBalance } = this.state;
    if (myContests.length > 0) {


      var contestCards = myContests.map((item, key) => {
        return (
          <React.Fragment key={key}>
            <Link href={{ pathname: '/completed-contest-details', query: { m: encrypt(this.props.matchId), c: encrypt(item.match_contest_id) } }}>
              <Card style={{ cursor: 'pointer' }}>
                <Card.Body>
                  <div className="mycontents-detail">
                    <Row className="m-0">
                      <Col lg={6} xs={6} md={6} sm={6} className="p-0">
                        <span className="prizepool">Prize Pool</span>
                        <h4 className="current-prize">&#x20B9;{item.max_prize}</h4>
                      </Col>
                      <Col lg={6} xs={6} md={6} sm={6} className="p-0 text-right">
                        <span className="prizepool">Entry</span>
                        <span className="prizepool-detail">
                          <b>&#x20B9;{item.entry_amount}</b>
                        </span>
                      </Col>
                    </Row>
                  </div>
                </Card.Body>
                <Card.Header className="cardfooter">
                  <ul className="float-left cardfooter-inner">
                    <li>
                      <img src="static/images/trophy.svg" className="winner-trophy" /> 1st Prize &#x20B9;{item.top_price > 0 ? millify(item.top_price) : 0}
                    </li>
                  </ul>
                  <ul className=" float-right cardfooter-inner ">
                    <li className=" mr-0"><img src="static/images/star.svg" className="winner-trophy" /> {item.pack_win_percentage}% cards win</li>
                  </ul>
                  <div className="clearfix"></div>
                </Card.Header>
                <Card.Header className="cardfooter mycontents-footer ">
                  <Row className="m-0  justify-content-center align-items-center">
                    <Col lg={4} xs={4} md={4} sm={4} className="p-0 fantasy-total text-capitalize">
                      {item.contest_name}
                    </Col>
                    <Col lg={4} xs={4} md={4} sm={4} className="text-center p-0 middel-prize">
                      {item.purchased_pack_count ? `Cards(${item.purchased_pack_count})` : ''}
                    </Col>
                    <Col lg={4} xs={4} md={4} sm={4} className="text-right p-0 total-user-rank">
                      {/* {
                        item.won_amount != null ? (
                          '&#x20B9;' + item.won_amount
                        ) : ''
                      } */}
                      {
                        item.won_amount && item.won_amount != null ? (
                          <ul className="float-right winner-right">
                            <li>
                              <img src="static/images/trophy.svg" className="winner-trophy" /> You won &#x20B9; {item.won_amount}
                            </li>
                          </ul>
                        ) : ''
                      }
                    </Col>
                  </Row>
                </Card.Header>
              </Card>
            </Link>
          </React.Fragment>
        )
      })
    }
    return (
      <React.Fragment>
        {isLoaded ?
          (
            <React.Fragment>
              {contestCards ? contestCards : <NoData headText="You haven't joined contest!" buttonText="Join a contest" buttonUrl={this.props.changeTab} />}
            </React.Fragment>
          ) : (
            <Loader />
          )
        }
      </React.Fragment>
    )
  }
}
export default MyCompletedContests;

