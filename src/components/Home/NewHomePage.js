import React, { Component } from "react";
import Cookies from "js-cookie";
import Link from 'next/link';
import { Container, Row, Col, Card, InputGroup, FormControl, Form, Badge } from 'react-bootstrap';
import { Snackbar, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import { AuthenticationService } from "_services/AuthenticationService";
import Loader from "components/CommonComponents/Loader";
import { Button } from '@material-ui/core';
import Slider from "react-slick";
// function Alert(props) {
//   return <MuiAlert elevation={6} variant="filled" {...props} />;
// }

class NewHomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      // isLoggedIn: false,
      liveMatches: [],
      upcommingMatches: [],
      allUpcommingMatches: [],
      myMatches: [],
      start: 0,
      last: 4,
      limit: 5,
      alert: {
        toast: false,
        toastMessage: '',
        severity: ''
      },
      isLoaded: false,
      homeIntervel: 0
    }
  }



  render() {

    const howToPlay = {
      centerMode: true,
      slidesToShow: 1,
      dots: false,
      arrows: true,
      swipe: true,
      infinite: false,
      swipeToSlide: true,
      lazyLoad: true,
      centerPadding: "35px",
    };
    const testomonial = {
      infinite: false,
      slidesToShow: 1,
      lazyLoad: true,
      dots: false,
      arrows: true,
      swipe: true,
      infinite: false,
      swipeToSlide: true,
    };


    // const { liveMatches, upcommingMatches, myMatches, isLoaded } = this.state;

    return (
      <React.Fragment>
        <div className="">
          <div className="latest-home-page">
            <Row className="justify-content-center align-items-center m-0">
              <Col lg={12} xs={12} sm={12} md={12} className="text-center">
                {/* Logo */}
                <Link href="/">
                  <a>
                    <img src="static/images/newlogo.png" alt="" className="newlogo" />
                  </a>
                </Link>
                {/* Account info */}
                <Link href="/">
                  <a>
                    <img src="static/images/account-circle-fill.png" alt="" className="right-account-info" />
                  </a>
                </Link>
              </Col>
            </Row>
            {/*  Do you have what it takes to become */}
            <div className="india-fantasy justify-content-center align-items-center">
              <p className="text-uppercase">
                Do you have what it takes to become
                </p>
              <h1 className="text-uppercase">India’s new Fantasy Hero?</h1>
            </div>
          </div>
          {/* Register Form */}
          <Row className="homepage-register-form">
            <Col lg={12} className="p-0">
              <Card >
                <Card.Header className="text-uppercase">
                  <span>take part in the</span>
                  <Card.Body>
                    <Card.Title> &#x20B9;1,000,000</Card.Title>
                    <Card.Text>
                      Guarantee Prize Pool Tournament!
                </Card.Text>
                  </Card.Body>
                </Card.Header>
                <div className="home-register">
                  <Form>
                    <Form.Group controlId="formMobileNumber">
                      <Form.Label>Phone Number</Form.Label>
                      <InputGroup size="lg">
                        <InputGroup.Prepend>
                          <InputGroup.Text id="phnumber">+91</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                          placeholder="Eg. 987 654 3210"
                          aria-label="phoneno"
                          aria-describedby="phnumber"
                          type="number"
                        />
                      </InputGroup>
                    </Form.Group>
                    <Button type="button" title="Register" fullWidth variant="contained" color="primary" className="text-capitalize">
                      Register
                  </Button>
                  </Form>
                </div>
              </Card>
            </Col>
          </Row>
          {/* End Register Form */}
          {/* Total Bonus Div */}
          <Row className="m-0 justify-content-center align-items-center">
            <Col lg={12} className="p-0">
              <Card className="bonus-banner">
                <h4>
                  Bonus &#x20B9;101
                  </h4>
                <p>
                  at Registration
                  </p>
              </Card>
            </Col>
          </Row>
          {/* End Total Bonus Div */}
        </div>
      </React.Fragment >
    )
  }
}
export default NewHomePage;