import React, { Component } from "react";
import Cookies from "js-cookie";
import Link from 'next/link';
import { Container, Row, Col, Card, Badge } from 'react-bootstrap';
import { Button, Modal, TextField, Select, MenuItem, Snackbar } from '@material-ui/core';
import Router from 'next/router';
import Slider from "react-slick";
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';

class UpdateHomePage extends Component {
  constructor(props) {
    super(props);

  }

  render() {
    const installationStep = {
      // centerMode: true,
      slidesToShow: 1,
      dots: false,
      autoplay: false,
      autoplaySpeed: 2000,
      arrows: true,
      swipe: true,
      infinite: false,
      swipeToSlide: true,
      lazyLoad: true,
      // centerPadding: "45px",
    };

    return (
      <React.Fragment>
        <div className="slider-home-page">
          <div className="home-header">
            <Row className="m-0 h-100">
              <Col lg={6} xs={6} md={6} sm={6} className="h-100 p-0">
                {/* Logo */}
                <Link href="/">
                  <a>
                    <img src="static/images/newlogo.svg" alt="" className="new-logo" />
                  </a>
                </Link>
              </Col>
              <Col lg={6} xs={6} md={6} sm={6} className="p-0">
                {/* after login */}
                {/* <Link href="/">
                  <a>
                    <img src="static/images/account-circle-fill.png" alt="" className="right-account-info" />
                  </a>
                </Link> */}
                <Link href="/login">
                  <Button title="Log In" className="login-new-home">Log In</Button>
                </Link>
              </Col>
            </Row>
          </div>
          {/*  Do you have what */}
          <div className="new-home-banner">
            <div>
              <h4>
                Do you have what <br /> it takes to become India’s <br />  new fantasy hero?
              </h4>
            </div>
            <div>
              <Link href="/register">
                <Button type="submit" title="Register Now" fullWidth variant="contained" color="primary" className="text-capitalize  home-new-register">
                  Register Now
              </Button>
              </Link>
            </div>
          </div>

          {/* ₹1,000,000 Guaranteed Prize Pool Tournament */}

          <Row className="prize-row text-center">
            <Col lg={12}>
              <img src="static/images/a1.jpg" />
              <p className="prize-amount-home">
                <b class="bold-size">₹1,000,000</b> Guaranteed <br />
                Prize Pool Tournament
                  </p>
              <div className="bottom-border"></div>
              <img src="static/images/a2.jpg" />
              <p>
                Bonus  <b class="bold-size"> ₹101 </b> at Registration
                </p>
              {/*  Register Now */}

            </Col>
          </Row>


          {/*  install steps*/}

          <div className="intsall-step-main">
            <Slider {...installationStep} >
              <div>
                <Row className="homepage-register-form">
                  <Col lg={12} className="p-0">
                    <div className="download-step">
                      <h4>1</h4>
                      <h5>Select a Match</h5>
                      <img src="static/images/spoer1.jpg" alt="Step 1" />
                    </div>
                  </Col>
                </Row>
              </div>
              <div>
                <Row className="homepage-register-form">
                  <Col lg={12} className="p-0">
                    <div className="download-step">
                      <h4>2</h4>
                      <h5>Enter a Contest</h5>
                      <img src="static/images/sports2.jpg" alt="Step 1" />
                    </div>
                  </Col>
                </Row>
              </div>
              <div>
                <Row className="homepage-register-form">
                  <Col lg={12} className="p-0">
                    <div className="download-step">
                      <h4>3</h4>
                      <h5>Create a Fantasy Card to Win Cash</h5>
                      <img src="static/images/sports3.jpg" alt="Step 1" />
                    </div>
                  </Col>
                </Row>
              </div>
            </Slider>
          </div>
          {/* review-user */}
          <Row className="review-user">
            <Col lg={12}>
              <div className="arrow_box review-box">
                <p>
                  <img src="static/images/quote1.png" className="quoteicon-left" />
                  The experience has been amazing and this app is unique from other fantasy platforms.
                  <img src="static/images/Avron.png" className="userpic" />
                  <img src="static/images/quote2.png" className="quoteicon-right" />
                </p>
                <b>- Avron M, ₹27,500 winner
                  <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-star-fill" fill="#ffd600" xmlns="http://www.w3.org/2000/svg">
                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                  </svg>
                </b>
              </div>
            </Col>
            <Col lg={12}>
              <div className="arrow_box2 review-box">
                <p>
                  <img src="static/images/Ashish.png" className="userpic" />
                  <img src="static/images/quote1.png" className="quoteicon-left" />
                  This is the first platform that allows us to play anytime pre-match as well as in-play.

                  <img src="static/images/quote2.png" className="quoteicon-right" />
                </p>
                <b>- Ashish J., ₹2,500 winner
                  <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-star-fill" fill="#ffd600" xmlns="http://www.w3.org/2000/svg">
                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                  </svg>
                </b>
              </div>
            </Col>
          </Row>
          <Row className="social-media">
            <Col lg={12} className="text-center ">
              <ul>
                <li>
                  <a href="https://instagram.com/" target="_blank" title="Instagram">
                    <InstagramIcon />
                  </a>
                </li>
                <li>
                  <a href="https://twitter.com/" target="_blank" title="Twitter">
                    <TwitterIcon />
                  </a>
                </li>
                <li>
                  <a href="https://www.facebook.com/" target="_blank" title="Facebook">
                    <FacebookIcon />
                  </a>
                </li>
              </ul>
              <p>Copyright &copy; 2020 All Rights Reserved</p>
            </Col>
          </Row>
          <div className="app-download-fixed">
            <div className="download-app">
              <a href={process.env.APK_URL} title="Download App">
                Download App &amp; Get INR 25 Cash
              </a>
            </div>
          </div>
        </div>
      </React.Fragment >
    )
  }
}
export default UpdateHomePage;