import React, { Component } from "react";

import Cookies from "js-cookie";
import Link from 'next/link';
import { Container, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import { Snackbar, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import { encrypt, decrypt } from '_helper/EncrDecrypt';
import { HomeService } from '_services/HomeService';
import { isMobile } from "react-device-detect";
import Slider from "react-slick";
import millify from 'millify';
import { AuthenticationService } from "_services/AuthenticationService";
import Countdown from 'react-countdown';
import Router from "next/router";
import * as moment from 'moment';
import Loader from "components/CommonComponents/Loader";
import NoData from "components/Contests/NoData";
import { GAevent } from "_helper/google-analytics";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      // isLoggedIn: false,
      liveMatches: [],
      upcommingMatches: [],
      allUpcommingMatches: [],
      myMatches: [],
      start: 0,
      last: 4,
      limit: 5,
      alert: {
        toast: false,
        toastMessage: '',
        severity: ''
      },
      isLoaded: false,
      homeIntervel: 0
    }
  }

  componentDidMount() {
    this.getHomeMatches();
    // if (AuthenticationService.currentUserValue && AuthenticationService.currentUserValue != null) {
    //   this.setState({
    //     isLoggedIn: true
    //   });
    // } else {
    //   this.setState({
    //     isLoggedIn: false
    //   });
    // }
    // window.addEventListener('popstate', (event) => {
    //   alert()
    // });
  }

  componentDidUpdate() {
    // if (!this.props.isLogged) {
    //   if (this.state.isLoggedIn) {
    //     this.setState({
    //       isLoggedIn: false
    //     });
    //   }
    // }
  }

  getHomeMatches = () => {
    clearInterval(this.state.homeIntervel);
    HomeService.getHomeMatches((response) => {
      if (response.status) {
        this.setState({
          liveMatches: response['data']['live'],
          upcommingMatches: response['data']['upcomming'],
          myMatches: response['data']['mymatches'],
          allUpcommingMatches: response['data']['upcomming'],
          isLoaded: true
        });
        this.setState({
          homeIntervel: setInterval(() => {
            this.getHomeMatches()
          }, 10000)
        })
      } else {
        this.setState({
          isLoaded: true
        });
      }
    });
  }

  hexToRGBA = (hex) => {
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    if (hex) {
      hex = hex.replace(shorthandRegex, function (m, r, g, b) {
        return r + r + g + g + b + b;
      });

      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

      if (result) {
        var r = parseInt(result[1], 16);
        var g = parseInt(result[2], 16);
        var b = parseInt(result[3], 16);
        // return r + "," + g + "," + b;//return 23,14,45 -> reformat if needed 
        return `rgba(${r}, ${g}, ${b}, 0.44)`
      }
    }

    // hex = '0x' + hex
    // let r = hex >> 16 & 0xFF
    // let g = hex >> 8 & 0xFF
    // let b = hex & 0xFF
  }

  changePriceFormat(price) {
    return millify(price);
  }

  loadMore = () => {
    const { start, last, limit, allUpcommingMatches } = this.state;
    var nextStart = start + limit;
    var nextLast = last + limit;
    this.setState((prevState) => {
      return {
        // upcommingMatches: upcommingMatches,
        start: nextStart,
        last: prevState.last + 5
      }
    })
  }

  changeLogged = (status) => {
    this.props.changeLogged(status);
  }

  enterContest = (matchId) => {
    AuthenticationService.clearCookies();
    Router.push({
      pathname: '/contests',
      query: { m: encrypt(matchId) }
    });
  }

  renderer = ({ days, hours, minutes, seconds, completed }) => {
    if (completed) {
      return <span>{days == 0 ? '' : days < 10 ? '0' + days + 'd' : days + 'd'} {hours < 10 ? '0' + hours : hours}h {minutes < 10 ? '0' + minutes : minutes}m {seconds < 10 ? '0' + seconds : seconds}s</span>;
    } else {
      // Render a countdown
      return <span>{days == 0 ? '' : days < 10 ? '0' + days + 'd' : days + 'd'} {hours < 10 ? '0' + hours : hours}h {minutes < 10 ? '0' + minutes : minutes}m {seconds < 10 ? '0' + seconds : seconds}s</span>;
    }
  };

  componentWillUnmount() {
    clearInterval(this.state.homeIntervel);
  }

  render() {

    const settings = {
      infinite: false,
      slidesToShow: 1,
      lazyLoad: true,
      speed: 100,
      dots: true,
      arrows: false,
      autoplay: true
    };

    const { liveMatches, upcommingMatches, myMatches, isLoaded } = this.state;



    var myMatchCards = '';
    var liveCards = '';
    var upcommingCards = '';
    if (AuthenticationService.isLogged) {
      myMatchCards = myMatches.map((item, key) => {
        return (
          item.match_status == 2 ? (
            <Card key={key}>
              <Card.Body>
                <div className="ipl-matches">{item.matchNumber}</div>
                <div className="team-listing">
                  <Link href={{ pathname: '/match-details', query: { m: encrypt(item.match_id) } }}>
                    <Row>
                      <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                        <div className="team-logo team-left">
                          <div className="logo-line line-left" style={{ background: this.hexToRGBA(item.hometeamcolorcode) }}>
                          </div>
                          <div className="team-name team-left-name" style={{ background: item.hometeamcolorcode, boxShadow: `0px 8px 15px 0px ${this.hexToRGBA(item.hometeamcolorcode)}` }}>
                            {item.hometeamname}
                          </div>
                        </div>
                      </Col>
                      <Col lg={4} xs={4} md={4} sm={4} className="text-center">
                        <div className="completed-match">
                          {item.match_status == 0 ? 'Upcoming' : item.match_status == 1 ? 'Live' : item.match_status == 2 ? 'Completed' : ''}
                        </div>
                      </Col>
                      <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                        <div className="team-logo team-right">
                          <div className="logo-line line-right" style={{ background: this.hexToRGBA(item.awayteamcolorcode) }}>
                          </div>
                          <div className="team-name team-right-name" style={{ background: item.awayteamcolorcode, boxShadow: `0px 8px 15px 0px ${this.hexToRGBA(item.awayteamcolorcode)}` }}>
                            {item.awayteamname}
                          </div>
                        </div>
                      </Col>
                    </Row>
                  </Link>
                </div>
              </Card.Body>
              <Card.Header className="cardfooter">
                <ul className=" float-left cardfooter-inner">
                  <li >
                    <b>{item.no_of_pack}</b> Cards
                       </li>
                  <li>
                    <b>{item.no_of_contest}</b> Contests
                       </li>
                </ul>
                {
                  item.won_amount && item.won_amount != null ? (
                    <ul className="float-right winner-right">
                      <li>
                        <img src="static/images/trophy.svg" className="winner-trophy" /> You won &#x20B9; {item.won_amount}
                      </li>
                    </ul>
                  ) : ''
                }
                <div className="clearfix"></div>
              </Card.Header>
            </Card>
          ) : item.match_status == 1 ? (
            <Card key={key}>
              <Card.Body>
                <div className="ipl-matches">{item.matchNumber}</div>
                <time className="live-start-match">
                  <img src="static/images/circle.svg" /> Live
                </time>
                {/* <time className="match-start-time">06h 20m 30s</time> */}
                <div className="team-listing" onClick={() => this.enterContest(item.match_id)}>
                  {/* <Link href={{ pathname: '/create-pack', query: { m: encrypt(item.match_id) } }}> */}
                  <Row>
                    <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                      <div className="team-logo team-left">
                        <div className="logo-line line-left" style={{ background: this.hexToRGBA(item.hometeamcolorcode) }}>
                        </div>
                        <div className="team-name team-left-name" style={{ background: item.hometeamcolorcode, boxShadow: `0px 8px 15px 0px ${this.hexToRGBA(item.hometeamcolorcode)}` }}>
                          {item.hometeamname}
                        </div>
                      </div>
                    </Col>
                    <Col lg={4} xs={4} md={4} sm={4} className="text-center">
                      <img src="static/images/vs.svg" className="team-vs" />
                    </Col>
                    <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                      <div className="team-logo team-right">
                        <div className="logo-line line-right" style={{ background: this.hexToRGBA(item.awayteamcolorcode) }}>
                        </div>
                        <div className="team-name team-right-name" style={{ background: item.awayteamcolorcode, boxShadow: `0px 8px 15px 0px ${this.hexToRGBA(item.awayteamcolorcode)}` }}>
                          {item.awayteamname}
                        </div>
                      </div>
                    </Col>
                  </Row>
                  {/* </Link> */}
                </div>
              </Card.Body>
              <Card.Header className="cardfooter">
                <ul className=" float-left cardfooter-inner">
                  <li >
                    <b>{item.no_of_pack}</b> Cards
                       </li>
                  <li>
                    <b>{item.no_of_contest}</b> Contests
                       </li>
                </ul>
                {
                  item.won_amount && item.won_amount != null ? (
                    <ul className="float-right winner-right">
                      <li>
                        <img src="static/images/trophy.svg" className="winner-trophy" /> You won &#x20B9; {item.won_amount}
                      </li>
                    </ul>
                  ) : ''
                }
                <div className="clearfix"></div>
              </Card.Header>
            </Card>
          ) : (
                <Card key={key}>
                  <Card.Body>
                    <div className="ipl-matches">{item.matchNumber}</div>
                    <time className="match-start-time"><Countdown date={moment(item.match_date_time).format()} renderer={this.renderer} /></time>
                    <div className="team-listing" onClick={() => this.enterContest(item.match_id)}>
                      {/* <Link href={}> */}
                      <Row>
                        <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                          <div className="team-logo team-left">
                            <div className="logo-line line-left" style={{ background: this.hexToRGBA(item.hometeamcolorcode) }}>
                            </div>
                            <div className="team-name team-left-name" style={{ background: item.hometeamcolorcode, boxShadow: `0px 8px 15px 0px ${this.hexToRGBA(item.hometeamcolorcode)}` }}>
                              {item.hometeamname}
                            </div>
                          </div>
                        </Col>
                        <Col lg={4} xs={4} md={4} sm={4} className="text-center">
                          <img src="static/images/vs.svg" className="team-vs" />
                        </Col>
                        <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                          <div className="team-logo team-right">
                            <div className="logo-line line-right" style={{ background: this.hexToRGBA(item.awayteamcolorcode) }}>
                            </div>
                            <div className="team-name team-right-name" style={{ background: item.awayteamcolorcode, boxShadow: `0px 8px 15px 0px ${this.hexToRGBA(item.awayteamcolorcode)}` }}>
                              {item.awayteamname}
                            </div>
                          </div>
                        </Col>
                      </Row>
                      {/* </Link> */}
                    </div>
                  </Card.Body>
                  <Card.Header className="cardfooter">
                    <ul className=" float-left cardfooter-inner">
                      <li >
                        <b>{item.no_of_pack}</b> Cards
                       </li>
                      <li>
                        <b>{item.no_of_contest}</b> Contests
                       </li>
                    </ul>
                    {
                      item.won_amount && item.won_amount != null ? (
                        <ul className="float-right winner-right">
                          <li>
                            <img src="static/images/trophy.svg" className="winner-trophy" /> You won &#x20B9; {item.won_amount}
                          </li>
                        </ul>
                      ) : ''
                    }
                    <div className="clearfix"></div>
                  </Card.Header>
                </Card>
              )
        )
      });
    }

    //live cards
    liveCards = liveMatches.map((item, key) => {
      return (
        // <Link href={{ pathname: '/contests', query: { m: encrypt(item.match_id) } }}>
        <Card key={key}>
          <Card.Body>
            <div className="ipl-matches">{item.matchNumber}</div>
            <time className="live-start-match">
              <img src="static/images/circle.svg" /> Live
              </time>
            {/* <time className="match-start-time">06h 20m 30s</time> */}
            <div className="team-listing" onClick={() => this.enterContest(item.match_id)}>
              {/* <Link href={{ pathname: '/create-pack', query: { m: encrypt(item.match_id) } }}> */}
              <Row>
                <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                  <div className="team-logo team-left">
                    <div className="logo-line line-left" style={{ background: this.hexToRGBA(item.hometeamcolorcode) }}>
                    </div>
                    <div className="team-name team-left-name" style={{ background: item.hometeamcolorcode, boxShadow: `0px 8px 15px 0px ${this.hexToRGBA(item.hometeamcolorcode)}` }}>
                      {item.hometeamname}
                    </div>
                  </div>
                </Col>
                <Col lg={4} xs={4} md={4} sm={4} className="text-center">
                  <img src="static/images/vs.svg" className="team-vs" />
                </Col>
                <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                  <div className="team-logo team-right">
                    <div className="logo-line line-right" style={{ background: this.hexToRGBA(item.awayteamcolorcode) }}>
                    </div>
                    <div className="team-name team-right-name" style={{ background: item.awayteamcolorcode, boxShadow: `0px 8px 15px 0px ${this.hexToRGBA(item.awayteamcolorcode)}` }}>
                      {item.awayteamname}
                    </div>
                  </div>
                </Col>
              </Row>
              {/* </Link> */}
            </div>
          </Card.Body>
          <Card.Header className="cardfooter">
            {
              item.max_prize && item.max_prize != null ? (
                <ul className=" float-left cardfooter-inner">
                  <li >
                    <Link href="/">
                      <a title="Max Prize Money" className="max-prize-money">
                        Max Prize Money
                        </a>
                    </Link> <b>&#x20B9;{item.max_prize > 0 ? millify(item.max_prize) : 0}</b>
                  </li>
                </ul>
              ) : ''
            }
            {
              item.won_amount && item.won_amount != null ? (
                <ul className="float-right winner-right">
                  <li>
                    <img src="static/images/trophy.svg" className="winner-trophy" /> You won &#x20B9; {item.won_amount}
                  </li>
                </ul>
              ) : ''
            }
            <div className="clearfix"></div>
          </Card.Header>
        </Card>
        // </Link>
      )
    });

    //upcoming cards
    upcommingCards = upcommingMatches.map((item, key) => {
      return (
        <Card key={key}>
          <Card.Body>
            <div className="ipl-matches">{item.matchNumber}</div>
            <time className="match-start-time"><Countdown date={moment(item.match_date_time).format()} renderer={this.renderer} /></time>
            <div className="team-listing" onClick={() => this.enterContest(item.match_id)}>
              {/* <Link href={}> */}
              <Row>
                <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                  <div className="team-logo team-left">
                    <div className="logo-line line-left" style={{ background: this.hexToRGBA(item.hometeamcolorcode) }}>
                    </div>
                    <div className="team-name team-left-name" style={{ background: item.hometeamcolorcode, boxShadow: `0px 8px 15px 0px ${this.hexToRGBA(item.hometeamcolorcode)}` }}>
                      {item.hometeamname}
                    </div>
                  </div>
                </Col>
                <Col lg={4} xs={4} md={4} sm={4} className="text-center">
                  <img src="static/images/vs.svg" className="team-vs" />
                </Col>
                <Col lg={4} xs={4} md={4} sm={4} className="p-0">
                  <div className="team-logo team-right">
                    <div className="logo-line line-right" style={{ background: this.hexToRGBA(item.awayteamcolorcode) }}>
                    </div>
                    <div className="team-name team-right-name" style={{ background: item.awayteamcolorcode, boxShadow: `0px 8px 15px 0px ${this.hexToRGBA(item.awayteamcolorcode)}` }}>
                      {item.awayteamname}
                    </div>
                  </div>
                </Col>
              </Row>
              {/* </Link> */}
            </div>
          </Card.Body>
          <Card.Header className="cardfooter">
            {item.max_prize && item.max_prize != null ?
              <ul className=" float-left cardfooter-inner">
                <li >
                  <React.Fragment>
                    <Link href="/">
                      <a title="Max Prize Money" className="max-prize-money">
                        Max Prize Money
                            </a>
                    </Link>
                    <b>&#x20B9;{item.max_prize > 0 ? millify(item.max_prize) : 0}</b>
                  </React.Fragment>
                </li>
              </ul>
              : ''}
            {/* <ul className="float-right winner-right">
              <li>
                <img src="static/images/trophy.svg" className="winner-trophy" /> You won &#x20B9; 45
                        </li>
            </ul> */}
            <div className="clearfix"></div>
          </Card.Header>
        </Card>
      )
    });

    return (
      <React.Fragment>
        { isLoaded ? (
          <div className="body-content home-page" >
            <div className="slikslider-bar">
              <div className="slikslider-banner"></div>
            </div>
            <Row className="h-100">
              <Col lg={12} className="h-100">
                {
                  myMatchCards != '' || liveCards != '' || upcommingCards != '' ? (
                    <React.Fragment>
                      {
                        myMatchCards != '' ?
                          (
                            <div className="outer-card">
                              <h2 className="heading float-left">My Matches</h2>
                              <Link href="/my-matches">
                                <a className="view-all float-right" title="View All">View All
                                  <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-chevron-right" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fillRule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                                  </svg>
                                </a>
                              </Link>
                              <div className="clearfix"></div>
                              <Slider {...settings} >
                                {myMatchCards}
                              </Slider>
                            </div>
                          ) : ''
                      }

                      <div className="outer-card">
                        {/* Live Macth */}
                        {
                          liveCards != '' ? (
                            <React.Fragment>
                              <h1 className="heading other-heading" style={{ color: myMatchCards == '' ? '#ffffff' : '' }}>Live Matches</h1>
                              <div className="clearfix"></div>
                              <Slider {...settings} >
                                {liveCards}
                              </Slider>
                            </React.Fragment>
                          ) : ''
                        }
                        {/* Live Match */}

                        {/* Upcomming Macth */}
                        {
                          upcommingCards != '' ? (
                            <React.Fragment>
                              <h1 className="heading other-heading" style={{ color: liveCards == '' && myMatchCards == '' ? '#ffffff' : '' }}>Upcoming Matches</h1>
                              <div className="clearfix"></div>
                              {upcommingCards}
                            </React.Fragment>
                          ) : ''
                        }
                        {/* Upcomming Match */}
                      </div>
                    </React.Fragment>
                  ) : (
                      <div className="outer-card" style={{ marginTop: '30%' }}>
                        <NoData headText="No Upcoming Matches." />
                      </div>
                    )
                }
              </Col>
            </Row>
          </div>
        ) : (
            <Loader />
          )}
      </React.Fragment >

    )
  }
}
export default Home;