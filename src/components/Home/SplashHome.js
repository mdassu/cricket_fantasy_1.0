import React, { Component } from "react";
import Cookies from "js-cookie";
import Link from 'next/link';
import { Container, Row, Col, Card, Badge } from 'react-bootstrap';
import { AuthenticationService } from "_services/AuthenticationService";
const { flag, code, name, countries } = require('country-emoji');
import ReactCountryFlag from "react-country-flag";
import { Button, Modal, TextField, Select, MenuItem, Snackbar } from '@material-ui/core';
import { CommonService } from "_services/CommonService";
import { ErrorMessages, isMobileNumberValid } from 'components/MyValidations/MyValidations';
import Router from 'next/router';
import { encrypt } from "_helper/EncrDecrypt";
import { GAevent } from "_helper/google-analytics";
// function Alert(props) {
//   return <MuiAlert elevation={6} variant="filled" {...props} />;
// }

class SplashHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: true,
      formData: {
        mobile: '',
        countryCode: '91',
        otp: ''
      },
      error: false,
      errorMessage: {
        mobile: '',
        otp: ''
      },
      alert: {
        toast: false,
        toastMessage: '',
        severity: ''
      }
    }
  }

  componentDidMount() {
    AuthenticationService.clearCookies();
    GAevent('before_Login_HomePage', 'Page Load', 'How many users came to home page before login', 1);
    CommonService.countrySubject.subscribe(countries => {
      this.setState({
        countries: countries
      });
    });
  }

  handleChange = name => ({ target: { value, checked } }) => {
    if (name == 'terms') {
      this.setState({
        formData: {
          ...this.state.formData,
          [name]: checked
        }
      });
    } else {
      this.setState({
        formData: {
          ...this.state.formData,
          [name]: value
        }
      });
    }
  }

  registerSubmit = (event) => {
    event.preventDefault();
    const { formData } = this.state;
    var procced = false;

    var msgMobile = '';

    var isValid = false;

    // mobile number. validation
    if (formData.mobile) {
      if (!isMobileNumberValid(formData.mobile)) {
        msgMobile = ErrorMessages.VALIDATION_ERROR_MOBILE;
      }
    } else {
      msgMobile = 'Mobile number is required';
    }

    if (!msgMobile) {
      isValid = true;
    }

    if (isValid) {
      this.setState({
        error: true,
        errorMessage: {
          mobile: ''
        },
        isLoaded: false
      });

      var params = {
        country_code: formData['countryCode'],
        mobile: formData['mobile']
      }
      AuthenticationService.signUp(params, (res) => {
        if (res.status) {
          var data = {
            otpMobile: formData.mobile,
            otpCountryCode: formData.countryCode,
          };
          Cookies.set('_mobData', encrypt(JSON.stringify(data)));
          Router.push('/verify-otp');
        } else {
          this.setState({
            error: true,
            errorMessage: { mobile: res.message },
            isLoaded: true
          });
        }
      });
    } else {
      this.setState({
        error: true,
        errorMessage: {
          mobile: msgMobile
        }
      });
    }


  };

  setCountryCode = (event) => {
    const { value } = event.currentTarget.dataset;
    this.setState({
      formData: {
        countryCode: value
      }
    })
  }



  render() {

    const howToPlay = {
      centerMode: true,
      slidesToShow: 1,
      dots: false,
      arrows: true,
      swipe: true,
      infinite: false,
      swipeToSlide: true,
      lazyLoad: true,
      centerPadding: "35px",
    };
    const testomonial = {
      infinite: false,
      slidesToShow: 1,
      lazyLoad: true,
      dots: false,
      arrows: true,
      swipe: true,
      infinite: false,
      swipeToSlide: true,
    };

    const { countries, isLoaded, formData: { mobile, countryCode }, otpMobile, otpCountryCode, alert } = this.state;
    var country = '';
    if (countries && countries.length > 0) {
      country = countries.map((country, key) =>
        <MenuItem key={key} value={country.country_code}>
          <ReactCountryFlag countryCode={country.iso_code} svg /> &nbsp;+{country.country_code}
        </MenuItem>
      );
    }

    return (
      <React.Fragment>
        <div className="">
          <div className="latest-home-page">
            <Row className="justify-content-center align-items-center m-0">
              <Col lg={12} xs={12} sm={12} md={12} className="text-center">
                {/* Logo */}
                <Link href="/">
                  <a>
                    <img src="static/images/newlogo.png" alt="" className="newlogo" />
                  </a>
                </Link>
                {/* Account info */}
                {/* after login */}
                {/* <Link href="/">
                  <a>
                    <img src="static/images/account-circle-fill.png" alt="" className="right-account-info" />
                  </a>
                </Link> */}
                <Link href="/login">
                  <Button title="Log In" className="right-account-login">Log In</Button>
                </Link>
              </Col>
            </Row>
            {/*  Do you have what it takes to become */}
            <div className="india-fantasy justify-content-center align-items-center">
              <p className="text-uppercase">
                DO YOU HAVE WHAT IT TAKES TO BECOME
                </p>
              <h1 className="text-uppercase">INDIA'S NEW FANTASY HERO?</h1>
            </div>
          </div>
          {/* Register Form */}
          <Row className="homepage-register-form">
            <Col lg={12} className="p-0">
              <Card >
                <Card.Header >
                  <span className="text-uppercase">REGISTER FOR THE</span>
                  <Card.Body>
                    <Card.Title> &#x20B9;{process.env.DAILY_WINNING}</Card.Title>
                    <Card.Text className="text-uppercase">
                      GUARANTEED PRIZE POOL TOURNAMENT!
                      </Card.Text>
                  </Card.Body>
                </Card.Header>
                <div className="home-register">
                  <form onSubmit={this.logInSubmit} className="login-form">
                    {/* <div className="phone-input clearfix">
                      <Row>
                        <Col lg={12} className="inputNumber">
                          {country != '' ?
                            (< Select value={this.state.formData['countryCode']} onChange={this.handleChange('countryCode')} variant="outlined" className="country-select">
                              {country}
                            </Select>)
                            : ''}
                          <TextField
                            className="number-input mobile-input"
                            variant="outlined"
                            type="number"
                            inputProps={{
                              name: 'mobile',
                            }}
                            autoFocus={true}
                            value={mobile}
                            onChange={this.handleChange('mobile')}
                            error={!!this.state.errorMessage.mobile}
                            helperText={!!this.state.errorMessage.mobile ? this.state.errorMessage.mobile : ''}
                          />
                        </Col>
                      </Row>
                    </div> */}
                    <Link href="/register">
                      <Button type="submit" title="Register Now" fullWidth variant="contained" color="primary" className="text-uppercase mt-0">
                        Register Now
                      </Button>
                    </Link>

                  </form>

                </div>
              </Card>
            </Col>
          </Row>
          {/* End Register Form */}
          {/* Total Bonus Div */}
          <Row className="m-0 justify-content-center align-items-center">
            <Col lg={12} className="p-0">
              <Card className="bonus-banner">
                <h4>
                  Bonus &#x20B9;{process.env.REGISTER_BONUS}
                </h4>
                <p>
                  at Registration
                  </p>
              </Card>
            </Col>
          </Row>
          {/* End Total Bonus Div */}
        </div>
      </React.Fragment >
    )
  }
}
export default SplashHome;