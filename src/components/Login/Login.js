import Link from 'next/link';
import React, { Component } from "react";
import Cookies from 'js-cookie';
import { AuthenticationService } from '_services/AuthenticationService';
import PhoneInput from 'react-phone-input-2';
import { Col, Button } from 'react-bootstrap';
import './Login.css';
import Popup from 'components/Login/Popup';


class Login extends Component {
  // const classes = useStyles();
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      popupType: 'login'
    }
  }

  componentDidMount() {
    if (this.props.params && Object.keys(this.props.params).length > 0) {
      this.setState({
        open: true
      });
    }
  }

  handleOpen = (type) => {
    // setOpen(true);
    this.setState({
      open: true,
      popupType: type
    });
  };

  handleClose = () => {
    this.setState({
      open: false
    });
    // setOpen(false);
  };

  changeLogged = (status) => {
    this.props.onLogged(status);
  }

  openLogin = () => {
    this.setState({
      popupView: 'login'
    });
  }

  depositClick = () => {
    Cookies.set('_depositClick', '1');
    this.handleOpen();
  }


  render() {

    const { open, popupType } = this.state;
    return (

      <div className="mobile-btnns user-auth-btn">
        <Col xs={12} className="mobile-bottom-btn p-0">
          <div className="common-btn">
            <Link href="/register">
              <Button variant="success" title="Register">Register</Button>
            </Link>
            <Link href="/login">
              <Button variant="secondary" title="Log In" className="login-top">Log In</Button>
            </Link>
          </div>
        </Col>
        {open ? <Popup hidePopup={this.handleClose} onLogged={this.changeLogged} showPoup={this.props.params} popupType={popupType} /> : ''}
      </div>
    )
  }
}

export default Login;