import Link from 'next/link';
import React, { Component, useCallback } from "react";
import { AuthenticationService } from '_services/AuthenticationService';
import PhoneInput from 'react-phone-input-2';
import { Button, Modal, TextField, Select, MenuItem, Snackbar } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import OtpInput from 'react-otp-input';
import CloseIcon from '@material-ui/icons/Close';
import { ErrorMessages, isNameValid, isMobileNumberValid } from 'components/MyValidations/MyValidations';
import './Login.css';
import { CommonService } from '_services/CommonService';
const { flag, code, name, countries } = require('country-emoji');
import ReactCountryFlag from "react-country-flag";
import Router from 'next/router';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class Popup extends Component {
  // const classes = useStyles();
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      popupView: 'login',
      otpMobile: '',
      otpReferral: '',
      otpCountryCode: '',
      isLoaded: true,
      terms: false,
      mobile: '',
      formData: {
        mobile: '',
        countryCode: '91',
        otp: '',
        firstName: '',
        lastName: '',
        referralCode: ''
      },
      error: false,
      errorMessage: {
        mobile: '',
        firstName: '',
        lastName: '',
        otp: '',
        terms: '',
        referralCode: ''
      },
      alert: {
        toast: false,
        toastMessage: '',
        severity: ''
      },
      countDown: 0,
      countries: []
    }
  }

  componentDidMount() {
    if ((this.props.showPoup && Object.keys(this.props.showPoup).length > 0) || this.props.popupType == 'register') {
      this.openRegister();
    }
    CommonService.countrySubject.subscribe(countries => {
      this.setState({
        countries: countries
      });
    });
  }

  handleOpen = () => {
    // setOpen(true);
    this.setState({
      open: true
    });
  };

  handleClose = () => {
    this.setState({
      open: false
    });
    this.props.hidePopup();
    var referral = this.props.showPoup;
    if (referral) {
      Router.push('/');
    }
  };

  handleChange = name => ({ target: { value, checked } }) => {
    if (name == 'terms') {
      this.setState({
        formData: {
          ...this.state.formData,
          [name]: checked
        }
      });
    } else {
      this.setState({
        formData: {
          ...this.state.formData,
          [name]: value
        }
      });
    }

  }

  handleOTPChange = otp => this.setState({
    formData: {
      otp: otp
    }
  });

  logInSubmit = (event, type = null) => {
    if (event) {
      event.preventDefault();
    }
    const { formData } = this.state;
    var mobileNumber = formData.mobile;
    if (type == 'resendOtp') {
      this.setState({
        countDown: 59
      });
      var countdown = setInterval(() => {
        this.setState({
          countDown: this.state.countDown - 1
        });
        if (this.state.countDown == 0) {
          clearInterval(countdown);
          this.setState({
            countDown: 0
          });
        }
      }, 1000);
      mobileNumber = this.state.mobile;
      this.setState({
        formData: {
          mobile: this.state.mobile,
          countryCode: '91',
          otp: '',
          firstName: '',
          lastName: '',
        }
      });
    } else {
      this.setState({
        mobile: mobileNumber
      });
    }

    // const { formData } = this.state;
    var isValid = false;
    if (mobileNumber) {
      if (!isMobileNumberValid(mobileNumber)) {
        this.setState({
          error: true,
          errorMessage: { mobile: ErrorMessages.VALIDATION_ERROR_MOBILE }
        });
      } else {
        isValid = true;
      }
    } else {
      this.setState({
        error: true,
        errorMessage: { mobile: 'Mobile number is required' }
      });
    }

    if (isValid) {
      this.setState({
        error: false,
        errorMessage: {},
        isLoaded: false
      });
      var params = {
        country_code: formData.countryCode,
        mobile: mobileNumber
      };
      AuthenticationService.signIn(params, (res) => {
        if (res.status) {
          this.setState({
            popupView: 'otp',
            otpMobile: formData.mobile,
            otpCountryCode: formData.countryCode,
            isLoaded: true
          });
        } else {
          // this.setState({
          //   alert: {
          //     toast: true,
          //     toastMessage: res.message,
          //     severity: 'error'
          //   }
          // })
          this.setState({
            error: true,
            errorMessage: { mobile: res.message },
            isLoaded: true
          });
        }
      });
    }
  };

  verifyOTP = (event) => {
    event.preventDefault();
    const { formData, otpMobile, otpReferral } = this.state;
    if (formData.otp && formData.otp.length == 6) {
      var params = {
        mobile: otpMobile,
        otp: formData.otp,
        referral_code: otpReferral
      }
      this.setState({
        isLoaded: false
      });
      AuthenticationService.verifyOTP(params, (res) => {

        if (res.status) {
          this.props.onLogged(true);
          this.handleClose();
        } {
          // this.setState({
          //   alert: {
          //     toast: true,
          //     toastMessage: res.message,
          //     severity: 'error'
          //   }
          // })
          this.setState({
            error: true,
            errorMessage: { otp: res.message },
            isLoaded: true
          });
        }
      });
    } else {
      // this.setState({
      //   alert: {
      //     toast: true,
      //     toastMessage: 'Please enter OTP.',
      //     severity: 'error'
      //   }
      // })
      this.setState({
        error: true,
        errorMessage: { otp: 'Please enter otp.' },
        isLoaded: true
      });
    }
  };

  openRegister = () => {
    var refId = '';
    var referral = this.props.showPoup;
    if (referral) {
      refId = referral.refId
    }
    this.setState({
      popupView: 'register',
      formData: {
        mobile: '',
        countryCode: '91',
        otp: '',
        firstName: '',
        lastName: '',
        referralCode: refId
      },
      errorMessage: {
        mobile: '',
        firstName: '',
        lastName: '',
        otp: '',
        referralCode: ''
      },
    });
  }

  openLogin = () => {
    this.setState({
      popupView: 'login',
      formData: {
        mobile: '',
        countryCode: '91',
      },
      errorMessage: {
        mobile: '',
        firstName: '',
        lastName: '',
        otp: ''
      },
    });
  }

  registerSubmit = (event) => {
    event.preventDefault();
    const { formData } = this.state;
    var procced = false;

    var msgMobile = '';
    var msgFirstName = '';
    var msgLastName = '';
    var msgCheckTerms = '';

    var isValid = false;

    // first name validation
    if (formData.firstName) {
      if (!isNameValid(formData.firstName)) {
        msgFirstName = (ErrorMessages.VALIDATION_ERROR_NAME).replace('full', 'first');
      }
    } else {
      msgFirstName = 'First name is required';
    }

    // last name validation
    if (formData.lastName) {
      if (!isNameValid(formData.lastName)) {
        msgLastName = (ErrorMessages.VALIDATION_ERROR_NAME).replace('full', 'last');
      }
    } else {
      msgLastName = 'Last name is required';
    }

    // mobile number. validation
    if (formData.mobile) {
      if (!isMobileNumberValid(formData.mobile)) {
        msgMobile = ErrorMessages.VALIDATION_ERROR_MOBILE;
      }
    } else {
      msgMobile = 'Mobile number is required';
    }

    // if (!this.state.terms) {
    //   msgCheckTerms = 'Please accept Terms & Conditions';
    // }

    if (!msgFirstName && !msgLastName && !msgMobile && !msgCheckTerms) {
      isValid = true;
    }

    if (isValid) {
      this.setState({
        error: true,
        errorMessage: {
          firstName: '',
          lastName: '',
          mobile: '',
          terms: ''
        },
        isLoaded: false
      });
      if (formData.referralCode != '' && formData.referralCode != undefined) {
        var request = {
          referral_code: formData.referralCode,
        }
        AuthenticationService.checkReferralCode(request, (res) => {
          if (res.status) {
            this.setState({
              otpReferral: formData.referralCode
            });
            var params = {
              country_code: formData['countryCode'],
              mobile: formData['mobile'],
              first_name: formData['firstName'],
              last_name: formData['lastName'],
            }
            AuthenticationService.signUp(params, (res) => {
              if (res.status) {
                this.setState({
                  popupView: 'otp',
                  otpMobile: formData.mobile,
                  otpCountryCode: formData.countryCode,
                  isLoaded: true
                });
              } else {
                this.setState({
                  error: true,
                  errorMessage: { mobile: res.message },
                  isLoaded: true
                });
              }
            });
          } else {
            this.setState({
              error: true,
              errorMessage: {
                referralCode: res.message
              },
              isLoaded: true
            });
          }
        });
      } else {
        var params = {
          country_code: formData['countryCode'],
          mobile: formData['mobile'],
          first_name: formData['firstName'],
          last_name: formData['lastName'],
        }
        AuthenticationService.signUp(params, (res) => {
          if (res.status) {
            this.setState({
              popupView: 'otp',
              otpMobile: formData.mobile,
              otpCountryCode: formData.countryCode,
              isLoaded: true
            });
          } else {
            this.setState({
              error: true,
              errorMessage: { mobile: res.message },
              isLoaded: true
            });
          }
        });
      }
    } else {
      this.setState({
        error: true,
        errorMessage: {
          firstName: msgFirstName,
          lastName: msgLastName,
          mobile: msgMobile,
          terms: msgCheckTerms
        }
      });
    }


  };

  setCountryCode = (event) => {
    const { value } = event.currentTarget.dataset;
    this.setState({
      formData: {
        countryCode: value
      }
    })
  }

  checkTerms = (event) => {
    this.setState({
      terms: event.target.checked
    });
  }


  render() {
    // const classes = useStyles;
    const { countries, isLoaded, formData: { mobile, countryCode, otp, firstName, lastName, terms, referralCode }, otpMobile, otpCountryCode, alert } = this.state;
    var country = '';
    if (countries && countries.length > 0) {
      country = countries.map((country, key) =>
        <MenuItem key={key} value={country.country_code}>
          <ReactCountryFlag countryCode={code(country.country_name)} svg /> &nbsp;+{country.country_code}
        </MenuItem>
      );
    }
    const bodyLogin = (
      <div className="modal-div">
        <Button type="button" className="close-btn" title="Close" onClick={this.handleClose}>
          <CloseIcon />
        </Button>
        <h3 className="modal-title">Login</h3>
        <p className="modal-description">
          Enter your mobile number to proceed
        </p>
        <div>
          <form onSubmit={this.logInSubmit} className="login-form">
            <div className="phone-input clearfix">
              {country != '' ?
                (< Select value={this.state.formData['countryCode']} onChange={this.handleChange('countryCode')} variant="outlined" className="country-select">
                  {country}
                </Select>)
                : ''}
              <TextField
                className="number-input mobile-input"
                variant="outlined"
                type="number"
                inputProps={{
                  name: 'phone',
                  autoFocus: true,
                }}
                value={mobile}
                onChange={this.handleChange('mobile')}
                error={!!this.state.errorMessage.mobile}
                helperText={!!this.state.errorMessage.mobile ? this.state.errorMessage.mobile : ''}
              />
            </div>

            {isLoaded ? (
              <Button type="submit" title="Continue" fullWidth variant="contained" color="primary" className="register-now continue-btn text-capitalize p-9 ">
                Continue
              </Button>) :
              (<Button type="button" title="Continue" fullWidth variant="contained" color="primary" className="register-now continue-btn text-capitalize p-9 ">
                <img src="static/images/loader.gif" alt="" style={{ width: '7.388%' }} className="empty-mybet" />
              </Button>
              )}
          </form>
          <div className="register-here">
            <small>Don't have an account? <strong onClick={this.openRegister}>Register Here</strong></small>
          </div>
        </div>
      </div >
    );
    //Start bodyOtp
    const bodyOtp = (
      <div className="modal-div">
        <Button type="button" className="close-btn" title="Close" onClick={this.handleClose}><CloseIcon /></Button>
        <h3 className="modal-title">Enter verification code</h3>
        <p className="modal-description">
          OTP sent to +{otpCountryCode}-{otpMobile}
        </p>
        <div>
          <form className="otp-form" onSubmit={this.verifyOTP}>
            <input type="hidden" name="mobile" value={otpMobile} />
            <OtpInput
              value={otp}
              onChange={this.handleOTPChange}
              numInputs={6}
              isInputNum
              shouldAutoFocus
            // separator={<span>-</span>}
            />
            {this.state.errorMessage.otp ? (<small className="text-danger">{this.state.errorMessage.otp}</small>) : ''}
            {isLoaded ? (
              <Button type="submit" title="Verify & Proceed" fullWidth variant="contained" color="primary" className="register-now text-capitalize p-9 mt-26">
                Verify & Proceed
              </Button>) :
              (<Button type="button" title="Verify & Proceed" fullWidth variant="contained" color="primary" className="register-now text-capitalize p-9 mt-26">
                <img src="static/images/loader.gif" alt="" style={{ width: '7.388%' }} className="empty-mybet" />
              </Button>
              )}
          </form>

          <div className="register-here resend-otp">
            {this.state.countDown ? (<small>00:
              {this.state.countDown} sec</small>) : (<small>Resend <strong onClick={() => this.logInSubmit('', 'resendOtp')}>OTP?</strong></small>)}<br />
          </div>
        </div>
      </div>
    );
    // End bodyOtp
    // Start bodyRegister
    const bodyRegister = (
      <div className="modal-div">
        <Button type="button" className="close-btn" title="Close" onClick={this.handleClose}><CloseIcon /></Button>
        <h3 className="modal-title">Register</h3>
        <p className="modal-description">
          Create your account, it’s free!
          </p>
        <div>
          <form className="register-form" onSubmit={this.registerSubmit}>
            <TextField
              label="First Name"
              type="text"
              variant="outlined"
              fullWidth
              className="mb-3"
              name="firstName"
              inputProps={{
                name: 'phone',
                autoFocus: true,
                maxLength: 20
              }}
              value={firstName}
              onChange={this.handleChange('firstName')}
              error={!!this.state.errorMessage.firstName}
              helperText={!!this.state.errorMessage.firstName ? this.state.errorMessage.firstName : ''}
            />
            <TextField
              label="Last Name"
              type="text"
              variant="outlined"
              fullWidth
              className="mb-3"
              name="lastName"
              value={lastName}
              onChange={this.handleChange('lastName')}
              error={!!this.state.errorMessage.lastName}
              helperText={!!this.state.errorMessage.lastName ? this.state.errorMessage.lastName : ''}
              inputProps={{ maxLength: 20 }}
            />
            {country != '' ?
              (
                <Select value={countryCode} onChange={this.handleChange('countryCode')} variant="outlined" className="country-select">
                  {country}
                </Select>
              )
              : ''}
            <TextField
              className="number-input mobile-input"
              variant="outlined"
              type="number"
              inputProps={{
                name: 'phone',
              }}
              value={mobile}
              name="mobile"
              onChange={this.handleChange('mobile')}
              error={!!this.state.errorMessage.mobile}
              helperText={!!this.state.errorMessage.mobile ? this.state.errorMessage.mobile : ''}
            />
            <TextField
              label="Referral Code"
              type="text"
              variant="outlined"
              fullWidth
              className="mt-3"
              value={referralCode}
              name="referralCode"
              onChange={this.handleChange('referralCode')}
              error={!!this.state.errorMessage.referralCode}
              helperText={!!this.state.errorMessage.referralCode ? this.state.errorMessage.referralCode : ''}
            />
            {/* <div className="form-check termcondtion">
              <input type="checkbox" className="form-check-input" id="bycontinuing" checked={this.state.terms} name="terms" onChange={this.checkTerms} />
              <label className="form-check-label" for="bycontinuing">By continuing, you agree to the
                <Link href="/terms-and-conditions">
                  <a target="_blank" title="Terms &amp; Conditions">
                    &nbsp;Terms &amp; Conditions
                  </a>
                </Link>
                <Link href="/privacy-policy">
                  <a target="_blank" title="Privacy Policy">
                    &nbsp; Privacy Policy
                  </a>
                </Link>
              </label>
              {!!this.state.errorMessage.terms ? (<small className="text-danger">{this.state.errorMessage.terms}</small>) : ''}
            </div> */}
            {isLoaded ? (
              <Button type="submit" title="Register Now" fullWidth variant="contained" color="primary" className="register-now text-capitalize p-9 mt-26 mt-lg-5">
                Register Now
              </Button>
            ) : (
                <Button type="button" title="Register Now" fullWidth variant="contained" color="primary" className="register-now text-capitalize p-9 mt-26 mt-lg-5">
                  <img src="static/images/loader.gif" alt="" style={{ width: '7.388%' }} className="empty-mybet" />
                </Button>
              )}
          </form>

          <div className="register-here">
            <small>Already have an account? <strong onClick={this.openLogin}>Log in here</strong></small>
          </div>
        </div>
      </div>
    );
    // End bodyRegister

    return (
      <div>
        <Snackbar open={alert.toast} autoHideDuration={2000} onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })}>
          <Alert onClose={() => this.setState({ alert: { toast: false, toastMessage: alert.toastMessage, severity: alert.severity } })} severity={alert.severity}>
            {alert.toastMessage}
          </Alert>
        </Snackbar>
        {/* <Button type="button" title="Register / Login" onClick={this.handleOpen} variant="contained" color="primary" className="ml-5 mr-2 register-now ">
          <i className="far fa-user"></i> Register / Login
        </Button>
        <Button variant="contained" title="Deposit Money" color="primary" className="ml-2 deposit-money">
          <i className="fas fa-rupee-sign"></i> Deposit Money
        </Button> */}
        <Modal
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          disableEnforceFocus
        >
          {this.state.popupView == 'login' ? bodyLogin : this.state.popupView == 'register' ? bodyRegister : bodyOtp}

        </Modal>
      </div>
    )
  }
}

export default Popup;