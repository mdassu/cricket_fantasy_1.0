import React, { Component } from "react";
import Cookies from "js-cookie";
import Link from 'next/link';
import { Container, Row, Col, Card, Badge } from 'react-bootstrap';
import { AuthenticationService } from "_services/AuthenticationService";
const { flag, code, name, countries } = require('country-emoji');
import ReactCountryFlag from "react-country-flag";
import { Button, Modal, TextField, Select, MenuItem, Snackbar } from '@material-ui/core';
import { CommonService } from "_services/CommonService";
import { ErrorMessages, isMobileNumberValid } from 'components/MyValidations/MyValidations';
import Router from 'next/router';
import { encrypt } from "_helper/EncrDecrypt";
import Slider from "react-slick";
// function Alert(props) {
//   return <MuiAlert elevation={6} variant="filled" {...props} />;
// }

class Play extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: true,
      formData: {
        mobile: '',
        countryCode: '91',
        otp: ''
      },
      error: false,
      errorMessage: {
        mobile: '',
        otp: ''
      },
      alert: {
        toast: false,
        toastMessage: '',
        severity: ''
      }
    }
  }

  render() {
    const installationStep = {
      centerMode: true,
      slidesToShow: 1,
      dots: false,
      autoplay: true,
      autoplaySpeed: 12000,
      arrows: false,
      swipe: true,
      infinite: false,
      swipeToSlide: true,
      lazyLoad: true,
      centerPadding: "45px",
    };

    const { countries, isLoaded, formData: { mobile, countryCode }, otpMobile, otpCountryCode, alert } = this.state;
    var country = '';
    if (countries && countries.length > 0) {
      country = countries.map((country, key) =>
        <MenuItem key={key} value={country.country_code}>
          <ReactCountryFlag countryCode={country.iso_code} svg /> &nbsp;+{country.country_code}
        </MenuItem>
      );
    }

    return (
      <React.Fragment>
        <div className="slider-home-page">
          <div className="latest-home-page">
            <Row className="justify-content-center align-items-center m-0">
              <Col lg={12} xs={12} sm={12} md={12} className="text-center">
                {/* Logo */}
                <Link href="/">
                  <a>
                    <img src="static/images/newlogo.png" alt="" className="newlogo" />
                  </a>
                </Link>
                {/* Account info */}
                {/* after login */}
                {/* <Link href="/">
                  <a>
                    <img src="static/images/account-circle-fill.png" alt="" className="right-account-info" />
                  </a>
                </Link> */}
                {AuthenticationService.isLogged ? '' : (
                  <Link href="/login">
                    <Button title="Log In" className="right-account-login">Log In</Button>
                  </Link>
                )}
              </Col>
            </Row>
            {/* Download App */}
            <div className="home-register">
              <div className="login-form">
                <a href="https://play.google.com/store" title="Download App" target="_blank" >
                  <Button type="submit" title="Download App" fullWidth variant="contained" color="primary" className="text-uppercase mt-0"> Download App </Button>
                </a>
              </div>
            </div>
            {/* End Download App */}
          </div>

          {/*  install steps*/}
          <Row className="m-0 justify-content-center align-items-center text-center install-step">
            <Col lg={12} className="p-0">
              <h4>
                Follow these stepas to install the
                </h4>
              <b>
                Sports.Cards App
               </b>
            </Col>
          </Row>

          <Slider {...installationStep} >
            <div>
              <Row className="homepage-register-form">
                <Col lg={12} className="p-0">
                  <Card className="download-step more-download" >
                    <Card.Body>
                      <h4>1</h4>
                      <h5>Top ‘OK’ to download the app</h5>
                      <img src="static/images/step1.jpg" alt="Step 1" />
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </div>
            <div>
              <Row className="homepage-register-form">
                <Col lg={12} className="p-0">
                  <Card className="download-step more-download text-left" >
                    <Card.Body>
                      <h4>2</h4>
                      <h5>Top ‘OK’ to download the app</h5>
                      <img src="static/images/step1.jpg" alt="Step 1" />
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </div>
            <div>
              <Row className="homepage-register-form">
                <Col lg={12} className="p-0">
                  <Card className="download-step more-download" >
                    <Card.Body>
                      <h4>3</h4>
                      <h5>Top ‘OK’ to download the app</h5>
                      <img src="static/images/step1.jpg" alt="Step 1" />
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </div>
          </Slider>

          {/* Download the Offical App */}
          <Row className="m-0 justify-content-center align-items-center text-center download-application">
            <Col lg={12} className="p-0">
              <h4>
                Download the Offical App
                </h4>
              <a href="https://play.google.com/store" title="Download App" target="_blank">
                <img src="static/images/download-app.png" />
              </a>
            </Col>
          </Row>
          {/* Row Download the Offical App */}
        </div>
      </React.Fragment >
    )
  }
}
export default Play;