import React from 'react';
import { Dialog, DialogActions, DialogContent, Slide } from '@material-ui/core';
import { Row, Col, Button } from 'react-bootstrap';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function AlertPopup(props) {

  return (
    <React.Fragment>
      {/* Confirmation modal */}
      <Dialog
        className="modal-slide-modal"
        open={props.alertPopup}
        TransitionComponent={Transition}
        keepMounted
        // onClick={this.goBackClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <div >
          <DialogContent >
            <div>
              <div className="create-packs-tabs">
                <React.Fragment>
                  <div className="create-my-pack header-row-my-pack  m-0">
                    <Row className="m-0  justify-content-center align-items-center">
                      <Col xs={12} className="text-center slide-modal-body ">
                        <h5>Alert!</h5>
                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-exclamation-circle" fill="#844ff7" xmlns="http://www.w3.org/2000/svg">
                          <path fillRule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                          <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z" />
                        </svg>
                        <p>{props.alertMsg}</p>
                      </Col>
                      <DialogActions >
                        <Col xs={12} className="slide-modal mobile-bottom-btn p-0">
                          <div className="common-btn">
                            <Button variant="success" disabled={props.buttonStatus} title={props.alertButton} onClick={() => props.buttonAction(props.type)}>{props.alertButton}</Button>
                            {
                              props.alertSecButton ? (
                                <Button variant="secondary" title={props.alertSecButton} onClick={() => props.secButtonAction()}>{props.alertSecButton}</Button>
                              ) : ''
                            }
                          </div>
                        </Col>
                      </DialogActions>
                    </Row>
                  </div>
                </React.Fragment>
              </div>
            </div>
          </DialogContent>
        </div>
      </Dialog>
      {/* End Confirmation modal */}
    </React.Fragment>
  );

}

export default AlertPopup;