import React, { Component } from "react";
import Cookies from "js-cookie";
import Link from 'next/link';
import { Container, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import { Snackbar, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import { AuthenticationService } from "_services/AuthenticationService";
import Loader from "components/CommonComponents/Loader";

// function Alert(props) {
//   return <MuiAlert elevation={6} variant="filled" {...props} />;
// }

class Play extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      // isLoggedIn: false,
      liveMatches: [],
      upcommingMatches: [],
      allUpcommingMatches: [],
      myMatches: [],
      start: 0,
      last: 4,
      limit: 5,
      alert: {
        toast: false,
        toastMessage: '',
        severity: ''
      },
      isLoaded: false,
      homeIntervel: 0
    }
  }



  render() {

    const settings = {
      infinite: false,
      slidesToShow: 1,
      lazyLoad: true,
      speed: 100,
      dots: true,
      arrows: false,
      autoplay: true
    };

    // const { liveMatches, upcommingMatches, myMatches, isLoaded } = this.state;

    return (
      <React.Fragment>

        <div className="body-content home-page new-home-page" >
          {/* <div className="slikslider-bar">
            <div className="slikslider-banner"></div>
          </div> */}
          <Row className="h-100">
            <Col lg={12} className="h-100">
              <h1>How to play</h1>
              <Card className="homepage-card">
                <Row className="homepage-card-top">
                  <Col lg={6} xs={6} md={6} sm={6} >
                    <div className="box-number">
                      <span>1</span>
                    </div>
                  </Col>
                  <Col lg={6} xs={6} md={6} sm={6}>
                    <div className="float-right homepage-card-right">
                      <img src="static/images/home-rightcard.jpg" />
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg={12} xs={12} md={12} sm={12}>
                    <div className="home-card-content">
                      <h4>Select a match</h4>
                      <p>Choose an upcoming or live match</p>
                    </div>
                  </Col>
                </Row>
              </Card>
              <Card className="homepage-card">
                <Row className="homepage-card-top">
                  <Col lg={6} xs={6} md={6} sm={6} >
                    <div className="box-number">
                      <span>2</span>
                    </div>
                  </Col>
                  <Col lg={6} xs={6} md={6} sm={6}>
                    <div className="float-right homepage-card-right">
                      <img src="static/images/home-rightcard1.jpg" />
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg={12} xs={12} md={12} sm={12}>
                    <div className="home-card-content">
                      <h4>Create a fantasy card</h4>
                      <ul className="p-0">
                        <li>Choose events like Match wins, Total sixes etc.</li>
                        <li>Distribute 100 credits amongst the events.</li>
                        <li>Submit card</li>
                      </ul>
                    </div>
                  </Col>
                </Row>
              </Card>
              <Card className="homepage-card homepage-last-card">
                <Row className="homepage-card-top">
                  <Col lg={6} xs={6} md={6} sm={6} >
                    <div className="box-number">
                      <span>3</span>
                    </div>
                  </Col>
                  <Col lg={6} xs={6} md={6} sm={6}>
                    <div className="float-right homepage-card-right">
                      <img src="static/images/home-rightcard2.jpg" />
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg={12} xs={12} md={12} sm={12}>
                    <div className="home-card-content">
                      <h4>Enter a contest to win Cash</h4>
                      <p>Choose between contests to earn points and win cash</p>
                    </div>
                  </Col>
                </Row>
              </Card>
              <h2>INDIA’S 1st REAL TIME SPORTS FANTASY APP</h2>
              <Card className="homepage-bottom-banner">
                <div >
                  <ul className="p-0">
                    <li>
                      <h5><img src="static/images/homepage-star.png" /> ₹{process.env.REGISTER_BONUS}</h5>
                      <span>Registration bonus</span>
                    </li>
                    <li>
                      <h5><img src="static/images/trophy.svg" /> Daily ₹{process.env.DAILY_WINNING}</h5>
                      <span>Winning distributed</span>
                    </li>
                  </ul>
                </div>
              </Card>
            </Col>
          </Row>
        </div>
        {/* <Loader /> */}

      </React.Fragment >

    )
  }
}
export default Play;