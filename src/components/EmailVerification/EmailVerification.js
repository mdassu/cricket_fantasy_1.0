import Link from 'next/link';
import React, { Component } from "react";
import { Container, Row, Col, Card, Badge } from 'react-bootstrap';
import Button from '@material-ui/core/Button';
import { AuthenticationService } from '_services/AuthenticationService';
import Router from 'next/router';

class EmailVerification extends Component {

  constructor(props) {
    super(props);
    this.state = {
      verifyMessage: '',
      isLoaded: false,
      isVerified: false
    }
  }

  componentDidMount() {
    this.verifyEmail();
  }

  verifyEmail = () => {
    AuthenticationService.emailVerify(Router.query, res => {
      if (res.status) {
        this.setState({
          verifyMessage: 'Your email has been verified successfully',
          isVerified: true,
          isLoaded: true
        });
      } else {
        this.setState({
          verifyMessage: 'The requested link is expried! Please try again.',
          isLoaded: true
        });
      }
    });
  }

  render() {
    const { isLoaded, isVerified, verifyMessage } = this.state;
    return (
      <div>
        <Container>
          <Row>
            {isLoaded ? (
              <Col lg={{ span: 8, offset: 2 }} xs={12} sm={12} md={12} className="mt-5">
                <div className="empty-mybet-data text-center verificaion-page">
                  {/* <i className="fas fa-times-circle"></i> */}
                  {isVerified ? (<i className="fas fa-check-circle"></i>) : (<i className="fas fa-times-circle"></i>)}
                  <p>
                    {verifyMessage}
                  </p>
                  <Link href="/">
                    <Button variant="contained" title=" Back to Home" className="">
                      Back to Home
                      </Button>
                  </Link>
                </div>
              </Col>
            ) : ''}

          </Row>
        </Container>
      </div>
    )
  }
}
export default EmailVerification;

