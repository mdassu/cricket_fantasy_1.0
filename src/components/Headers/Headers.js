import Link from 'next/link';
import Cookies from 'js-cookie';
import Router from 'next/router';
import React, { Component } from "react";
import { Row, Col, Card, Button, Tooltip } from 'react-bootstrap';
import { decrypt } from '_helper/EncrDecrypt';
import { AuthenticationService } from '_services/AuthenticationService';
import { ContestsService } from '_services/ContestsService';
import { Dialog, DialogActions, DialogContent, Slide } from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Collapse from '@material-ui/core/Collapse';
import { GAevent } from "_helper/google-analytics";
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const TransitionTop = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="down" ref={ref} {...props} />;
});

class HomeHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {}
        }
    }


    componentDidMount() {
        this.getCurrentUser();
    }

    getCurrentUser() {
        AuthenticationService.currentUser.subscribe(user => {
            if (user && user != null) {
                this.setState({
                    user: {
                        firstName: user['first_name'],
                        lastName: user['last_name'],
                        profilePic: user['profile_picture']
                    }
                });

            }
        });
    }

    render() {
        const { user } = this.state;
        return (
            <header>
                <Row className="m-0 ">
                    {/* Download bar */}
                    <Col lg={5} sm={12} md={12} className="download-bar">
                        <Row>
                            <Col lg={5} xs={6}>
                                <p>
                                    Sports.Cards is better
                                    on the app
                  </p>
                            </Col>
                            <Col lg={7} xs={6} className="text-right">
                                <div className="download-app">
                                    <Link href="/" >
                                        <a title="Download App">
                                            Download App
                     </a>
                                    </Link>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row className="m-0 ">
                    {/* End Download bar */}
                    <Col lg={5} sm={12} md={12} className="appbar">
                        <Row className=" justify-content-center align-items-center">
                            {/* Logo  */}
                            <Col lg={6} sm={6} md={6} xs={6} className="logoname">
                                <Link href="/">
                                    <a className="navbar-brand headerhome-logo" title="Sports.Cards">
                                        <img src='static/images/logo.svg' />
                                    </a>
                                </Link>
                            </Col>
                            {/* Right bar header menu */}
                            <Col lg={6} sm={6} md={6} xs={6} className="text-right">
                                {AuthenticationService.isLogged ?
                                    (
                                        <React.Fragment>
                                            {/* <div className="notification">
                                                <Link href="/" >
                                                <a title="Notification">
                                                    <img src="static/images/notification-bell-outline-interface-symbol.svg" alt="" />
                                                </a>
                                                </Link>
                                            </div> */}
                                            {/*   Download App icon */}
                                            <div className="download-header-app">
                                                <Link href="/" >
                                                    <a title="Download App">
                                                        <img src="static/images/android.svg" alt="" />
                                                        Download App
                                                    </a>
                                                </Link>

                                            </div>
                                            {/* profilePic */}
                                            <div className="user-avtar header-home-new">
                                                <Link href="/my-profile" >
                                                    <a title="My Profile"><img src={user.profilePic ? process.env.IMG_URL + user.profilePic : 'static/images/winner.jpg'} alt="" className="user-avtar-img" /></a>
                                                </Link>
                                            </div>
                                        </React.Fragment>
                                    ) : ''
                                }
                            </Col>
                            {/*End  Right bar header menu */}
                        </Row>
                    </Col>
                </Row>
            </header>
        )
    }
}

class ContestHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {},
            matchData: {},
            openWalletopen: false
        }
    }


    componentDidMount() {
        this.getMatchDetails();
        this.getCurrentUser();
    }

    getMatchDetails() {
        var params = {
            match_id: decrypt(Router.query.m)
        }
        ContestsService.getMatchDetails(params, res => {
            if (res.status) {
                this.setState({
                    matchData: res['data']
                });
            } else {
                this.setState({
                    matchData: {}
                });
            }
        });
    }

    getCurrentUser() {
        if (Cookies.get('_user')) {
            var user = decrypt(Cookies.get('_user'));
            if (user && user != null) {
                this.setState({
                    user: {
                        firstName: user['first_name'],
                        lastName: user['last_name'],
                        profilePic: user['profile_picture'],
                        walletBalance: user['walletBalance'],
                        addedBalance: user['addedBalance'],
                        bonusBalance: user['bonusBalance'],
                        winningBalance: user['winningBalance'],
                    }
                });
            }
        }
    }

    // openWalletopen
    openWallet = () => {
        this.getCurrentUser();
        this.setState({
            openWalletopen: !this.state.openWalletopen
        });
    };


    closeModal = () => {
        this.setState({
            openWalletopen: false
        });
    };

    render() {
        const { user, matchData, openWalletopen, } = this.state;
        return (
            <React.Fragment>
                <header>
                    <Row className="m-0 ">
                        {/* Download bar */}
                        <Col lg={5} sm={12} md={12} className="download-bar">
                            <Row>
                                <Col lg={5} xs={6}>
                                    <p>
                                        Sports.Cards is better
                                        on the app
                  </p>
                                </Col>
                                <Col lg={7} xs={6} className="text-right">
                                    <div className="download-app">
                                        <Link href="/" >
                                            <a title="Download App">
                                                Download App
                     </a>
                                        </Link>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="m-0 ">
                        {/* End Download bar */}
                        <Col lg={5} sm={12} md={12} className="appbar">
                            <Row className=" justify-content-center align-items-center">
                                {/* Logo  */}
                                <Col lg={4} sm={4} md={4} xs={4} className="logoname">
                                    {/* <Link href="/"> */}
                                    <a className="navbar-brand" title="" onClick={() => Router.back()}>
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-arrow-left-short" fill="#fff" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z" />
                                        </svg>
                                        {/* Back icon */}
                                    </a>
                                    {/* </Link> */}
                                </Col>
                                {/* team matches header */}
                                <Col lg={4} sm={4} md={4} xs={4} className="apptop-header">
                                    <Row className="team-app-header m-0">
                                        <Col lg={4} xs={4} md={4} sm={4} className="text-center p-0">
                                            <div className="my-team-name mi-team" style={{ background: matchData['hometeamcolorcode'] }}>
                                                {matchData['hometeamname']}
                                            </div>
                                        </Col>
                                        <Col lg={4} xs={4} md={4} sm={4} className="text-center p-0">
                                            <img src="static/images/whitevs.png" className="team-vs m-0" />
                                        </Col>
                                        <Col lg={4} xs={4} md={4} sm={4} className="text-center p-0 ">
                                            <div className="my-team-name iplteam" style={{ background: matchData['awayteamcolorcode'] }}>
                                                {matchData['awayteamname']}
                                            </div>
                                        </Col>
                                    </Row>
                                </Col>
                                {/* End team matches header */}
                                {/* Right bar header menu */}
                                <Col lg={4} sm={4} md={4} xs={4} className="text-right">
                                    {AuthenticationService.isLogged ? (
                                        <React.Fragment>
                                            {/* <div className="notification">
                        <Link href="/" >
                          <a title="Notification">
                            <img src="static/images/notification-bell-outline-interface-symbol.svg" alt="" />
                          </a>
                        </Link>
                      </div> */}
                                            {/* <div className="wallet-header">
                      <Link href="/" >
                      <a title="Help">
                      <img src="static/images/information.svg" alt="" />
                      </a>
                      </Link>
                    </div> */}
                                            <div className="wallet-header">
                                                <a title="Wallet" onClick={this.openWallet} style={{ cursor: 'pointer' }}>
                                                    <img src="static/images/wallet.svg" alt="" />
                                                </a>
                                            </div>
                                        </React.Fragment>
                                    ) : (
                                            <div className="user-avtar">
                                                <Link href="/login" >
                                                    <a title="Login"><img src='static/images/avatar.png' alt="" className="user-avtar-img" /></a>
                                                </Link>
                                            </div>
                                        )
                                    }
                                </Col>
                                {/*End  Right bar header menu */}
                            </Row>
                        </Col>
                    </Row>
                </header>
                {/* My wallet popup */}

                <Dialog
                    open={openWalletopen}
                    className="common-modals headermodal"
                    onClose={this.closeModal}
                    TransitionComponent={TransitionTop}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                >

                    <DialogContent>
                        <div id="alert-dialog-slide-description">
                            <Row>
                                <Col lg={12} className="p-0">
                                    <div className="outer-card mt-0">
                                        <div className="view-all-winner-div mt-3">
                                            <Col lg={12} className="p-1 ">
                                                <Card>
                                                    <Card.Body>
                                                        <Row className="justify-content-center align-items-center add-cash-page">
                                                            <Col xs={12} className="mobile-bottom-btn add-cash text-center">
                                                                <span>
                                                                    Total Balance
                              </span>
                                                                <br />
                                                                <b>
                                                                    &#x20B9;{parseFloat(user['walletBalance'])}
                                                                </b>
                                                                <div className="common-btn w-100">
                                                                    <Link href="/add-cash">
                                                                        <a title="Add Cash">
                                                                            <Button className="" variant="success" title="Add Cash" >Add Cash</Button>
                                                                        </a>
                                                                    </Link>
                                                                </div>
                                                            </Col>

                                                            <Accordion className="mb-3" defaultExpanded>
                                                                <AccordionSummary
                                                                    aria-label="Expand"
                                                                    expandIcon={<ExpandMoreIcon />}
                                                                >
                                                                </AccordionSummary>
                                                                <div >
                                                                    <Col xs={12} className="wallet-listing mt-4 p-0">
                                                                        <ul>
                                                                            <li className="">
                                                                                <h5> Amount Added</h5>
                                        &#x20B9;{user['addedBalance'] ? user['addedBalance'] : 0}
                                                                                <span className="left-info-icon">
                                                                                    <OverlayTrigger
                                                                                        placement="left"
                                                                                        delay={{ show: 100, hide: 400 }}
                                                                                        overlay={<Tooltip id="button-tooltip">Added Amount</Tooltip>}
                                                                                    >
                                                                                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                            <path fillRule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                                                        </svg>
                                                                                    </OverlayTrigger>
                                                                                </span>
                                                                            </li>
                                                                            <li>
                                                                                <h5> Winnings</h5>
                                      &#x20B9;{user['winningBalance'] ? user['winningBalance'] : 0}
                                                                                <Link href="/withdrawal">
                                                                                    <Button className="withdrawal_btn" variant="success" title="Withdrawal Request" >Withdrawal Request</Button>
                                                                                </Link>
                                                                                <span className="left-info-icon">
                                                                                    <OverlayTrigger
                                                                                        placement="left"
                                                                                        delay={{ show: 100, hide: 400 }}
                                                                                        overlay={<Tooltip id="button-tooltip">Winning Amount</Tooltip>}
                                                                                    >
                                                                                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                            <path fillRule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                                                        </svg>
                                                                                    </OverlayTrigger>
                                                                                </span>
                                                                            </li>
                                                                            <li>
                                                                                <h5>  Cash Bonus</h5>
                                         &#x20B9; {user['bonusBalance'] ? user['bonusBalance'] : 0}
                                                                                <span className="left-info-icon">
                                                                                    <OverlayTrigger
                                                                                        placement="left"
                                                                                        delay={{ show: 100, hide: 400 }}
                                                                                        overlay={<Tooltip id="button-tooltip">Bonus Amount</Tooltip>}
                                                                                    >
                                                                                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                            <path fillRule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                                                                        </svg>
                                                                                    </OverlayTrigger>
                                                                                </span>
                                                                            </li>
                                                                        </ul>
                                                                    </Col>
                                                                </div>
                                                            </Accordion>
                                                        </Row>
                                                    </Card.Body>
                                                </Card>
                                            </Col>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </DialogContent>
                </Dialog>
                {/* End wallet popup */}
            </React.Fragment>
        )
    }
}

class PackHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            goBackModalOpen: false,
            matchData: {},
            email: '',
            mobile: ''
        }
    }

    componentDidMount() {
        this.getCurrentUser();
    }

    getCurrentUser() {
        AuthenticationService.currentUser.subscribe(user => {
            if (user && user != null) {
                this.setState({
                    email: user['email'],
                    mobile: user['mobile'],
                });
            }
        });
    }

    backPage = () => {
        if (this.props.headerText == 'Create Card') {
            this.goBackModal();
        } else {
            Router.back();
        }
    }

    goBackModal = () => {
        this.setState({
            goBackModalOpen: true
        });
    };


    goBackClose = (type) => {
        this.setState({
            goBackModalOpen: false
        });
        if (type == 'discard') {
            AuthenticationService.clearCookies();
            var value = '';
            if (this.state.mobile && this.state.mobile != '') {
                value = this.state.mobile
            } else {
                value = this.state.email
            }
            GAevent('Pack_Discard', 'On Click', 'How many users dropped of with partial pack creation', value);
            Router.back();
        }
    };

    render() {
        return (
            <header>
                <Row className="m-0 ">
                    {/* Download bar */}
                    <Col lg={5} sm={12} md={12} className="download-bar">
                        <Row>
                            <Col lg={5} xs={6}>
                                <p>
                                    Sports.Cards is better
                                    on the app
                  </p>
                            </Col>
                            <Col lg={7} xs={6} className="text-right">
                                <div className="download-app">
                                    <Link href="/" >
                                        <a title="Download App">
                                            Download App
                     </a>
                                    </Link>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row className="m-0 ">
                    {/* End Download bar */}
                    <Col lg={5} sm={12} md={12} className="appbar">
                        <Row className=" justify-content-center align-items-center">
                            {/* Logo  */}
                            <Col lg={8} sm={8} md={8} xs={8} className="logoname">
                                {/* <Link href="/"> */}
                                <a className="navbar-brand" title="" onClick={this.backPage}>
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-arrow-left-short" fill="#fff" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z" />
                                    </svg>
                                    {/* Back icon */}
                                </a>
                                {/* </Link> */}
                                <a className="navbar-brand" title="">
                                    {this.props.headerText}
                                </a>
                            </Col>
                            {/* Right bar header menu */}
                            <Col lg={4} sm={4} md={4} xs={4} className="text-right">
                                <div className="user-avtar">
                                    <Link href="/how-to-play" >
                                        {/* <a title="Login"><img src='static/images/avatar.png' alt="" className="user-avtar-img" /></a> */}
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-question-circle" fill="#fff" xmlns="http://www.w3.org/2000/svg">
                                            <path fillRule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                            <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z" />
                                        </svg>
                                    </Link>
                                </div>
                            </Col>
                            {/*End  Right bar header menu */}
                        </Row>
                    </Col>
                </Row>
                {/* Go back confimation modal */}
                <Dialog
                    className="modal-slide-modal"
                    open={this.state.goBackModalOpen}
                    TransitionComponent={Transition}
                    keepMounted
                    onClick={this.goBackClose}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                >
                    <div >
                        <DialogContent >
                            <div>
                                <div className="create-packs-tabs">
                                    <React.Fragment>
                                        <div className="create-my-pack header-row-my-pack  m-0">
                                            <Row className="m-0  justify-content-center align-items-center">
                                                <Col xs={12} className="text-center slide-modal-body ">
                                                    <h5>Go Back?</h5>
                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-exclamation-circle" fill="#844ff7" xmlns="http://www.w3.org/2000/svg">
                                                        <path fillRule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                        <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z" />
                                                    </svg>
                                                    <p>Your Card will not be saved!</p>
                                                </Col>
                                                <DialogActions >
                                                    <Col xs={12} className="slide-modal mobile-bottom-btn p-0">
                                                        <div className="common-btn">
                                                            <Button variant="secondary" title="Continue Editing" onClick={() => this.goBackClose('continue')}>Continue Editing</Button>
                                                            <Button variant="success" title="Discard Card" onClick={() => this.goBackClose('discard')}>Discard Card</Button>
                                                        </div>
                                                    </Col>
                                                </DialogActions>
                                            </Row>
                                        </div>
                                    </React.Fragment>
                                </div>
                            </div>
                        </DialogContent>
                    </div>
                </Dialog>
            </header>
        )
    }
}

class TabHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {}
        }
    }


    componentDidMount() {
        this.getCurrentUser();
    }

    getCurrentUser() {
        AuthenticationService.currentUser.subscribe(user => {
            if (user && user != null) {
                this.setState({
                    user: {
                        firstName: user['first_name'],
                        lastName: user['last_name'],
                        profilePic: user['profile_picture'],
                        walletBalance: user['walletBalance']
                    }
                });

            }
        });
    }

    render() {
        const { user } = this.state;
        return (
            <header>
                <Row className="m-0 ">
                    {/* Download bar */}
                    <Col lg={5} sm={12} md={12} className="download-bar">
                        <Row>
                            <Col lg={5} xs={6}>
                                <p>
                                    Sports.Cards is better
                                    on the app
                  </p>
                            </Col>
                            <Col lg={7} xs={6} className="text-right">
                                <div className="download-app">
                                    <Link href="/" >
                                        <a title="Download App">
                                            Download App
                     </a>
                                    </Link>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row className="m-0 ">
                    {/* End Download bar */}
                    <Col lg={5} sm={12} md={12} className="appbar">
                        <Row className=" justify-content-center align-items-center">
                            {/* Logo  */}
                            <Col lg={4} sm={4} md={4} xs={4} className="logoname">
                            </Col>
                            <Col lg={4} sm={4} md={4} xs={4} className="logoname p-0" style={{ textAlign: 'center' }}>
                                {/* <Link href="/"> */}
                                <a className="navbar-brand" title="">
                                    {this.props.headerText}
                                </a>
                                {/* </Link> */}
                            </Col>
                            {/* Right bar header menu */}
                            <Col lg={4} sm={4} md={4} xs={4} className="text-right">
                                {AuthenticationService.isLogged ?
                                    (
                                        <React.Fragment>
                                            {/* <div className="notification">
                        <Link href="/" >
                          <a title="Notification">
                            <img src="static/images/notification-bell-outline-interface-symbol.svg" alt="" />
                          </a>
                        </Link>
                      </div> */}
                                            <div className="user-avtar">
                                                <Link href="/my-profile" >
                                                    <a title="My Profile"><img src={user.profilePic ? process.env.IMG_URL + user.profilePic : 'static/images/winner.jpg'} alt="" className="user-avtar-img" /></a>
                                                </Link>
                                            </div>
                                        </React.Fragment>
                                    ) : ''
                                }
                            </Col>
                            {/*End  Right bar header menu */}
                        </Row>
                    </Col>
                </Row>
            </header>
        )
    }
}

class InnerHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            goBackModalOpen: false,
            profilePic: ''
        }
    }


    componentDidMount() {
        this.getCurrentUser();

    }

    getCurrentUser() {
        AuthenticationService.currentUser.subscribe(user => {
            if (user && user != null) {
                this.setState({
                    profilePic: user['profile_picture']
                });

            }
        });
    }

    backPage = () => {
        // if (Object.keys(Router.router['components']).length <= 2) {
        //   Router.push({
        //     pathname: '/'
        //   });
        // } else {
        //   Router.back();
        // }
        if (this.props.headerText == 'Add Cash') {
            Router.push('/wallet');
        } else if (this.props.headerText == 'Low Balance') {
            this.setState({
                goBackModalOpen: true
            });
        } else {
            Router.back();
        }

    }


    goBackClose = (type) => {
        this.setState({
            goBackModalOpen: false
        });
        if (type == 'discard') {
            AuthenticationService.clearCookies();
            Router.back();
        }
    };

    render() {
        const { profilePic } = this.state;
        return (
            <header>
                <Row className="m-0 ">
                    {/* Download bar */}
                    <Col lg={5} sm={12} md={12} className="download-bar">
                        <Row>
                            <Col lg={5} xs={6}>
                                <p>
                                    Sports.Cards is better
                                    on the app
                  </p>
                            </Col>
                            <Col lg={7} xs={6} className="text-right">
                                <div className="download-app">
                                    <Link href="/" >
                                        <a title="Download App">
                                            Download App
                     </a>
                                    </Link>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row className="m-0 ">
                    {/* End Download bar */}
                    <Col lg={5} sm={12} md={12} className="appbar">
                        <Row className=" justify-content-center align-items-center">
                            {/* Logo  */}
                            <Col lg={8} sm={8} md={8} xs={8} className="logoname">
                                {/* <Link href="/"> */}
                                <a className="navbar-brand" title="" onClick={this.backPage}>
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-arrow-left-short" fill="#fff" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z" />
                                    </svg>
                                    {/* Back icon */}
                                </a>
                                {/* </Link> */}
                                <a className="navbar-brand" title="">
                                    {this.props.headerText ? this.props.headerText : ''}
                                </a>
                            </Col>
                            {/* Right bar header menu */}
                            <Col lg={4} sm={4} md={4} xs={4} className="text-right">
                                {AuthenticationService.isLogged ? (
                                    <React.Fragment>
                                        {/* <div className="notification">
                      <Link href="/" >
                        <a title="Notification">
                          <img src="static/images/notification-bell-outline-interface-symbol.svg" alt="" />
                        </a>
                      </Link>
                    </div> */}
                                        <div className="user-avtar">
                                            <Link href="/my-profile" >
                                                <a title="My Profile"><img src={profilePic ? process.env.IMG_URL + profilePic : 'static/images/winner.jpg'} alt="" className="user-avtar-img" /></a>
                                            </Link>
                                        </div>
                                    </React.Fragment>
                                ) : ''
                                }
                            </Col>
                            {/*End  Right bar header menu */}
                        </Row>
                    </Col>
                </Row>
                {/* Go back confimation modal */}
                <Dialog
                    className="modal-slide-modal"
                    open={this.state.goBackModalOpen}
                    TransitionComponent={Transition}
                    keepMounted
                    onClick={this.goBackClose}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                >
                    <div >
                        <DialogContent >
                            <div>
                                <div className="create-packs-tabs">
                                    <React.Fragment>
                                        <div className="create-my-pack header-row-my-pack  m-0">
                                            <Row className="m-0  justify-content-center align-items-center">
                                                <Col xs={12} className="text-center slide-modal-body ">
                                                    <h5>Go Back?</h5>
                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-exclamation-circle" fill="#844ff7" xmlns="http://www.w3.org/2000/svg">
                                                        <path fillRule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                        <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z" />
                                                    </svg>
                                                    <p>Your Card will not be saved!</p>
                                                </Col>
                                                <DialogActions >
                                                    <Col xs={12} className="slide-modal mobile-bottom-btn p-0">
                                                        <div className="common-btn">
                                                            <Button variant="secondary" title="Continue Editing" onClick={() => this.goBackClose('continue')}>Continue</Button>
                                                            <Button variant="success" title="Discard Card" onClick={() => this.goBackClose('discard')}>Discard Card</Button>
                                                        </div>
                                                    </Col>
                                                </DialogActions>
                                            </Row>
                                        </div>
                                    </React.Fragment>
                                </div>
                            </div>
                        </DialogContent>
                    </div>
                </Dialog>
            </header>
        )
    }
}

export {
    HomeHeader,
    ContestHeader,
    PackHeader,
    TabHeader,
    InnerHeader
};

