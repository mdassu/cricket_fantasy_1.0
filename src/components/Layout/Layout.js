import React, { Component } from 'react';
import Cookies from 'js-cookie';
import Head from 'next/head';
import { Nav, Navbar } from 'react-bootstrap';
import Link from 'next/link';
import { Container, Row, Col, Card, Button, Breadcrumb } from 'react-bootstrap';
import './Layout.css';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import HomeIcon from '@material-ui/icons/Home';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import MenuIcon from '@material-ui/icons/Menu';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Login from 'components/Login/Login';
import { ContestHeader, HomeHeader, InnerHeader, PackHeader, TabHeader } from 'components/Headers/Headers';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { isMobile } from 'react-device-detect';
import Router from 'next/router';
import { slide as Menu } from 'react-burger-menu';
import Popup from 'components/Login/Popup';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';
import { AuthenticationService } from '_services/AuthenticationService';
import Overlay from 'react-bootstrap/Overlay';
import { CommonService } from '_services/CommonService';
import { withRouter } from "react-router-dom";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { decrypt } from '_helper/EncrDecrypt';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

class Layout extends Component {


  constructor(props) {
    super(props);

    this.state = {
      // isLoggedIn: false,
      onHome: true,
      menuOpen: false,
      isLogOut: false,
      creditOpen: false,
      coins: '',
      creditType: '',
      logoutOpen: false,
      popupOpen: false,
      openWalletopen: false,
      value: 0,
      routeType: 'home',
      user: {
        firstName: '',
        lastName: '',
        profilePic: '',
        walletBalance: ''
      },
      updateWalletInterval: ''
    }
  }


  // const[value, setValue] = React.useState(0);

  componentDidMount() {
    this.getCurrentUser();
    CommonService.getCountry();

    if (Router.pathname != '/') {
      this.setState({
        onHome: false
      })
    } else {
      this.setState({
        onHome: true
      })
    }
    // if (AuthenticationService.currentUserValue && AuthenticationService.currentUserValue != null) {
    //   this.setState({
    //     isLoggedIn: true
    //   });
    //   if (typeof (this.props.isLogged) === 'function') {
    //     this.props.isLogged(true);
    //   }
    // } else {
    //   this.setState({
    //     isLoggedIn: false
    //   });
    // }
  }

  componentDidUpdate(prevProps) {
    // if (!this.state.isLoggedIn && this.props.isLogged !== this.state.isLoggedIn && !this.state.isLogOut) {
    //   this.setState({
    //     isLoggedIn: this.props.isLogged
    //   });
    //   this.changeLogged(this.props.isLogged);
    // }



  }

  getCurrentUser() {
    // AuthenticationService.currentUser.subscribe(user => {
    if (Cookies.get('_user')) {
      var user = decrypt(Cookies.get('_user'));
      if (user && user != null) {
        this.setState({
          user: {
            firstName: user['first_name'],
            lastName: user['last_name'],
            profilePic: user['profile_picture'],
            walletBalance: user['walletBalance']
          },
          // isLoggedIn: true
        });
        // this.props.isLogged(true);
      }
    }
    // });
  }

  changeLogged = (status) => {
    // if (status) {
    //   var user = AuthenticationService.currentUserValue;
    //   if (user && user != null) {
    //     if (user['creditedCash'] != null) {
    //       var creditType = 'Daily login';
    //       if (user['creditedType'] == 'Register coin') {
    //         creditType = 'Signup'
    //       }
    //       this.setState({
    //         creditOpen: true,
    //         coins: user['creditedCash'],
    //         creditType: creditType
    //       });
    //     }
    //     this.setState({
    //       user: {
    //         firstName: user['first_name'],
    //         lastName: user['last_name'],
    //         profilePic: user['profile_picture'],
    //         walletBalance: user['walletBalance'],
    //       },
    //       // isLoggedIn: true
    //     });
    //     if (Cookies.get('_depositClick')) {
    //       Cookies.remove('_depositClick');
    //       Router.push('/coins')
    //     }
    //   }
    // }
  }

  logOut = () => {
    AuthenticationService.logout();
    this.setState({
      // isLoggedIn: false,
      isLogOut: true,
      logoutOpen: false
    });
    this.closeMenu();
    // this.props.isLogged(false);
    Router.push('/');
    this.setState({ menuOpen: false })
  }

  handleStateChange(state) {
    this.setState({ menuOpen: state.isOpen })
  }

  closeMenu() {
    this.setState({ menuOpen: false })
  }

  toggleMenu(e) {
    CommonService.updateUserWallet();
    setTimeout(() => {
      this.getCurrentUser();
    }, 700);
    this.setState(state => ({ menuOpen: !state.menuOpen }))
  }

  changeRoute(type) {
    this.setState(prevState => {
      if (prevState.routeType != type) {
        return { routeType: type }
      }
    });
  }

  handleOpen = () => {
    // setOpen(true);
    this.setState({
      popupOpen: true
    });
  };

  handleClose = () => {
    this.setState({
      popupOpen: false
    });
    // setOpen(false);
  };

  depositClick = () => {
    Cookies.set('_depositClick', '1');
    this.handleOpen();
  }

  confimationDilogOpen = () => {
    this.setState({
      logoutOpen: true,
      menuOpen: false
    });
  };

  confimationDilogClose = () => {
    this.setState({
      logoutOpen: false
    });
  };

  congratClose = () => {
    this.setState({
      creditOpen: false
    });
  };

  // openWalletopen
  openWallet = () => {
    this.setState({
      openWalletopen: true
    });
  };


  closeModal = () => {
    this.setState({
      openWalletopen: false
    });
  };

  navigateTo = (url) => {
    Router.push(url);
  }


  render() {
    const { user, popupOpen, menuOpen, logoutOpen, openWalletopen, onHome } = this.state;
    return (
      <React.Fragment>
        {/* Head part of the website */}
        < Head >
          <meta charSet="utf-8" />
          <meta name="theme-color" content="#773cf6" />
          <title>Sports.Cards</title>
          <link
            rel="icon"
            href="static/images/favicon.png"
            type="image/gif"
            sizes="16x16" />
          <meta
            name="viewport"
            content="width=device-width,  initial-scale=1 maximum-scale=1.0, user-scalable=no" />
          <link
            rel="stylesheet"
            href="/static/styles/style.css" />
          {/* <link rel="stylesheet preload" as="style" href="https://pro.fontawesome.com/releases/v5.8.2/css/all.css" /> */}
          {/* <link
            rel="stylesheet"
            type="text/css"
            charSet="UTF-8"
            href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
          />
          <link
            rel="stylesheet"
            type="text/css"
            href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
          /> */}
          <link
            rel="stylesheet"
            type="text/css"
            href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css"
          />

        </Head >
        {/* <!-- End Google Tag Manager (noscript) --> */}
        {/* Web Navigation Started here */}

        {/* header */}

        {/* WALLET POPUP DILOG BUTTN */}
        {/* <div onClick={this.openWallet}>wallet</div> */}

        {
          this.props.header == 'homeHeader' ?
            (
              <HomeHeader />
            ) : this.props.header == 'contestHeader' ?
              (
                <ContestHeader headerText={this.props.headerText} />
              ) : this.props.header == 'packHeader' ?
                (
                  <PackHeader headerText={this.props.headerText} />
                ) : this.props.header == 'tabHeader' ?
                  (
                    <TabHeader headerText={this.props.headerText} />
                  ) : this.props.header == 'innerHeader' ?
                    (
                      <InnerHeader headerText={this.props.headerText} />
                    ) : ''}
        {/* Header End */}

        {/* Sidebar Menu bar */}

        <div className="desktop-menu-show">
          <Menu left
            isOpen={menuOpen}
            onStateChange={(state) => this.handleStateChange(state)}
          >
            <div className="justify-content-center align-items-center">
              <Row className="sidebar-profile m-0">
                <Col lg="3" xs={4}>
                  <Link href="/my-profile" >
                    <div className="profile-menu">
                      <img src={user.profilePic ? process.env.IMG_URL + user.profilePic : 'static/images/avatar.png'} alt="" />
                    </div>
                  </Link>
                </Col>
                <Col lg="9" xs={8} className=" userside-profile">
                  <h4>{user.firstName} {user.lastName}</h4>
                  <span>
                    &#x20B9;{user.walletBalance}
                  </span>
                </Col>
              </Row>
              <Row className="m-0 sidebar-menu">
                <Col lg={12} className="pl-0">
                  <ul>
                    <li>
                      <Link href="/my-profile" >
                        <a title="My Profile" onClick={() => this.closeMenu()}>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-person-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 0 0 8 15a6.987 6.987 0 0 0 5.468-2.63z" />
                            <path fillRule="evenodd" d="M8 9a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                            <path fillRule="evenodd" d="M8 1a7 7 0 1 0 0 14A7 7 0 0 0 8 1zM0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8z" />
                          </svg>
                            My Profile
                        </a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/my-matches" >
                        <a title="My Matches" onClick={() => this.closeMenu()}>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-star" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                          </svg>
                            My Matches
                          </a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/add-cash" >
                        <a title="Add Cash" onClick={() => this.closeMenu()}>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-wallet" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M0 3a2 2 0 0 1 2-2h13.5a.5.5 0 0 1 0 1H15v2a1 1 0 0 1 1 1v8.5a1.5 1.5 0 0 1-1.5 1.5h-12A2.5 2.5 0 0 1 0 12.5V3zm1 1.732V12.5A1.5 1.5 0 0 0 2.5 14h12a.5.5 0 0 0 .5-.5V5H2a1.99 1.99 0 0 1-1-.268zM1 3a1 1 0 0 0 1 1h12V2H2a1 1 0 0 0-1 1z" />
                          </svg>
                            Add Cash
                          </a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/winners" >
                        <a title=" Winners" onClick={() => this.closeMenu()}>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-bar-chart" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M4 11H2v3h2v-3zm5-4H7v7h2V7zm5-5h-2v12h2V2zm-2-1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1h-2zM6 7a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v7a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1V7zm-5 4a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1v-3z" />
                          </svg>
                            Winners
                          </a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/contact-us" >
                        <a title=" Contact us" onClick={() => this.closeMenu()}>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-telephone" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
                          </svg>
                            Contact us
                          </a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/faq" >
                        <a title="FAQs" onClick={() => this.closeMenu()}>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-question-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                            <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z" />
                          </svg>
                          FAQs
                        </a>
                      </Link>
                    </li>

                    <li>
                      <Link href="/terms-and-conditions" >
                        <a title="Terms and Conditions" onClick={() => this.closeMenu()}>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-journal-text" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z" />
                            <path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z" />
                            <path fillRule="evenodd" d="M5 10.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5zm0-2a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0-2a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0-2a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                          </svg>
                          Terms and Conditions
                        </a>
                      </Link>
                    </li>

                    <li>
                      <Link href="/privacy-policy" >
                        <a title="Privacy Policy" onClick={() => this.closeMenu()}>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-shield-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M5.443 1.991a60.17 60.17 0 0 0-2.725.802.454.454 0 0 0-.315.366C1.87 7.056 3.1 9.9 4.567 11.773c.736.94 1.533 1.636 2.197 2.093.333.228.626.394.857.5.116.053.21.089.282.11A.73.73 0 0 0 8 14.5c.007-.001.038-.005.097-.023.072-.022.166-.058.282-.111.23-.106.525-.272.857-.5a10.197 10.197 0 0 0 2.197-2.093C12.9 9.9 14.13 7.056 13.597 3.159a.454.454 0 0 0-.315-.366c-.626-.2-1.682-.526-2.725-.802C9.491 1.71 8.51 1.5 8 1.5c-.51 0-1.49.21-2.557.491zm-.256-.966C6.23.749 7.337.5 8 .5c.662 0 1.77.249 2.813.525a61.09 61.09 0 0 1 2.772.815c.528.168.926.623 1.003 1.184.573 4.197-.756 7.307-2.367 9.365a11.191 11.191 0 0 1-2.418 2.3 6.942 6.942 0 0 1-1.007.586c-.27.124-.558.225-.796.225s-.526-.101-.796-.225a6.908 6.908 0 0 1-1.007-.586 11.192 11.192 0 0 1-2.417-2.3C2.167 10.331.839 7.221 1.412 3.024A1.454 1.454 0 0 1 2.415 1.84a61.11 61.11 0 0 1 2.772-.815z" />
                            <path fillRule="evenodd" d="M10.854 6.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 8.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
                          </svg>
                          Privacy Policy
                        </a>
                      </Link>
                    </li>
                  </ul>
                  <Button type="button" title="Logout" variant="contained" color="primary" onClick={this.confimationDilogOpen} className="ml-5 mr-2 register-now logout-sidebar ">
                    <img src="static/images/log-out.svg" alt="" className="mr-1" />  Logout
                  </Button>
                </Col>
              </Row>
            </div>
          </Menu>
        </div>
        {/* Content page  */}
        <div className="Content">
          <Container fluid className="h-100" >
            <Row className={`h-100 ${this.props.customClass}`}>
              <Col lg={5} col={12} sm={12} md={12} className="left-panel h-100" >
                <div className="left-panel-scroll" >
                  {this.props.children}
                </div>
              </Col>
              <Col lg={5} col={12} sm={12} md={12} ></Col>
              <Col lg={7} col={12} sm={12} md={7} className="p-0 right-panel">
                <h1 className="">
                  <span>Fastest Growing</span> <br />
                Fantasy Sports Platform
             </h1>
              </Col>
            </Row>
          </Container>
        </div>

        {/* Logout Modal */}
        <Dialog
          className="modal-slide-modal"
          id="myCofirmation"
          open={logoutOpen}
          keepMounted
          onClose={this.confimationDilogClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <div >
            <DialogContent >
              <div>
                <div className="create-packs-tabs">
                  <React.Fragment>
                    <div className="create-my-pack header-row-my-pack  m-0">
                      <Row className="m-0  justify-content-center align-items-center">
                        <Col xs={12} className="text-center slide-modal-body ">
                          <h5> Are you sure you want to logout</h5>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-question-circle" fill="#844ff7" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                            <path d="M5.255 5.786a.237.237 0 0 0 .241.247h.825c.138 0 .248-.113.266-.25.09-.656.54-1.134 1.342-1.134.686 0 1.314.343 1.314 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.003.217a.25.25 0 0 0 .25.246h.811a.25.25 0 0 0 .25-.25v-.105c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.267 0-2.655.59-2.75 2.286zm1.557 5.763c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z" />
                          </svg>
                          {/* <p>Your Card will not be saved!</p> */}
                        </Col>
                        <DialogActions >
                          <Col xs={12} className="slide-modal mobile-bottom-btn p-0">
                            <div className="common-btn">
                              <Button variant="secondary" autoFocus onClick={this.logOut} color="primary">
                                Yes
                                </Button>
                              <Button variant="success" onClick={this.confimationDilogClose} color="primary">
                                No
                            </Button>
                            </div>
                          </Col>
                        </DialogActions>
                      </Row>
                    </div>
                  </React.Fragment>
                </div>
              </div>
            </DialogContent>
          </div>
        </Dialog>


        {/* <Dialog
          id="myCofirmation"
          open={logoutOpen}
          keepMounted
          onClose={this.confimationDilogClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <div >
            <DialogContent >
              <div className="modal-contents">
                Are you sure you want to logout?
                            </div>
            </DialogContent>
          </div>

          <DialogActions className="myCofirmationBox">
            <Button className="close-yes" autoFocus onClick={this.logOut} color="primary">
              Yes
              </Button>
            <Button className="close-no" onClick={this.confimationDilogClose} color="primary">
              No
              </Button>
          </DialogActions>
        </Dialog> */}



        {/* My wallet popup */}
        <Dialog
          open={openWalletopen}
          className="common-modals headermodal"
          TransitionComponent={Transition}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >

          <DialogContent>
            <div id="alert-dialog-slide-description">
              <Row>
                <Col lg={12} className="p-0">
                  <div className="outer-card mt-0">
                    <div className="view-all-winner-div mt-3">
                      <Col lg={12} className="p-1 ">
                        <Card>
                          <Card.Body>
                            <Row className="justify-content-center align-items-center add-cash-page">
                              <Col xs={12} className="mobile-bottom-btn add-cash text-center">
                                <span>
                                  Total Balance
                              </span>
                                <br />
                                <b>
                                  &#x20B9;490
                              </b>
                                <div className="common-btn w-100">
                                  <Button className="" onClick={this.closeModal} variant="success" title="Add Cash" >Add Cash</Button>
                                </div>
                              </Col>
                              <Accordion className="mb-3" defaultExpanded>
                                <AccordionSummary
                                  aria-label="Expand"
                                  expandIcon={<ExpandMoreIcon />}
                                >
                                </AccordionSummary>
                                <div >
                                  <Col xs={12} className="wallet-listing mt-4 p-0">
                                    <ul>
                                      <li className="">
                                        <h5> Amount Added</h5>
                                        &#x20B9; 100
                                        <span className="left-info-icon">
                                          <OverlayTrigger
                                            placement="left"
                                            delay={{ show: 100, hide: 400 }}
                                            overlay={<Tooltip id="button-tooltip">Added Amount</Tooltip>}
                                          >
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fillRule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                            </svg>
                                          </OverlayTrigger>
                                        </span>
                                      </li>
                                      <li>
                                        <h5> Winnings</h5>
                                      &#x20B9; 250
                                        <span className="left-info-icon">
                                          <OverlayTrigger
                                            placement="left"
                                            delay={{ show: 100, hide: 400 }}
                                            overlay={<Tooltip id="button-tooltip">Winning Amount</Tooltip>}
                                          >
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fillRule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                            </svg>
                                          </OverlayTrigger>
                                        </span>
                                      </li>
                                      <li>
                                        <h5>  Cash Bonus</h5>
                                         &#x20B9; 0
                                       <span className="left-info-icon">
                                          <OverlayTrigger
                                            placement="left"
                                            delay={{ show: 100, hide: 400 }}
                                            overlay={<Tooltip id="button-tooltip">Bonus Amount</Tooltip>}
                                          >
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-info-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                              <path fillRule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                            </svg>
                                          </OverlayTrigger>
                                        </span>
                                      </li>
                                    </ul>
                                  </Col>
                                </div>
                              </Accordion>
                            </Row>
                          </Card.Body>
                        </Card>
                      </Col>
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
          </DialogContent>
        </Dialog>
        {/* End wallet popup */}


        {/* End footer section here */}

        {AuthenticationService.isLogged && (Router.pathname == '/' || Router.pathname == '/winners' || Router.pathname == '/my-matches' || Router.pathname == '/wallet' || Router.pathname == '/my-profile') ?
          (
            <Col lg={5} col={12} sm={12} md={5}  >
              <div className="pwa-bottom">
                <BottomNavigation>
                  {/* <Link href="/winners"> */}
                  <BottomNavigationAction onClick={() => this.navigateTo('/winners')} className={`${Router.pathname == '/winners' ? 'bottom-menu-active' : ''}`} label="Winners" icon={<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-award" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" d="M9.669.864L8 0 6.331.864l-1.858.282-.842 1.68-1.337 1.32L2.6 6l-.306 1.854 1.337 1.32.842 1.68 1.858.282L8 12l1.669-.864 1.858-.282.842-1.68 1.337-1.32L13.4 6l.306-1.854-1.337-1.32-.842-1.68L9.669.864zm1.196 1.193l-1.51-.229L8 1.126l-1.355.702-1.51.229-.684 1.365-1.086 1.072L3.614 6l-.25 1.506 1.087 1.072.684 1.365 1.51.229L8 10.874l1.356-.702 1.509-.229.684-1.365 1.086-1.072L12.387 6l.248-1.506-1.086-1.072-.684-1.365z" />
                    <path d="M4 11.794V16l4-1 4 1v-4.206l-2.018.306L8 13.126 6.018 12.1 4 11.794z" />
                  </svg>} />
                  {/* </Link> */}
                  {/* <Link href="/my-matches"> */}
                  <BottomNavigationAction onClick={() => this.navigateTo('/my-matches')} className={`${Router.pathname == '/my-matches' ? 'bottom-menu-active' : ''}`} label="My Matches" icon={<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trophy" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" d="M2.5.5A.5.5 0 0 1 3 0h10a.5.5 0 0 1 .5.5c0 .538-.012 1.05-.034 1.536a3 3 0 1 1-1.133 5.89c-.79 1.865-1.878 2.777-2.833 3.011v2.173l1.425.356c.194.048.377.135.537.255L13.3 15.1a.5.5 0 0 1-.3.9H3a.5.5 0 0 1-.3-.9l1.838-1.379c.16-.12.343-.207.537-.255L6.5 13.11v-2.173c-.955-.234-2.043-1.146-2.833-3.012a3 3 0 1 1-1.132-5.89A33.076 33.076 0 0 1 2.5.5zm.099 2.54a2 2 0 0 0 .72 3.935c-.333-1.05-.588-2.346-.72-3.935zm10.083 3.935a2 2 0 0 0 .72-3.935c-.133 1.59-.388 2.885-.72 3.935zM3.504 1c.007.517.026 1.006.056 1.469.13 2.028.457 3.546.87 4.667C5.294 9.48 6.484 10 7 10a.5.5 0 0 1 .5.5v2.61a1 1 0 0 1-.757.97l-1.426.356a.5.5 0 0 0-.179.085L4.5 15h7l-.638-.479a.501.501 0 0 0-.18-.085l-1.425-.356a1 1 0 0 1-.757-.97V10.5A.5.5 0 0 1 9 10c.516 0 1.706-.52 2.57-2.864.413-1.12.74-2.64.87-4.667.03-.463.049-.952.056-1.469H3.504z" />
                  </svg>} />
                  {/* </Link> */}
                  {/* <Link href="/"> */}
                  <BottomNavigationAction onClick={() => this.navigateTo('/')} className={`${Router.pathname == '/' ? 'bottom-menu-active' : ''}`} label="" icon={<img src="static/images/centerIcon.png" className="center-menu" />} />
                  {/* </Link> */}
                  {/* <Link href="/wallet"> */}
                  <BottomNavigationAction onClick={() => this.navigateTo('/wallet')} className={`${Router.pathname == '/wallet' ? 'bottom-menu-active' : ''}`} label="Wallet" icon={<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-wallet" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" d="M0 3a2 2 0 0 1 2-2h13.5a.5.5 0 0 1 0 1H15v2a1 1 0 0 1 1 1v8.5a1.5 1.5 0 0 1-1.5 1.5h-12A2.5 2.5 0 0 1 0 12.5V3zm1 1.732V12.5A1.5 1.5 0 0 0 2.5 14h12a.5.5 0 0 0 .5-.5V5H2a1.99 1.99 0 0 1-1-.268zM1 3a1 1 0 0 0 1 1h12V2H2a1 1 0 0 0-1 1z" />
                  </svg>} />
                  {/* </Link> */}
                  <BottomNavigationAction className={`${this.state.menuOpen ? 'bottom-menu-active' : ''}`} label="Other" icon={<svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-list" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" d="M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                  </svg>} onClick={() => this.toggleMenu()} />
                </BottomNavigation>
              </div>
            </Col>
          ) : !AuthenticationService.isLogged && onHome ? (
            ''
            // <Login onLogged={this.changeLogged} params={this.props.params} />
          ) : ''
        }
        {popupOpen ? <Popup hidePopup={this.handleClose} onLogged={this.changeLogged} showPoup={this.props.params} /> : ''}
      </React.Fragment >
    );
  }
}

export default Layout;
