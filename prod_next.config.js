// next.config.js 
const withCSS = require('@zeit/next-css')

module.exports = withCSS({

  module: {
    loaders: [
      {
        test: /\.(sass|less|css)$/,
        loaders: ['style-loader', 'css-loader', 'less-loader']
      }
    ],
    cssModules: true,
    cssLoaderOptions: {
      importLoaders: 1,
      localIdentName: "[local]___[hash:base64:5]",
    }

  },
  devIndicators: {
    autoPrerender: false,
  },
  env: {
    GET_API_URL: 'https://services.sports.cards/api/RTFGet/',
    POST_API_URL: 'https://services.sports.cards/api/RTFPost/',
    IMG_URL: 'https://services.sports.cards/'
  },
})