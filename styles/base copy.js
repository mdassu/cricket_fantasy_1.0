import css from 'styled-jsx/css';

// Use styled-jsx instead of global CSS because global CSS
// does not work for AMP pages: https://github.com/vercel/next.js/issues/10549
export default css.global`

.userprofle-form .email-status-main input{
  padding-right: 40px !important;
}
body {
  background-color:rgba(0,0,0,0.2);
  padding-right: 0px !important;
  height: 100%;
  background-size: cover !important;
  overflow-y: hidden;
}
.left-panel {
  background: #e9eef5 !important;
  width: 100%;
  max-height: 100%;
  overflow-y: auto;
  padding-bottom: 60px;
  position: fixed;
  overflow-y: scroll;
  overflow-x: hidden;
  z-index: 99;
}

.right-panel {
  padding-top: 30px;
  padding-left: 120px;
}

html {
  height: 100%;
}

.card-header {
  font-weight: 700;
}
.match-odd-status {
}
.modal-title {
}

.navigation-bar li a {
}

.logout-btn button span {
  font-weight: 700 !important;
}

.footer-link ul li a {
}
.copyright {
}
.contet-page p {
}
#__next {
  min-height: 700px;
  height: 100%;
}

.float-buttons button {
}
.slikslider-bar {
 position: relative;
 padding-top: 15px;
}
.slikslider-banner {
  position: absolute;
  top: 0px;
  left: -15px;
  height: 130px;
  min-width: 109%;
}


.right-panel {
  position: relative;
}

.right-panel h1{
  font-size: 55px;
  color: #46fd93;
  position: absolute;
  top: 50%;
  left: 30px;
  margin-top: -132px;
}
.right-panel h1 span{
  color: #ffffff;
}
input[type="number"] {
  -moz-appearance: textfield;
}

input[type="number"]::-webkit-inner-spin-button,
input[type="number"]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  appearance: none;
}
.ipl-matches {
}
@media (min-width: 300px) and (max-width: 767px) {
  .slick-next,
  .slick-prev {
    display: none !important;
  }
}
.slick-slider:focus {
  outline: none !important;
  border: 0px !important;
}

:focus {
  outline: none;
  border: 0px;
}
a {
  font-size: 15px;
  color: #a6a6a6;
}
button:focus {
  outline: none !important;
}
.Content {
  height: 100%;
}
.slick-dots li {
  margin: 0px auto !important;
  width: 12px !important;
}
.slick-dots li button {
  margin: 0px auto !important;
}


.left-panel::-webkit-scrollbar {
  width: 0px;
}
/* .left-panel:hover::-webkit-scrollbar {
  width: 3px !important;
} */
.left-panel::-webkit-scrollbar-track {
  background:  linear-gradient(0deg, rgba(255, 0, 0, 1) 0%, rgba(7, 0, 211, 1) 100%);
  visibility: hidden;
}

.left-panel::-webkit-scrollbar-thumb {
  background:  linear-gradient(0deg, rgba(255, 0, 0, 1) 0%, rgba(7, 0, 211, 1) 100%);
  visibility: hidden;
}
/*.left-panel:hover::-webkit-scrollbar-track{
  visibility: visible !important;
}
.left-panel:hover::-webkit-scrollbar-thumb{
  visibility: visible !important;
}
*/
.header-top {
  background: #773cf6;
}
.MuiInputLabel-outlined.MuiInputLabel-shrink {
  color: #1a73e8 !important;
}
.MuiFormLabel-filled {
  color: #1a73e8 !important;
}
.register-now {
  border-radius: 5px;
  background-image:linear-gradient( 0deg, rgb(83 30 196) 0%, rgb(157 60 246) 100% );
}

.float-buttons button .MuiButton-label {
  display: flex !important;
  align-items: center;
  justify-content: center;
}
.float-buttons button img {
  height: 19px;
}
.float-buttons button {
  text-align: center;
  border: 0px;
  text-transform: capitalize;
  font-size: 14px;
  width: 170px;
  height: 40px;
  line-height: 40px;
}
.float-buttons button:focus {
  border: 0px;
  outline: none;
}
.breadcrumb {
  border-radius: 0px;
  height: 120px;
}
.float-buttons button i {
  font-size: 20px;
  margin-right: 10px;
}

/* footer */
.footer {
  background: #773cf6;
  padding: 30px 0px;
  /* position: relative; */
  /* bottom: 0px; */
  width: 100%;
}

.copyright {
  font-size: 14px;
  color: #a6a6a6;
  padding-top: 10px;
}
.copyright span {
  color: #fff;
}

.user-avtar {
  width: 30px;
  cursor: pointer;
  height: 30px;
  /* float: right; */
  display: inline-block;
  margin-left:7px;
}
.user-avtar svg{
  font-size: 22px;
}
.user-avtar-img {
  width: 100%;
  border-radius: 100%;
  height: 100%;
  border: 4px solid #8a58f7;
  border-radius: 50%;
  background-color: rgb(244, 244, 244);
  box-shadow: 0px 2px 7px 0px rgba(153, 153, 154, 0.34);
}

.navigation-bar {
  padding-right: 10%;
}
.navigation-bar li a {
  display: inline-block;
  padding: 0px 15px;
  color: #6d7a89;
  font-size: 16px;
  transition: 0.3s ease-in-out;
}
.navigation-bar li {
  margin: 0px 20px;
}
.navigation-bar .active a {
  color: #fff !important;
  padding-left: 3px !important;
}
.navigation-bar a {
  color: #fff !important;
}
.navigation-bar a:hover {
  color: #fff !important;
}

.navigation-bar .active .betmenu {
  visibility: visible;
}

.register-here a {
  color: #3d8fff;
  font-size: 14px !important;
  font-weight: 600 !important;
}
.user-otp div{
  display: inline-block !important;
}

/* Preview Image */

.image-container {
  height: 100%;
  width: 100%;
}
.image-container img {
  height: 100%;
  width: 100%;
  border-radius: 100%;
}

.upload-btn-wrapper {
  position: relative;
  display: inline-block;
  cursor: pointer;
}

.btn-browse {
  border-radius: 20px;
  border: 0px solid #fff;
  border-radius: 50%;
  background: #fff;
  background-size: cover;
  width: 90px;
  height: 90px;
  position: relative;
  padding: 0px;
  cursor: pointer;
}

.upload-btn-wrapper input[type="file"] {
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
  height: 100%;
  width: 100%;
}

.btn-browse .svg-icon {
  position: absolute;
  right: -2px;
  bottom: -1px;
  border-style: solid;
  border-width: 3px;
  border-color: rgb(255, 255, 255);
  border-radius: 50%;
  background-color: rgb(255, 255, 255);
  display: block;
  height: 29px;
  width: 29px;
  text-align: center;
  line-height: 19px;
  font-size: 14px;
  color: #4480c9;
}
.myprofile {
  padding: 15px 0px;
}
.user-profilename {
  font-weight: 600;
  color: #262f3e;
  font-size: 18px;
  display: block;
  width: 100%;
  padding: 10px 0px;
}
.form-container {
  margin-top: -40px;
}
.updateprofile-btn {
  background-image: linear-gradient( 0deg,rgb(83 30 196) 0%,rgb(157 60 246) 100% ) !important;
  width: 100% !important;
  height: 43px;
  margin: 5px auto 10px !important;
  border-radius: 5px;
}
.card-detail-box .updateprofile-btn span {
  color: #ffffff;
  text-transform: capitalize;
  font-weight: 500;
}

.profile-breadcrumb {
  display: block !important;
}
.email-status-main {
  position: relative;
}
.email-status {
  position: absolute;
  top: 50%;
  right: 25px;
  margin-top: -32px;
  font-size: 20px;
}

.email-status .fa-check-circle {
  color: green;
}
.email-status .fa-times-circle {
  color: red;
}
.userprofle-form .MuiTextField-root {
  margin-bottom: 30px !important;
  width: 100% !important;
}

.MuiSnackbar-anchorOriginBottomCenter {
  left: 10% !important;
  bottom: 75px !important;
 transform: unset !important;
}
.logout-sidebar {
  color: #fff !important;
  font-weight: 700 !important;
  text-transform: uppercase;
  font-size: 13px !important;
}
.sidebar-menu ul li svg{
  margin-right: 10px;
  font-size: 20px;
  color: #6425d0;
}
.bm-menu-wrap {
  z-index: 9999999999  !important;
}
/* Media Query for home responsive */

@media (min-width: 300px) and (max-width: 767px) {
  .contet-page {
    padding: 30px 15px !important;
  }
  .mobile-bottom-btn button img {
    width: 22px;
  }

  .berfore-login {
    width: 100px;
    margin-top: 10px !important;
    text-transform: capitalize !important;
  }
  .berfore-login i {
    margin-right: 10px;
    vertical-align: middle;
  }

  .pwa-bottom {
    display: block;
  }

  .user-avtar {
    display: inline-block;
  }

  .pwa-bottom {
    position: fixed;
    bottom: 0px;
    left: 0px;
    width: 100%;
    background-color: rgb(255, 255, 255);
    box-shadow: 0px 3px 114px 0px rgba(67, 67, 67, 0.18);
  }

  body {
    padding-bottom: 80px;
    position: relative;
    overflow-x: hidden;
    -webkit-overflow-scrolling: hidden;
  }
}
@media (min-width: 768px) and (max-width: 1023px) {
  .contet-page {
    padding: 30px 15px !important;
  }

  .navigation-bar li {
    margin: 0px 10px;
  }
  .navbar-light .navbar-brand {
    width: 14%;
  }
  .navbar-light .navbar-brand img {
    width: 100%;
  }
  .register-now {
    margin: 0px !important;
  }
  .float-buttons button {
    padding: 7px 5px;
    border: 0px;
    text-transform: capitalize;
    font-size: 11px;
    font-family: "SF Pro Text";
    font-weight: 400;
    height: 40px;
    width: auto;
  }

  .float-buttons button i {
    font-size: 14px;
    margin-right: 3px;
  }
  .navigation-bar li a {
    display: inline-block;
    padding: 0px 10px;
    color: #6d7a89;
    font-size: 15px;
  }
}

.right-sidebar .card-title {
  font-size: 18px;
  font-weight: 700;
  padding: 30px 60px;
  color: rgb(38, 47, 62);
  line-height: 1.389;
}
.logout-btn button {
  background-image: none !important;
  background-color: transparent !important;
  border: 0px !important;
  padding: 0px !important;
  box-shadow: none !important;
  width: auto !important;
  height: auto !important;
  margin: 8px 0px 0px 5px !important;
}
.logout-btn button span {
  text-transform: capitalize !important;
  color: #262f3e !important;
  font-size: 15px !important;
}
.logout-btn button:focus {
  outline: 0px !important;
}
.logout-btn button:hover span {
  color: #e11b23 !important;
}

.logout-btn-mobile button {
  background-image: none !important;
  background-color: transparent !important;
  border: 0px !important;
  padding: 0px !important;
  box-shadow: none !important;
  width: auto !important;
  height: auto !important;
  margin: 8px 0px 0px 5px !important;
}
.logout-btn-mobile button span {
  text-transform: capitalize !important;
  color: #ffffff !important;
  font-weight: 600 !important;
  font-size: 15px !important;
}
.logout-btn-mobile button:focus {
  outline: 0px !important;
}
.logout-btn-mobile button:hover span {
  color: #e11b23 !important;
}

/* Position and sizing of burger button */
.bm-burger-button {
  position: fixed;
  width: 36px;
  height: 30px;
  left: 36px;
  top: 36px;
  display: none;
}

/* Color/shape of burger icon bars */
.bm-burger-bars {
  background: #373a47;
}

/* Color/shape of burger icon bars on hover*/
.bm-burger-bars-hover {
  background: #a90000;
}

/* Position and sizing of clickable cross button */
.bm-cross-button {
  height: 24px;
  width: 24px;
}

/* Color/shape of close button cross */
.bm-cross {
  background: #bdc3c7;
}

/*
Sidebar wrapper styles
Note: Beware of modifying this element as it can break the animations - you should not need to touch it in most cases
*/

/* General sidebar styles */
.bm-menu {
  background: #ffffff;
}

/* Morph shape necessary with bubble or elastic */
.bm-morph-shape {
  fill: #373a47;
}

/* Individual item */
.bm-item {
  display: inline-block;
}

/* Styling of overlay */
.bm-overlay {
  background: rgba(0, 0, 0, 0.3);
}

.bm-menu-wrap {
  height: 100%;
  width: 390px !important;
}

.bm-cross-button {
  display: none;
}
.profile-menu {
  border-radius: 100%;
  border-style: solid;
  border-width: 2px;
  border-color: rgb(255, 255, 255);
  border-radius: 50%;
  background-color: rgb(244, 244, 244);
  box-shadow: 0px 6px 6px 0px rgba(153, 153, 154, 0.2);
  height: 71px;
  width: 71px;
}
.profile-menu img {
  width: 100%;
  height: 100%;
  border-radius: 100%;
}
.sidebar-profile {
  border-bottom: 2px solid rgb(231, 236, 241);
  background-color: #e9eef5;
  padding: 20px 30px;
}
.userside-profile {
  padding-top: 11px;
}
.userside-profile h4 {
  font-size: 20px;
  color: #262f3e;
  vertical-align: middle;
  margin: 0px;
  padding: 0px;
  text-transform: capitalize;
}
.userside-profile span {
  font-size: 15px;
  color: #7a92ae;
}
.sidebar-menu {
  padding: 15px 30px;
}
.sidebar-menu ul {
  margin: 0px;
  padding: 0px;
}
.sidebar-menu ul li {
  display: block;
  border-bottom: 1px solid rgb(231, 236, 241);
  padding: 0px 15px;
}
.sidebar-menu ul li a {
  line-height: 60px;
  font-size: 16px;
  color: #262f3e;
  transition: 0.4s;
  font-weight:600;
}
.sidebar-menu ul li a i {
  margin-right: 10px;
  font-size: 20px !important;
  color: #606671;
}
.sidebar-menu ul li:hover a {
  transition: 0.4s;
  text-decoration: none;
  color: #0052b8;
}
.sidebar-menu ul li:hover i {
  color: #0052b8;
}
.logout-sidebar {
  width: 328px;
  position: fixed !important;
  bottom: 20px;
  left: -20px;
  height: 45px;
}

@media (min-width: 1200px) and (max-width: 1300px) {
  .sidebar-menu ul li a {
    line-height: 53px;
    font-size: 14px;
  }
  .sidebar-menu ul li a i {
    font-size: 14px !important;
  }
}

@media (min-width: 1100px) and (max-width: 1200px) {
  .sidebar-menu ul li a {
    line-height: 43px;
    font-size: 14px;
  }
  .sidebar-menu ul li a i {
    font-size: 14px !important;
  }
}
@media (min-width: 1000px) and (max-width: 1100px) {
  .sidebar-menu ul li a {
    line-height: 50px;
    font-size: 14px;
  }
  .sidebar-profile {
    border-bottom: 2px solid rgb(231, 236, 241);
    background-color: rgba(231, 236, 241, 0.2);
    padding: 8px 0px;
  }
  .sidebar-menu {
    padding: 0px 0px;
  }
  .sidebar-menu ul {
    overflow: auto;
    max-height: 307px;
  }
  .sidebar-menu ul::-webkit-scrollbar {
    width: 1px;
  }
}
@media (min-width: 300px) and (max-width: 767px) {

  .myprofile {
    padding: 0px;
  }
  .form-container {
    margin: 20px auto;
  }
  .btn-browse {
    width: 75px;
    height: 75px;
  }
  .bm-menu-wrap {
    height: 100%;
    width: 295px !important;
    z-index: 99999 !important;
  }

  .sidebar-menu {
    padding: 10px 5px;
  }
  .sidebar-profile {
    padding: 10px 15px;
  }
  .userside-profile {
    padding-left: 0px;
    padding-top: 15px;
  }
  .logout-sidebar {
    width: 240px;
  }
}

.navbar-brand img {
  margin-top: -6px;
  width: 200px;
}
.betmenu {
  width: 18px;
}
.logout-sidebar img {
  width: 20px;
}

@media (min-width: 300px) and (max-width: 767px) {
  .logout-sidebar {
    right: auto;
    left: 0px !important;
  }

  .navbar-brand img {
    margin-top: 9px;
  }
  .login-form .register-now {
    margin-top: 40px !important;
  }
  .register-form .register-now {
    margin-top: 40px !important;
  }
  .mobile-bottom-btn {
    padding-bottom: 100px;
  }
  .pwa-bottom i {
    font-size: 20px;
  }
  .pwa-bottom .fa-university {
    font-size: 22px;
  }
  .pwa-bottom .fa-bars {
    margin-top: 10px;
    font-size: 23px;
  }
}

.MuiBottomNavigationAction-label.MuiBottomNavigationAction-iconOnly {
  opacity: 1 !important;

}

.bottom-menu-active {
  color: #773cf6 !important;
}
.flagcountry {
  width: 22px !important;
  height: 20px !important;
}

.MuiFormLabel-root.Mui-focused {
  color: #1a73e8 !important;
}

/*  20Sports.Cards css */

/* Home Page css */
.heading {
  font-size: 18px;
  color: #ffffff;
  padding-bottom: 10px;
}
.other-heading {
  color: #262f3e;
}
.body-content {
  height: 100%;
}

.ipl-matches {
  font-size: 15px;
  color: #262f3e;
  font-weight: 600;
}
.card {
  border: 0px;
  border-radius: 5px;
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 3px 15px 0px rgba(67, 67, 67, 0.1);
  margin-bottom: 20px;
}

.card-header {
  border: 0px;
  border-top: 1px solid #d5dbe4;
  background: #fbfcfd;
  border-radius: 0px 0px 5px 5px;
  padding: 5px 10px;
  min-height: 36px;

  vertical-align: middle;
}

.card-body {
  padding: 10px;
  position: relative;
}
.outer-card {
  width: 100%;
  margin: 0px auto;
  /* overflow: hidden; */
}
/* .slick-list {
  overflow: visible;
} */
.view-all {
  font-size: 13px;
  color: #ffffff;
  vertical-align: middle;
}
.view-all svg {
  margin-left: 5px;
  vertical-align: middle;
  margin-top: -1px;
}
.view-all:hover {
  color: rgb(70, 253, 147);
  text-decoration: none;
}
.match-start-time {
  position: absolute;
  font-size: 13px;
  color: #262f3e;
  min-width: 102px;
  padding: 0 8px;
  text-align: center;
  left: 50%;
  top: 0px;
  margin-left: -51px;
  line-height: 25px;
  border-radius: 0px 0px 3px 3px;
  background-color: rgb(70, 253, 147);
  font-weight: 600;
}
.cardfooter ul {
  margin: 0px;
  padding: 0px;
  font-size: 13px;
}
.cardfooter ul li {
  display: inline-block;
}
.winner-trophy {
  max-width: 20px;
  width: 100px;
}
.cardfooter-inner li {
  margin-right: 10px;
  color: #797e87;
}
.cardfooter-inner li b {
  font-size: 14px;
  color: #262f3e;
  font-weight: 700;
}
.winner-right li {
  color: #ff9a14;
}
.cardfooter ul li img {
  margin-top: -3px;
}
.team-listing {
  position: relative;
  height: 70px;
  cursor: pointer;
}
.team-logo {
  width: 80px;
  position: relative;
  height: 65px;
  margin-top: 8px;
}

.team-right {
  float: right;
}

.team-left {
  float: left;
}

.logo-line {
  width: 30px;
  height: 20px;
  display: block;
  position: absolute;
  margin-top: -10px;

  top: 50%;
}

.line-left {
  left: 5px;
  background: #ffd49c;
}
.line-right {
  right: 5px;
  background: #9daadd;
}
.team-name {
  border-radius: 50%;
  position: absolute;
  width: 58px;
  height: 58px;
  top: 50%;
  margin-top: -29px;
  color: #fff;
  font-size: 18px;
  text-transform: uppercase;
  line-height: 55px;
  text-align: center;
  font-weight: 700;
}

.team-left-name {
  left: 30px;
  background-image: -moz-linear-gradient(
    0deg,
    rgb(253, 179, 87) 0%,
    rgb(255, 171, 60) 100%
  );
  background-image: -webkit-linear-gradient(
    0deg,
    rgb(253, 179, 87) 0%,
    rgb(255, 171, 60) 100%
  );
  background-image: -ms-linear-gradient(
    0deg,
    rgb(253, 179, 87) 0%,
    rgb(255, 171, 60) 100%
  );
  box-shadow: 0px 8px 15px 0px rgba(255, 173, 66, 0.44);
}
.team-right-name {
  right: 30px;
  background-image: -moz-linear-gradient(
    0deg,
    rgb(72, 99, 195) 0%,
    rgb(61, 84, 188) 100%
  );
  background-image: -webkit-linear-gradient(
    0deg,
    rgb(72, 99, 195) 0%,
    rgb(61, 84, 188) 100%
  );
  background-image: -ms-linear-gradient(
    0deg,
    rgb(72, 99, 195) 0%,
    rgb(61, 84, 188) 100%
  );
  box-shadow: 0px 8px 15px 0px rgba(63, 87, 189, 0.44);
}

.team-vs {
  margin: 24px auto;
  max-width: 77px;
  width: 100%;
}
.slick-slide img {
  display: inline-block;
}

.max-prize-money {
  color: #00cd00;
  font-size: 12px;
  background-color: rgba(0, 205, 0, 0.078);
  border: 1px solid rgb(0, 205, 0);
  display: inline-block;
  padding: 2px 7px;
  border-radius: 3px;
  margin-right: 5px;
  vertical-align: middle;
}
.max-prize-money:hover {
  text-decoration: none;

}
.slick-dots {
  bottom: -10px !important;
}
.slick-dots li button:before {
  font-size: 8px !important;
}
.slick-dots li.slick-active button:before {
  color: #773cf6 !important;
}
.slick-dots li button:before {
  color: #a8acb0 !important;
}
.winner_price a {
  color: #fff;
  font-size: 16px;
}
.winner_price {
  display: block;
}

.pwa-bottom {
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 3px 114px 0px rgba(67, 67, 67, 0.18);
  position: fixed;
  left: 0px;
  bottom: 0px;
  flex: 0 0 41.666667%;
  max-width: 41.666667%;
  width: 100%;
  z-index: 99;
}

/* Header */

.appbar {
  background: #773cf6;
  padding: 5px 15px;
}
.download-bar {
  background: #262f3e;
  color: #fff;
}
.download-bar p {
  margin: 0px;
  padding: 0px;
  line-height: 1.2;
  font-size: 15px;
  padding-top: 7px;
}

.download-app a {
  border-radius: 5px;
  background-color: rgb(70, 253, 147);
  color: #262f3e;
  font-size: 15px;
  display: inline-block;
  width: 132px;
  text-align: center;
  line-height: 30px;
  height: 32px;
  margin: 10px 0px;
}
.download-app a:hover {
  text-decoration: none;
  color: #fff;
}
.navbar-brand {
  font-size: 23px;
  color: #ffffff;
  font-weight: 600;
}
.navbar-brand:hover {
  color: #ffffff !important;
}

.notification {
  position: relative;
  display: inline-block;
  margin:0px 7px;

}
.notification img {
  width: 20px;
}
.notification::after {
  position: absolute;

  right: 1px;
  top: 4px;

  /* padding: 5px; */
  height: 8px;
  width: 8px;
  content: "";
  background: #f72509;
  border: 2px solid #fff;
  border-radius: 100%;
}

.progressbar {
  margin: 8px 0px 0px 0px ;
}
.progress {
  height: 7px;
  background: #e9eef5;
}
.progress-bar {
  background-image: -moz-linear-gradient(
    0deg,
    rgb(255, 193, 16) 0%,
    rgb(246, 133, 12) 100%
  );
  background-image: -webkit-linear-gradient(
    0deg,
    rgb(255, 193, 16) 0%,
    rgb(246, 133, 12) 100%
  );
  background-image: -ms-linear-gradient(
    0deg,
    rgb(255, 193, 16) 0%,
    rgb(246, 133, 12) 100%
  );
  border-radius: 6px;
}
.progressbar ul {
  margin: 0px;
  padding: 0px;
}
.progressbar ul li {
  display: inline-block;
}
.left-point {
  color: #ff9a14;
  font-weight: 600;
  font-size: 14px;
}
.totalpacks {
  font-weight: 600;
  font-size: 14px;
  color: #262f3e;
}

.contests-tabs {
  margin: 0px -15px !important;
}
.contests-tabs .MuiTabs-indicator {
  background: #773cf6 !important;
}
.MuiTab-root {
  text-transform: capitalize !important;
}
.contests-tabs  .MuiAppBar-colorPrimary {
  background: #fff !important;
  color: #797e87 !important;
  padding: 0px 15px;
  font-size: 16px !important;

}
.contests-tabs .Mui-selected .MuiTab-wrapper {
  color: #262f3e !important;
}
.contests-tabs .MuiTabPanel-root {
padding: 15px !important;
}

.contents-listing {
  padding: 0px 5px;
}
.prizepool {
  font-size: 14px;
  color: #797e87;
  display: block;
  font-weight: 600;
}
.current-prize {
  /*font-size: 15px;*/
  font-size: 18px;
  color: #262f3e;
  font-weight: 600;
  /*padding: 4px 0px 3px 0px;*/
  padding: 11px 0px;
  margin: 0px;
 line-height: 20px;
}
.max-prize {
  font-style: italic;
  color: #858a93;
  font-size: 14px;
}
.prize-pool-btn {
  padding: 0px 7px;
  border-radius: 5px;
  background-color: rgb(70, 253, 148);
  display: inline-block;
  font-size: 14px;
  color: #262f3e;
  height: 30px;
  line-height: 30px;
  text-transform: uppercase;
  min-width: 72px;
  text-align: center;
  font-weight: 700;
  margin-top: 12px;
}
.prize-pool-btn:hover{
  text-decoration: none;

}


.contests-tabs .cardfooter ul li img{
  max-width: 14px;
}
.bm-menu-wrap{
  top: 0px !important;
}
.download-bar {
  display: none;
}
.edit-team {
  color: #fff;
  font-size: 16px;
}
.edit-team:hover{
  color: #fff;
}
.overlay-div {
  background: rgba(0,0,0,0.3);
  padding: 10px;
  border-radius: 5px 5px 0px 0px;
}
.mypacks {
  padding: 0px 0px;
  border-radius: 5px 5px 0px 0px;
}
.mypacks h5{
  font-size: 16px;
  color: #ffffff;
  margin: 0px;
  padding: 0px;
  font-weight: 600;
}

.mypacks-footer h6 {
  color: #797e87;
  margin: 0px;
  padding: 0px;
  font-size: 14px;
  line-height: 25px;
}
.mypacks-footer h6 b {
  font-size: 14px;
  color: #262f3e;
  font-weight: 700;
}


.my-team-name {
  border-radius: 50%;
  width: 58px;
  height: 58px;
  color: #fff;
  font-size: 18px;
  text-transform: uppercase;
  line-height: 53px;
  text-align: center;
  border:4px solid #b3d5a2;
  font-weight: 700;
  display: inline-block;
  box-shadow: 0px 12px 21px 0px rgba(16, 0, 82, 0.44);
}

.mi-team {
  background: #feb04c;
}
.iplteam {
  background: #455fc1;
}
.fantasy-pack {
  padding:20px 0px 20px 0px;
}
.fantasy-pack .team-vs {
  margin: 19px auto;
}
/* .create-pack{
  position: fixed;
  bottom: 170px;
  width: 120px;
} */
.create-pack a{

  border-radius: 5px;
  background-color: rgb(69, 250, 146);
  /* box-shadow: 0px 15px 54px 0px rgba(38, 47, 62, 0.47); */
  font-size: 14px;
  text-align: center;
  line-height: 30px;
  display: block;
  font-weight: 500;
  width: 120px;
  color: #262f3e;
  margin: 0px auto;
}
.create-pack a:hover{
  text-decoration: none;
  color:#262f3e ;
}

.no-data img{
  max-width:70%;
  margin-bottom: 60px;

}
.no-data h6{
  font-size: 17px;
  color: #262f3e;
}
.no-data {
  padding-top: 30px;
}
.center-menu {
  max-width: 54px;
  margin-top: -32px;
  position: relative;
  z-index: 9999999999999; 
}
.MuiBottomNavigationAction-root {
  padding: 6px 0px 8px !important;
}
.create-packs-tabs .MuiTab-root {
  max-width: 127px !important;
  min-width: 90px !important;
}
.create-packs-tabs .MuiPaper-root {
    background-color: #46fd94 !important;
}
.MuiBox-root {
  padding: 24px 15px !important;
}
/*
.create-packs-tabs  .Mui-selected .MuiTab-wrapper {
  color: #293241 !important;
}
 */
 .create-packs-tabs {
  margin: 0px -15px !important;
 }
 .contests-tabs .MuiTab-textColorPrimary.Mui-selected{
     color: #262f3e !important;
 }
 .create-packs-tabs .MuiTab-textColorPrimary.Mui-selected {
  color: #773cf6 !important;
}
.contests-tabs .MuiTab-wrapper {
  color: rgba(41, 50, 65, 1) !important;
}
.create-my-pack {
  margin: -24px -15px;
}
.select-row {
  background: #fff;
}
.select-row  p{
  padding: 0px;
  margin: 0px;
  line-height: 40px;
  font-size: 15px;
  color: #797e87;
  font-weight: 600;
}
.header-row {
  padding: 10px 0px;
  min-height: 60px;
}
.header-row span{
  color: #262f3e;
  font-size: 15px;
  font-weight: 600;
  display: inline-block;
  height: 1.2;

}

.potential-point {
  font-style: italic;
}

.match-win{
  font-size: 18px !important;
}

.match-win svg{

  cursor: pointer;
}
.match-win small{
  margin-left: 10px;
  position: relative;
  display: inline-block;
  vertical-align: text-top;
}
.hover-tooltip {
  color: red;
  background: #ffff;
  box-shadow: 0px 0px 5px #ccc;
  font-size: 12px;
  display:none;
  position: absolute;
  right: -30px;
  top: 5px;
  width: 100%;
}
.match-win small:hover .hover-tooltip {
 display: block !important;
}
.team-label{
  font-size: 14px;
  font-weight: 600;
  color: #262f3e;
  margin: 0px;
  padding: 0px;
}
.match-win-row {
  background: #fff;
  padding: 5px 7px 5px 5px;
  position: relative;
  /* height: 45px; */
  border-bottom: 1px solid #eaedf1;
}

.match-win-row p{
  padding: 0px;
  margin: 0px;
  color: #797e87;
  font-size: 14px;
   /* line-height: 0.714; */
}
.multiplier-value {
  width: 100%;
  line-height: 40px;
  background: #844ff7;
  text-align: center;
  height: 40px;
  font-size: 16px;
  color: #ffffff;
}
.potential-value {
  width: 100%;
  line-height: 40px;
  height: 40px;
  background: #ceb9fc;
  text-align: center;
  font-size: 16px;
  color: #ffffff;
}
.credit-value {
  height: 40px;
  width: 100%;
  background: #a983f9;
  text-align: center;
  font-size: 16px;
  color: #ffffff;
  padding: 3px 7px;
}

.credit-value .MuiFilledInput-input {
  padding: 2px 0px 0px 0px;
    font-weight: 400;
    height: 30px !important;
    line-height: 30px !important;
    text-align: center;
    font-size: 16px;
    color: #797e87;
    border-style: solid;
    border-width: 1px;
    border-color: rgb(121, 126, 135);
    border-radius: 5px;
    background-color: rgb(255, 255, 255);
}
.credit-value .MuiInputBase-root {
  line-height: 30px !important;
}
.create-packs-tabs .MuiBox-root {
overflow: hidden !important;
}
.pl2 {
  padding-left: 11px;
}
.match-pack {
  background: #773cf6;
}
.match-pack .team-vs {
  max-width: 100%;
}
.match-pack .my-team-name {
  height: 50px;
  width: 50px;
  line-height: 42px;
  font-size: 16px !important;
  border: 3px solid #fff;
}
.match-pack .team-vs {
  margin: 6px auto;
}

.starttime {
font-size: 15px;
color: #fff;
font-weight: 600;
padding-bottom: 30px;
}
.starttime span{
  color: #46fd94;
}


.scoreboard-top {
  padding: 0px;
  margin: 0px;
  font-size: 15px;
  color: #ffffff;
  font-weight: 600;
}
.scoreboard-top b{
  font-size: 24px;
  color: #46fd94;
  font-weight: 500;
}
.ml-25 {
  margin-left: 25px;
}
.mlm-25{
  margin-left: -25px;
  z-index: 999;
}
.mrm--25 {
  margin-right: -25px;
  z-index: 999;
}
.mr-25 {
  margin-right: 25px;
}
.totalscore {
  font-size: 16px;
  color: #ffffff;
  font-weight: 600;
  padding-top: 10px;
}
.total-top-fill-board ul{
  margin: 0px auto;
  padding: 0px;
  border-radius: 10px;
  text-align: center;
}
.total-top-fill-board ul li{
  display: inline-block;
  min-width: 40px;
  text-align: center;
  line-height: 15px;
  height: 15px;
  font-size: 12px;
  color: #2c3543;
  background: #fff;
  margin: 0px 1px;
  font-weight: 600;
}


.total-top-fill-board ul li:first-child{
  border-radius: 10px 0px 0px 10px;
}
.total-top-fill-board ul li:last-child{
  border-radius: 0px 10px 10px 0px;
}
.total-top-fill-board  {
  background: #6b36dd;
  padding: 10px 0px;
  margin: 0px 0px;
}

.active-board {
  background: #46fd94 !important;
}
.requird-borad {
  background: #cdcfd2 !important;
}

.common-btn {
  margin: 30px auto;
  text-align: center;
}
.common-btn .btn-success {
    background: #46fd94;
    color: #262f3e;
    box-shadow: 0px 15px 54px 0px rgba(38, 47, 62, 0.47);
}
.common-btn .btn-secondary {
    background: #8798b0;
    color: #ffffff;
}
.common-btn button{
  min-width: 170px;
  height: 40px;
  font-size: 16px;
  border: 0px;
  margin: 0px 10px;
  font-weight: 600;
}

.completed-match {
  text-transform: uppercase;
  font-size: 12px;
  text-align: center;
  color: #00cd00;
  background-color: rgba(0, 205, 0, 0.078);
  border: 0px;
  max-width: 110px;
  padding: 5px 0px;
  border-radius: 4px;
  font-weight: 600;
  margin: 26px auto;
}
/* .mycontent-tab .MuiTab-root {
  max-width: 134px !important;
  margin: 0px auto;
} */
.prizepool-detail {
  color: #797e87;
  font-size: 14px;
  font-weight: 400;
}
.mycontents-footer {
  background: #ecfef4;
  width: 100%;
  font-size: 13px;
}
.mycontents-detail .cardfooter ul li img{
  vertical-align: sub !important;
}
.middel-prize {
  color: #797e87;
}
.total-user-rank {
  color: #773cf6;
}
.fantasy-total {
  color: #262f3e;
}
.winner-prize {
  color: #10a500;
  font-size: 13px;
  font-weight: 400;
}
.match-detail-header {
  background: #773cf6;
  padding: 20px 0px;
}
.header-team-score {
  font-size: 15px;
  color: #fff;
  text-transform: uppercase;
  font-weight: 600;
  position: relative;
}
.header-team-score  b{
  font-weight: 500;
  font-size: 14px;
  display: block;
  width: 100%;
}
.match-result-text p{
  padding: 0px;
  font-size: 15px;
  color: #fff;
  font-weight: 400;
  margin: 0px;
}
.match-result-text {
  position: relative;
}
.completed-match-label{
  text-transform: uppercase;
  font-size: 12px;
  text-align: center;
  color: #262f3e;
  background-color:#46fd94;
  border: 0px;
  max-width: 110px;
  padding: 5px 0px;
  width: 100%;
  border-radius: 4px;
  font-weight: 600;
  position: absolute;
  top: -30px;
  left: 50%;
  margin-left: -55px;
}

.pts-circle {
  position: absolute;
  top: -50px;
  right: 0px;
  height: 35px;
  width: 35px;
  border: 3px solid #fff;
  line-height: 30px;
  text-transform: uppercase;
  font-weight: 600;
  border-radius: 100%;
  z-index: 9999;
}
.serial-circle {
  background: #e9eef5;
  height: 20px;
  width: 20px;
  line-height: 19px;
  margin-right: 5px;
  font-size: 11px;
  font-weight: 600;
  color: #262f3e;
  text-align: center;
  display: inline-block;
  border-radius: 100%;
}

.fansty-row .team-label {
  width: 46%;
  vertical-align: middle;
}
.MuiAppBar-positionStatic {
  position: sticky !important;
  top: 0px;
}

.live-start-match {
  position: absolute;
  font-size: 13px;
  color: #dd332a;
  text-align: center;
  left: 50%;
  top: 10px;
  font-weight: 600;
  margin-left: -25px;
  width: 50px;
}
.live-start-match img {
 max-width: 10px;
 vertical-align: initial;
}
.header-selection {
  background: #46fd94;
  padding: 10px 15px;
 font-size: 15px;
 font-weight: 400;
}
.header-selection b{
  color:#844ff7;
  font-size: 19px;
}
.header-row-my-pack .fansty-row .team-label {
  width: 36%;
  font-size: 13px;
}
.wallet-header img{
  width: 23px;
}
.wallet-header {
  display: inline-block;
  margin: 0px 7px;
}
.team-app-header .my-team-name {
  width: 38px;
  height: 38px;
  line-height: 31px;
  font-size: 13px;
  box-shadow: none;
}
.team-app-header .team-vs {
  margin: 8px auto !important;
}
.logoname svg {
  font-size: 30px;
}
.content-number {
  color: #22cc62;
  font-size: 15px;
  font-weight: 600;
}
.ipl-match-number {
  position: relative;
}
.ipl-match-number span {
  font-size: 13px;
  color: #797e87;
  text-align: center;
  position: absolute;
  left: 0px;
  width: 100%;
  text-align: center;
  font-weight: 600;
}
.winners-card {
  margin: 15px 0px 0px 0px !important;
  padding: 15px 15px 0px 15px;
  border-top: 1px solid #d5dbe4;
}
.winners-card .card-img-top {
  width: 60px;
  height: 60px;
  margin: 10px auto;
  border-radius: 100%;
  display: block;
}
.winners-card .card-footer {
  background: #f4f6fa;
  color: #262f3e;
  font-size: 12px;
  font-weight: 600;
  border-color: #d5dbe4;
  padding: 5px 0px;
}
.winners-card .card {
  box-shadow: none !important;
  border: 1px solid #d5dbe4 !important;
  min-height:158px;
}
.winners-card .card-title {
  font-size: 15px;
  color: #262f3e;
  margin: 0px;
  padding: 0px;
  font-weight: 500;
}
.winners-card .card-text {
  color: #797e87;
  font-size: 13px;
  margin: 0px;
  padding: 0px;
  font-weight: 600;
}

/* the slides */

.winners-card .card-body {
    padding: 10px 10px 0px 10px  !important;
}
.mobile-bottom-btn .common-btn  {
  position: fixed;
  /*bottom: 56px; */
  bottom: 25px;
  flex: 0 0 41.666667%;
  max-width: 41.666667%;
  width: 100%;
  left: 0px;
  margin: 0px auto;
  z-index: 999;
}

.mobile-bottom-btn .common-btn button {
  margin: 0px auto;
  min-width: 114px;
  height: 36px;
  font-size: 15px;
  border: 0px;
  margin: 0px 5px;
  line-height: 30px;
  padding: 0px;
}
.mobile-bottom-btn .common-btn .btn-secondary {
  /*background: #fff;
  color: #262f3e;*/
  background: #6c757d;
  color: #fff;
  border:1px solid #6c757d !important;
  box-shadow: 0px 3px 15px 0px rgba(67,67,67,0.1);
}
.mobile-bottom-btn .common-btn button:focus{
  outline: none;
  border: 0px;
}

.view-all-winners svg {
  display: block;
  width: 100%;
  font-size: 25px;
  margin-top: 40px;
}
.view-all-winners {
  background: #f4f6fa !important;
  cursor: pointer;
}
.contests-tabs table th{
  color: #797e87;
  font-size: 14px;
  font-weight: 600;
  background: #fafcff;
  border-bottom: 0px;
  border-color: #d8dde3;
  border-left: 0px;
  border-right: 0px;
}
.contests-tabs table tr td{
  background: #fff;
  color: #262f3e;
  font-size: 13px;
  font-weight: 600;
  border-bottom: 0px;
  border-left: 0px;
  border-right: 0px;
  vertical-align: middle;
}
.userimg {
  height: 30px;
  width: 30px;
  border-radius: 100%;
  margin-right: 10px;
  vertical-align: middle;
}
.leaderboard-table  table tr td{
  padding: 7px .75rem !important;

}

.lds-roller {
    display: inline-block;
    width: 78px;
    height: 78px;
    /*position: absolute;
    left: 50%;
    top: 40%;
    margin-left: -39px;
    margin-top: -39px;*/
    margin-left: 45%;
    margin-top: 35%;
  }
  .lds-roller div {
  animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
  transform-origin: 40px 40px;
  }
  .lds-roller div:after {
  content: " ";
  display: block;
  position: absolute;
  width: 7px;
  height: 7px;
  border-radius: 50%;
  background: #773cf6;
  margin: -4px 0 0 -4px;
  }
  .lds-roller div:nth-child(1) {
  animation-delay: -0.036s;
  }
  .lds-roller div:nth-child(1):after {
  top: 63px;
  left: 63px;
  }
  .lds-roller div:nth-child(2) {
  animation-delay: -0.072s;
  }
  .lds-roller div:nth-child(2):after {
  top: 68px;
  left: 56px;
  }
  .lds-roller div:nth-child(3) {
  animation-delay: -0.108s;
  }
  .lds-roller div:nth-child(3):after {
  top: 71px;
  left: 48px;
  }
  .lds-roller div:nth-child(4) {
  animation-delay: -0.144s;
  }
  .lds-roller div:nth-child(4):after {
  top: 72px;
  left: 40px;
  }
  .lds-roller div:nth-child(5) {
  animation-delay: -0.18s;
  }
  .lds-roller div:nth-child(5):after {
  top: 71px;
  left: 32px;
  }
  .lds-roller div:nth-child(6) {
  animation-delay: -0.216s;
  }
  .lds-roller div:nth-child(6):after {
  top: 68px;
  left: 24px;
  }
  .lds-roller div:nth-child(7) {
  animation-delay: -0.252s;
  }
  .lds-roller div:nth-child(7):after {
  top: 63px;
  left: 17px;
  }
  .lds-roller div:nth-child(8) {
  animation-delay: -0.288s;
  }
  .lds-roller div:nth-child(8):after {
  top: 56px;
  left: 12px;
  }
  @keyframes lds-roller {
  0% {
  transform: rotate(0deg);
  }
  100% {
  transform: rotate(360deg);
  }
  }
.all-winner-content {
  margin: 0px -15px;
}
.all-winner-content .winners-card {
    padding: 10px 0px;
    background: #f9fbfc;
    text-align: center;
}
.all-winner-content .winners-card a{
    color: #262f3e;
    font-size: 14px;
    font-weight: 600;
}

.view-all-footer{
    color: #797e87;
    font-size: 12px;
    font-weight: 600;
    padding: 3px 10px !important;
    background: #f9fafc !important;
}
.view-all-footer span{
  color: #262f3e;
  font-size: 18px;
  font-weight: 700;
  background: #f9fafc;
}
.view-all-winner-div img {
    max-width: 60px;
    margin: 0px ;
}
.pwa-bottom .bi-list{
  font-size: 21px;
}
.view-all-winner-div .card-title{
  margin: 0px;
  padding: 0px;
  font-size: 18px;
  font-weight: 600;
  color: #262f3e;

}
.view-all-winner-div p span{
  color:#262f3e;
  font-size: 13px;
  font-weight: 600;
  display: block;
  width: 100%;
  padding-top: 5px;
}
.view-all-winner-div p small{
  color:#797e87;
  font-size: 12px;
  font-weight: 600;
  display: block;
  width: 100%;
  line-height: 15px;
  padding-top: 5px;
}
.view-all-winner-div p {
  padding: 0px ;
  margin: 0px;
}
.add-cash button{
  width: 50% !important;
}
.add-cash .common-btn {
  bottom: 74px !important;
}
.add-cash-col {
  padding: 0px 20px !important;
}

.add-cash-col input{
  background: #f9fafc;
  border-color: #d5dbe4 !important;
}
.add-cash-col input::placeholder{
  color: #646b78;
  font-weight: 600;
  font-size: 14px;
}
.add-money-label {
  margin: 0px;
  padding: 0px;
  text-align: center;
  padding-top: 10px;
}

.add-money-label button{
  padding: 5px 15px;
  border-color: #d5dbe4 !important;
  width: 100%;
}
.add-money-label {
  margin-top: 10px;
}
.all-winner-content h5{
  font-size: 15px;
  padding: 10px 0px;
  margin: 0px;
  padding: 0px;
}

.addcards a {
  color: #262f3e;
  font-size: 18px !important;
  font-weight: 600;
}
.addcards .align-items-center {
  min-height: 60px;
}
.addcards span{
  color: #797e87;
  font-size: 14px !important;
  font-weight: 600;
  display: block;
  width: 100%;
}
.addcards a:hover{
  color: #262f3e;
  text-decoration: none;
}
.addcards img{
  width: 30px;
}
.link-account a{
  color: #3d8fff;
  font-size: 14px !important;
  font-weight:600 !important;
}



.user-pages .MuiFormControl-root {
    width: 100% !important;
}
.modal-dailog {
  background-color: rgb(46, 47, 47, 0.7);
}

.modal-div {
  position: absolute;
  max-width: 421px;
  top: 50%;
  left: 50%;
  margin-left: -210.5px;
  margin-top: -200px;
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 5px 45px 0px rgba(19, 29, 42, 0.1);
  border-radius: 15px;
  padding: 33px 48px 40px;
}
.modal-title {
  color: #262f3e;
  font-size: 22px;
  font-weight: 700;
  margin-bottom: 5px;
}
/* Chrome, Safari, Edge, Opera */
.login-form input[type="number"]::-webkit-outer-spin-button,
.login-form input[type="number"]::-webkit-inner-spin-button {
  -webkit-appearance: none;
}

/* Firefox */
.login-form input[type="number"] {
  -moz-appearance: textfield;
}
.modal-description {
  font-size: 15px;
  font-weight: 500;
  color: #7a92ae;
  margin-bottom: 38px;
}
.register-form .MuiOutlinedInput-root,
.login-form .MuiOutlinedInput-root {
  /* border-radius: 5px 0px 0px 5px; */
  height: 42px;
  /* margin-bottom: 42px; */
  background-color: #f1f3f680;
}
/* .register-form .MuiOutlinedInput-notchedOutline, */
.login-form .MuiOutlinedInput-notchedOutline {
  border: #e7ecf1 solid 1px !important;
}
fieldset {
  border-color: #e7ecf1 !important;
}


.login-form .country-select .MuiOutlinedInput-root {
  border-radius: 5px 0px 0px 5px;
  float: left;
  font-size: 14px;
  color: #262f3e;
  font-weight: 600;
  width: 100% !important;
}
.login-form .number-input .MuiOutlinedInput-root {
  border-radius: 5px;
  background-color: #ffffff;
  width: 100% !important;
}
.country-select {
  border-radius: 5px 0px 0px 5px !important;
}
.number-input .MuiOutlinedInput-root {
  border-radius: 0px 5px 5px 0px !important;
}
.login-form .country-select .MuiOutlinedInput-input {
  padding: 11.5px 10px !important;
  width: 100% !important;
  font-size: 14px;
  color: #646b78;
  font-weight: 600;
}
.login-form .country-select .MuiOutlinedInput-input img {
  vertical-align: text-top;
}
.login-form .country-select .MuiSelect-icon {
  top: calc(50% - 10px);
}
.login-form .country-select .MuiSelect-iconOutlined {
  right: 2px;
  font-size: 20px;
  color: #646b78;
}
.login-form .country-select .MuiButtonBase-root {
  font-size: 14px;
  color: #646b78;
  font-weight: 600;
}

.login-form .MuiInputLabel-formControl {
  top: -6px;
}
.p-9 {
  padding: 9.5px 0px !important;
}
.register-here {
  text-align: center;
  padding-top: 40px;
  font-size: 13px;
}
.resend-otp {
  padding-top: 20px;
}
.register-here small {
  font-weight: 500;
  font-size: 13px;
  color: #7a92ae;
  font-weight: 500;
  text-align: center;
}

.register-here strong {
  color: #262f3e;
  font-weight: 700;
  cursor: pointer;
}

.register-here strong:hover {
  color: #3f51b5 !important;
}

.close-btn {
  min-width: initial !important;
  width: 40px !important;
  height: 40px !important;
  border-radius: 50% !important;
  position: absolute !important;
  background-color: #ffffff !important;
  box-shadow: 0px 5px 10px 0px #131d2a69;
  top: -20px;
  right: -20px;
  font-size: 25px !important;
  color: #5d51ea !important;
  z-index: 9999 !important;
}

/* Register Popup Css */
.login-form .MuiOutlinedInput-input,
.register-form .MuiOutlinedInput-input {
  padding: 12px 10px !important;
  font-size: 14px;
  font-weight: 500;
}
.register-form .number-input .MuiOutlinedInput-input {
  width: 225px;
}
.register-form .MuiSelect-select {
  width: 60px !important;
}
.register-form .MuiOutlinedInput-input img {
  margin-right: 0px !important;
  vertical-align: text-top;
}
.register-form .MuiSelect-iconOutlined {
  right: 0px;
  color: #646b78;
}
.login-form .MuiInputLabel-formControl,
.register-form .MuiInputLabel-formControl {
  top: -6px !important;
  font-size: 14px;
  color: #v;
  font-weight: 500;
}
.register-form .MuiOutlinedInput-root {
  /* margin-bottom: 15px !important; */
  background-color: #f1f3f680;
}
.MuiFormHelperText-contained {
  margin: 0px !important;
}
.mt-26 {
  margin-top: 30px !important;
}
/* Otp Popup Css */
.otp-form > div {
  margin-bottom: 16px;
}
.otp-form input {
  display: inline-block !important;
  border: #e7ecf1 solid 1px;
  background-color: #f1f3f680;
  border-radius: 5px;
  width: 42px !important;
  height: 42px;
  margin: 0px 5px;
  color: #646b78;
}
.otp-form input:focus {
  outline: 1px !important;
  border: 1px solid #262f3e !important;
}
.continue-btn {
  margin-top: 40px !important;
  filter: drop-shadow(0px 5px 22.5px rgba(19, 29, 42, 0.1)) !important;
}
.register-now:focus {
  outline: 0px !important;
}
.register-form .MuiSelect-select {
  font-weight: 600;
}


.dilogs-modal .MuiDialog-scrollPaper {
  justify-content: end !important;
}
.dilogs-modal .MuiDialogContent-root {
  padding: 0px 0px !important;
}
.dilogs-modal .MuiDialog-paper {
  margin: 0px 0px !important;
}
.dilogs-modal .MuiDialog-paper::-webkit-scrollbar {
  width: 0px !important;
}

.dilogs-modal .MuiPaper-rounded {
  border-radius: 0px !important;
}
.dilogs-modal {
  position: relative;
}
.otp-form .text-danger {
  display: block;
  width: 100%;
  text-align: center;
}
.close-no {
  position: absolute;
  z-index: 999999;
  right: 5px;
  top: 5px;
  background: transparent !important;
  border: 0px !important;
  /* color: #fff !important; */
  font-size: 38px !important;
  height: auto;
  width: auto;
  padding: 0px !important;
  /* background: red; */
  /* width: 43px; */
  height: auto !important;
  line-height: 0px;

}
.modaltile {
  position: absolute;
  left: 15px;
  top: -28px;
  color: #fff;
  font-weight: 600;
  font-size: 22px;
}
.dilogs-modal .create-packs-tabs {
  margin: 0px 0px !important;
}
.dilogs-modal .MuiDialog-paperScrollPaper {
  max-height: calc(100% - 20px) !important;
  margin-top: 50px !important;
}

.match-win-row::last-child:after {
  border-bottom: 0px !important;
}

.blank-input {
  background: #fff;
  height: 100%;
  margin: 0px 4px;
  line-height: 32px;
  color: #262f3e;
}
.dilogs-modal .credit-value {
  padding: 3px 0px;
}
.MuiDialog-paperWidthSm {
  width: 100% !important;
  min-width: 41.666667%;
  flex: 0 0 41.666667%;
  max-width: 41.666667%;
}
.my-tractions .MuiAccordion-root {
  background: #f6f8fb !important;
  box-shadow: 0px 3px 35px 0px rgba(67, 67, 67, 0.1) !important;
}
.my-tractions .MuiAccordionSummary-root {
  background: #fff !important;
  border: 0px !important;
  box-shadow: 0px 3px 35px 0px rgba(67, 67, 67, 0.1);
  border-style: solid;
  border-width: 2px;
  border-color: rgb(213, 219, 228);
  border-radius: 10px;
}
.my-tractions .MuiAccordion-root:before {
  background: transparent !important;
}
.my-tractions tr td {
  border: 0px !important;
  padding: 5px 10px;
  color: #262f3e;
  font-size: 13px;
}
.my-tractions tr td b {
  color: #262f3e;
  font-weight: 500;
}
.my-tractions .MuiAccordion-rounded {
  border-radius: 5px !important;
}
.my-tractions .MuiAccordionSummary-root {
  border-radius: 5px !important;
  padding: 0px 10px !important;
}
.my-tractions p {
  color: #797e87;
  width: 90%;
  font-size: 16px;
  text-align: center !important;
}
.my-tractions p span {
  color: #262f3e;
  text-align: left !important;
  font-size: 14px;
  font-weight: 600;
  text-align: left !important;
  min-width: 100px;
  float: left;
  text-align: left !important;
}
.my-tractions .MuiAccordionSummary-root.Mui-expanded {
  min-height: 50px !important;
}
.tractions-title {
  margin: 0px;
  padding: 0px;
  color: #797e87;
  padding-bottom: 10px;
  font-weight: 600;
  font-size: 16px;
}
.my-tractions .MuiAccordionSummary-content.Mui-expanded {
  margin: 0px !important;
}
.my-tractions .MuiAccordion-root.Mui-expanded {
  margin: 0px !important;
}
.my-tractions .MuiCollapse-entered {
  border-top: 1px solid #d5dbe4 !important;
}
.my-tractions .MuiAccordionDetails-root {
  padding: 8px 0px !important;
}
.add-cash-page  .mobile-bottom-btn .common-btn{
      position: unset !important;
}
.add-cash-page span{
    font-size: 16px;
    color: #797e87;
    font-weight: 600;
}
.add-cash-page b {
    color: #262f3e;
    font-size: 17px;
    display: block;
    padding: 10px 0px;
}
.wallet-listing ul {
  margin: 0px;
  padding: 0px;
}
.wallet-listing ul li{
  display: block;
  border-top: 1px solid #d5dbe4;
  position: relative;
  padding: 10px 10px;
  color: #262f3e;
  font-size: 14px;
  font-weight: 600;
}
.wallet-listing ul li:last-child{
  padding-bottom: 0px !important;
}
.wallet-listing h5{
  margin: 0px;
  padding: 0px;
  color: #797e87;
  font-size: 14px;
  padding-bottom: 3px;
}
.left-info-icon {
  position: absolute;
  right: 15px;
  top: 50%;
  margin-top: -14px;
  width: auto !important;
  display: inline-block !important;
}
.wallet-listing .card{
  position: relative;
  margin-bottom: 10px !important;
}
.wallet-listing .card a{
  display: block;
  width: 100%;
  position: relative;
}

.wallet-listing .align-items-center{
  min-height: 30px !important;
}
.wallet-listing .card a svg{
  font-size: 17px !important;
}
.wallet-listing .card a  {
  font-size: 14px !important;
}
.add-cash button {
    max-width: 130px;
}
.invite-friends {
  margin-top: 213px;
}

.invite-friend-top {
  min-height: 300px;
  background-size:cover;
  width: 100%;
  position: absolute;
  top: 0px;
}
.invite-friends span{
  font-size: 16px;
  color: #262f3e;
}
.invite-friends p{
  color: #797e87;
  font-size: 14px;
}
.invite-friends b{
  color: #646b78;
  font-size: 14px;
  font-weight: 600;
  display: inline-block;

  vertical-align: middle;
}
.invite-friends svg{
  margin-right: 5px;
}


.copycode-clipboard {
  width: 80%;
  text-align: left;
  border: 2px dashed #959ea9;
  border-radius: 3px;
  margin: 20px auto;
  line-height: 43px;
  font-weight: 500;
  color: #262f3e;
  font-size: 14px;
  padding-left: 15px;
  position: relative;
  background: #fff;

  height: 46px;
  display: block;
}
.copycode {
  position: absolute !important;
  right: -3px !important;
  top: -3px;
  border-radius: 3px;
 background: #773cf6 !important;
  box-shadow: 0px 3px 35px 0px rgba(67, 67, 67, 0.15);
  width: 112px;
  height: 48px;
}
.copycode span {
  font-size: 14px;
  letter-spacing: 0px;
  color: #ffffff;
  font-weight: 600;
  text-transform: capitalize;
}
.invite-header-card .card-header{
  background: #f6f8fb !important;
}

.invite-body .phonecontact{
  width: 80%;
  background: #45fa92 !important;
  color: #262f3e;
  font-size: 15px;
  font-weight: 600;
  line-height: 30px !important;
  border-color: #45fa92 !important;
}
.invite-body button {
  width: 80%;
  display: block !important;
  color: #262f3e !important;
  font-size: 15px;
  font-weight: 600;
  line-height: 30px !important;
  border:2px solid #a4a8ad;
  background: transparent !important;
  margin: 0px auto;
  margin-bottom: 15px !important;
}
.invite-body button:hover{
  color: #262f3e;
}
.invite-body .card-title{
  color: #262f3e;
  font-size: 14px;
  font-weight: 600;
}
.join-pack {
  background-color: rgb(136, 152, 177) !important;
  color: #ffffff !important;
  border: 0px !important;
}
.mobile-bottom-btn .common-btn .join-pack {
  border: 0px !important;
}
.inputNumber {
  position: relative;
}
.country-select {
  position: absolute !important;
  width: 83px !important;
  background: #fff !important;
  z-index: 999;
}
.winner-page-class .left-panel{
  padding-bottom: 170px;
}
.navbar-brand {
  cursor: pointer;

}
.mymatches-class .left-panel{
  padding-bottom: 170px;
}
.walletpage-class .left-panel {
  padding-bottom: 170px;
}
.createpage-class .left-panel {
  padding-bottom: 170px;
}
.my-tractions .MuiCollapse-wrapper {
  margin-bottom: 15px;
}

.appbar {
  position: relative;
  z-index: 999999999;
}

@-moz-document url-prefix() {
  .left-panel::-webkit-scrollbar {
    width: 0px;
  }
  .winner-page-class .left-panel-scroll{
    padding-bottom: 120px;
  }
  .mymatches-class .left-panel-scroll{
  padding-bottom: 100px;
}
.createpage-class .left-panel-scroll {
  padding-bottom: 170px;
}
.walletpage-class .left-panel-scroll{
  padding-bottom: 120px;
}

  .winner-page-class .left-panel{
    padding-bottom: 170px;
  }
  .left-panel {
    overflow-x: hidden;
  }
 .left-panel-scroll {
  padding-bottom: 56px;
 }
 .home-page {
  padding-bottom: 80px;
 }
 .MuiDialog-paperWidthSm {
  width: 100% !important;
  min-width: 41.666667%;
  -webkit-flex: 0 0 41.666667%;
  -ms-flex: 0 0 41.666667%;
  flex: 0 0 41.666667%;
  max-width: 41.666667%!important;
  position:fixed !important;
  left:0px;
  }
  /* .winner-content {
    padding-bottom: 56px;
   } */
}

.slide-modal .common-btn  {
  position: unset !important;
  flex: 0 0 100% !important;
  max-width: 100% !important;

}

.slide-modal .common-btn   button{
min-width: 170px !important;
}
.slide-modal-body {
padding: 15px 15px 30px 15px !important;
}

.slide-modal-body svg{
  font-size: 30px;
  border-radius: 100%;
  margin: 10px auto;
  box-shadow: 0px 1px 11px -3px rgba(132, 79, 247, 0.66);
}
.slide-modal-body p{
font-size: 15px;
color: #797e87;
}

.modal-slide-modal {
position: fixed !important;
bottom: 0px !important;
}
.modal-slide-modal  .MuiDialog-paperScrollPaper {
position: fixed !important;
bottom: 0px !important;
left: 0px !important;
width: 100% !important;
min-width: 41.666667% !important;
flex: 0 0 41.666667% !important;
max-width: 41.666667% !important;
margin: 0px !important;
}

.common-modals {
position: relative;
}
.close-popup {
position: absolute;
right: -10px;
top: -10px;
height: 30px !important;
width: 30px !important;
border-radius: 100%;
border-style: solid;
border-width: 1px;
padding: 4px;
border-color: rgb(184, 188, 195);
border-radius: 50%;
background-color: rgb(255, 255, 255);
box-shadow: 0px 6px 46px 0px rgba(38, 47, 62, 0.27);
cursor: pointer;
}
.common-modals .MuiDialog-paper {
overflow-y: visible !important;
}

.common-modals h2{
font-size: 22px;
color: #262f3e;
font-weight: 600;
}
.common-modals p{
color: #797e87;
font-size: 14px;
font-weight: 500;
}
.common-modals p b{
color: #262f3e;
font-weight: 500;
}
.common-modals table tr td{
color: #262f3e;
font-size: 14px;
font-weight: 500;
border: 0px;
vertical-align: middle;
}
.tablefooter td{
background: #f7f7f8;
border-top: 1px solid #e5e6e8 !important;
}
.common-modals table tr td img{
width: 20px;
vertical-align: top;
margin-right: 10px;

}
.common-modals  .common-btn {
margin: 0px auto;
margin-bottom: 30px;
}

.common-modals .MuiDialog-paperWidthSm {
position: fixed;
left: 0px;
margin: 15px;
min-width: 39.666667% !important;
flex: 0 0 39.666667% !important;
max-width: 39.666667% !important;
}
.common-modals .card{
box-shadow: none  !important;
}
.headermodal .MuiBackdrop-root {
  top: 54px !important;
  margin: 0px !important;
}
.headermodal .MuiDialog-paperWidthSm {
top: 54px !important;
}

.headermodal .MuiPaper-rounded {
border-radius: 0px !important;
}
.headermodal  .MuiDialog-paperWidthSm{
min-width: 41.666667% !important;
flex: 0 0 41.666667% !important;
max-width: 41.666667% !important;
margin: 0px !important;
}
.headermodal  .common-btn .btn-success {
  box-shadow: none !important;
}

.headermodal  .MuiAccordion-root{
width: 100% !important;
box-shadow: none !important;
}
.headermodal .MuiAccordion-root:before {
display: none !important;
}
.headermodal  .MuiAccordionSummary-content {
display: none !important;
}

.headermodal  .MuiButtonBase-root {
position: absolute;
bottom: -25px;
left: 50%;
z-index: 999999;
margin-left: -8px;
padding: 0px !important;
}
.headermodal  .MuiAccordionSummary-root.Mui-expanded {
position: absolute;
bottom: -25px;
left: 50%;
z-index: 999999;
margin-left: -8px;
padding: 0px !important;
}
.join-heading {
margin: 0px;
padding: 0px;
color: #262f3e;
}

.radionbtn  .PrivateSwitchBase-root-1{
  padding: 0px !important;
}
.radionbtn .MuiFormControlLabel-root {
  margin: 0px !important;
  padding: 0px !important;
}
.selectall .MuiFormControlLabel-label  {
font-weight: 500 !important;
margin-left: 7px;
font-size: 14px !important;
color: #262f3e !important;
}
.radionbtn  .MuiRadio-colorSecondary.Mui-checked {
color: #773cf6 !important;
}

.radionbtn  .MuiRadio-root {
  color: #c5c9d0 !important;
}

.right-radio {
line-height: 178px;
}

.select-packs .card{
margin-bottom: 0px !important;
}
.credit-value .MuiFilledInput-underline:before {
  border-color: #ceb9fc !important;
  border-radius: 5px !important;
}
.top-app-header-row {
padding-top: 10px;
}

.top-app-header-row h4{
color: #0ab550;

}
.top-app-header-row svg{
font-size: 20px;
cursor: pointer;
}
.disabled-row {
pointer-events: none;
opacity: 0.7;
position: relative;
}
.disabled-row::after{
background: rgba(0,0,0,0.1);
height: 100%;
width: 100%;
z-index: 999999;
content: '';
position: absolute;
}
.bottom-row {
margin-bottom: 50px;
}
/* .stickybar{
position: sticky !important;
  top: -1px;
  z-index: 9;
}
*/
/* .dynamic-class .left-panel{
padding-bottom: 56px !important;
} */


.register-form .number-input .MuiOutlinedInput-input {
padding-left: 95px !important;
}

.login-form .number-input .MuiOutlinedInput-input {
  padding-left: 95px !important;
  }
.homepage-class .left-panel{
  padding-bottom: 163px;
}
.createpack-class  .left-panel{
  padding-bottom: 170px;
}
.contests-tabs .MuiTab-root{
  padding: 6px 0px !important;
}
.content-pages p{
  font-size: 13px;
  color: #797979;
  line-height: 22px;
  font-weight: 400;
}
.content-pages li{
  font-size: 13px;
  color: #797979;
  line-height: 22px;
  font-weight: 400;
}
.content-pages h1 {
  text-transform: capitalize;
  padding: 10px 0px;
  font-size: 18px !important;
  font-weight: 600 !important;
  color: #1a1a1a !important;
}
.content-pages h2{
  text-transform: capitalize;
  padding: 10px 0px;
  font-size: 18px !important;
  font-weight: 600 !important;
  color: #1a1a1a !important;
}
.content-pages h3{
  text-transform: capitalize;
  padding: 10px 0px;
  font-size: 18px !important;
  font-weight: 600 !important;
  color: #1a1a1a !important;
}
.content-pages h4{
  text-transform: capitalize;
  padding: 10px 0px 0px 0px;
  font-size: 16px !important;
  font-weight: 600 !important;
  color: #1a1a1a !important;
}
.content-pages h5{
  text-transform: capitalize;
  padding: 10px 0px;
  font-size: 18px !important;
  font-weight: 600 !important;
  color: #1a1a1a !important;
}
.content-pages h6{
  text-transform: capitalize;
  padding: 10px 0px;
  font-size: 18px !important;
  font-weight: 600 !important;
  color: #1a1a1a !important;
}
.content-pages  ul{
  padding: 0px 0px 0px 20px;
  margin: 0px;
}
.content-pages ol{
  padding: 0px 0px 0px 20px;
  margin: 10px 0px;
}
.content-pages b{
  font-size: 15px !important;
  font-weight: 600 !important;
  color: #1a1a1a !important;
}
.tooltip  {
  z-index: 999999;
}
.previewPack  {
  padding-top: 17px;
}
.my-earn-vs {
  padding: 0px;
}
.create-packmodal {
  z-index: 99999999999999 !important;
}
.create-packmodal  .MuiDialog-paperScrollPaper {
  margin-top: 90px !important;
  padding-bottom: 20px;
}
.fliud-img {
  margin:0px auto !important;
}
.verifybtn{
  color: #fff;
  box-shadow: 0px 15px 54px 0px rgba(38,47,62,0.47);
  min-width: 169px;
  height: 36px;
  font-size: 15px;
  border: 0px;
  margin: 0px 5px;
  line-height: 19px;
  font-weight: 600;
  padding: 0px;
  display: block;
    margin: 0px auto;

}
@-moz-document url-prefix() { 
  .create-packmodal  .MuiDialog-paperScrollPaper { 
    padding-bottom: 40px;
  }
  .create-packmodal .create-packs-tabs {
    padding-bottom: 20px !important;
  }
}

@media (min-width: 1800px) and (max-width: 14000px) {
  .create-packmodal .MuiDialog-paper {
    position: fixed !important;
    bottom: 0px !important;
  }
 }

 .leaderboard-user {
  width: 59px;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow-x: hidden;
  vertical-align: -5px;
 }


/* New Home page*/
 .new-home-page {
  padding: 10px 0px;
}
.new-home-page h1 {
  font-size: 20px;
  color: #262f3e;
  font-weight: 600;
  padding: 10px 0px;
  text-transform: uppercase;
  margin-bottom: 45px !important;
}
.homepage-card {
  margin-bottom: 75px !important;
  padding: 15px;
}
.homepage-last-card {
  margin-bottom: 35px !important;
}
.box-number {
  width: 50px;
  height: 50px;
  border-radius: 50%;
  filter: drop-shadow(0px 12px 10.5px rgba(53, 192, 112, 0.26));
  background-image: linear-gradient(0deg, #3be583 0%, #45fd93 100%);
  border: 3px solid #ffffff;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0px 10px;
}
.box-number span {
  color: #262f3e;
  font-weight: 700;
  font-size: 22px;
}
.homepage-card-top {
  align-items: center !important;
  margin-top: -11%;
  margin-bottom: 15px;
}
.homepage-card-right img {
  width: 100%;
  border-radius: 5px;
  filter: drop-shadow(0px 3px 17.5px rgba(67, 67, 67, 0.1));
}
.home-card-content h4 {
  font-size: 18px;
  color: #262f3e;
  font-weight: 600;
  text-transform: uppercase;
  margin: 0px !important;
}
.home-card-content p {
  font-size: 14px;
  line-height: 24px;
  margin: 0px !important;
  color: #797e87;
  font-weight: 500;
}
.home-card-content ul {
  margin: 5px 0px 0px !important;
}

.home-card-content ul li {
  font-size: 14px;
  line-height: 24px;
  margin: 0px !important;
  color: #797e87;
  font-weight: 500;
  margin: 0px 20px !important;
}
.new-home-page h2 {
  font-size: 18px;
  color: #262f3e;
  font-weight: 600;
  text-transform: uppercase;
  margin-bottom: 15px !important;
}

.homepage-bottom-banner ul {
  list-style: none !important;
  display: flex;
  margin: 0px !important;
  padding: 0px !important;
}
.homepage-bottom-banner ul li {
  width: 50% !important;
  display: block !important;
  padding: 30px;
  border-right: 1px solid rgba(233, 238, 245, 0.38823529411764707);
}
.homepage-bottom-banner ul li:last-child {
  border-right: 0px;
}
.homepage-bottom-banner ul li h5 {
  font-size: 30px;
  color: #fff;
  font-weight: 700;
  margin: 0px;
}
.homepage-bottom-banner ul li h5 img {
  vertical-align: baseline !important;
  width: 25px !important;
}
.homepage-bottom-banner ul li span {
  font-size: 16px;
  color: #fff;
  font-weight: 500;
}
@media (max-width: 480px) and (min-width: 300px) {
  .new-home-page h1 {
    font-size: 19px;
    margin-bottom: 25px !important;
  }
  .homepage-card {
    margin-bottom: 45px !important;
  }
  .homepage-last-card {
    margin-bottom: 30px !important;
  }
  .box-number {
    width: 45px;
    height: 45px;
    margin: 0px 5px;
  }
  .box-number span {
    font-size: 19px;
  }
  .home-card-content h4 {
    font-size: 16px;
  }
  .home-card-content p {
    font-size: 13px;
  }
  .home-card-content ul li {
    font-size: 13px;
  }
  .homepage-bottom-banner ul li {
    padding: 20px;
  }
  .homepage-bottom-banner ul li h5 {
    font-size: 18px;
  }
  .homepage-bottom-banner ul li h5 img {
    width: 18px !important;
  }
  .homepage-bottom-banner ul li span {
    font-size: 14px;
  }
  .new-home-page h2 {
    font-size: 16px;
  }
}
`;
