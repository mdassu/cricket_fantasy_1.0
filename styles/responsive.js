import css from 'styled-jsx/css';

// Use styled-jsx instead of global CSS because global CSS
// does not work for AMP pages: https://github.com/vercel/next.js/issues/10549
export default css.global`

@media (min-width: 300px) and (max-width: 993px) {
  .myregister-class .register-now {
    margin-top: 18px !important;
  }
  .app-download-fixed {
    flex: 0 0 100%;
    max-width: 100%;
  }
  .app-download-fixed .download-app a {
    width: 100%;
  }
  .more-download h5 {
    font-size: 12px;
  }
  .more-download {
    padding: 10px 10px !important;
  }
  .socillogin button {
    max-width: 120px;
  }
  .right-account-login{
    padding:0px 0px !important;
  }
  .pointpotential {
    display:block;
    width:100%;
  }
  .contact-circle {
   
    line-height: 43px !important;
  }
  .content-pages h4 {
    font-size: 14px !important;
  }
  .winner-completed-tr td{
    white-space: nowrap;
  }
  .content-pages p {
    font-size: 13px;
  }
  .navbar-brand img{
    margin-top: 0px; 
    width: 145px;
  }
  .p-xs-0 {
    padding:0px;
    
  }
  .email-status {
    position: absolute;
    top: 50%;
    right: 25px;
    margin-top: -21px;
    font-size: 16px;
}
  .match-detail-header {
    padding-top: 40px;
  }
  .right-panel {
    display: none;
  }

  .mobile-bottom-btn .common-btn  {
    flex: 0 0 100%;
    max-width: 100%;
    width: 100%;
  }
  .pwa-bottom {
    position: fixed;
    left: 0px;
    bottom: 0px;
    flex: 0 0 100%;
    max-width: 100%;
    width: 100%;
  }
  .review-user {
    padding-bottom: 0px;
  }
  .pwa-bottom  .MuiBottomNavigationAction-root{
    max-width: 159px !important;
    min-width: 74px !important;
  }
  body {
    background: #e9eef5 !important;
  }
  .mr-25 {
    margin-right:0px !important;
  }
  .mlm-25 {
    margin-left: 0px !important;
  }
  .totalscore{
    font-size: 12px;
  }
  .mrm--25{
    margin-right:0px !important;
  }
  .ml-25 {
    margin-left: 0px !important;
  }
  .total-top-fill-board ul li {
    min-width: 25px;
  }
  .total-top-fill-board {
    margin: 0px;
  }
  .match-pack .team-vs {
    margin: 4px auto;
  }
 
  .match-win {
    font-size: 13px !important;
}
.header-row span {
  font-size: 13px;
}
.view-all-footer span {
  font-size: 13px;
}
.view-all-winner-div .card-title {
  
  font-size: 15px;

}
.view-all-winner-div p small {
  font-size: 11px;
}
.view-all-winner-div p span {
  font-size: 12px;
}
.add-cash {
  padding-bottom: 0px;
}
.userprofle-form  {
  padding-top:20px;
}
.profile-breadcrumb {
  padding-top:20px;
}
.userprofle-form .MuiTextField-root {
  margin-bottom: 15px !important;
}
.MuiSnackbar-anchorOriginBottomCenter {
  left: 50% !important;
  transform: translateX(-50%)!important;
  width:100% !important;
}
.logout-sidebar {
  left:-20px !important;
}
.ipl-matches{
  font-size: 11px;
}

.invite-friend-top {
  min-height: 289px;
  height: 289px;
}
.invite-friends {
  margin-top: 176px;
}
.invite-body .phonecontact {
  width: 100%;
}
.invite-body button{
  width: 100%;
}
.copycode-clipboard  {
  width: 100%;
}
.MuiDialog-paperWidthSm {
  width: 100% !important;
  min-width: 100% !important;
  flex: 0 0 100%;
  max-width: 100%;
}
.header-row-my-pack .fansty-row .team-label {
  width: 75% !important;
}
.common-btn button {
  min-width: 132px !important;
  height: 35px !important;
  font-size: 13px !important;
  margin: 0px 8px !important;
}
.header-selection b {
  font-size: 15px !important;
}
.starttime {
  padding-bottom: 12px;
}
.fantasy-pack {
  padding: 32px 0px 10px 0px;
}
.header-team-score b {
  font-size: 12px;
}
.apptop-header {
  padding:0px 0px;
}
// .mobile-input .MuiFormHelperText-root.Mui-error {
//   margin-left: -100px !important;
// }
.phone-input {
  position: relative;
}
.left-panel {
  padding-bottom: 102px;
}
.modal-slide-modal  .MuiDialog-paperScrollPaper {
  position: fixed !important;
  bottom: 0px !important;
  left: 0px !important;
  width: 100% !important;
  min-width: 100% !important;
  flex: 100% !important;
  max-width: 100% !important;
  margin: 0px !important;
}  
.common-modals .MuiDialog-paperWidthSm {
  position: fixed;
  left: 0px;
  margin: 10px;
  min-width: 94% !important;
  flex: 0 0 94% !important;
  max-width: 94% !important;
}
.headermodal  .MuiDialog-paperWidthSm{
  min-width: 100% !important;
  flex: 0 0 100% !important;
  max-width: 100% !important;
}
.mypacks-footer h6 {
  font-size: 10px !important;
}
.mypacks-footer h6 b {
  font-size: 10px !important;
}
.contests-tabs .MuiTab-wrapper {
  font-size: 12px !important;
}
.slide-modal .common-btn button {
  min-width: 120px !important;
  padding: 0px 10px;
}
.match-pack .my-team-name {
  height: 34px;
  width: 34px;
  line-height: 30px;
  font-size: 10px !important;
}
.modaltile {
  font-size: 15px;
}
.close-no {
  top: 7px;
  font-size: 33px !important;
}
.team-label {
  font-size: 14px;
}
.sidebar-menu {
  padding-bottom: 100px !important;
}
.addcards a {
  font-size: 15px !important;
}

.myearned-pack .fantasy-pack{
  padding: 10px 0px 10px 0px;
}
.my-earn-vs {
  padding: 0px;
}
.totalscore {
  font-size: 13px;
}
.create-packmodal .fantasy-pack {
  padding: 19px 0px 20px 0px;
}
.navbar-brand {
  font-size: 19px;
  margin: 0px;
}
.create-packs-tabs .MuiTab-root {
  padding: 6px 7px;
  font-size: 13px;
}
.fantasy-pack {
  padding: 3px 0px 10px 0px;
}
.bm-menu-wrap {
  z-index: 99999999999 !important;
}
.userside-profile {
   padding: 0px 0px !important;
}
.profile-menu {
  height: 52px;
  width: 52px;
}
.fliud-img{
  margin:0px auto !important;
}
.newpack-fansty {
  padding: 10px 0px 10px 0px;
}
.newpack-fansty .my-team-name{
  line-height: 41px;
  width: 50px;
  height: 50px;
  font-size: 14px;
}

.sticky-packs-tabs  .starttime{
  font-size: 10px;
  padding-bottom: 0px;
}
.total-top-fill-board ul li {
  line-height: 10px !important;
    height: 10px !important;
    font-size: 9px !important;
}
.sticky-packs-tabs  .MuiTab-root {
  min-height: 31px !important;
}
.sticky-packs-tabs  .MuiTabs-root {
  min-height: 31px !important;
}
.sticky-packs-tabs .MuiTab-root {
  font-size: 11px !important;
}
.sticky-packs-tabs .match-pack .my-team-name {
  height: 25px;
  width: 25px;
  line-height: 19px;
  font-size: 8px !important;
}
.sticky-packs-tabs  .totalscore {
  font-size: 10px;
  padding-top: 3px;
}
.sticky-packs-tabs .scoreboard-top b {
  font-size: 18px;
}
.sticky-packs-tabs .scoreboard-top {
  font-size: 11px;
}
.sticky-packs-tabs .select-row p {
  line-height: 23px;
  font-size: 11px;  
}
.sticky-packs-tabs .fantasy-pack {
  padding: 3px 0px 2px 0px;
}
.new-home-page h1 {
  font-size: 16px;
}

.homepage-last-card {
  margin-bottom: 30px !important;
}
.box-number {
  width: 45px;
  height: 45px;
}
.box-number span {
  font-size: 19px;
}
.home-card-content h4 {
  font-size: 16px;
}
.home-card-content p {
  font-size: 13px;
}
.home-card-content ul li {
  font-size: 13px;
}
.homepage-bottom-banner ul li {
  padding: 20px 15px;
}
.homepage-bottom-banner ul li h5 {
  font-size: 14px;
}
.homepage-bottom-banner ul li h5 img {
  width: 18px !important;
}
.homepage-bottom-banner ul li span {
  font-size: 13px;
}
.new-home-page h2 {
  font-size: 16px;
}


.new-home-page .slick-next {
  display: block !important;
}
.new-home-page  .slick-prev {
  display: block !important;
}
.new-home-page .slick-prev {
  height: 62px !important;
  width: 62px !important;
  background-size: cover !important;
  right: 15px !important;
}
.new-home-page .slick-next {
  height: 62px !important;
  width: 62px !important;
  background-size: cover !important;
  right: 15px !important;
}
.top-testimonial .slick-next {
  right: -25px !important;
}
.home-testimonial-review {
  padding: 18px 30px !important;
}
.testmonial-circle {
  border-style: solid;
    border-width: 3px;
    border-color: rgb(255,255,255);
    border-radius: 50%;
    width: 65px;
    height: 65px;
}
.home-testimonial-review p {
  margin: 10px 0px 0px 0px;
  padding: 0px;
}
.quoteimg {
  max-width: 38px;
  top: 15px;
  right: 15px;
}

.top-testimonial .slick-prev {
  left: -21px !important;
}
.top-testimonial .slick-next{
  right: -21px !important;
}
.user-auth-btn button {
  min-width: 82px !important;
  height: 30px !important;
  font-size: 13px !important;
  margin: 0px 3px !important;
  line-height: 25px;
}
.user-auth-btn .common-btn {
  right: 10px !important;
  width: auto !important;
  top: 12px;
  left: auto !important;
}
.latest-home-page {
  height: 380px;
}
/* .mobile-bottom-btn {
  display: none;
}
.appbar {
  display: none;
}
.left-panel {
  padding-top: 0px !important;
} */
.homepage-register-form {
  margin: 20px 0px 0px 0px;
}

.newlogo {
  max-width: 115px;
}
.right-account-login{
  width: 81px;
  height: 30px;
}
.new-home-banner {
  padding: 80px 42px 62px 42px;
  height: 274px;
}
.home-header {
  height: 82px;
}


.new-logo {
  margin-top: 17px;
}
.prize-row {
  padding: 20px 0px;
}
.bottom-border {
  margin: 17px auto;
}
.home-new-register {
  margin: 27px auto 20px auto !important;
}
.intsall-step-main .slick-next {
  display: block !important;
  right: 6px !important;
}
.intsall-step-main .slick-prev {
  display: block !important;
  left: 6px !important;
}
.review-box {
  padding: 15px;
}
.arrow_box {
  margin-bottom: 35px !important;
}
.arrow_box,
.arrow_box2 {
  width: auto;
}
.arrow_box {
  margin-left: 20px !important;
}
.arrow_box2 {
  margin-right: 20px !important;
}
.review-box p {
  font-size: 15px;
}
.download-step img {
  width: 75%;
}
}
@media (min-width: 300px) and (max-width: 570px) {  

  .latest-home-page {
    height: 300px;
    background-position: top 0px center;
}
}
@media (min-width: 500px) and (max-width: 1000px) { 
  .home-header {
    height: 102px;
}
.new-home-banner {
  top: -36px;
}
.prize-row {
  margin-top: -36px;
}
  .latest-home-page {
    height: 573px;
}
.bonus-banner {
  height: 141px;
}
}
@media (min-width: 800px) and (max-width: 1280px) {
  .invite-friend-top {
   
    background-repeat-x: repeat !important;
 }
 .newlogo{
  max-width: 145px;
 }
@media (min-width: 1000px) and (max-width: 1366px) {
  .otp-form input{
    width: 38px !important;
    height: 38px;
  }
  user-auth-btn .common-btn {
    left: 179px;
  }
  .navbar-brand img {
    margin-top: -6px;
    width: 153px;
}
.homepage-bottom-banner ul li span {
  font-size: 13px;
}
.homepage-bottom-banner ul li h5 {
  font-size: 18px;
}
.homepage-bottom-banner ul li {
  padding: 20px 15px;
}
.testmonial-circle {
  width: 76px;
  height: 76px;
}
.home-testimonial-review {
  padding: 20px 39px !important;
}
 }
`;
